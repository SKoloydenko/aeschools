package ru.mephi.aeschools.imageservice.service

import ru.mephi.aeschools.imageservice.model.request.ThumbnailRequest

interface ImageService {
    fun handle(request: ThumbnailRequest)
}