package ru.mephi.aeschools.imageservice.service.impl

import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.name
import kotlin.io.path.notExists

@Component
class StorageFileManager {
    private val root: Path = Path("uploads").also { if (it.notExists()) Files.createDirectory(it) }

    fun retrieve(path: String) = File("${root.name}/$path")

}