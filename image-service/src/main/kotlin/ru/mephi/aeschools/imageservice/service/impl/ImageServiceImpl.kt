package ru.mephi.aeschools.imageservice.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.imageservice.model.request.ThumbnailRequest
import ru.mephi.aeschools.imageservice.service.ImageService
import ru.mephi.aeschools.imageservice.service.ThumbnailatorService


@Service
class ImageServiceImpl(
    private val thumbnailService: ThumbnailatorService
) : ImageService {

    override fun handle(request: ThumbnailRequest) {
        thumbnailService.handle(request)
    }
}