package ru.mephi.aeschools.imageservice.config

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.imageservice.util.IMAGE_THUMBNAIL_QUEUE


@Configuration
class RabbitQueueConfig {

    @Bean
    fun broadcastQueue(): Queue {
        return Queue(IMAGE_THUMBNAIL_QUEUE)
    }

}
