package ru.mephi.aeschools.imageservice.service.impl

import net.coobird.thumbnailator.Thumbnails
import org.springframework.stereotype.Service
import ru.mephi.aeschools.imageservice.model.request.ThumbnailRequest
import ru.mephi.aeschools.imageservice.service.ThumbnailatorService
import java.io.File

@Service
class ThumbnailatorServiceImpl(
    private val fileManager: StorageFileManager,
) : ThumbnailatorService {

    override fun handle(request: ThumbnailRequest) {
        val image = fileManager.retrieve(request.path)
        if (!image.exists()) return
        Thumbnails.of(image)
            .size(1920, 1080)
            .toFile(image)

        val mediumImage =
            File(image.parentFile.absolutePath + "/" + image.nameWithoutExtension + ".medium." + image.extension)
        try {
            if (!mediumImage.exists()) mediumImage.createNewFile()
            Thumbnails.of(image)
                .size(800, 450)
                .toFile(mediumImage)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
