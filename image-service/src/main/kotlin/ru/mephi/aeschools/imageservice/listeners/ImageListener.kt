package ru.mephi.aeschools.imageservice.listeners

import ru.mephi.aeschools.imageservice.model.request.ThumbnailRequest


interface ImageListener {
    fun compressImage(request: ThumbnailRequest)
}
