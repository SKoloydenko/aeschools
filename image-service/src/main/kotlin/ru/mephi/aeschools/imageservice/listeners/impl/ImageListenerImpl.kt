package ru.mephi.aeschools.imageservice.listeners.impl

import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.mephi.aeschools.imageservice.listeners.ImageListener
import ru.mephi.aeschools.imageservice.model.request.ThumbnailRequest
import ru.mephi.aeschools.imageservice.service.ImageService
import ru.mephi.aeschools.imageservice.util.IMAGE_THUMBNAIL_QUEUE


@Component
@EnableRabbit
class ImageListenerImpl(
    val handler: ImageService
) : ImageListener {

    @RabbitListener(queues = [IMAGE_THUMBNAIL_QUEUE])
    override fun compressImage(request: ThumbnailRequest) {
        handler.handle(request)
    }
}
