package ru.mephi.aeschools.imageservice.model.request

class ThumbnailRequest(
    val path: String
)