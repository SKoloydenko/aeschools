plugins {
    application
    kotlin("jvm")
    kotlin("plugin.spring")
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    id("com.google.cloud.tools.jib")
}

group = "ru.mephi"

application {
    mainClass.set("ru.mephi.aeschools.imageservice.ImageServiceApplicationKt")
}

tasks {
    bootJar {
        archiveFileName.set("${project.name}.jar")
    }
    jar {
        enabled = false
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-amqp")

    testImplementation("org.springframework.amqp:spring-rabbit-test")
    testImplementation("org.springframework.security:spring-security-test")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    implementation("net.coobird:thumbnailator:0.4.18")
}
