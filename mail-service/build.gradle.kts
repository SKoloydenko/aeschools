plugins {
    application
    kotlin("jvm")
    kotlin("plugin.spring")
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    id("com.google.cloud.tools.jib")
}

group = "ru.mephi"

application {
    mainClass.set("ru.mephi.aeschoolsbroadcast.serverbroadcast.MailServiceAppKt")
}

tasks {
    bootJar {
        archiveFileName.set("${project.name}.jar")
    }
    jar {
        enabled = false
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-amqp")
    implementation("org.springframework.boot:spring-boot-starter-mail")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")

    testImplementation("org.springframework.amqp:spring-rabbit-test")
}
