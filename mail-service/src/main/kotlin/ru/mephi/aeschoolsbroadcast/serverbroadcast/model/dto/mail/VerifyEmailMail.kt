package ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.MailReceiver

class VerifyEmailMail(
    tokenTtl: Long,
    receiversEmails: List<MailReceiver>,
) : AbstractMail(
    receiversEmails,
    "Подтверждение почты",
    mapOf("tokenTtl" to tokenTtl)
) {

    override fun getTemplateLocation() = "mails/VerifyEmailMail.html"
}
