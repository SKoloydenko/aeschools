package ru.mephi.aeschoolsbroadcast.serverbroadcast.service.impl


import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.AbstractMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.service.EmailSender
import javax.mail.Message
import javax.mail.internet.InternetAddress

@Service
class EmailSenderImpl(
    val mailSender: JavaMailSender,
    val templateEngine: SpringTemplateEngine,

    @Value("\${spring.mail.from-mail}")
    private val from: String,
) : EmailSender {


    override fun sendEmail(metadata: AbstractMail) {
        val mail = mailSender.createMimeMessage()
        val helper = MimeMessageHelper(mail, true)
        helper.setSubject(metadata.subject)
        helper.setFrom(from)

        metadata.receiversEmails.forEach {
            try {
                val address = InternetAddress.parse(it.email).first()

                val context = Context()
                context.setVariables(metadata.context + it.personalData)
                val emailBody = templateEngine.process(metadata.getTemplateLocation(), context)
                helper.setText(emailBody, true)

                mail.setRecipient(Message.RecipientType.TO, address)
                mailSender.send(mail)
            } catch (e: Exception) {
                e.printStackTrace()
                return@forEach
            }
        }
    }
}
