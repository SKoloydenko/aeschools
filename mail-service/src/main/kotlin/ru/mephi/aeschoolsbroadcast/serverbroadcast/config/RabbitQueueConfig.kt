package ru.mephi.aeschoolsbroadcast.serverbroadcast.config

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_BROADCAST
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_BROADCAST_IGNORE_PREFERENCES
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_RESET_PASSWORD
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_VERIFY_EMAIL

@Configuration
class RabbitQueueConfig {

    @Bean
    fun broadcastQueue(): Queue {
        return Queue(MAIL_BROADCAST)
    }

    @Bean
    fun broadcastIgnorePreferencesQueue(): Queue {
        return Queue(MAIL_BROADCAST_IGNORE_PREFERENCES)
    }

    @Bean
    fun verifyEmailQueue(): Queue {
        return Queue(MAIL_VERIFY_EMAIL)
    }

    @Bean
    fun resetPasswordQueue(): Queue {
        return Queue(MAIL_RESET_PASSWORD)
    }
}
