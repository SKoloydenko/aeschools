package ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const

const val MAIL_BROADCAST = "mail.broadcast"
const val MAIL_BROADCAST_IGNORE_PREFERENCES = "mail.broadcast-ignore-preferences"
const val MAIL_RESET_PASSWORD = "mail.reset-password"
const val MAIL_VERIFY_EMAIL = "mail.verify-email"
