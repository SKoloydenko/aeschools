package ru.mephi.aeschoolsbroadcast.serverbroadcast

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MailServiceApp

fun main(args: Array<String>) {
    runApplication<MailServiceApp>(*args)
}

