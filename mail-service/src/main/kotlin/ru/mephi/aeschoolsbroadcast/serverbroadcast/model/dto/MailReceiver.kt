package ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto

class MailReceiver(
    val email: String,
    val personalData: MutableMap<String, String> = mutableMapOf(),
)
