package ru.mephi.aeschoolsbroadcast.serverbroadcast.service

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.AbstractMail


interface EmailSender {
    fun sendEmail(metadata: AbstractMail)
}
