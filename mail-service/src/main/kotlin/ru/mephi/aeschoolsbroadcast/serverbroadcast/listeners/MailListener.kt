package ru.mephi.aeschoolsbroadcast.serverbroadcast.listeners

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastIgnorePreferencesMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.ResetPasswordMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.VerifyEmailMail

interface MailListener {
    fun broadcastMail(mail: BroadcastMail)
    fun broadcastIgnorePreferencesMail(mail: BroadcastIgnorePreferencesMail)
    fun resetPasswordEmail(mail: ResetPasswordMail)
    fun verifyEmailMail(mail: VerifyEmailMail)
}
