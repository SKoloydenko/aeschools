package ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.MailReceiver
import java.io.Serializable

abstract class AbstractMail(
    val receiversEmails: List<MailReceiver> = mutableListOf(),
    val subject: String,
    val context: Map<String, Any> = mapOf(),
) : Serializable {
    abstract fun getTemplateLocation(): String
}
