package ru.mephi.aeschoolsbroadcast.serverbroadcast.service

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastIgnorePreferencesMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.ResetPasswordMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.VerifyEmailMail


interface EmailService {
    fun sendBroadcastMail(mail: BroadcastMail)
    fun sendBroadcastMailIgnorePreferences(mail: BroadcastIgnorePreferencesMail)
    fun sendResetPasswordMail(mail: ResetPasswordMail)
    fun sendVerifyEmailMail(mail: VerifyEmailMail)
}
