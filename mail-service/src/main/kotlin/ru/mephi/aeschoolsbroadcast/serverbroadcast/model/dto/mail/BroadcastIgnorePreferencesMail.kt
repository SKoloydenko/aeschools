package ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.MailReceiver

class BroadcastIgnorePreferencesMail(
    title: String,
    text: String,
    receiversEmails: List<MailReceiver>,
) : AbstractMail(
    receiversEmails,
    "Рассылка портала \"Передовые Инженерные Школы\"",
    mapOf("title" to title, "text" to text)
) {

    override fun getTemplateLocation(): String = "mails/broadcast-ignore-preferences.html"
}
