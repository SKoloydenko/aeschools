package ru.mephi.aeschoolsbroadcast.serverbroadcast.listeners.impl

import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.mephi.aeschoolsbroadcast.serverbroadcast.listeners.MailListener
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastIgnorePreferencesMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.ResetPasswordMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.VerifyEmailMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.service.EmailService
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_BROADCAST
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_BROADCAST_IGNORE_PREFERENCES
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_RESET_PASSWORD
import ru.mephi.aeschoolsbroadcast.serverbroadcast.util.const.MAIL_VERIFY_EMAIL

@Component
@EnableRabbit
class MailListenerImpl(
    val service: EmailService,
) : MailListener {

    @RabbitListener(queues = [MAIL_BROADCAST])
    override fun broadcastMail(mail: BroadcastMail) {
        service.sendBroadcastMail(mail)
    }

    @RabbitListener(queues = [MAIL_BROADCAST_IGNORE_PREFERENCES])
    override fun broadcastIgnorePreferencesMail(mail: BroadcastIgnorePreferencesMail) {
        service.sendBroadcastMailIgnorePreferences(mail)
    }

    @RabbitListener(queues = [MAIL_RESET_PASSWORD])
    override fun resetPasswordEmail(mail: ResetPasswordMail) {
        service.sendResetPasswordMail(mail)
    }

    @RabbitListener(queues = [MAIL_VERIFY_EMAIL])
    override fun verifyEmailMail(mail: VerifyEmailMail) {
        service.sendVerifyEmailMail(mail)
    }
}
