package ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.MailReceiver

class BroadcastMail(
    title: String,
    text: String,
    val unsubscribeLink: String,
    receiversEmails: List<MailReceiver>,
) : AbstractMail(
    receiversEmails,
    "Рассылка портала \"Передовые Инженерные Школы\"",
    mapOf("title" to title, "text" to text, "unsubscribeLink" to unsubscribeLink)
) {

    override fun getTemplateLocation(): String = "mails/broadcast.html"
}
