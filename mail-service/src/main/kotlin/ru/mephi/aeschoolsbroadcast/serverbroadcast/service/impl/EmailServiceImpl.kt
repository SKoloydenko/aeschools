package ru.mephi.aeschoolsbroadcast.serverbroadcast.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastIgnorePreferencesMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.BroadcastMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.ResetPasswordMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail.VerifyEmailMail
import ru.mephi.aeschoolsbroadcast.serverbroadcast.service.EmailSender
import ru.mephi.aeschoolsbroadcast.serverbroadcast.service.EmailService


@Service
class EmailServiceImpl(
    val sender: EmailSender,
) : EmailService {
    override fun sendBroadcastMail(mail: BroadcastMail) {
        mail.receiversEmails.forEach {
            it.personalData["unsubscribeLink"] =
                mail.unsubscribeLink + "/" + it.personalData["unsubscribeEmailToken"]
        }
        sender.sendEmail(mail)
    }

    override fun sendBroadcastMailIgnorePreferences(mail: BroadcastIgnorePreferencesMail) {
        sender.sendEmail(mail)
    }

    override fun sendResetPasswordMail(mail: ResetPasswordMail) {
        sender.sendEmail(mail)
    }

    override fun sendVerifyEmailMail(mail: VerifyEmailMail) {
        sender.sendEmail(mail)
    }
}
