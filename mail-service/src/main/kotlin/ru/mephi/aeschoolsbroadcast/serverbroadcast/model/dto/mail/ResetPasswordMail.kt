package ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.mail

import ru.mephi.aeschoolsbroadcast.serverbroadcast.model.dto.MailReceiver

class ResetPasswordMail(
    receiversEmails: MutableList<MailReceiver>,
) : AbstractMail(
    receiversEmails,
    "Сброс пароля",
) {

    override fun getTemplateLocation(): String = "mails/ResetPasswordEmail.html"

}
