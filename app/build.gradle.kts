plugins {
    application
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.spring")
    kotlin("plugin.jpa")
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    id("com.google.cloud.tools.jib")
}

repositories {
    mavenCentral()
    maven(url = "https://plugins.gradle.org/m2/")
    gradlePluginPortal()
}

group = "ru.mephi"

springBoot {
    buildInfo() // For getting buildProperties.version
}

tasks {
    bootJar {
        layered {
            enabled = true
        }
        archiveFileName.set("${project.name}.jar")
    }

    jar {
        enabled = false
    }

    compileJava {
        options.compilerArgs.addAll(
            arrayOf(
                "-Amapstruct.suppressGeneratorTimestamp=true",
                "-Amapstruct.suppressGeneratorVersionInfoComment=true",
                "-Amapstruct.verbose=true"
            )
        )
    }
}

application {
    mainClass.set("ru.mephi.aeschools.AppKt")
}

gradle.taskGraph.whenReady {
    allTasks.filter { it.hasProperty("duplicatesStrategy") } // Because it's some weird decorated wrapper that I can't cast.
        .forEach {
            it.setProperty("duplicatesStrategy", "EXCLUDE")
        }
}

dependencies {

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.springframework.boot:spring-boot-starter-amqp")
    developmentOnly("org.springframework.boot:spring-boot-devtools")

    implementation("net.logstash.logback:logstash-logback-encoder:7.2")
    runtimeOnly("org.mariadb.jdbc:mariadb-java-client:3.0.5")
    runtimeOnly("mysql:mysql-connector-java:8.0.29")

    implementation("org.springdoc:springdoc-openapi-data-rest:1.6.9")
    implementation("org.springdoc:springdoc-openapi-kotlin:1.6.9")
    implementation("org.springdoc:springdoc-openapi-ui:1.6.9")
    implementation("org.springdoc:springdoc-openapi-webflux-ui:1.6.9")

    implementation("org.apache.poi:poi-ooxml:5.2.2")
    implementation("org.apache.poi:poi:5.2.2")

    implementation("net.coobird:thumbnailator:0.4.18")

    runtimeOnly("io.jsonwebtoken:jjwt-impl:0.11.5")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:0.11.5")
    implementation("io.jsonwebtoken:jjwt-api:0.11.5")

    kapt("com.querydsl:querydsl-apt:5.0.0")
    implementation("com.querydsl:querydsl-jpa:5.0.0")
    implementation("com.querydsl:querydsl-apt:5.0.0")

    implementation("com.google.guava:guava:31.1-jre")

    kapt("org.mapstruct:mapstruct-processor:1.5.0.RC1")
    implementation("org.mapstruct:mapstruct:1.5.0.RC1")

    implementation("org.apache.tika:tika-core:2.5.0")

    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("org.springframework.boot:spring-boot-starter-data-redis")

    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")

    implementation("com.hazelcast:hazelcast-spring:5.1.2")
    implementation("com.github.vladimir-bukhtoyarov:bucket4j-core:7.5.0")
    implementation("com.github.vladimir-bukhtoyarov:bucket4j-jcache:7.5.0")
    implementation("com.github.vladimir-bukhtoyarov:bucket4j-hazelcast:7.5.0")
    implementation("javax.cache:cache-api:1.1.1")
}
