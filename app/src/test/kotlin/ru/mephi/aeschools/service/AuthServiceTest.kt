package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.email.ResetPasswordTokenDao
import ru.mephi.aeschools.database.repository.user.RefreshTokenDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.database.repository.user.VerifyEmailTokenDao
import ru.mephi.aeschools.model.dto.auth.request.ResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SendEmailToResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SetNewPasswordRequest
import ru.mephi.aeschools.model.dto.user.request.LoginRequest
import ru.mephi.aeschools.model.dto.user.request.RefreshRequest
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import ru.mephi.aeschools.model.enums.user.EducationDegree
import ru.mephi.aeschools.model.exceptions.auth.CorruptedTokenException
import ru.mephi.aeschools.model.exceptions.auth.IncorrectPasswordException
import ru.mephi.aeschools.model.exceptions.auth.NotFoundRequestToChangePassword
import ru.mephi.aeschools.model.exceptions.auth.NotVerifyEmailException
import ru.mephi.aeschools.model.exceptions.auth.TokenToVerifyEmailNotFound
import ru.mephi.aeschools.model.exceptions.auth.UserNotFoundByEmailException
import ru.mephi.aeschools.model.exceptions.auth.VerifyTokenExpiredException
import ru.mephi.aeschools.model.exceptions.common.ResourceAlreadyExistsException
import ru.mephi.aeschools.model.exceptions.common.ValidationException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.UserMock
import ru.mephi.aeschools.service.impl.auth.EmailVerificationScheduler
import ru.mephi.aeschools.service.impl.auth.VerifyEmailServiceImpl
import ru.mephi.aeschools.util.TokenManager
import java.time.Instant
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AuthServiceTest {
    @Autowired
    private lateinit var userHelper: UserHelper

    @Autowired
    private lateinit var emailVerificationScheduler: EmailVerificationScheduler

    @Autowired
    private lateinit var userDao: UserDao

    @Autowired
    private lateinit var verifyEmailTokenDao: VerifyEmailTokenDao

    @Autowired
    private lateinit var resetPasswordDao: ResetPasswordTokenDao

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var authService: AuthService

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    private lateinit var resetPasswordService: ResetPasswordService

    @Autowired
    private lateinit var verifyEmailService: VerifyEmailServiceImpl

    @Autowired
    private lateinit var tokenManager: TokenManager

    @Autowired
    private lateinit var refreshTokenDao: RefreshTokenDao

    fun UserHelper.registerAndVerifyUser(
        email: String = UserMock.EMAIL.value,
    ): User {
        val user = registerUser(email)
        verifyEmailService.deleteById(user.id as UUID)
        return user
    }

    @BeforeAll
    fun enableVerification() {
        userHelper.enableVerification()
        cleanData()
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
    }

    @Test
    fun `test service injection`() {
        // pon
    }

    @Test
    fun `register non-student user`() {
        val request = userHelper.generateRegistrationRequest()
        val response = authService.register(request)
        assertEquals(request.email, response.email)
    }

    @Test
    fun `register student user`() {
        val request = userHelper.generateStudentRegistrationRequest()
        val response = authService.register(request)
        assertEquals(request.email, response.email)
    }

    @Test
    fun `invalid non-student registration request `() {
        val request = RegistrationRequest(
            email = UserMock.EMAIL.value,
            password = UserMock.PASSWORD.value,
            name = "Name",
            surname = "Surname",
            phone = UserMock.PHONE.value,
            student = false,
            jobRole = UserMock.JOB_ROLE.value,
            university = UserMock.UNIVERSITY.value,
            enableEmail = false
        )
        assertThrows<ValidationException> { authService.register(request) }
    }

    @Test
    fun `invalid student registration request `() {
        val request = RegistrationRequest(
            email = UserMock.EMAIL.value,
            password = UserMock.PASSWORD.value,
            name = "Name",
            surname = "Surname",
            phone = UserMock.PHONE.value,
            student = true,
            organization = UserMock.ORGANIZATION.value,
            enableEmail = false
        )
        assertThrows<ValidationException> { authService.register(request) }
    }

    @Test
    fun `email already exists exception`() {
        val request = userHelper.generateRegistrationRequest()
        assertThrows<ResourceAlreadyExistsException> {
            authService.register(request)
            authService.register(request)
        }
    }

    @Test
    fun `login with wrong password`() {
        userHelper.registerAndVerifyUser()
        val request =
            LoginRequest(
                UserMock.EMAIL.value,
                "wrong password",
                UserMock.FINGERPRINT.value
            )
        assertThrows<IncorrectPasswordException> { authService.login(request) }
    }

    @Test
    fun `login with wrong email`() {
        userHelper.registerAndVerifyUser()
        val request =
            LoginRequest(
                "wrong@mail.ru",
                UserMock.PASSWORD.value,
                UserMock.FINGERPRINT.value
            )
        assertThrows<UserNotFoundByEmailException> { authService.login(request) }
    }

    @Test
    fun `test access token`() {
        val user = userHelper.registerAndVerifyUser()
        val request =
            LoginRequest(
                UserMock.EMAIL.value,
                UserMock.PASSWORD.value,
                UserMock.FINGERPRINT.value
            )
        val (response, _) = authService.login(request)
        val principal = tokenManager.parseTokenPrincipal(response.accessToken)
        assertEquals(principal.userId, user.id)
    }

    @Test
    fun `test refresh token`() {
        val user = userHelper.registerAndVerifyUser()
        val loginRequest =
            LoginRequest(
                UserMock.EMAIL.value,
                UserMock.PASSWORD.value,
                UserMock.FINGERPRINT.value
            )
        val (_, token) = authService.login(loginRequest)
        val refreshRequest =
            RefreshRequest(UserMock.FINGERPRINT.value).apply {
                refreshToken = token
            }
        val (response, _) = authService.refresh(refreshRequest)
        val principal = tokenManager.parseTokenPrincipal(response.accessToken)
        assertEquals(principal.userId, user.id)
    }

    @Test
    @Transactional
    fun `expired refresh token`() {
        userHelper.registerAndVerifyUser()
        val loginRequest =
            LoginRequest(
                UserMock.EMAIL.value,
                UserMock.PASSWORD.value,
                UserMock.FINGERPRINT.value
            )
        val (_, token) = authService.login(loginRequest)
        refreshTokenDao.findByFingerprintDeviceId(UserMock.FINGERPRINT.value)
            .get()
            .apply { expiresAt = Date.from(Instant.now()) }
        val refreshRequest =
            RefreshRequest(UserMock.FINGERPRINT.value).apply {
                refreshToken = token
            }
        assertThrows<CorruptedTokenException> {
            authService.refresh(
                refreshRequest
            )
        }
    }

    @Test
    @Transactional
    fun `conflict refresh token`() {
        userHelper.registerAndVerifyUser()
        val loginRequest =
            LoginRequest(
                UserMock.EMAIL.value,
                UserMock.PASSWORD.value,
                UserMock.FINGERPRINT.value
            )
        val (_, token) = authService.login(loginRequest)
        refreshTokenDao.findByFingerprintDeviceId(UserMock.FINGERPRINT.value)
            .get()
            .apply { hash = "invalid hash" }
        val refreshRequest =
            RefreshRequest(UserMock.FINGERPRINT.value).apply {
                refreshToken = token
            }
        assertThrows<CorruptedTokenException> {
            authService.refresh(
                refreshRequest
            )
        }
    }

    @Test
    fun `register and verify email + success login`() {
        val user =
            authService.register(userHelper.generateRegistrationRequest())
        val token = verifyEmailService.findVerifyTokenById(user.id)
        verifyEmailService.verifyEmail(token.token!!)
        val request =
            LoginRequest(
                user.email,
                UserMock.PASSWORD.value,
                UserMock.FINGERPRINT.value
            )
        assertDoesNotThrow { authService.login(request) }
    }

    @Test
    fun `try login when email haven't verified`() {
        val user =
            authService.register(userHelper.generateRegistrationRequest())
        assertThrows<NotVerifyEmailException> {
            authService.login(
                LoginRequest(
                    user.email,
                    UserMock.PASSWORD.value,
                    UserMock.FINGERPRINT.value
                )
            )
        }
    }

    @Test
    fun `verify email when token expired`() {
        val user =
            authService.register(userHelper.generateRegistrationRequest())
        verifyEmailTokenDao.setNewCreatedAtTimeById(
            LocalDateTime.now().minusDays(2), user.id
        )
        val token = verifyEmailService.findVerifyTokenById(user.id)
        assertThrows<VerifyTokenExpiredException> {
            verifyEmailService.verifyEmail(token.token!!)
        }
    }

    @Test
    fun `second registration when verify email token expired`() {
        val request = userHelper.generateRegistrationRequest()
        val user = authService.register(request)
        verifyEmailTokenDao.setNewCreatedAtTimeById(
            LocalDateTime.now().minusDays(2), user.id
        )
        authService.register(request)
    }

    @Test
    fun `verify email by not existing token`() {
        assertThrows<TokenToVerifyEmailNotFound> {
            verifyEmailService.verifyEmail("ThisTokenNotExist")
        }
    }

    @Test
    fun `test cascade by user and verifyEmailToken`() {
        val request = userHelper.generateRegistrationRequest()
        val userResponse1 = authService.register(request)
        userService.findById(userResponse1.id)
        verifyEmailService.findVerifyTokenById(userResponse1.id)

        verifyEmailService.deleteById(userResponse1.id)
        userService.findById(userResponse1.id)
        assertEquals(false, verifyEmailService.existsById(userResponse1.id))
        userService.delete(userResponse1.id)

        val userResponse2 = authService.register(request)
        userService.findById(userResponse2.id)
        verifyEmailService.findVerifyTokenById(userResponse2.id)
        userService.delete(userResponse2.id)
        assertEquals(false, verifyEmailService.existsById(userResponse2.id))
    }

    @Test
    fun `set new password`() {
        val user = userHelper.registerAndVerifyUser()
        val request =
            SetNewPasswordRequest(UserMock.PASSWORD.value, "new_password")
        authService.setNewPassword(request, user.id as UUID)
        val entityUser = userService.findUserEntityById(user.id as UUID)
        assertEquals(
            passwordEncoder.matches("new_password", entityUser.hash),
            true
        )
    }

    @Test
    fun `set new password with too short length`() {
        val user = userHelper.registerAndVerifyUser()
        val request = SetNewPasswordRequest(UserMock.PASSWORD.value, "12345")
        assertThrows<ValidationException> {
            authService.setNewPassword(request, user.id as UUID)
        }
    }

    @Test
    fun `login with new password`() {
        val user = userHelper.registerAndVerifyUser()
        val request =
            SetNewPasswordRequest(UserMock.PASSWORD.value, "new_password")
        authService.setNewPassword(request, user.id as UUID)
        authService.login(
            LoginRequest(
                user.email,
                "new_password",
                UserMock.FINGERPRINT.value
            )
        )
    }

    @Test
    fun `set new password and login with old`() {
        val user = userHelper.registerAndVerifyUser()
        val request =
            SetNewPasswordRequest(UserMock.PASSWORD.value, "new_password")
        authService.setNewPassword(request, user.id as UUID)
        assertThrows<IncorrectPasswordException> {
            authService.login(
                LoginRequest(
                    user.email,
                    UserMock.PASSWORD.value,
                    UserMock.FINGERPRINT.value
                )
            )
        }
    }

    @Test
    @Transactional
    fun `try reset password when user account not verified`() {
        val user =
            authService.register(userHelper.generateRegistrationRequest())
        val entity = userService.findUserEntityById(user.id)
        assertThrows<NotVerifyEmailException> {
            authService.sendEmailToResetPassword(
                SendEmailToResetPasswordRequest(
                    entity.email
                )
            )
        }
    }

    @Test
    @Transactional
    fun `reset password , login with new password, login with old`() {
        val user = userHelper.registerAndVerifyUser()
        val entity = userService.findUserEntityById(user.id as UUID)
        authService.sendEmailToResetPassword(
            SendEmailToResetPasswordRequest(
                entity.email
            )
        )
        val token = resetPasswordService.findByUserId(user.id as UUID).get()
        authService.resetPassword(
            ResetPasswordRequest(
                token.token!!,
                "new_password",
                UserMock.FINGERPRINT.value
            )
        )
        assertEquals(passwordEncoder.matches("new_password", entity.hash), true)
        authService.login(
            LoginRequest(
                user.email,
                "new_password",
                UserMock.FINGERPRINT.value
            )
        )
        assertThrows<IncorrectPasswordException> {
            authService.login(
                LoginRequest(
                    user.email,
                    UserMock.PASSWORD.value,
                    UserMock.FINGERPRINT.value
                )
            )
        }
    }

    @Test
    @Transactional
    fun `send email to reset password and enter invalid token`() {
        val user = userHelper.registerAndVerifyUser()
        val entity = userService.findUserEntityById(user.id as UUID)
        authService.sendEmailToResetPassword(
            SendEmailToResetPasswordRequest(
                entity.email
            )
        )
        val token = resetPasswordService.findByUserId(user.id as UUID)
            .get().token!!.substring(10)
        val request = ResetPasswordRequest(
            token,
            "new_password",
            UserMock.FINGERPRINT.value
        )
        assertThrows<NotFoundRequestToChangePassword> {
            authService.resetPassword(
                request
            )
        }
    }

    @Test
    @Transactional
    fun `reset password and one more try reset with this token`() {
        val user = userHelper.registerAndVerifyUser()
        val entity = userService.findUserEntityById(user.id as UUID)
        authService.sendEmailToResetPassword(
            SendEmailToResetPasswordRequest(
                entity.email
            )
        )
        val token = resetPasswordService.findByUserId(user.id as UUID).get()
        authService.resetPassword(
            ResetPasswordRequest(
                token.token!!,
                "new_password",
                UserMock.FINGERPRINT.value
            )
        )
        assertThrows<NotFoundRequestToChangePassword> {
            authService.resetPassword(
                ResetPasswordRequest(
                    token.token!!,
                    "new_password",
                    UserMock.FINGERPRINT.value
                )
            )
        }
    }

    @Test
    @Transactional
    fun `test cascade 1`() {
        val request = userHelper.generateRegistrationRequest()
        val user = authService.register(request)
        verifyEmailService.verifyEmail(
            verifyEmailTokenDao.findById(user.id).get().token!!
        )

        val entity = userService.findUserEntityById(user.id)
        authService.sendEmailToResetPassword(
            SendEmailToResetPasswordRequest(
                entity.email
            )
        )
        resetPasswordDao.deleteById(entity.id as UUID)
        userService.findById(entity.id as UUID)
        assertEquals(false, resetPasswordDao.existsById(entity.id as UUID))
        assertEquals(true, userDao.existsById(user.id))
    }

    @Test
    fun `test cascade 2`() {
        val request = userHelper.generateRegistrationRequest()
        val user = authService.register(request)

        verifyEmailService.verifyEmail(
            verifyEmailTokenDao.findById(user.id).get().token!!
        )
        authService.sendEmailToResetPassword(
            SendEmailToResetPasswordRequest(
                user.email
            )
        )
        userService.delete(user.id)
        assertEquals(false, resetPasswordDao.existsById(user.id))
        assertEquals(false, verifyEmailService.existsById(user.id))
    }

    @Test
    fun `testing email verification scheduler`() {
        val size = 10
        val users = mutableListOf<UserResponse>()
        repeat(size) {
            users += authService.register(
                RegistrationRequest(
                    name = "name",
                    surname = "surname",
                    password = UserMock.PASSWORD.value,
                    email = "email-${it}@mail.ru",
                    phone = UserMock.PHONE.value,
                    student = true,
                    university = UserMock.UNIVERSITY.value,
                    educationDegree = EducationDegree.BACHELOR,
                    grade = 1,
                    specialty = UserMock.SPECIALTY.value,
                    enableEmail = true
                ),
            )
        }
        assertEquals(size, verifyEmailService.findAll().count())
        Thread.sleep(500)
        emailVerificationScheduler.handle()
        assertEquals(size, verifyEmailService.findAll().count())
        users.forEach {
            verifyEmailService.dao.setNewCreatedAtTimeById(
                LocalDateTime.now().minusMinutes(60),
                it.id
            )
        }
        emailVerificationScheduler.handle()
        assertEquals(0, verifyEmailService.findAll().count())
    }
}
