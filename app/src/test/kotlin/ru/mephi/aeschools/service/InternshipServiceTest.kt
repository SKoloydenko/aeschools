package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.ContentPhotoDao
import ru.mephi.aeschools.database.repository.internship.InternshipDao
import ru.mephi.aeschools.database.repository.internship.QuestionnaireDao
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.internship.UserNotInternshipApplicantException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockInternshipRequest
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockQuestionnaireRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InternshipServiceTest {

    @Autowired
    lateinit var internshipService: InternshipService

    @Autowired
    lateinit var internshipDao: InternshipDao

    @Autowired
    private lateinit var userHelper: UserHelper

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var schoolService: SchoolService

    @Autowired
    lateinit var schoolDao: SchoolDao

    @Autowired
    lateinit var questionnaireService: QuestionnaireService

    @Autowired
    lateinit var questionnaireDao: QuestionnaireDao

    @Autowired
    lateinit var contentPhotoService: ContentPhotoService

    @Autowired
    lateinit var contentPhotoDao: ContentPhotoDao

    @Autowired
    lateinit var storageHelper: StorageManagerHelper

    @MockBean
    lateinit var imageManager: StorageImageManager

    @BeforeAll
    fun disableVerification() {
        userHelper.disableVerification()
        cleanData()
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        questionnaireDao.deleteAll()
        internshipDao.deleteAll()
        schoolDao.deleteAll()
        contentPhotoDao.deleteAll()
    }

    @Test
    @Transactional
    fun `list internships for public`() {
        val user = userHelper.registerUser()
        val internships = List(5) {
            internshipService.createInternship(
                user.id as UUID,
                mockInternshipRequest(),
                null
            ).id
        }
        val response =
            internshipService.listInternships("", "", mockPageRequest()).content.map { it.id }
        assertTrue(response.containsAll(internships))
    }

    @Test
    @Transactional
    fun `list internships for public by tags`() {
        val user = userHelper.registerUser()
        List(5) {
            internshipService.createInternship(
                user.id as UUID,
                mockInternshipRequest(tags = setOf("Tag", "Tag $it")),
                null
            ).id
        }
        val response = internshipService.listInternships("", "Tag 2", mockPageRequest())
        assertEquals(1, response.size)
        assertEquals(setOf("Tag", "Tag 2"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `list internships for user by query`() {
        val user = userHelper.registerUser()
        val student = userHelper.registerStudentUser("student@email.com")
        List(5) {
            val internshipId = internshipService.createInternship(
                user.id as UUID,
                mockInternshipRequest(title = "Title $it"),
                null
            ).id
            questionnaireService.createQuestionnaire(
                internshipId,
                student.id as UUID,
                mockQuestionnaireRequest(),
                null
            )
        }
        val response = internshipService.listInternshipsForUser(
            student.id as UUID,
            "Title 4",
            "",
            mockPageRequest()
        )
        assertEquals(1, response.size)
        assertEquals("Title 4", response.content.first().title)
    }

    @Test
    @Transactional
    fun `list internships for user by tags`() {
        val user = userHelper.registerUser()
        val student = userHelper.registerStudentUser("student@email.com")
        List(5) {
            val internshipId = internshipService.createInternship(
                user.id as UUID,
                mockInternshipRequest(tags = setOf("Tag", "Tag $it")),
                null
            ).id
            questionnaireService.createQuestionnaire(
                internshipId,
                student.id as UUID,
                mockQuestionnaireRequest(),
                null
            )
        }
        val response = internshipService.listInternshipsForUser(
            student.id as UUID,
            "",
            "Tag 2",
            mockPageRequest()
        )
        assertEquals(1, response.size)
        assertEquals(setOf("Tag", "Tag 2"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `list applicants`() {
        val user = userHelper.registerUser()
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        val users = List(5) { userHelper.registerUser("user$it@email.com").id }
        users.forEach {
            questionnaireService.createQuestionnaire(internship.id, it as UUID, mockQuestionnaireRequest(), null)
        }
        val response = internshipService.listInternshipApplicants(
            internship.id,
            mockPageRequest()
        ).content.map { it.id }
        assertTrue(response.containsAll(users))
    }

    @Test
    @Transactional
    fun `list internship with school`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        internshipService.createSchoolInternship(
            user.id as UUID,
            school.id,
            mockInternshipRequest(),
            null
        )
        val response =
            internshipService.listSchoolInternships(school.id, "", "", mockPageRequest())
        assertEquals(school.name, response.content.first().title)
    }

    @Test
    @Transactional
    fun `list internship with school by tags`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        List(5) {
            internshipService.createSchoolInternship(
                user.id as UUID,
                school.id,
                mockInternshipRequest(tags = setOf("Tag", "Tag $it")),
                null
            )
        }
        val response =
            internshipService.listSchoolInternships(school.id, "", "Tag 1", mockPageRequest())
        assertEquals(setOf("Tag", "Tag 1"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `find school internship by id`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val internship = internshipService.createSchoolInternship(user.id as UUID, school.id, mockInternshipRequest(), null)
        val response = internshipService.findSchoolInternshipById(school.id, internship.id)
        assertEquals(internship.id, response.id)
    }

    @Test
    fun `create internship with content photos`() {
        val file = MockPart("file.png", "content".toByteArray())
        `when`(imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true))
            .thenReturn("Path")
        contentPhotoService.createContentPhoto(file)
        val user = userHelper.registerUser()
        val internship =
            internshipService.createInternship(
                user.id as UUID,
                mockInternshipRequest(content = "<img src=\"${storageHelper.getPhotoPath("Path")}\"></img>"),
                null
            )
        assertEquals(user.id, internship.author?.id)
        Thread.sleep(5000)
        assertEquals(internship.id, contentPhotoDao.findByPhotoPath("Path").get().content?.id)
    }

    @Test
    fun `update internship`() {
        val user = userHelper.registerUser()
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        val response =
            internshipService.updateInternship(
                user.id as UUID,
                internship.id,
                mockInternshipRequest("New title", tags = setOf("Tag 1", "Tag 3"))
            )
        assertEquals("New title", response.title)
        assertEquals(setOf("Tag 1", "Tag 3"), response.tags)
    }

    @Test
    fun `upload internship photo`() {
        val user = userHelper.registerUser()
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        val file = MockPart("file.png", "content".toByteArray())
        `when`(imageManager.storeImage(file, ImageStoreDir.INTERNSHIP, null, true))
            .thenReturn("Path")
        val response =
            internshipService.uploadInternshipPhoto(
                user.id as UUID,
                internship.id,
                file
            )
        assertEquals("Path", response.photoPath)
    }

    @Test
    fun `delete internship`() {
        val user = userHelper.registerUser()
        val internships = List(5) {
            internshipService.createInternship(
                user.id as UUID,
                mockInternshipRequest(),
                null
            ).id
        }
        internships.forEach { internshipService.deleteInternship(user.id as UUID, it) }
        val response = internshipService.listInternships("", "", mockPageRequest())
        assertTrue(response.content.isEmpty())
    }

    @Test
    @Transactional
    fun `delete internship applications`() {
        val user = userHelper.registerUser()
        val users = List(5) { userHelper.registerUser("user$it@email.com").id }
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        users.forEach {
            questionnaireService.createQuestionnaire(internship.id, it as UUID, mockQuestionnaireRequest(), null)
        }
        users.forEach {
            questionnaireService.deleteQuestionnaire(
                internship.id,
                it as UUID,
            )
        }
        users.forEach {
            val internships =
                internshipService.listInternshipsForUser(it as UUID, "", "", mockPageRequest())
            assertTrue(internships.content.isEmpty())
        }
    }

    @Test
    fun `delete internship application without application`() {
        val user = userHelper.registerUser()
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        assertThrows<UserNotInternshipApplicantException> {
            questionnaireService.deleteQuestionnaire(
                internship.id,
                user.id as UUID,
            )
        }
    }

    @Test
    fun `when internship deleted then users are present`() {
        val user = userHelper.registerUser()
        val users = List(5) { userHelper.registerUser("user$it@email.com").id }
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        users.forEach {
            questionnaireService.createQuestionnaire(internship.id,it as UUID, mockQuestionnaireRequest(), null)
        }
        internshipService.deleteInternship(user.id as UUID, internship.id)
        users.forEach { assertTrue(userDao.existsById(it as UUID)) }
    }

    @Test
    fun `when users deleted then applicants are deleted`() {
        val user = userHelper.registerUser()
        val users = List(5) { userHelper.registerUser("user$it@email.com").id }
        val internship =
            internshipService.createInternship(user.id as UUID, mockInternshipRequest(), null)
        users.forEach {
            questionnaireService.createQuestionnaire(internship.id, it as UUID, mockQuestionnaireRequest(), null)
        }
        users.forEach { userService.delete(it as UUID) }
        val applicants =
            internshipService.listInternshipApplicants(internship.id, mockPageRequest())
        assertTrue(applicants.content.isEmpty())
    }
}
