package ru.mephi.aeschools.service.helpers

import org.springframework.stereotype.Component
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.FeatureToggleUpdateRequest
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.model.enums.user.EducationDegree
import ru.mephi.aeschools.service.AuthService
import ru.mephi.aeschools.service.FeatureToggleService
import ru.mephi.aeschools.service.UserService
import javax.transaction.Transactional

@Component
class UserHelper(
    private val authService: AuthService,
    private val userService: UserService,
    private val featureToggleService: FeatureToggleService,
) {
    fun enableVerification() {
        val request = FeatureToggleUpdateRequest("verifyEmail", true)
        featureToggleService.changeFeatureState(request)
    }

    fun disableVerification() {
        val request = FeatureToggleUpdateRequest("verifyEmail", false)
        featureToggleService.changeFeatureState(request)
    }

    fun generateRegistrationRequest(
        email: String = UserMock.EMAIL.value,
    ) = RegistrationRequest(
        name = "Name",
        surname = "Surname",
        password = UserMock.PASSWORD.value,
        email = email,
        phone = UserMock.PHONE.value,
        student = false,
        organization = UserMock.ORGANIZATION.value,
        jobRole = UserMock.JOB_ROLE.value,
        enableEmail = true
    )

    fun generateStudentRegistrationRequest(
        email: String = UserMock.EMAIL.value,
    ) = RegistrationRequest(
        name = "Name",
        surname = "Surname",
        password = UserMock.PASSWORD.value,
        email = email,
        phone = UserMock.PHONE.value,
        student = true,
        university = UserMock.UNIVERSITY.value,
        educationDegree = EducationDegree.BACHELOR,
        grade = 1,
        specialty = UserMock.SPECIALTY.value,
        enableEmail = true
    )

    @Transactional
    fun registerUser(
        email: String = UserMock.EMAIL.value,
    ): User {
        val user = authService.register(generateRegistrationRequest(email))
        return userService.findUserEntityById(user.id)
    }

    @Transactional
    fun registerStudentUser(
        email: String = UserMock.EMAIL.value,
    ): User {
        val user = authService.register(generateStudentRegistrationRequest(email))
        return userService.findUserEntityById(user.id)
    }
}
