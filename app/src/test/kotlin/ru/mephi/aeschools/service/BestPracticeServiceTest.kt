package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.ContentPhotoDao
import ru.mephi.aeschools.database.repository.best_practice.BestPracticeDao
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockBestPracticeRequest
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BestPracticeServiceTest {

    @Autowired
    lateinit var bestPracticeService: BestPracticeService

    @Autowired
    lateinit var bestPracticeDao: BestPracticeDao

    @Autowired
    lateinit var schoolService: SchoolService

    @Autowired
    lateinit var schoolDao: SchoolDao

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userHelper: UserHelper

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var contentPhotoService: ContentPhotoService

    @Autowired
    lateinit var contentPhotoDao: ContentPhotoDao

    @Autowired
    lateinit var storageHelper: StorageManagerHelper

    @MockBean
    lateinit var imageManager: StorageImageManager

    init {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        bestPracticeDao.deleteAll()
        schoolDao.deleteAll()
        contentPhotoDao.deleteAll()
    }

    @Test
    fun `test service injection`() {
        // pon
    }

    @Test
    @Transactional
    fun `list best practices for public`() {
        val user = userHelper.registerUser()
        val bestPractices = List(5) {
            bestPracticeService.createBestPractice(
                user.id as UUID,
                mockBestPracticeRequest(),
                null
            ).id
        }
        val response =
            bestPracticeService.listBestPractices("", "", mockPageRequest()).content.map { it.id }
        assertTrue(response.containsAll(bestPractices))
    }

    @Test
    @Transactional
    fun `list best practices by query`() {
        val user = userHelper.registerUser()
        List(5) {
            bestPracticeService.createBestPractice(
                user.id as UUID,
                mockBestPracticeRequest(title = "Title $it"),
                null
            ).id
        }
        val response =
            bestPracticeService.listBestPractices("Title 2", "", mockPageRequest())
        assertEquals("Title 2", response.content.first().title)
    }

    @Test
    @Transactional
    fun `list best practices by tags`() {
        val user = userHelper.registerUser()
        List(5) {
            bestPracticeService.createBestPractice(
                user.id as UUID,
                mockBestPracticeRequest(tags = setOf("Tag", "Tag $it")),
                null
            ).id
        }
        val response =
            bestPracticeService.listBestPractices("", "Tag 4", mockPageRequest())
        assertEquals(setOf("Tag", "Tag 4"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `list best practices for admin`() {
        val user = userHelper.registerUser()
        val bestPractices = List(5) {
            bestPracticeService.createBestPractice(
                user.id as UUID,
                mockBestPracticeRequest(),
                null
            ).id
        }
        val response =
            bestPracticeService.listBestPracticesForAdmin(
                "",
                "",
                mockPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(bestPractices))
    }

    @Test
    @Transactional
    fun `list school best practices by query`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        List(5) {
            bestPracticeService.createSchoolBestPractice(
                user.id as UUID,
                school.id,
                mockBestPracticeRequest(title = "Title $it"),
                null
            ).id
        }
        val response =
            bestPracticeService.listSchoolBestPractices(school.id, "Title 2", "", mockPageRequest())
        assertEquals("Title 2", response.content.first().title)
    }

    @Test
    @Transactional
    fun `list school best practices by tags`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        List(5) {
            bestPracticeService.createSchoolBestPractice(
                user.id as UUID,
                school.id,
                mockBestPracticeRequest(tags = setOf("Tag", "Tag $it")),
                null
            ).id
        }
        val response =
            bestPracticeService.listSchoolBestPractices(school.id, "", "Tag 4", mockPageRequest())
        assertEquals(setOf("Tag", "Tag 4"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `update best practice`() {
        val user = userHelper.registerUser()
        val bestPractice =
            bestPracticeService.createBestPractice(user.id as UUID, mockBestPracticeRequest(), null)
        val response = bestPracticeService.updateBestPractice(
            user.id as UUID,
            bestPractice.id,
            mockBestPracticeRequest(tags = setOf("Tag 1", "Tag 3"))
        )
        assertEquals(setOf("Tag 1", "Tag 3"), response.tags)
    }

    @Test
    fun `upload best practice photo`() {
        val user = userHelper.registerUser()
        val bestPractice =
            bestPracticeService.createBestPractice(user.id as UUID, mockBestPracticeRequest(), null)
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.BEST_PRACTICE, null, true))
            .thenReturn("Path")
        val response =
            bestPracticeService.uploadBestPracticePhoto(user.id as UUID, bestPractice.id, file)
        assertEquals("Path", response.photoPath)
    }

    @Test
    fun `delete best practice`() {
        val user = userHelper.registerUser()
        val bestPractice =
            bestPracticeService.createBestPractice(user.id as UUID, mockBestPracticeRequest(), null)
        bestPracticeService.deleteBestPractice(user.id as UUID, bestPractice.id)
        assertThrows<ResourceNotFoundException> {
            bestPracticeService.findBestPracticeById(
                bestPractice.id
            )
        }
    }

    @Test
    fun `create best practice with content photos`() {
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true))
            .thenReturn("Path")
        contentPhotoService.createContentPhoto(file)
        val user = userHelper.registerUser()
        val bestPractice =
            bestPracticeService.createBestPractice(
                user.id as UUID,
                mockBestPracticeRequest(content = "<img src=\"${storageHelper.getPhotoPath("Path")}\"></img>"),
                null
            )
        assertEquals(user.id, bestPractice.author?.id)
        Thread.sleep(5000)
        assertEquals(bestPractice.id, contentPhotoDao.findByPhotoPath("Path").get().content?.id)
    }

    @Test
    fun `create school best practice`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val bestPractice =
            bestPracticeService.createSchoolBestPractice(
                user.id as UUID,
                school.id,
                mockBestPracticeRequest(),
                null
            )
        assertEquals(user.id, bestPractice.author?.id)
        assertEquals(school.id, bestPractice.school?.id)
    }

    @Test
    fun `when user deleted then best practice is present`() {
        val user = userHelper.registerUser()
        val bestPractice =
            bestPracticeService.createBestPractice(user.id as UUID, mockBestPracticeRequest(), null)
        userService.delete(user.id as UUID)
        assertTrue(bestPracticeDao.existsById(bestPractice.id))
    }
}
