package ru.mephi.aeschools.service.helpers

enum class UserMock(val value: String) {
    EMAIL("example@email.com"),
    PASSWORD("password"),
    PHONE("+79999999999"),
    FINGERPRINT("36a154f0-38f2-40b1-9eea-84dce50ed979"),
    UNIVERSITY("NRNU MEPhI"),
    SPECIALTY("01.01.01"),
    ORGANIZATION("Organization"),
    JOB_ROLE("Job Role")
}
