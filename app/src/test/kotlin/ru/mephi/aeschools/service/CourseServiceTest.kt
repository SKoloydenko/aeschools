package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.ContentPhotoDao
import ru.mephi.aeschools.database.repository.course.CourseDao
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockCourseRequest
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CourseServiceTest {

    @Autowired
    lateinit var courseService: CourseService

    @Autowired
    lateinit var courseDao: CourseDao

    @Autowired
    lateinit var schoolService: SchoolService

    @Autowired
    lateinit var schoolDao: SchoolDao

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userHelper: UserHelper

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var contentPhotoService: ContentPhotoService

    @Autowired
    lateinit var contentPhotoDao: ContentPhotoDao

    @Autowired
    lateinit var storageHelper: StorageManagerHelper

    @MockBean
    lateinit var imageManager: StorageImageManager

    init {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        courseDao.deleteAll()
        schoolDao.deleteAll()
        contentPhotoDao.deleteAll()
    }

    @Test
    fun `test service injection`() {
        // pon
    }

    @Test
    @Transactional
    fun `list courses for public`() {
        val user = userHelper.registerUser()
        val courses = List(5) {
            courseService.createCourse(
                user.id as UUID,
                mockCourseRequest(),
                null
            ).id
        }
        val response =
            courseService.listCourses("", "", mockPageRequest()).content.map { it.id }
        assertTrue(response.containsAll(courses))
    }

    @Test
    @Transactional
    fun `list courses by tags for public`() {
        val user = userHelper.registerUser()
        List(5) {
            courseService.createCourse(
                user.id as UUID,
                mockCourseRequest(tags = setOf("Tag", "Tag $it")),
                null
            )
        }
        val response = courseService.listCourses(
            "",
            "Tag 3",
            mockPageRequest()
        )
        assertEquals(setOf("Tag", "Tag 3"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `update course`() {
        val user = userHelper.registerUser()
        val course = courseService.createCourse(user.id as UUID, mockCourseRequest(), null)
        val response = courseService.updateCourse(
            user.id as UUID,
            course.id,
            mockCourseRequest(title = "New title")
        )
        assertEquals("New title", response.title)
    }

    @Test
    fun `upload course photo`() {
        val user = userHelper.registerUser()
        val course =
            courseService.createCourse(user.id as UUID, mockCourseRequest(), null)
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.COURSE, null, true))
            .thenReturn("Path")
        val response =
            courseService.uploadCoursePhoto(user.id as UUID, course.id, file)
        assertEquals("Path", response.photoPath)
    }

    @Test
    fun `delete course`() {
        val user = userHelper.registerUser()
        val course =
            courseService.createCourse(user.id as UUID, mockCourseRequest(), null)
        courseService.deleteCourse(user.id as UUID, course.id)
        assertThrows<ResourceNotFoundException> {
            courseService.findCourseById(
                course.id
            )
        }
    }

    @Test
    fun `create course with content photos`() {
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true))
            .thenReturn("Path")
        contentPhotoService.createContentPhoto(file)
        val user = userHelper.registerUser()
        val course =
            courseService.createCourse(
                user.id as UUID,
                mockCourseRequest(content = "<img src=\"${storageHelper.getPhotoPath("Path")}\"></img>"),
                null
            )
        assertEquals(user.id, course.author?.id)
        Thread.sleep(5000)
        assertEquals(course.id, contentPhotoDao.findByPhotoPath("Path").get().content?.id)
    }

    @Test
    fun `create school course`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val course =
            courseService.createSchoolCourse(
                user.id as UUID,
                school.id,
                mockCourseRequest(),
                null
            )
        assertEquals(user.id, course.author?.id)
        assertEquals(school.id, course.school?.id)
    }

    @Test
    fun `when user deleted then course is present`() {
        val user = userHelper.registerUser()
        val course =
            courseService.createCourse(user.id as UUID, mockCourseRequest(), null)
        userService.delete(user.id as UUID)
        assertTrue(courseDao.existsById(course.id))
    }
}
