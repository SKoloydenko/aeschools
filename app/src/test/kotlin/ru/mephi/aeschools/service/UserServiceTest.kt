package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertDoesNotThrow
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.internship.InternshipDao
import ru.mephi.aeschools.database.repository.internship.QuestionnaireDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.dto.user.request.BanRequest
import ru.mephi.aeschools.model.dto.user.response.UserStudentResponse
import ru.mephi.aeschools.model.dto.user.response.UserWorkerResponse
import ru.mephi.aeschools.model.exceptions.common.AdminDeleteException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.helpers.mockUserRequest
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    private lateinit var userHelper: UserHelper

    @Autowired
    private lateinit var internshipDao: InternshipDao

    @Autowired
    private lateinit var questionnaireDao: QuestionnaireDao

    @Autowired
    private lateinit var schooLService: SchoolService

    @BeforeAll
    fun disableVerification() {
        userHelper.disableVerification()
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        questionnaireDao.deleteAll()
        internshipDao.deleteAll()
    }

    @Test
    fun `test service injection`() {
        // pon
    }

    @Test
    fun `list users`() {
        val users = List(5) { userHelper.registerUser("user$it@mail.ru").id }
        val response = userService.findAll().map { it.id }
        assertTrue(response.containsAll(users))
    }

    @Test
    fun `list student users`() {
        List(5) { userHelper.registerStudentUser("user$it@mail.ru") }
        val response = userService.list("", null, false, mockPageRequest())
        response.content.forEach {
            assertEquals(
                "NRNU MEPhI",
                (it as UserStudentResponse).university
            )
        }
    }

    @Test
    fun `list worker users`() {
        List(5) { userHelper.registerUser("user$it@mail.ru") }
        val response = userService.list("", null, false, mockPageRequest())
        response.content.forEach {
            assertEquals(
                "Organization",
                (it as UserWorkerResponse).organization
            )
        }
    }

    @Test
    fun `find user by email`() {
        val user = userHelper.registerUser(email = "another@mail.ru")
        assertDoesNotThrow { userService.findUserEntityByEmail(user.email) }
    }

    @Test
    fun `list not moderators order by students`() {
        val user = userHelper.registerUser()
        val student = userHelper.registerStudentUser(email = "student@email.com")
        userHelper.registerUser(email = "worker@email.com")
        val moderator = userHelper.registerUser(email = "moderator@email.com")
        val school = schooLService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        schooLService.createModerator(user.id as UUID, school.id, moderator.id as UUID)
        val response = userService.list("",
            moderator = false,
            orderStudents = false,
            mockPageRequest()
        ).content.map { it.id }
        assertEquals(response.last(), student.id as UUID)
    }

    @Test
    fun `update user info`() {
        val user = userHelper.registerUser()
        val response = userService.update(
            user.id as UUID,
            mockUserRequest(name = "New name", surname = "New surname")
        )
        assertEquals("New surname New name", response.fullName)
    }

    @Test
    fun `delete self`() {
        val user = userHelper.registerUser()
        userService.delete(user.id as UUID)
        assertThrows<ResourceNotFoundException> { userService.findById(user.id as UUID) }
    }

    @Test
    fun `delete user by id`() {
        val user = userHelper.registerUser()
        userService.delete(user.id as UUID, UUID.randomUUID())
        assertThrows<ResourceNotFoundException> { userService.findById(user.id as UUID) }
    }

    @Test
    @Transactional
    fun `delete admin self`() {
        val user = userHelper.registerUser()
        assertThrows<AdminDeleteException> { userService.delete(user.id as UUID, user.id as UUID) }
    }

    @Test
    fun `ban by id`() {
        val user = userHelper.registerUser()
        userService.setBanStatus(user.id as UUID, BanRequest(banned = true))
        assertTrue(userService.findById(user.id as UUID).banned)
    }

    @Test
    fun `unban by id`() {
        val user = userHelper.registerUser()
        userService.setBanStatus(user.id as UUID, BanRequest(banned = false))
        assertFalse(userService.findById(user.id as UUID).banned)
    }
}
