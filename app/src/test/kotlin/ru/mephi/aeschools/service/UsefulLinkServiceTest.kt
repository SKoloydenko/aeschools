package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.useful_link.UsefulLinkDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockUsefulLinkRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import java.util.*

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UsefulLinkServiceTest {

    @Autowired
    lateinit var usefulLinkService: UsefulLinkService

    @Autowired
    lateinit var usefulLinkDao: UsefulLinkDao

    @Autowired
    lateinit var userHelper: UserHelper

    @Autowired
    lateinit var userDao: UserDao

    @MockBean
    lateinit var imageManager: StorageImageManager

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        usefulLinkDao.deleteAll()
    }

    @Test
    fun `test service injection`() {
        // pon
    }

    @Test
    fun `list useful links`() {
        val user = userHelper.registerUser()
        val links = List(5) {
            usefulLinkService.createUsefulLink(user.id as UUID, mockUsefulLinkRequest(), null).id
        }
        val response =
            usefulLinkService.listUsefulLinks(mockPageRequest()).content.map { it.id }
        assertTrue(response.containsAll(links))
    }

    @Test
    fun `create useful link`() {
        val user = userHelper.registerUser()
        val request = mockUsefulLinkRequest()
        val file = MockPart(
            "photo.png", "content".toByteArray()
        )
        `when`(
            imageManager.storeImage(
                file, ImageStoreDir.USEFUL_LINK, null, true
            )
        ).thenReturn("Path")
        val response =
            usefulLinkService.createUsefulLink(user.id as UUID, request, file)
        assertEquals(request.title, response.title)
    }

    @Test
    fun `update useful link`() {
        val title = "New Useful Link"
        val user = userHelper.registerUser()
        val link =
            usefulLinkService.createUsefulLink(user.id as UUID, mockUsefulLinkRequest(), null)
        val response =
            usefulLinkService.updateUsefulLink(
                user.id as UUID,
                link.id,
                mockUsefulLinkRequest(title)
            )
        assertEquals(title, response.title)
    }

    @Test
    fun `upload useful link photo`() {
        val path = "New Path"
        val user = userHelper.registerUser()
        val link =
            usefulLinkService.createUsefulLink(user.id as UUID, mockUsefulLinkRequest(), null)
        val file = MockPart(
            "photo.png", "content".toByteArray()
        )
        `when`(
            imageManager.storeImage(
                file, ImageStoreDir.USEFUL_LINK, link.photoPath, true
            )
        ).thenReturn(path)
        val response =
            usefulLinkService.uploadUsefulLinkPhoto(user.id as UUID, link.id, file)
        assertEquals(path, response.photoPath)
    }

    @Test
    fun `delete useful link`() {
        val user = userHelper.registerUser()
        val link =
            usefulLinkService.createUsefulLink(user.id as UUID, mockUsefulLinkRequest(), null)
        usefulLinkService.deleteUsefulLink(user.id as UUID, link.id)
    }
}
