package ru.mephi.aeschools.service


import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.ContentPhotoDao
import ru.mephi.aeschools.database.repository.event.EventDao
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockPublicationPageRequest
import ru.mephi.aeschools.service.helpers.mockEventRequest
import ru.mephi.aeschools.service.helpers.mockForeignEventRequest
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EventServiceTest {

    @Autowired
    private lateinit var eventDao: EventDao

    @Autowired
    private lateinit var eventService: EventService

    @Autowired
    private lateinit var userHelper: UserHelper

    @Autowired
    private lateinit var userDao: UserDao

    @Autowired
    private lateinit var schoolService: SchoolService

    @Autowired
    private lateinit var schoolDao: SchoolDao

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var contentPhotoService: ContentPhotoService

    @Autowired
    private lateinit var contentPhotoDao: ContentPhotoDao

    @Autowired
    private lateinit var storageHelper: StorageManagerHelper

    @MockBean
    private lateinit var imageManager: StorageImageManager

    init {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }

    @BeforeAll
    fun disableVerification() {
        userHelper.disableVerification()
        cleanData()
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        eventDao.deleteAll()
        schoolDao.deleteAll()
        contentPhotoDao.deleteAll()
    }

    @Test
    @Transactional
    fun `list all events for public`() {
        val user = userHelper.registerUser()
        val events = List(5) {
            eventService.createEvent(user.id as UUID, mockEventRequest(), null).id
            eventService.createEvent(user.id as UUID, mockForeignEventRequest(), null).id
        }
        val response =
            eventService.listEvents("", "", mockPublicationPageRequest()).content.map { it.id }
        assertTrue(response.containsAll(events))
    }

    @Test
    @Transactional
    fun `hide future events for public`() {
        val user = userHelper.registerUser()
        eventService.createEvent(
            user.id as UUID,
            mockEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
            null
        )
        eventService.createEvent(
            user.id as UUID,
            mockForeignEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
            null
        )
        val response =
            eventService.listEvents("", "", mockPublicationPageRequest())
        assertTrue(response.content.isEmpty())
    }

    @Test
    @Transactional
    fun `list events for admin`() {
        val user = userHelper.registerUser()
        val events = List(5) {
            eventService.createEvent(
                user.id as UUID,
                mockEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            ).id
            eventService.createEvent(
                user.id as UUID,
                mockForeignEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            ).id
        }
        val response =
            eventService.listEventsForAdmin(
                "", "",
                mockPublicationPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(events))
    }

    @Test
    @Transactional
    fun `list events by query for public`() {
        val user = userHelper.registerUser()
        List(5) {
            eventService.createEvent(
                user.id as UUID,
                mockEventRequest(title = "Title $it"),
                null
            )
        }
        val response = eventService.listEvents(
            "Title 2", "",
            mockPublicationPageRequest()
        )
        assertEquals("Title 2", response.content.first().title)
    }

    @Test
    @Transactional
    fun `list all school events`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        eventService.createEvent(user.id as UUID, mockForeignEventRequest(), null)
        eventService.createSchoolEvent(
            user.id as UUID,
            school.id,
            mockEventRequest(),
            null
        )
        val schoolEvents =
            eventService.listSchoolEvents(school.id, "", "", mockPublicationPageRequest())
        val events = eventService.listEvents("", "", mockPublicationPageRequest())
        assertEquals(1, schoolEvents.content.size)
        assertEquals(2, events.content.size)
    }

    @Test
    @Transactional
    fun `list events for user`() {
        val user = userHelper.registerUser()
        val events = List(5) {
            eventService.createEvent(user.id as UUID, mockEventRequest(), null).id
        }
        events.forEach { eventService.joinEvent(user.id as UUID, it) }
        val response = eventService.listEventsForUser(
            user.id as UUID,
            "",
            "",
            mockPageRequest()
        ).content.map { it.id }
        assertTrue(response.containsAll(events))
    }

    @Test
    @Transactional
    fun `list events for user by tags`() {
        val user = userHelper.registerUser()
        val events = List(5) {
            eventService.createEvent(user.id as UUID, mockEventRequest(tags = setOf("Tag", "Tag $it")), null).id
        }
        events.forEach { eventService.joinEvent(user.id as UUID, it) }
        val response = eventService.listEventsForUser(
            user.id as UUID,
            "",
            "Tag 3",
            mockPageRequest()
        )
        assertEquals(setOf("Tag", "Tag 3"), response.content.first().tags)
    }

    @Test
    fun `create event`() {
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(user.id as UUID, mockEventRequest(), null)
        assertEquals(user.id, event.author?.id)
    }

    @Test
    fun `create event with file`() {
        val user = userHelper.registerUser()
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.EVENT, null, true))
            .thenReturn("Path")
        val event =
            eventService.createEvent(user.id as UUID, mockEventRequest(), file)
        assertEquals(user.id, event.author?.id)
        assertEquals("Path", event.photoPath)
    }

    @Test
    fun `create event with content photos`() {
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true))
            .thenReturn("Path")
        contentPhotoService.createContentPhoto(file)
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(
                user.id as UUID,
                mockEventRequest(content = "<img src=\"${storageHelper.getPhotoPath("Path")}\"></img>"),
                null
            )
        assertEquals(user.id, event.author?.id)
        Thread.sleep(5000)
        assertEquals(event.id, contentPhotoDao.findByPhotoPath("Path").get().content?.id)
    }

    @Test
    @Transactional
    fun `update event`() {
        val user = userHelper.registerUser()
        val event = eventService.createEvent(user.id as UUID, mockEventRequest(), null)
        val response = eventService.updateEvent(
            user.id as UUID,
            event.id,
            mockEventRequest("New title")
        )
        assertEquals("New title", response.title)
    }

    @Test
    fun `upload event photo`() {
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(user.id as UUID, mockEventRequest(), null)
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.EVENT, null, true))
            .thenReturn("Path")
        val response =
            eventService.uploadEventPhoto(user.id as UUID, event.id, file)
        assertTrue(response.photoPath != null)
    }

    @Test
    fun `delete event`() {
        val user = userHelper.registerUser()
        val event = eventService.createEvent(user.id as UUID, mockEventRequest(), null)
        eventService.deleteEvent(user.id as UUID, event.id)
        assertThrows<ResourceNotFoundException> {
            eventService.findEventById(event.id)
        }
    }

    @Test
    fun `create school event`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val event = eventService.createSchoolEvent(
            user.id as UUID,
            school.id,
            mockEventRequest(),
            null
        )
        assertEquals(user.id, event.author?.id)
        assertEquals(school.id, event.school?.id)
    }

    @Test
    @Transactional
    fun `find event by id for public`() {
        val user = userHelper.registerUser()
        val event = eventService.createEvent(user.id as UUID, mockEventRequest(), null)
        val response = eventService.findEventById(event.id)
        assertEquals(event.id, response.id)
    }

    @Test
    @Transactional
    fun `find future event by id for public`() {
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(
                user.id as UUID,
                mockEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            )
        assertThrows<ResourceNotFoundException> { eventService.findEventById(event.id) }
    }

    @Test
    @Transactional
    fun `find event by id for admin`() {
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(
                user.id as UUID,
                mockEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            )
        val response = eventService.findEventByIdForAdmin(event.id)
        assertEquals(event.id, response.id)
    }

    @Test
    @Transactional
    fun `find school event by id for admin`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val event =
            eventService.createSchoolEvent(
                user.id as UUID,
                school.id,
                mockEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            )
        val response = eventService.findSchoolEventByIdForAdmin(school.id, event.id)
        assertEquals(event.id, response.id)
    }

    @Test
    fun `when user deleted then event is present`() {
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(user.id as UUID, mockEventRequest(), null)
        userService.delete(user.id as UUID)
        assertTrue(eventDao.existsById(event.id))
    }

    @Test
    @Transactional
    fun `join event`() {
        val user = userHelper.registerUser()
        val event =
            eventService.createEvent(
                user.id as UUID,
                mockEventRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            )
        eventService.joinEvent(user.id as UUID, event.id)
    }
}
