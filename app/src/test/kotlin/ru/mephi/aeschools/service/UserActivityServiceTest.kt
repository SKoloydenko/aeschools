package ru.mephi.aeschools.service

import org.apache.catalina.connector.Connector
import org.apache.catalina.connector.Request
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.activity.UserActivityDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.impl.activity.UserActivityServiceImpl
import java.util.*

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserActivityServiceTest {
    @Autowired
    lateinit var userActivityService: UserActivityServiceImpl

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var userActivityDao: UserActivityDao

    @Autowired
    lateinit var userHelper: UserHelper

    @BeforeAll
    fun disableVerification() {
        userHelper.disableVerification()
        cleanData()
    }

    @AfterEach
    fun cleanData() {
        userActivityDao.deleteAll()
        userDao.deleteAll()
    }

    @Test
    fun `time setting must be in the right order`() {
        val user = userHelper.registerUser()
        val request = Request(Connector())
        request.userPrincipal = UsernamePasswordAuthenticationToken(user.id, user.id)
        userActivityService.handle(request)
        val activity1 = userActivityService.findEntityById(user.id as UUID)
        Thread.sleep(500)
        userActivityService.handle(request)
        val activity2 = userActivityService.findEntityById(user.id as UUID)
        assertTrue(activity2.time.isAfter(activity1.time) || activity1.time == activity2.time)
    }

    @Test
    fun `delete activity`() {
        val user = userHelper.registerUser()
        val request = Request(Connector())
        request.userPrincipal = UsernamePasswordAuthenticationToken(user.id, null)
        userActivityService.handle(request)
        userActivityService.findEntityById(user.id as UUID)
        userActivityService.deleteById(user.id as UUID)
        assertTrue(userActivityService.findAll().isEmpty())
    }
}
