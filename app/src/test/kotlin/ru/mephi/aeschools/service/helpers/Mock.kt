package ru.mephi.aeschools.service.helpers

import org.springframework.data.domain.Sort
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.best_practice.request.BestPracticeRequest
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.model.dto.event.request.ForeignEventRequest
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.model.dto.publication.request.PublicationRequest
import ru.mephi.aeschools.model.dto.school.request.ContactsRequest
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.util.constants.DEFAULT_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.constants.DEFAULT_PAGE_NUMBER_PARAM
import ru.mephi.aeschools.util.constants.DEFAULT_PAGE_SIZE_PARAM
import ru.mephi.aeschools.util.constants.DEFAULT_PUBLICATION_PAGE_FIELD_PARAM
import java.time.LocalDateTime

fun mockUserRequest(name: String = "Name", surname: String = "Surname") = UserRequest(
    name = name,
    surname = surname,
    phone = UserMock.PHONE.value,
    student = false,
    organization = UserMock.ORGANIZATION.value,
    jobRole = UserMock.JOB_ROLE.value
)

fun mockSchoolRequest(name: String = "Name", shortName: String = "Short Name") = SchoolRequest(
    university = "University",
    name = name,
    shortName = shortName,
    actionScope = "Action scope",
    about = "About",
    educationalActivities = "Educational Activities",
    researchActivities = "Research Activities",
    innovationalActivities = "Innovational Activities",
    contacts = mockContactsRequest()
)

fun mockLeaderRequest(fullName: String = "Full Name") = LeaderRequest(
    fullName = fullName,
    jobRole = "Job Role",
    ranks = "Ranks",
    achievements = "Achievements"
)

fun mockContactsRequest() = ContactsRequest(
    website = "Website",
    email = "Email",
    phone = "Phone"
)

fun mockIndustrialPartnerRequest(name: String = "Name") = IndustrialPartnerRequest(
    name = name,
    partnershipFormat = "Format",
    schoolRole = "School Role",
    partnerRole = "Partner Role",
    cooperationDirection = "Direction"
)

fun mockUniversityPartnerRequest(name: String = "Name") = UniversityPartnerRequest(
    name = name,
    description = "Description",
    schoolRole = "School Role",
    universityRole = "University Role",
    cooperationDirection = "Direction"
)

fun mockInternshipRequest(
    title: String = "Name",
    content: String = "Content",
    publicationDate: LocalDateTime = LocalDateTime.now().minusHours(1),
    tags: Set<String> = setOf("Tag 1", "Tag 2"),
) = InternshipRequest(
    title = title,
    content = content,
    publicationDate = publicationDate,
    company = "Company",
    tags = tags
)

fun mockQuestionnaireRequest(motivationalLetter: String = "Letter") = QuestionnaireRequest(
    motivationalLetter = motivationalLetter,
    achievements = "Achievements"
)

fun mockUsefulLinkRequest(title: String = "Useful Link") = UsefulLinkRequest(
    title = title,
    link = "Link"
)

fun mockPublicationRequest(
    title: String = "Title",
    content: String = "Content",
    publicationDate: LocalDateTime = LocalDateTime.now().minusHours(1),
    tags: Set<String> = setOf("Tag 1", "Tag 2"),
) =
    PublicationRequest(
        title = title,
        content = content,
        publicationDate = publicationDate,
        tags = tags,
    )

fun mockEventRequest(
    title: String = "Title",
    content: String = "Content",
    eventDate: LocalDateTime = LocalDateTime.now().plusDays(1),
    publicationDate: LocalDateTime = LocalDateTime.now().minusHours(1),
    tags: Set<String> = setOf("Tag 1", "Tag 2"),
) = EventRequest(
    title = title,
    content = content,
    location = "Location",
    eventDate = eventDate,
    publicationDate = publicationDate,
    tags = tags,
)

fun mockForeignEventRequest(
    title: String = "Title",
    content: String = "Content",
    publicationDate: LocalDateTime = LocalDateTime.now().minusHours(1),
    tags: Set<String> = setOf("Tag 1", "Tag 2"),
) = ForeignEventRequest(
    title = title,
    content = content,
    publicationDate = publicationDate,
    tags = tags,
)

fun mockBestPracticeRequest(
    title: String = "Title",
    content: String = "Content",
    publicationDate: LocalDateTime = LocalDateTime.now().minusHours(1),
    tags: Set<String> = setOf("Tag 1", "Tag 2"),
) = BestPracticeRequest(
    title = title,
    content = content,
    publicationDate = publicationDate,
    tags = tags,
)

fun mockCourseRequest(
    title: String = "Title",
    content: String = "Content",
    publicationDate: LocalDateTime = LocalDateTime.now().minusHours(1),
    tags: Set<String> = setOf("Tag 1", "Tag 2"),
) = CourseRequest(
    title = title,
    content = content,
    publicationDate = publicationDate,
    enrollmentFinishingDate = LocalDateTime.now().plusDays(1),
    tags = tags,
)

fun mockPageRequest() =
    ExtendedPageRequest(
        DEFAULT_PAGE_NUMBER_PARAM,
        DEFAULT_PAGE_SIZE_PARAM,
        Sort.DEFAULT_DIRECTION,
        DEFAULT_PAGE_FIELD_PARAM
    )

fun mockPublicationPageRequest() =
    ExtendedPageRequest(
        DEFAULT_PAGE_NUMBER_PARAM,
        DEFAULT_PAGE_SIZE_PARAM,
        Sort.DEFAULT_DIRECTION,
        DEFAULT_PUBLICATION_PAGE_FIELD_PARAM
    )
