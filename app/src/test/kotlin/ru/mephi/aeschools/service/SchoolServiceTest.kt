package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.jpa.repository.Modifying
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceAlreadyExistsException
import ru.mephi.aeschools.model.exceptions.school.StudentCanNotBeModerator
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockIndustrialPartnerRequest
import ru.mephi.aeschools.service.helpers.mockLeaderRequest
import ru.mephi.aeschools.service.helpers.mockPageRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.helpers.mockUniversityPartnerRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SchoolServiceTest {
    @Autowired
    private lateinit var schoolService: SchoolService

    @Autowired
    private lateinit var userHelper: UserHelper

    @Autowired
    private lateinit var userDao: UserDao

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var schoolDao: SchoolDao

    @MockBean
    private lateinit var imageManager: StorageImageManager

    @BeforeAll
    fun disableVerification() {
        userHelper.disableVerification()
        cleanData()
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        schoolDao.deleteAll()
    }

    @Test
    fun `list schools`() {
        val user = userHelper.registerUser()
        val schools = List(5) {
            schoolService.createSchool(
                user.id as UUID,
                mockSchoolRequest(
                    name = "Name $it",
                    shortName = "Short Name $it"
                ),
                null
            ).id
        }
        val response = schoolService.listSchools("").map { it.id }
        assertTrue(response.containsAll(schools))
    }

    @Test
    @Transactional
    fun `find school by id`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val response = schoolService.findSchoolById(school.id)
        assertEquals(school.id, response.id)
    }

    @Test
    @Transactional
    fun `create school duplicate name`() {
        val user = userHelper.registerUser()
        val request = mockSchoolRequest()
        assertThrows<ResourceAlreadyExistsException> {
            schoolService.createSchool(user.id as UUID, request, null)
            schoolService.createSchool(user.id as UUID, request, null)
        }
    }

    @Test
    @Transactional
    fun `update school`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val updated =
            schoolService.updateSchool(
                user.id as UUID,
                school.id,
                mockSchoolRequest(name = "New name")
            )
        val response = schoolService.findSchoolById(school.id)
        assertEquals(updated.name, response.name)
    }

    @Test
    fun `update school name duplicated`() {
        val user = userHelper.registerUser()
        schoolService.createSchool(
            user.id as UUID,
            mockSchoolRequest("New school", "New short name"),
            null
        )
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        assertThrows<ResourceAlreadyExistsException> {
            schoolService.updateSchool(user.id as UUID, school.id, mockSchoolRequest("New school"))
        }
    }

    @Test
    fun `update school with photo`() {
        val user = userHelper.registerUser()
        val file = MockPart("file.png", "content".toByteArray())
        Mockito.`when`(imageManager.storeImage(file, ImageStoreDir.SCHOOL, null, true))
            .thenReturn("Path")
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), file)
        val response =
            schoolService.updateSchool(
                user.id as UUID,
                school.id,
                mockSchoolRequest(name = "New name")
            )
        assertEquals("New name", response.name)
        assertEquals("Path", response.photoPath)
    }

    @Test
    @Modifying
    fun `delete school`() {
        val user = userHelper.registerUser()
        val schools = List(5) {
            schoolService.createSchool(
                user.id as UUID,
                mockSchoolRequest(name = "Name $it", shortName = "Short name $it"),
                null
            )
        }
        schools.forEach { schoolService.deleteSchool(user.id as UUID, it.id) }
        val response = schoolService.listSchools("")
        assertTrue(response.isEmpty())
    }

    @Test
    fun `update leader`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val leader = schoolService.createLeader(
            user.id as UUID,
            school.id,
            mockLeaderRequest(),
            null
        )
        val response =
            schoolService.updateLeader(
                user.id as UUID,
                school.id,
                leader.id,
                mockLeaderRequest(fullName = "New full name")
            )
        assertEquals("New full name", response.fullName)
    }

    @Test
    @Transactional
    fun `delete leader`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val leaders = List(5) {
            schoolService.createLeader(
                user.id as UUID,
                school.id,
                mockLeaderRequest(),
                null
            ).id
        }
        leaders.forEach { schoolService.deleteLeader(user.id as UUID, school.id, it) }
        val response = schoolService.listLeaders(school.id, mockPageRequest())
        assertTrue(response.content.isEmpty())
    }

    @Test
    fun `list industrial partners`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val partners = List(5) {
            schoolService.createIndustrialPartner(
                user.id as UUID,
                school.id,
                mockIndustrialPartnerRequest(),
                null
            ).id
        }
        val response =
            schoolService.listIndustrialPartners(
                "",
                school.id,
                mockPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(partners))
    }

    @Test
    fun `update industrial partner`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val partner = schoolService.createIndustrialPartner(
            user.id as UUID,
            school.id,
            mockIndustrialPartnerRequest(),
            null
        )
        val response =
            schoolService.updateIndustrialPartner(
                user.id as UUID,
                school.id,
                partner.id,
                mockIndustrialPartnerRequest(name = "New name")
            )
        assertEquals("New name", response.name)
    }

    @Test
    fun `delete industrial partner`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val partners = List(5) {
            schoolService.createIndustrialPartner(
                user.id as UUID,
                school.id,
                mockIndustrialPartnerRequest(),
                null
            ).id
        }
        partners.forEach { schoolService.deleteIndustrialPartner(user.id as UUID, school.id, it) }
        val response =
            schoolService.listIndustrialPartners(
                "",
                school.id,
                mockPageRequest()
            ).content.map { it.id }
        assertTrue(response.isEmpty())
    }

    @Test
    fun `list university partners`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val partners = List(5) {
            schoolService.createUniversityPartner(
                user.id as UUID,
                school.id,
                mockUniversityPartnerRequest(),
                null
            ).id
        }
        val response =
            schoolService.listUniversityPartners(
                "",
                school.id,
                mockPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(partners))
    }

    @Test
    fun `update university partner`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val partner = schoolService.createUniversityPartner(
            user.id as UUID,
            school.id,
            mockUniversityPartnerRequest(),
            null
        )
        val response =
            schoolService.updateUniversityPartner(
                user.id as UUID,
                school.id,
                partner.id,
                mockUniversityPartnerRequest(name = "New name")
            )
        assertEquals("New name", response.name)
    }

    @Test
    fun `delete university partner`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val partners = List(5) {
            schoolService.createUniversityPartner(
                user.id as UUID,
                school.id,
                mockUniversityPartnerRequest(),
                null
            ).id
        }
        partners.forEach { schoolService.deleteUniversityPartner(user.id as UUID, school.id, it) }
        val response =
            schoolService.listUniversityPartners(
                "",
                school.id,
                mockPageRequest()
            ).content.map { it.id }
        assertTrue(response.isEmpty())
    }

    @Test
    @Transactional
    fun `list moderators`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val users = List(5) { userHelper.registerUser("user$it@mail.ru").id }
        users.forEach {
            schoolService.createModerator(
                user.id as UUID,
                school.id,
                it as UUID
            )
        }
        val response =
            schoolService.listModerators(
                school.id,
                "",
                mockPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(users))
    }

    @Test
    fun `student moderator`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val student = userHelper.registerStudentUser(email = "student@email.com")
        assertThrows<StudentCanNotBeModerator> {
            schoolService.createModerator(
                user.id as UUID,
                school.id,
                student.id as UUID
            )
        }
    }

    @Test
    fun `delete moderator`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val users = List(5) { userHelper.registerUser("user$it@mail.ru").id }
        users.forEach {
            schoolService.createModerator(
                user.id as UUID,
                school.id,
                it as UUID
            )
        }
        users.forEach { schoolService.deleteModerator(user.id as UUID, school.id, it as UUID) }
        val response =
            schoolService.listModerators(school.id, "", mockPageRequest()).content.map { it.id }
        assertTrue(response.isEmpty())
    }

    @Test
    fun `when school is deleted then users are present`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val users = List(5) { userHelper.registerUser("user$it@mail.ru").id }
        users.forEach {
            schoolService.createModerator(
                user.id as UUID,
                school.id,
                it as UUID
            )
        }
        schoolService.deleteSchool(user.id as UUID, school.id)
        users.forEach { assertTrue(userDao.existsById(it as UUID)) }
    }

    @Test
    fun `when users are deleted then moderators are deleted`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val users = List(5) { userHelper.registerUser("user$it@mail.ru").id }
        users.forEach {
            schoolService.createModerator(
                user.id as UUID,
                school.id,
                it as UUID
            )
        }
        users.forEach { userService.delete(it as UUID) }
        val response = schoolService.listModerators(school.id, "", mockPageRequest())
        assertTrue(response.content.isEmpty())
    }
}
