package ru.mephi.aeschools.service

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mock.web.MockPart
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import ru.mephi.aeschools.database.repository.ContentPhotoDao
import ru.mephi.aeschools.database.repository.publication.PublicationDao
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.service.helpers.UserHelper
import ru.mephi.aeschools.service.helpers.mockPublicationPageRequest


import ru.mephi.aeschools.service.helpers.mockPublicationRequest
import ru.mephi.aeschools.service.helpers.mockSchoolRequest
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@SpringBootTest
@TestPropertySource(locations = ["classpath:application-test.yml"])
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PublicationServiceTest {

    @Autowired
    private lateinit var publicationDao: PublicationDao

    @Autowired
    private lateinit var publicationService: PublicationService

    @Autowired
    private lateinit var userHelper: UserHelper

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var userDao: UserDao

    @Autowired
    private lateinit var schoolService: SchoolService

    @Autowired
    private lateinit var schoolDao: SchoolDao

    @Autowired
    private lateinit var contentPhotoService: ContentPhotoService

    @Autowired
    private lateinit var contentPhotoDao: ContentPhotoDao

    @Autowired
    private lateinit var storageHelper: StorageManagerHelper

    @MockBean
    lateinit var imageManager: StorageImageManager

    init {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }

    @BeforeAll
    fun disableVerification() {
        userHelper.disableVerification()
        cleanData()
    }

    @AfterEach
    fun cleanData() {
        userDao.deleteAll()
        publicationDao.deleteAll()
        schoolDao.deleteAll()
        contentPhotoDao.deleteAll()
    }

    @Test
    @Transactional
    fun `list all publications for public`() {
        val user = userHelper.registerUser()
        val publications = List(5) {
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null).id
        }
        val response =
            publicationService.listPublications(
                "",
                "",
                mockPublicationPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(publications))
    }

    @Test
    @Transactional
    fun `hide future publications for public`() {
        val user = userHelper.registerUser()
        publicationService.createPublication(
            user.id as UUID,
            mockPublicationRequest(publicationDate = LocalDateTime.now().plusHours(1)),
            null
        )
        val response =
            publicationService.listPublications("", "", mockPublicationPageRequest())
        assertTrue(response.content.isEmpty())
    }

    @Test
    @Transactional
    fun `list publications for admin`() {
        val user = userHelper.registerUser()
        val publications = List(5) {
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            ).id
        }
        val response =
            publicationService.listPublicationsForAdmin(
                "",
                "",
                mockPublicationPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(publications))
    }

    @Test
    @Transactional
    fun `list publications by query for public`() {
        val user = userHelper.registerUser()
        List(5) {
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(title = "Title $it"),
                null
            )
        }
        val response = publicationService.listPublications(
            "Title 2",
            "",
            mockPublicationPageRequest()
        )
        assertEquals("Title 2", response.content.first().title)
    }

    @Test
    @Transactional
    fun `list publications by tags for public`() {
        val user = userHelper.registerUser()
        List(5) {
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(tags = setOf("Tag", "Tag $it")),
                null
            )
        }
        val response = publicationService.listPublications(
            "",
            "Tag 3",
            mockPublicationPageRequest()
        )
        assertEquals(setOf("Tag", "Tag 3"), response.content.first().tags)
    }

    @Test
    fun `create publication`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        assertEquals(user.id, publication.author?.id)
    }

    @Test
    fun `create publication with content photos`() {
        val file = MockPart("file.png", "content".toByteArray())
        `when`(imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true))
            .thenReturn("Path")
        val user = userHelper.registerUser()
        contentPhotoService.createContentPhoto(file)
        val publication =
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(content = "<img src=\"${storageHelper.getPhotoPath("Path")}\"></img>"),
                null
            )
        assertEquals(user.id, publication.author?.id)
        Thread.sleep(1000)
        assertEquals(publication.id, contentPhotoDao.findByPhotoPath("Path").get().content?.id)
    }

    @Test
    fun `delete excluded content photos`() {
        val file = MockPart("file.png", "content".toByteArray())
        `when`(imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true))
            .thenReturn("Path")
        contentPhotoService.createContentPhoto(file)
        Thread.sleep(5000)
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(content = "<img src=\"${storageHelper.getPhotoPath("Path")}\"></img>"),
                null
            )
        publicationService.updatePublication(user.id as UUID, publication.id, mockPublicationRequest())
        Thread.sleep(5000)
        assertTrue(contentPhotoDao.findAll().toList().isEmpty())
    }

    @Test
    fun `create publication with file`() {
        val user = userHelper.registerUser()
        val file = MockPart("file.png", "content".toByteArray())
        `when`(imageManager.storeImage(file, ImageStoreDir.PUBLICATION, null, true))
            .thenReturn("Path")
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), file)
        assertEquals(user.id, publication.author?.id)
        assertEquals("Path", publication.photoPath)
    }

    @Test
    @Transactional
    fun `list all school publications`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        publicationService.createSchoolPublication(
            user.id as UUID,
            school.id,
            mockPublicationRequest(),
            null
        )
        val schoolPublications =
            publicationService.listSchoolPublications(
                school.id,
                "",
                "",
                mockPublicationPageRequest()
            )
        val publications =
            publicationService.listPublications("", "", mockPublicationPageRequest())
        assertEquals(1, schoolPublications.content.size)
        assertEquals(2, publications.content.size)
    }

    @Test
    @Transactional
    fun `hide future school publications for public`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        publicationService.createSchoolPublication(
            user.id as UUID,
            school.id,
            mockPublicationRequest(publicationDate = LocalDateTime.now().plusHours(1)),
            null
        )
        val response =
            publicationService.listSchoolPublications(
                school.id,
                "",
                "",
                mockPublicationPageRequest()
            )
        assertTrue(response.content.isEmpty())
    }

    @Test
    @Transactional
    fun `list school publications for admin`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val publications = List(5) {
            publicationService.createSchoolPublication(
                user.id as UUID,
                school.id,
                mockPublicationRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            ).id
        }
        val response =
            publicationService.listSchoolPublicationsForAdmin(
                school.id,
                "",
                "",
                mockPublicationPageRequest()
            ).content.map { it.id }
        assertTrue(response.containsAll(publications))
    }

    @Test
    @Transactional
    fun `list school publications by query`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        List(5) {
            publicationService.createSchoolPublication(
                user.id as UUID,
                school.id,
                mockPublicationRequest(title = "Title $it"),
                null
            )
        }
        val response = publicationService.listSchoolPublications(
            school.id,
            "Title 2",
            "",
            mockPublicationPageRequest()
        )
        assertEquals("Title 2", response.content.first().title)
    }

    @Test
    @Transactional
    fun `list school publications by tags`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        List(5) {
            publicationService.createSchoolPublication(
                user.id as UUID,
                school.id,
                mockPublicationRequest(tags = setOf("Tag", "Tag $it")),
                null
            )
        }
        val response = publicationService.listSchoolPublications(
            school.id,
            "",
            "Tag 4",
            mockPublicationPageRequest()
        )
        assertEquals(setOf("Tag", "Tag 4"), response.content.first().tags)
    }

    @Test
    @Transactional
    fun `find publication by id for public`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        val response = publicationService.findPublicationById(publication.id)
        assertEquals(publication.id, response.id)
    }

    @Test
    @Transactional
    fun `find future publication by id for public`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            )
        assertThrows<ResourceNotFoundException> { publicationService.findPublicationById(publication.id) }
    }

    @Test
    @Transactional
    fun `find publication by id for admin`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(
                user.id as UUID,
                mockPublicationRequest(publicationDate = LocalDateTime.now().plusHours(1)),
                null
            )
        val response = publicationService.findPublicationByIdForAdmin(publication.id)
        assertEquals(publication.id, response.id)
    }

    @Test
    fun `create school publication`() {
        val user = userHelper.registerUser()
        val school = schoolService.createSchool(user.id as UUID, mockSchoolRequest(), null)
        val publication = publicationService.createSchoolPublication(
            user.id as UUID,
            school.id,
            mockPublicationRequest(),
            null
        )
        assertEquals(user.id, publication.author?.id)
        assertEquals(school.id, publication.school?.id)
        assertEquals(setOf("Tag 1", "Tag 2"), publication.tags)
    }

    @Test
    @Transactional
    fun `update publication`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        val response = publicationService.updatePublication(
            user.id as UUID,
            publication.id,
            mockPublicationRequest(title = "New title", tags = setOf("Tag 1", "Tag 3"))
        )
        assertEquals("New title", response.title)
        assertEquals(setOf("Tag 1", "Tag 3"), response.tags)
    }

    @Test
    fun `upload publication photo`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        val file = MockPart("file.png", "content".toByteArray())
        `when`(imageManager.storeImage(file, ImageStoreDir.PUBLICATION, null, true))
            .thenReturn("Path")
        val response =
            publicationService.uploadPublicationPhoto(user.id as UUID, publication.id, file)
        assertEquals("Path", response.photoPath)
    }

    @Test
    fun `delete publication`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        publicationService.deletePublication(user.id as UUID, publication.id)
        assertThrows<ResourceNotFoundException> {
            publicationService.findPublicationById(publication.id)
        }
    }

    @Test
    fun `when user deleted then publication is present`() {
        val user = userHelper.registerUser()
        val publication =
            publicationService.createPublication(user.id as UUID, mockPublicationRequest(), null)
        userService.delete(user.id as UUID)
        assertTrue(publicationDao.existsById(publication.id))
    }
}
