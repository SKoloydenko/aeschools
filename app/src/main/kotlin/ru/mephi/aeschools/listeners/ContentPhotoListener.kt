package ru.mephi.aeschools.listeners

import ru.mephi.aeschools.model.dto.ContentPhotoRequest

interface ContentPhotoListener {
    fun handle(request: ContentPhotoRequest)
}
