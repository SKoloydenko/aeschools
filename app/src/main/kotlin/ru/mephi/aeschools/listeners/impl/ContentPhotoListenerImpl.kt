package ru.mephi.aeschools.listeners.impl

import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.mephi.aeschools.listeners.ContentPhotoListener
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.util.constants.CONTENT_PHOTO_QUEUE

@EnableRabbit
@Component
class ContentPhotoListenerImpl(
    private val contentPhotoService: ContentPhotoService,
) : ContentPhotoListener {
    @RabbitListener(queues = [CONTENT_PHOTO_QUEUE])
    override fun handle(request: ContentPhotoRequest) {
        contentPhotoService.updateContentPhoto(request)
    }
}
