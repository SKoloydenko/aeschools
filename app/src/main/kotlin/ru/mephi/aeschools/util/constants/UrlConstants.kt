package ru.mephi.aeschools.util.constants

// /api - app context-path
const val API_VERSION_1 = "/v1"
const val DEFAULT_PAGE_SIZE_PARAM = 20
const val DEFAULT_PAGE_NUMBER_PARAM = 1
const val DEFAULT_PAGE_FIELD_PARAM = "createdAt"
const val DEFAULT_PUBLICATION_PAGE_FIELD_PARAM = "publicationDate"
const val DEFAULT_USER_PAGE_FIELD_PARAM = "surname"
const val REFRESH_TOKEN_COOKIE = "RefreshToken"
