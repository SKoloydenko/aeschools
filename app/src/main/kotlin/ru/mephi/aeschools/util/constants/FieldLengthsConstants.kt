package ru.mephi.aeschools.util.constants

const val photoPathLength = 100
const val passwordMinLength = 8
const val passwordMaxLength = 31
const val passwordHashMaxLength = 60

const val specialtyLength = 8
const val phoneLength = 15
const val extraSmallLength = 30
const val smallLength = 60 // titles
const val mediumLength = 120 // little forms
const val largeLength = 300 // descriptions, links, titles
const val extraLargeLength = 600 // forms
const val shortPublicationLength = 2000
const val publicationLength = 60000 // publications

const val tagsSize = 5 // tag collection size
