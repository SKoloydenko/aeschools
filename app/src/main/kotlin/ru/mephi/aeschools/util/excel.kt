package ru.mephi.aeschools.util

import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Row
import java.time.LocalDate
import java.time.LocalDateTime


fun Row.writeToCell(columnNumber: Int, value: Any?, style: CellStyle) {
    sheet.autoSizeColumn(columnNumber)
    val cell = createCell(columnNumber)!!
    when (value) {
        is Boolean -> cell.setCellValue(value)
        is Double -> cell.setCellValue(value)
        is LocalDateTime -> cell.setCellValue(value.toString())
        is LocalDate -> cell.setCellValue(value.toString())
        else -> cell.setCellValue(value.toString())
    }
    cell.cellStyle = style
}
