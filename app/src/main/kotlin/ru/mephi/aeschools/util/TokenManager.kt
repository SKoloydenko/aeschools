package ru.mephi.aeschools.util

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import ru.mephi.aeschools.database.entity.user.Fingerprint
import ru.mephi.aeschools.database.entity.user.RefreshToken
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.user.FingerprintDao
import ru.mephi.aeschools.database.repository.user.RefreshTokenDao
import ru.mephi.aeschools.model.dto.user.UserPrincipal
import ru.mephi.aeschools.model.dto.user.request.RefreshRequest
import ru.mephi.aeschools.model.dto.user.response.TokenResponse
import ru.mephi.aeschools.model.exceptions.auth.CorruptedTokenException
import ru.mephi.aeschools.model.exceptions.auth.FingerprintNotFoundException
import java.math.BigInteger
import java.security.MessageDigest
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.transaction.Transactional

@Component
@Transactional
class TokenManager(
    @Value("\${spring.security.jwt.secret}")
    private val secret: String,

    @Value("\${spring.security.jwt.access.lifetime}")
    private val accessTokenLifetime: Long,

    @Value("\${spring.security.jwt.refresh.lifetime}")
    private val refreshTokenLifetime: Long,

    private val fingerprintDao: FingerprintDao,
    private val refreshTokenDao: RefreshTokenDao,
) {

    private val algorithm = SignatureAlgorithm.HS256
    private val key = Keys.hmacShaKeyFor(secret.toByteArray())

    private fun hash(token: String): String {
        val cipher = MessageDigest.getInstance("SHA-1")
        val hash = cipher.digest(token.toByteArray())
        return BigInteger(1, hash).toString(16)
    }

    private fun getTokenExpiration(lifetime: Long) =
        Instant.now().plus(lifetime, ChronoUnit.DAYS).let { Date.from(it) }

    private fun parseToken(token: String): Jws<Claims> {
        return try {
            Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
        } catch (throwable: JwtException) {
            throw CorruptedTokenException()
        }
    }

    private fun createFingerprint(user: User, deviceId: String): Fingerprint {
        fingerprintDao.deleteByDeviceId(deviceId)
        return Fingerprint(deviceId).apply {
            this.user = user
            fingerprintDao.save(this)
        }
    }

    private fun generateAccessToken(user: User): String {
        val expiration = getTokenExpiration(accessTokenLifetime)
        return Jwts.builder()
            .claim("userId", user.id)
            .setExpiration(expiration)
            .signWith(key, algorithm)
            .compact()
    }

    private fun generateRefreshToken(user: User, deviceId: String): String {
        refreshTokenDao.deleteByFingerprintDeviceIdAndFingerprintUser(
            deviceId,
            user
        )
        return RefreshToken().apply {
            this.fingerprint = createFingerprint(user, deviceId)
            this.expiresAt = getTokenExpiration(refreshTokenLifetime)
            this.hash = hash(token)
            refreshTokenDao.save(this)
        }.token
    }

    private fun generateTokenPair(fingerprint: Fingerprint): Pair<TokenResponse, String> {
        val refreshToken =
            generateRefreshToken(fingerprint.user, fingerprint.deviceId)
        val accessToken = generateAccessToken(fingerprint.user)
        return Pair(TokenResponse(accessToken), refreshToken)
    }

    fun generateTokenPair(
        user: User,
        deviceId: String,
    ): Pair<TokenResponse, String> {
        val refreshToken = generateRefreshToken(user, deviceId)
        val accessToken = generateAccessToken(user)
        return Pair(TokenResponse(accessToken), refreshToken)
    }

    fun refreshTokenPair(request: RefreshRequest): Pair<TokenResponse, String> {
        val refreshToken =
            refreshTokenDao.findByFingerprintDeviceId(request.fingerprint)
                .orElseThrow { FingerprintNotFoundException() }
        if (hash(request.refreshToken) != refreshToken.hash) throw CorruptedTokenException()
        if (refreshToken.expiresAt < Date.from(Instant.now())) throw CorruptedTokenException()
        return generateTokenPair(refreshToken.fingerprint)
    }

    fun deleteRefreshToken(refreshToken: String) {
        refreshTokenDao.deleteByHash(hash(refreshToken))
    }

    fun clearUserData(user: User) {
        fingerprintDao.deleteByUser(user)
    }

    fun parseTokenPrincipal(token: String): UserPrincipal {
        val claims = parseToken(token)

        val userId = try {
            UUID.fromString(claims.body["userId"].toString())
        } catch (e: Exception) {
            throw CorruptedTokenException()
        }
        return UserPrincipal(userId)
    }
}
