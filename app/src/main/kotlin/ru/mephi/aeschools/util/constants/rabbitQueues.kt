package ru.mephi.aeschools.util.constants

const val MAIL_BROADCAST = "mail.broadcast"
const val MAIL_BROADCAST_IGNORE_PREFERENCES = "mail.broadcast-ignore-preferences"
const val MAIL_RESET_PASSWORD = "mail.reset-password"
const val MAIL_VERIFY_EMAIL = "mail.verify-email"

const val IMAGE_THUMBNAIL_QUEUE = "image.handle.thumbnail"

const val CONTENT_PHOTO_QUEUE = "content-photo"
