package ru.mephi.aeschools.util

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.data.domain.Sort
import org.springframework.http.MediaType
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.created
import org.springframework.web.servlet.function.body
import org.springframework.web.servlet.function.paramOrNull
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.exceptions.common.InvalidMultipartException
import ru.mephi.aeschools.model.exceptions.common.InvalidPathVariable
import ru.mephi.aeschools.model.exceptions.common.MissingParameterException
import ru.mephi.aeschools.model.exceptions.common.MissingPathVariableException
import ru.mephi.aeschools.model.exceptions.common.PrincipalNotFoundException
import ru.mephi.aeschools.util.constants.DEFAULT_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.constants.DEFAULT_PAGE_NUMBER_PARAM
import ru.mephi.aeschools.util.constants.DEFAULT_PAGE_SIZE_PARAM
import java.net.URI
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.Part

fun Any.toOkBodyWithContentTypeJSON(): ServerResponse {
    return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
        .body(this)
}

fun Any.toCreatedBodyWithContentTypeJSON(
    uri: URI,
    postfix: String,
): ServerResponse {
    return created(URI(uri.path + postfix)).contentType(MediaType.APPLICATION_JSON)
        .body(this)
}

fun Any.toCreatedBodyWithContentTypeJSON(uri: URI, id: Any): ServerResponse {
    return created(URI(uri.path + "/$id")).contentType(MediaType.APPLICATION_JSON)
        .body(this)
}

fun ServerRequest.jsonPathVariable(name: String): String {
    var value = try {
        pathVariable(name)
    } catch (e: IllegalArgumentException) {
        throw MissingPathVariableException(name)
    }
    if (!value.startsWith("\"")) value = "\"$value"
    if (!value.endsWith("\"")) value = "$value\""
    return value
}

inline fun <reified T> ServerRequest.paramOrThrow(name: String): T {
    if (param(name).isEmpty) throw MissingParameterException(name)
    val value = paramOrNull(name)
    val mapper = jacksonObjectMapper()
    return mapper.readValue(value, T::class.java)
}

inline fun <reified T> ServerRequest.paramOrElse(
    name: String,
    default: () -> T,
): T {
    val value = paramOrNull(name) ?: return default()
    val mapper = jacksonObjectMapper().also { it.findAndRegisterModules() }
    return if (T::class == String::class) {
        value as T
    }
    else try {
        mapper.readValue(value, T::class.java)
    } catch (e: JsonProcessingException) {
        default()
    }
}

inline fun <reified T : Any> ServerRequest.pathVariableOrThrow(name: String): T {
    val value = jsonPathVariable(name)
    val mapper = jacksonObjectMapper().also { it.findAndRegisterModules() }
    return try {
        mapper.readValue(value, T::class.java)
    } catch (e: JsonProcessingException) {
        throw InvalidPathVariable(name)
    }
}

inline fun <reified T : Any> ServerRequest.bodyOrParam(name: String = "data"): T {
    return try {
        this.paramOrThrow(name)
    } catch (e: Exception) {
        body()
    }
}

fun ServerRequest.getMultiPartPhoto(key: String = "photo"): Part {
    return multipartData()[key]?.firstOrNull()
        ?: throw InvalidMultipartException()
}

fun ServerRequest.getMultiPartPhotoOrNull(key: String = "photo"): Part? {
    return try {
        getMultiPartPhoto(key)
    } catch (e: Exception) {
        null
    }
}

fun ServerRequest.getMultiPartFile(key: String = "file"): Part {
    return multipartData()[key]?.firstOrNull()
        ?: throw InvalidMultipartException()
}

fun ServerRequest.getMultiPartFileOrNull(key: String = "file"): Part? {
    return try {
        getMultiPartFile(key)
    } catch (e: Exception) {
        null
    }
}

fun ServerRequest.getPrincipal(): UUID {
    val token = try {
        principal().get() as UsernamePasswordAuthenticationToken
    } catch (e: NoSuchElementException) {
        throw PrincipalNotFoundException()
    }
    return try {
        UUID.fromString(token.principal.toString())
    } catch (e: Exception) {
        throw PrincipalNotFoundException()
    }
}

fun ServerRequest.getExtendedPageRequest(
    page: Int = paramOrElse("page") { DEFAULT_PAGE_NUMBER_PARAM },
    size: Int = paramOrElse("size") { DEFAULT_PAGE_SIZE_PARAM },
    order: Sort.Direction = paramOrElse("order") { Sort.Direction.DESC },
    field: String = paramOrElse("field") { DEFAULT_PAGE_FIELD_PARAM },
) = ExtendedPageRequest(page, size, order, field)

fun HttpServletRequest.getPrincipal(): UUID {
    if (userPrincipal == null)
        throw PrincipalNotFoundException()
    val token = userPrincipal as UsernamePasswordAuthenticationToken
    return try {
        UUID.fromString(token.principal.toString())
    } catch (e: Exception) {
        throw PrincipalNotFoundException()
    }
}
