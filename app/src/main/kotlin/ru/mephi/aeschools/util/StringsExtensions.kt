package ru.mephi.aeschools.util

fun String.containsAnyOf(vararg strings: String): Boolean {
    strings.forEach {
        if (this.contains(it)) return true
    }
    return false
}

fun String.startsWithAnyOf(vararg strings: String): Boolean {
    strings.forEach {
        if (this.startsWith(it)) return true
    }
    return false
}