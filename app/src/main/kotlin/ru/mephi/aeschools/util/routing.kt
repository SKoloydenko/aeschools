package ru.mephi.aeschools.util

import org.springframework.web.servlet.function.RouterFunctionDsl
import org.springframework.web.servlet.function.router
import ru.mephi.aeschools.controller.routers.handleCommonError
import ru.mephi.aeschools.model.exceptions.AppException

fun modernRouter(routes: (RouterFunctionDsl.() -> Unit)) = router {
    routes()
    onError<AppException> { error, _ -> (error as AppException).asResponse() }
    onError<Throwable> { error, _ -> handleCommonError(error) }
}
