package ru.mephi.aeschools.util

import org.jetbrains.annotations.TestOnly
import org.springframework.stereotype.Component

@Component
class StringGenerator {
    @TestOnly
    fun getRandomString(len: Int): String {
        val array = CharArray(len)
        val smbs = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        for (i in 0 until len) {
            array[i] = smbs.random()
        }
        return array.joinToString("", "", "")
    }
}