package ru.mephi.aeschools.util

fun String.parseContentPhotoPaths(): Set<String> {
    val regex = Regex("img .*?src=\"(.*?)\".*?>")
    return regex.findAll(this).map { it.groupValues[1] }.toSet()
}
