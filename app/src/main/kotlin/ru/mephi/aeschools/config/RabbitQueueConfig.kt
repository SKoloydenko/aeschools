package ru.mephi.aeschools.config

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.util.constants.CONTENT_PHOTO_QUEUE
import ru.mephi.aeschools.util.constants.IMAGE_THUMBNAIL_QUEUE

@Configuration
class RabbitQueueConfig {

    @Bean
    fun broadcastQueue(): Queue {
        return Queue(IMAGE_THUMBNAIL_QUEUE)
    }

    @Bean
    fun contentPhotoQueue(): Queue = Queue(CONTENT_PHOTO_QUEUE)
}
