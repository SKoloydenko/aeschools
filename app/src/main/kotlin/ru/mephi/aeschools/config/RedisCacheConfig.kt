package ru.mephi.aeschools.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import org.springframework.data.redis.cache.RedisCacheConfiguration
import org.springframework.data.redis.cache.RedisCacheManager
import org.springframework.data.redis.cache.RedisCacheWriter
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.RedisSerializationContext.SerializationPair
import ru.mephi.aeschools.config.properties.RedisProperties


@ConditionalOnProperty(
    prefix = "spring",
    name = ["cache.type"],
    havingValue = "redis"
)
@Configuration
@Import(RedisAutoConfiguration::class)
class RedisCacheConfig(
    private val properties: RedisProperties,
) {

    @Bean
    fun customRedisCacheConfig(): RedisCacheConfiguration {
        return RedisCacheConfiguration.defaultCacheConfig()
            .disableCachingNullValues()
            .serializeValuesWith(
                SerializationPair.fromSerializer(
                    GenericJackson2JsonRedisSerializer()
                )
            )
    }

    @Bean
    @Primary
    fun redisCacheManager(
        config: RedisCacheConfiguration,
        connectionFactory: LettuceConnectionFactory,
    ): RedisCacheManager {
        val map = HashMap<String, RedisCacheConfiguration>()
        val cacheWriter =
            RedisCacheWriter.nonLockingRedisCacheWriter(connectionFactory)
        return CustomRedisCacheManager(cacheWriter, config, map)
    }
}

