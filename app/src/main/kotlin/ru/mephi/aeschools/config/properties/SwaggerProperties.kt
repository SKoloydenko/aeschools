package ru.mephi.aeschools.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "swagger")
data class SwaggerProperties(
    val swaggerLogin: String,
    val swaggerPassword: String,
)
