package ru.mephi.aeschools.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "urls")
data class ServerUrls(
    val baseUrl: String = "aeschools.mephi.ru",
) {
    val fullBaseUrl = "https://$baseUrl"
    val homePage = "$fullBaseUrl/home"
}
