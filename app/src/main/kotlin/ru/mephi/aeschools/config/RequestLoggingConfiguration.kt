package ru.mephi.aeschools.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.interceptor.RequestLoggingFilter

@Configuration
class RequestLoggingConfiguration {
    @Bean
    fun requestLoggingFilter(): RequestLoggingFilter {
        val filter = RequestLoggingFilter()
        filter.setIncludeClientInfo(false)
        filter.setIncludeQueryString(true)
        filter.setIncludePayload(true)
        filter.setMaxPayloadLength(8000)
        filter.setIncludeHeaders(false)
        return filter
    }
}