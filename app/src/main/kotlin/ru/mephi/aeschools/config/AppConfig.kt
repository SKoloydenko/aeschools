package ru.mephi.aeschools.config

import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import ru.mephi.aeschools.interceptor.ActivityInterceptor
import ru.mephi.aeschools.interceptor.BanInterceptor
import ru.mephi.aeschools.interceptor.RateLimiterInterceptor
import ru.mephi.aeschools.util.constants.API_VERSION_1


@Configuration
@EnableCaching
class AppConfig(
    private val activityInterceptor: ActivityInterceptor,
    private val rateLimiterInterceptor: RateLimiterInterceptor,
    private val banInterceptor: BanInterceptor,
) : WebMvcConfigurer {
    @Bean
    fun corsFilter(): CorsFilter {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()
        config.allowCredentials = true
        config.addAllowedOrigin("*")
        config.addAllowedHeader("*")
        config.addAllowedMethod("*")
        source.registerCorsConfiguration("/api/v3/api-docs", config)
        return CorsFilter(source)
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.apply {
            addInterceptor(activityInterceptor)
            addInterceptor(rateLimiterInterceptor)
            addInterceptor(banInterceptor).excludePathPatterns(
                "$API_VERSION_1/user/me/**",
                "$API_VERSION_1/public/**",
                "/actuator/**",
                "/swagger/**",
                "/swagger-ui/**",
                "/v3/api-docs/**"
            )
        }
    }
}
