package ru.mephi.aeschools.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "actuator")
class ActuatorProperties(
    val username: String,
    val password: String,
)
