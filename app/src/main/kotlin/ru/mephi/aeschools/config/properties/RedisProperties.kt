package ru.mephi.aeschools.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "redis")
class RedisProperties {
    var cacheTTL: Long = 86400
}