package ru.mephi.aeschools.config

import com.hazelcast.core.HazelcastInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import ru.mephi.aeschools.security.RateLimiter
import ru.mephi.aeschools.security.RateLimiterProdImpl
import ru.mephi.aeschools.security.RateLimiterTestImpl


@Configuration
class RateLimiterConfig {
    @Autowired
    @Qualifier("HzInstance")
    private lateinit var hzInstance: HazelcastInstance

    @Profile("prod")
    @Bean("RateLimiter")
    fun prodRateLimiter(): RateLimiter {
        return RateLimiterProdImpl(hzInstance)
    }

    @Profile("dev | test | local")
    @Bean("RateLimiter")
    fun devRateLimiter(): RateLimiter {
        return RateLimiterTestImpl(hzInstance)
    }
}
