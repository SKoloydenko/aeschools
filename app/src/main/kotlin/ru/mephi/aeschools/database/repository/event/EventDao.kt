package ru.mephi.aeschools.database.repository.event

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.school.School
import java.util.*

@Repository
interface EventDao : AbstractRepository<AbstractEvent>,
    PagingAndSortingRepository<AbstractEvent, UUID> {
    fun findByTitleStartsWith(prefix: String, pageable: Pageable): Page<AbstractEvent>

    fun findByTitleStartsWithAndTagsTitle(
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    @Query(
        """
            SELECT e FROM AbstractEvent e
            WHERE e.title LIKE :query% AND e.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndPublicationDateBeforeNow(
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    @Query(
        """
            SELECT e FROM AbstractEvent e
            JOIN e.tags et
            WHERE e.title LIKE :query% AND et.title = :tag
            AND e.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    fun findBySchoolIdAndTitleStartsWith(
        schoolId: UUID,
        query: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    fun findBySchoolIdAndTitleStartsWithAndTagsTitle(
        schoolId: UUID,
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    @Query(
        """
            SELECT e FROM AbstractEvent e
            JOIN e.school s
            WHERE s.id = :schoolId AND e.title LIKE :query%
            AND e.publicationDate < CURRENT_TIMESTAMP()
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    @Query(
        """
            SELECT e FROM AbstractEvent e
            JOIN e.tags et
            JOIN e.school s
            WHERE s.id = :schoolId AND e.title LIKE :query% AND et.title = :tag
            AND e.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<AbstractEvent>

    fun findBySchoolIdAndId(schoolId: UUID, eventId: UUID): Optional<AbstractEvent>

    @Query(
        """
            SELECT e FROM AbstractEvent e
            WHERE e.id = :eventId AND e.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByIdAndPublicationDateBeforeNow(@Param("eventId") eventId: UUID): Optional<AbstractEvent>

    @Query(
        """
            SELECT e FROM AbstractEvent e
            JOIN e.school s
            WHERE s.id = :schoolId AND e.id = :eventId AND e.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndIdAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("eventId") eventId: UUID,
    ): Optional<AbstractEvent>

    @Modifying
    @Query("DELETE FROM AbstractEvent e WHERE e.id = :eventId")
    fun delete(@Param("eventId") eventId: UUID)

    fun findAllBySchool(school: School): List<AbstractEvent>
}
