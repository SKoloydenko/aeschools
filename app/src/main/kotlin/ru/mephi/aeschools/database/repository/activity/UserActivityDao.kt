package ru.mephi.aeschools.database.repository.activity

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.activity.UserActivity
import java.time.LocalDateTime
import java.util.*


@Repository
interface UserActivityDao : CrudRepository<UserActivity, UUID> {
    @Query("select count (distinct userId) from UserActivity where time < :before and time > :after")
    fun countDistinctUserByTimeAfterAndTimeBefore(
        after: LocalDateTime,
        before: LocalDateTime,
    ): Long
}
