package ru.mephi.aeschools.database.repository.user

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.user.VerifyEmailToken
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@Repository
interface VerifyEmailTokenDao : AbstractRepository<VerifyEmailToken>,
    PagingAndSortingRepository<VerifyEmailToken, UUID> {
    fun findByToken(token: String): Optional<VerifyEmailToken>
    fun existsByToken(token: String): Boolean

    @Modifying
    @Transactional
    @Query(value = "update VerifyEmailToken t set t.createdAt = :newTime where t.id = :id ")
    fun setNewCreatedAtTimeById(newTime: LocalDateTime, id: UUID)
}
