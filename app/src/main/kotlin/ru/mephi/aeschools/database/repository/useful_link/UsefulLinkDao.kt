package ru.mephi.aeschools.database.repository.useful_link

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.useful_link.UsefulLink
import java.util.*

@Repository
interface UsefulLinkDao : AbstractRepository<UsefulLink>,
    PagingAndSortingRepository<UsefulLink, UUID>
