package ru.mephi.aeschools.database.repository.publication

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.publication.Publication
import java.util.*

@Repository
interface PublicationDao : AbstractRepository<Publication>,
    PagingAndSortingRepository<Publication, UUID> {
    fun findByTitleStartsWith(query: String, pageable: Pageable): Page<Publication>

    fun findByTitleStartsWithAndTagsTitle(
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<Publication>

    @Query(
        """
            SELECT p FROM Publication p
            WHERE p.title LIKE :query% AND p.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndPublicationDateBeforeNow(
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<Publication>

    @Query(
        """
            SELECT p FROM Publication p
            JOIN p.tags pt
            WHERE p.title LIKE :query% AND pt.title = :tag
            AND p.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<Publication>

    fun findBySchoolIdAndTitleStartsWith(
        schoolId: UUID,
        query: String,
        pageable: Pageable,
    ): Page<Publication>

    fun findBySchoolIdAndTitleStartsWithAndTagsTitle(
        schoolId: UUID,
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<Publication>

    @Query(
        """
            SELECT p FROM Publication p
            JOIN p.school s
            WHERE s.id = :schoolId AND p.title LIKE :query%
            AND p.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<Publication>

    @Query(
        """
            SELECT p FROM Publication p
            JOIN p.tags pt
            JOIN p.school s
            WHERE s.id = :schoolId AND p.title LIKE :query% AND pt.title = :tag
            AND p.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<Publication>

    fun findBySchoolIdAndId(schoolId: UUID, id: UUID): Optional<Publication>

    @Query(
        """
            SELECT p FROM Publication p
            WHERE p.id = :publicationId AND p.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByIdAndPublicationDateBeforeNow(
        @Param("publicationId") publicationId: UUID,
    ): Optional<Publication>

    @Query(
        """
            SELECT p FROM Publication p
            JOIN p.school s
            WHERE s.id = :schoolId AND p.id = :publicationId AND p.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndIdAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("publicationId") publicationId: UUID,
    ): Optional<Publication>

    @Modifying
    @Query("DELETE FROM Publication p WHERE p.id = :publicationId")
    fun delete(@Param("publicationId") publicationId: UUID)
}
