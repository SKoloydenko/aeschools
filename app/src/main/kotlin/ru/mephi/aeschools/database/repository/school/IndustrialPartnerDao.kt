package ru.mephi.aeschools.database.repository.school

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.school.IndustrialPartner
import ru.mephi.aeschools.database.entity.school.School
import java.util.*

@Repository
interface IndustrialPartnerDao : AbstractRepository<IndustrialPartner>,
    PagingAndSortingRepository<IndustrialPartner, UUID> {
    fun findByNameStartingWithAndSchool(query: String, school: School, page: Pageable): Page<IndustrialPartner>
    fun findBySchoolAndId(school: School, id: UUID): IndustrialPartner

    fun deleteBySchoolAndId(school: School, id: UUID)
}
