package ru.mephi.aeschools.database.repository.user


import org.springframework.data.jpa.repository.Modifying
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.user.RefreshToken
import ru.mephi.aeschools.database.entity.user.User
import java.util.*

interface RefreshTokenDao : AbstractRepository<RefreshToken> {
    fun findByFingerprintDeviceId(deviceId: String): Optional<RefreshToken>

    @Modifying
    fun deleteByFingerprintDeviceIdAndFingerprintUser(
        deviceId: String,
        user: User,
    )

    @Modifying
    fun deleteByHash(hash: String)
}
