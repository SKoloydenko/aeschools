package ru.mephi.aeschools.database.entity.school

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Permissions(
    @Column(nullable = false)
    var moderatorPermission: Boolean = true,

    @Column(nullable = false)
    var internshipPermission: Boolean = true,

    @Column(nullable = false)
    var eventPermission: Boolean = true,

    @Column(nullable = false)
    var publicationPermission: Boolean = true,

    @Column(nullable = false)
    var bestPracticePermission: Boolean = true,

    @Column(nullable = false)
    var coursePermission: Boolean = true,

    @Column(nullable = false)
    var hiddenFieldsPermission: Boolean = true,
)
