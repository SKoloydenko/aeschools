package ru.mephi.aeschools.database.repository.preferences

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.preferences.UserPreferences
import java.util.*

@Repository
interface UserPreferencesDao : AbstractRepository<UserPreferences> {

    @Query(
        """
            SELECT u.id FROM UserPreferences up
            JOIN up.user u
            WHERE up.enableEmail = TRUE OR :ignore = TRUE
        """
    )
    fun findAllUsersIdsWithEnableEmail(@Param("ignore") ignorePreferences: Boolean): List<UUID>

    @Query(
        """
            SELECT u.id FROM UserPreferences up
            JOIN up.user u
            WHERE up.enableEmail = TRUE OR :ignore = TRUE AND u.school IS NULL
        """
    )
    fun findUsersIdsWithEnableEmail(@Param("ignore") ignorePreferences: Boolean): List<UUID>

    @Query(
        """
            SELECT u.id FROM UserPreferences up
            JOIN up.user u
            WHERE up.enableEmail = TRUE OR :ignore = TRUE AND u.school IS NOT NULL
        """
    )
    fun findModeratorsIdsWithEnableEmail(@Param("ignore") ignorePreferences: Boolean): List<UUID>
}
