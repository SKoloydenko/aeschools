package ru.mephi.aeschools.database.entity.action

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.hibernate.annotations.Type
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.user.User
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "UserAction")
class UserAction(
    @Enumerated(EnumType.STRING)
    @Column(name = "actionTarget", nullable = false)
    var actionTarget: ActionTarget,

    @Enumerated(EnumType.STRING)
    @Column(name = "actionVerb", nullable = true)
    var actionVerb: ActionVerb? = null,

    @Column(name = "targetId", nullable = true)
    @Type(type = "uuid-char")
    var targetId: UUID? = null,

    ) : AbstractCreatedAtEntity() {

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "userId", nullable = true)
    var user: User? = null
}
