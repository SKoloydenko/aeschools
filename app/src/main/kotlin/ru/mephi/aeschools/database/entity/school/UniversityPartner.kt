package ru.mephi.aeschools.database.entity.school

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.photoPathLength
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "UniversityPartner")
class UniversityPartner(
    @Column(nullable = false, length = smallLength)
    var name: String,

    @Column(nullable = false, length = extraLargeLength)
    var description: String,

    @Column(nullable = false, length = extraLargeLength)
    var schoolRole: String,

    @Column(nullable = false, length = extraLargeLength)
    var universityRole: String,

    @Column(nullable = false, length = extraLargeLength)
    var cooperationDirection: String,

    @Column(length = photoPathLength)
    var photoPath: String? = null,

    ) : AbstractCreatedAtEntity() {

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schoolId")
    lateinit var school: School
}
