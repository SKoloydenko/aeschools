package ru.mephi.aeschools.database.repository.school

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.school.Leader
import ru.mephi.aeschools.database.entity.school.School
import java.util.*

@Repository
interface LeaderDao : AbstractRepository<Leader>,
    PagingAndSortingRepository<Leader, UUID> {
    fun findBySchool(school: School, page: Pageable): Page<Leader>
    fun findBySchoolAndId(school: School, id: UUID): Leader

    fun deleteBySchoolAndId(school: School, id: UUID)
}
