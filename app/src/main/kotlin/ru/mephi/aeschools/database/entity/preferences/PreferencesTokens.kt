package ru.mephi.aeschools.database.entity.preferences

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractEntity
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.util.constants.extraSmallLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.MapsId
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "PreferencesTokens")
class PreferencesTokens(
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    @MapsId
    var user: User,

    @Column(length = extraSmallLength, nullable = false, unique = true)
    var emailToken: String,
) : AbstractEntity()
