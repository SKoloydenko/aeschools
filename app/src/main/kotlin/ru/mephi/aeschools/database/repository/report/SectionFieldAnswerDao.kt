package ru.mephi.aeschools.database.repository.report

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.report.Section
import ru.mephi.aeschools.database.entity.report.SectionField
import ru.mephi.aeschools.database.entity.report.SectionFieldAnswer
import ru.mephi.aeschools.database.entity.report.SectionFieldType
import ru.mephi.aeschools.database.entity.school.School
import java.util.*

@Repository
interface SectionFieldAnswerDao : CrudRepository<SectionFieldAnswer, UUID> {
    fun findBySectionFieldAndSchool(sectionField: SectionField, school: School): SectionFieldAnswer?
    fun findAllBySectionFieldIdInAndSchool(
        sectionFieldId: MutableCollection<UUID>,
        school: School,
    ): List<SectionFieldAnswer>

    fun findAllBySectionField(sectionField: SectionField, pageable: Pageable): Page<SectionFieldAnswer>

    fun countBySchoolIdAndSectionFieldSectionId(
        @Param("schoolId") schoolId: UUID,
        @Param("sectionFieldSectionId") sectionFieldSectionId: UUID,
    ): Int

    fun findAllBySectionField_Section(sectionField_section: Section): Iterable<SectionFieldAnswer>

    fun deleteBySectionFieldAndSchool(sectionField: SectionField, school: School)

    fun existsByAnswerAndSchoolAndType(answer: String, school: School, type: SectionFieldType): Boolean

    @Query(value = """
        SELECT count(sc) FROM School sc
        WHERE :totalFields = (
            SELECT count(sa) FROM SectionFieldAnswer sa
            WHERE :sectionId = sa.sectionField.section.id AND sa.school.id = sc
        )
    """)
    fun countBySchoolWithFullFillSection(@Param("sectionId") sectionId: UUID, @Param("totalFields") totalFields: Long): Long

    fun countBySectionField(sectionField: SectionField): Long
}
