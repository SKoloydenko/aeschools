package ru.mephi.aeschools.database.entity.report

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.hibernate.annotations.UpdateTimestamp
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.publicationLength
import java.time.LocalDateTime
import javax.persistence.*


@Entity
@Table(name = "SectionFieldAnswer")
class SectionFieldAnswer(

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schoolId", nullable = false)
    val school: School,

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sectionFieldId", nullable = false)
    val sectionField: SectionField,

    @Column(name = "answer", nullable = false, length = publicationLength)
    @Lob
    var answer: String,

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, columnDefinition = "enum('FREE_FORM', 'FILE')")
    val type: SectionFieldType

) : AbstractCreatedAtEntity() {

    @UpdateTimestamp
    @Column(name = "updatedAt", nullable = false)
    var updatedAt: LocalDateTime = LocalDateTime.now()

}
