package ru.mephi.aeschools.database.entity.school

import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Contacts(
    @Column(length = smallLength)
    var website: String? = null,

    @Column(length = smallLength)
    var email: String? = null,

    @Column(length = extraSmallLength)
    var phone: String? = null,
)
