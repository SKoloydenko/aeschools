package ru.mephi.aeschools.database.entity.event

import ru.mephi.aeschools.util.constants.largeLength
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "Event")
class Event(
    title: String,
    content: String,
    publicationDate: LocalDateTime,

    @Column(length = largeLength)
    var location: String? = null,

    @Column
    var eventDate: LocalDateTime,

) : AbstractEvent(title, content, publicationDate)
