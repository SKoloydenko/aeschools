package ru.mephi.aeschools.database.repository.best_practice

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.best_practice.BestPractice
import java.util.*

@Repository
interface BestPracticeDao : AbstractRepository<BestPractice>,
    PagingAndSortingRepository<BestPractice, UUID> {

    fun findByTitleStartsWith(query: String, pageable: Pageable): Page<BestPractice>

    fun findByTitleStartsWithAndTagsTitle(
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<BestPractice>

    @Query(
        """
            SELECT b FROM BestPractice b
            WHERE b.title LIKE :query% AND b.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndPublicationDateBeforeNow(
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<BestPractice>

    @Query(
        """
            SELECT b FROM BestPractice b
            JOIN b.tags bt
            WHERE b.title LIKE :query% AND bt.title = :tag
            AND b.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<BestPractice>

    fun findBySchoolIdAndTitleStartsWith(
        schoolId: UUID,
        query: String,
        pageable: Pageable,
    ): Page<BestPractice>

    fun findBySchoolIdAndTitleStartsWithAndTagsTitle(
        schoolId: UUID,
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<BestPractice>

    @Query(
        """
            SELECT b FROM BestPractice b
            JOIN b.school s
            WHERE s.id = :schoolId AND b.title LIKE :query%
            AND b.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<BestPractice>

    @Query(
        """
            SELECT b FROM BestPractice b
            JOIN b.school s
            JOIN b.tags bt
            WHERE s.id = :schoolId AND b.title LIKE :query% AND bt.title = :tag
            AND b.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<BestPractice>

    fun findBySchoolIdAndId(schoolId: UUID, id: UUID): Optional<BestPractice>

    @Query(
        """
            SELECT b FROM BestPractice b
            WHERE b.id = :bestPracticeId AND b.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByIdAndPublicationDateBeforeNow(
        @Param("bestPracticeId") bestPracticeId: UUID,
    ): Optional<BestPractice>

    @Query(
        """
            SELECT b FROM BestPractice b
            JOIN b.school s
            WHERE s.id = :schoolId AND b.id = :bestPracticeId AND b.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndIdAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("bestPracticeId") bestPracticeId: UUID,
    ): Optional<BestPractice>

    @Modifying
    @Query("DELETE FROM BestPractice b WHERE b.id = :bestPracticeId")
    fun delete(@Param("bestPracticeId") bestPracticeId: UUID)
}
