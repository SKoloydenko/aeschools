package ru.mephi.aeschools.database.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean
import ru.mephi.aeschools.database.AbstractEntity
import java.util.*

@NoRepositoryBean
interface AbstractRepository<E : AbstractEntity> : CrudRepository<E, UUID>
