package ru.mephi.aeschools.database.repository

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.ContentPhoto
import java.util.*

@Repository
interface ContentPhotoDao : AbstractRepository<ContentPhoto> {
    fun findByPhotoPath(photoPath: String): Optional<ContentPhoto>

    @Modifying
    @Query(
        """
            DELETE FROM ContentPhoto c
            WHERE c.content.id = :contentId AND c.photoPath NOT IN :photoPaths
        """
    )
    fun deleteByContentIdAndPhotoPathNotIn(
        @Param("contentId") contentId: UUID,
        @Param("photoPaths") photoPaths: List<String>,
    )

    fun deleteByContentId(contentId: UUID)
}
