package ru.mephi.aeschools.database.entity.event

import ru.mephi.aeschools.database.entity.AbstractContent
import ru.mephi.aeschools.database.entity.user.User
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany

@Entity
abstract class AbstractEvent(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
) : AbstractContent(title, content, publicationDate) {
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "eventId")],
        inverseJoinColumns = [JoinColumn(name = "userId")],
        name = "UsersEvents",
    )
    val applicants: MutableList<User> = mutableListOf()
}
