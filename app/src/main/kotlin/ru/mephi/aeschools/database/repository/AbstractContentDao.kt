package ru.mephi.aeschools.database.repository

import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.AbstractContent

@Repository
interface AbstractContentDao : AbstractRepository<AbstractContent>
