package ru.mephi.aeschools.database.entity.event

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "ForeignEvent")
class ForeignEvent(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
) : AbstractEvent(title, content, publicationDate)
