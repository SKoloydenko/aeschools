package ru.mephi.aeschools.database.entity.user

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table
import javax.persistence.Transient

@Entity
@Table(name = "RefreshToken")
class RefreshToken : AbstractEntity() {
    @Transient
    val token: String = UUID.randomUUID().toString()

    @Column(nullable = false)
    lateinit var expiresAt: Date

    @Column(nullable = false, length = 60)
    lateinit var hash: String

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "fingerprintId")
    lateinit var fingerprint: Fingerprint
}
