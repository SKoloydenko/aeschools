package ru.mephi.aeschools.database.entity.best_practice

import ru.mephi.aeschools.database.entity.AbstractContent
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "BestPractice")
class BestPractice(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
) : AbstractContent(title, content, publicationDate)
