package ru.mephi.aeschools.database.entity.useful_link

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.photoPathLength
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "UsefulLink")
class UsefulLink(

    @Column(length = smallLength)
    var title: String,

    @Column(length = largeLength)
    var link: String,

    @Column(length = photoPathLength)
    var photoPath: String? = null,

    ) : AbstractCreatedAtEntity() {

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "authorId", nullable = true)
    var author: User? = null
}
