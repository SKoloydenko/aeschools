package ru.mephi.aeschools.database.repository.broadcast

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.broadcast.Broadcast
import java.util.UUID

@Repository
interface BroadcastDao : AbstractRepository<Broadcast>, PagingAndSortingRepository<Broadcast, UUID>
