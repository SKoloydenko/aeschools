package ru.mephi.aeschools.database.repository.report

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.report.Section
import ru.mephi.aeschools.database.entity.report.SectionField
import java.util.*

@Repository
interface SectionFieldDao : PagingAndSortingRepository<SectionField, UUID> {
    fun findByIdAndSectionId(id: UUID, sectionId: UUID): Optional<SectionField>
    fun countBySectionId(sectionId: UUID): Long
    fun findAllBySection(section: Section, page: Pageable): Page<SectionField>
}
