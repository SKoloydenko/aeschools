package ru.mephi.aeschools.database.entity

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "Tag")
class Tag(
    @Column(nullable = false, length = smallLength)
    val title: String,
) : AbstractCreatedAtEntity() {
    @ManyToOne(fetch = FetchType.LAZY)
    lateinit var entity: AbstractContent
}
