package ru.mephi.aeschools.database.entity

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.photoPathLength
import ru.mephi.aeschools.util.constants.publicationLength
import java.time.LocalDateTime
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Inheritance
import javax.persistence.InheritanceType
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class AbstractContent(

    @Column(nullable = false, length = largeLength)
    var title: String,

    @Lob
    @Column(nullable = false, length = publicationLength)
    var content: String,

    @Column(nullable = false)
    var publicationDate: LocalDateTime,

    @Column(length = photoPathLength)
    var photoPath: String? = null,

) : AbstractCreatedAtEntity() {

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "authorId", nullable = true)
    var author: User? = null

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "schoolId", nullable = true)
    var school: School? = null

    @OneToMany(
        mappedBy = "content",
        cascade = [CascadeType.ALL],
        fetch = FetchType.LAZY,
        orphanRemoval = true
    )
    val photos: MutableSet<ContentPhoto> = mutableSetOf()

    @OneToMany(
        mappedBy = "entity",
        cascade = [CascadeType.ALL],
        fetch = FetchType.LAZY,
        orphanRemoval = true
    )
    val tags: MutableSet<Tag> = mutableSetOf()

    fun setTags(tags: Collection<Tag>) {
        this.tags.clear()
        this.tags.addAll(tags)
    }
}
