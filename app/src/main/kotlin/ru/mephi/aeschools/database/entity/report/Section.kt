package ru.mephi.aeschools.database.entity.report

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table


@Entity
@Table(name = "Section")
class Section(
    @Column(name = "title", nullable = false, length = largeLength)
    var title: String,

    @Column(name = "isBlocked", nullable = false)
    var isBlocked: Boolean = false

) : AbstractCreatedAtEntity() {

    @OneToMany(mappedBy = "section", cascade = [CascadeType.ALL], orphanRemoval = true)
    val fields: MutableList<SectionField> = mutableListOf()

    fun getIsBlocked(): Boolean = isBlocked
    fun setIsBlocked(isBlocked: Boolean) { this.isBlocked = isBlocked }
}
