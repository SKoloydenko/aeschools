package ru.mephi.aeschools.database.repository.preferences

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.preferences.PreferencesTokens
import ru.mephi.aeschools.database.entity.preferences.UserPreferences
import java.util.*

@Repository
interface PreferencesTokensDao : CrudRepository<PreferencesTokens, UUID> {
    fun existsByEmailToken(emailToken: String): Boolean

    @Query(
        """
        SELECT up FROM PreferencesTokens pt
        INNER JOIN UserPreferences up ON up.id = pt.id
        WHERE pt.emailToken = :token
    """
    )
    fun findUserPreferencesByEmailToken(@Param("token") emailToken: String): Optional<UserPreferences>
}
