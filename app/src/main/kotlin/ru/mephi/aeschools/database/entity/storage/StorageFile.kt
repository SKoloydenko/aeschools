package ru.mephi.aeschools.database.entity.storage

import ru.mephi.aeschools.database.AbstractEntity
import ru.mephi.aeschools.util.constants.mediumLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "StorageFile")
class StorageFile(
    @Column(name = "name", nullable = false, length = mediumLength)
    var name: String,
) : AbstractEntity()
