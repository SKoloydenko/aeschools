package ru.mephi.aeschools.database.repository.library

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.library.LibraryRecord
import java.util.*

@Repository
interface LibraryDao : PagingAndSortingRepository<LibraryRecord, UUID>,
    AbstractRepository<LibraryRecord> {
    fun findAllByTitleStartsWith(title: String, pageable: Pageable): Page<LibraryRecord>
}
