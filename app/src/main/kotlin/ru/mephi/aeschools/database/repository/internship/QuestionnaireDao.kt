package ru.mephi.aeschools.database.repository.internship

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.database.entity.internship.Questionnaire
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.user.User
import java.util.*

@Repository
interface QuestionnaireDao : AbstractRepository<Questionnaire> {
    fun existsByInternshipAndUser(
        internshipId: Internship,
        user: User,
    ): Boolean

    fun findByInternshipAndUser(
        internship: Internship,
        user: User,
    ): Optional<Questionnaire>

    fun findByInternshipSchoolAndInternshipAndUser(
        school: School,
        internship: Internship,
        user: User,
    ): Optional<Questionnaire>

    fun findByInternship(internship: Internship, pageable: Pageable): Page<Questionnaire>

    @Query(
        """
            SELECT q FROM Questionnaire q
            JOIN q.user u
            WHERE u.id = :userId AND q.createdAt = (SELECT MAX(q.createdAt) FROM Questionnaire q)
        """
    )
    fun findLastQuestionnaire(userId: UUID): Optional<Questionnaire>

    fun findByUser(user: User): List<Questionnaire>
}
