package ru.mephi.aeschools.database.repository.action

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.action.UserAction
import java.util.*

@Repository
interface UserActionDao : CrudRepository<UserAction, UUID> {

    fun findAllByUserId(userId: UUID, pageable: Pageable): Page<UserAction>
    fun findAllByTargetId(userId: UUID, pageable: Pageable): Page<UserAction>
}