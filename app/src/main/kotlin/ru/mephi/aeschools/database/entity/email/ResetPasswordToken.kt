package ru.mephi.aeschools.database.entity.email

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.user.User
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.MapsId
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "ResetPasswordToken")
class ResetPasswordToken(
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId
    var user: User? = null,

    @Column(name = "token", nullable = false, length = 20)
    var token: String? = null,
) : AbstractCreatedAtEntity()
