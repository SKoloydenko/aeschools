package ru.mephi.aeschools.database.entity.course

import ru.mephi.aeschools.database.entity.AbstractContent
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity

@Entity
class Course(
    title: String,
    content: String,
    publicationDate: LocalDateTime,

    @Column
    var enrollmentFinishingDate: LocalDateTime,

) : AbstractContent(title, content, publicationDate)
