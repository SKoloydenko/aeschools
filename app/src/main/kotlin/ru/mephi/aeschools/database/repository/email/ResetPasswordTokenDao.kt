package ru.mephi.aeschools.database.repository.email

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.email.ResetPasswordToken
import java.util.*

@Repository
interface ResetPasswordTokenDao :
    PagingAndSortingRepository<ResetPasswordToken, UUID>,
    AbstractRepository<ResetPasswordToken> {

    fun findByUserId(userId: UUID): Optional<ResetPasswordToken>
    fun findByToken(token: String): Optional<ResetPasswordToken>
}
