package ru.mephi.aeschools.database.entity.school


import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.best_practice.BestPractice
import ru.mephi.aeschools.database.entity.course.Course
import ru.mephi.aeschools.database.entity.event.Event
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.database.entity.publication.Publication
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.photoPathLength
import ru.mephi.aeschools.util.constants.publicationLength
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "School")
class School(

    @Column(nullable = false, length = largeLength)
    var university: String,

    @Column(nullable = false, length = largeLength, unique = true)
    var name: String,

    @Column(nullable = false, length = extraSmallLength, unique = true)
    var shortName: String,

    @Column(nullable = false, length = largeLength)
    var actionScope: String,

    @Lob
    @Column(nullable = false, length = publicationLength)
    var about: String,

    @Lob
    @Column(nullable = false, length = publicationLength)
    var educationalActivities: String,

    @Lob
    @Column(nullable = false, length = publicationLength)
    var researchActivities: String,

    @Lob
    @Column(nullable = false, length = publicationLength)
    var innovationalActivities: String,

    @Column(length = photoPathLength)
    var photoPath: String? = null,

    @Column(nullable = false)
    var hidden: Boolean = false,

    ) : AbstractCreatedAtEntity() {

    @OneToMany(mappedBy = "school", fetch = FetchType.LAZY)
    val leaders: MutableList<Leader> = mutableListOf()

    @Embedded
    var contacts: Contacts? = null

    @OneToMany(mappedBy = "school", fetch = FetchType.LAZY)
    val industrialPartners: MutableList<IndustrialPartner> = mutableListOf()

    @OneToMany(mappedBy = "school", fetch = FetchType.LAZY)
    val universities: MutableList<UniversityPartner> = mutableListOf()

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "schoolId")],
        inverseJoinColumns = [JoinColumn(name = "userId")],
        name = "ModeratorsSchools",
    )
    var moderators: MutableList<User> = mutableListOf()

    @OneToMany(mappedBy = "school", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val internships: MutableList<Internship> = mutableListOf()

    @OneToMany(mappedBy = "school", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val publications: MutableList<Publication> = mutableListOf()

    @OneToMany(mappedBy = "school", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val events: MutableList<Event> = mutableListOf()

    @OneToMany(mappedBy = "school", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val courses: MutableList<Course> = mutableListOf()

    @OneToMany(mappedBy = "school", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val bestPractices: MutableList<BestPractice> = mutableListOf()
}
