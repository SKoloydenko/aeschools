package ru.mephi.aeschools.database.repository.user


import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.service.impl.mail.UserIdUserEmailPair
import java.time.LocalDateTime
import java.util.*

@Repository
interface UserDao : AbstractRepository<User>,
    PagingAndSortingRepository<User, UUID> {
    fun countByCreatedAtAfterAndCreatedAtBefore(
        after: LocalDateTime,
        before: LocalDateTime,
    ): Long

    fun existsByEmail(email: String): Boolean
    fun findByEmail(email: String): Optional<User>

    @Query(
        """
            SELECT u FROM User u
            WHERE u.surname LIKE :prefix%
            AND (:moderator IS NULL
            OR (:moderator IS TRUE AND u.school IS NOT NULL)
            OR (:moderator IS FALSE AND u.school IS NULL))
        """
    )
    fun findBySurnameStartsWithAndModeratorAndOrderStudent(
        prefix: String,
        moderator: Boolean?,
        pageable: Pageable,
    ): Page<User>

    @Query("SELECT u.id AS userId, u.email as email FROM User u WHERE u.id IN :ids")
    fun findUserIdUserEmailPairs(ids: List<UUID>): List<UserIdUserEmailPair>
}
