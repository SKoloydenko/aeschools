package ru.mephi.aeschools.database.entity.user

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import org.springframework.data.jpa.repository.Modifying
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import javax.persistence.Table

@Entity
@Table(name = "Fingerprint")
class Fingerprint(

    @Column(nullable = false)
    val deviceId: String,

    ) : AbstractCreatedAtEntity() {

    @Column(nullable = false)
    lateinit var lastUsed: LocalDateTime

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId")
    lateinit var user: User

    @PrePersist
    fun setLastUsed() {
        lastUsed = LocalDateTime.now()
    }

    @Modifying
    @PreUpdate
    fun updateLastUsed() {
        lastUsed = LocalDateTime.now()
    }
}
