package ru.mephi.aeschools.database.repository

import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.registration_limiter.RegistrationLimiterSettings

@Service
class RegistrationLimiterDao(
    redis: StringRedisTemplate,
) : RedisRepository<RegistrationLimiterSettings>(redis)
