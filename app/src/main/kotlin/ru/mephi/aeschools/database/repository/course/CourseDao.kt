package ru.mephi.aeschools.database.repository.course

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.course.Course
import java.util.*

@Repository
interface CourseDao : AbstractRepository<Course>, PagingAndSortingRepository<Course, UUID> {
    fun findBySchoolIdAndId(schoolId: UUID, id: UUID): Optional<Course>

    fun findByTitleStartsWith(query: String, pageable: Pageable): Page<Course>

    fun findByTitleStartsWithAndTagsTitle(
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<Course>

    @Query(
        """
            SELECT c FROM Course c
            WHERE c.title LIKE :query% AND c.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndPublicationDateBeforeNow(
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<Course>

    @Query(
        """
            SELECT c FROM Course c
            JOIN c.tags ct
            WHERE c.title LIKE :query% AND ct.title = :tag
            AND c.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<Course>

    fun findBySchoolIdAndTitleStartsWith(
        schoolId: UUID,
        query: String,
        pageable: Pageable,
    ): Page<Course>

    fun findBySchoolIdAndTitleStartsWithAndTagsTitle(
        schoolId: UUID,
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<Course>

    @Query(
        """
            SELECT c FROM Course c
            JOIN c.school s
            WHERE c.title LIKE :query% AND s.id = :schoolId AND c.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<Course>

    @Query(
        """
            SELECT c FROM Course c
            JOIN c.tags ct
            JOIN c.school s
            WHERE s.id = :schoolId AND c.title LIKE :query% AND ct.title = :tag
            AND c.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<Course>

    @Query(
        """
            SELECT c FROM Course c
            WHERE c.id = :courseId AND c.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByIdAndPublicationDateBeforeNow(@Param("courseId") courseId: UUID): Optional<Course>

    @Query(
        """
            SELECT c FROM Course c
            JOIN c.school s
            WHERE c.id = :courseId AND s.id = :schoolId AND c.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndIdAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("courseId") courseId: UUID,
    ): Optional<Course>
}
