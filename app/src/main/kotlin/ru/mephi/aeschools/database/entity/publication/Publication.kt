package ru.mephi.aeschools.database.entity.publication

import ru.mephi.aeschools.database.entity.AbstractContent
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "Publication")
class Publication(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
) : AbstractContent(title, content, publicationDate)
