package ru.mephi.aeschools.database.repository.school

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.school.ModeratorSchool
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.user.User
import java.util.*

interface ModeratorSchoolDao : AbstractRepository<ModeratorSchool> {
    fun existsByUserId(userId: UUID): Boolean
    fun findBySchoolAndUser(
        school: School,
        user: User,
    ): Optional<ModeratorSchool>

    fun findBySchoolAndUserSurnameStartsWithOrderByUserNameAsc(
        school: School,
        prefix: String,
        pageable: Pageable,
    ): Page<ModeratorSchool>

    fun findByUserId(userId: UUID): Optional<ModeratorSchool>
}
