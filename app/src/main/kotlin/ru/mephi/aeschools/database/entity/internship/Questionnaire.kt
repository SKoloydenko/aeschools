package ru.mephi.aeschools.database.entity.internship

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.photoPathLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "Questionnaire")
class Questionnaire(
    @Column(nullable = false, length = extraLargeLength)
    var motivationalLetter: String,

    @Column(nullable = false, length = extraLargeLength)
    var achievements: String,

    @Column(length = photoPathLength)
    var filePath: String? = null,
) : AbstractCreatedAtEntity() {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    lateinit var user: User

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "internshipId")
    lateinit var internship: Internship
}

