package ru.mephi.aeschools.database.repository.event

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.event.UserEvent
import ru.mephi.aeschools.database.entity.user.User
import java.util.*

@Repository
interface UserEventDao : AbstractRepository<UserEvent>,
    PagingAndSortingRepository<UserEvent, UUID> {
    fun existsByEventAndUser(event: AbstractEvent, user: User): Boolean

    fun findByUserAndEventTitleStartsWithOrderByEventPublicationDateDesc(
        user: User,
        query: String,
        pageable: Pageable,
    ): Page<UserEvent>

    fun findByUserAndEventTitleStartsWithAndEventTagsTitleOrderByEventPublicationDateDesc(
        user: User,
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<UserEvent>

    fun findByEvent(event: AbstractEvent, pageable: Pageable): Page<UserEvent>

    fun findByEventAndUser(event: AbstractEvent, user: User): Optional<UserEvent>
}
