package ru.mephi.aeschools.database.repository.report

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.entity.report.Section
import java.util.*

@Repository
interface SectionDao : PagingAndSortingRepository<Section, UUID> {
    fun findAllByTitleStartsWith(title: String, pageable: Pageable): Page<Section>
    fun findAllByIsBlockedAndTitleStartingWith(isBlocked: Boolean, title: String?, pageable: Pageable): Page<Section>
}
