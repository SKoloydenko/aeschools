package ru.mephi.aeschools.database.entity.user

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.database.entity.best_practice.BestPractice
import ru.mephi.aeschools.database.entity.course.Course
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.database.entity.publication.Publication
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.useful_link.UsefulLink
import ru.mephi.aeschools.model.enums.user.EducationDegree
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.mediumLength
import ru.mephi.aeschools.util.constants.passwordHashMaxLength
import ru.mephi.aeschools.util.constants.phoneLength
import ru.mephi.aeschools.util.constants.smallLength
import ru.mephi.aeschools.util.constants.specialtyLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.PreRemove
import javax.persistence.Table
import javax.persistence.Transient

@Entity
@Table(name = "User")
final class User(
    @Column(nullable = false, length = mediumLength, unique = true)
    var email: String,

    @Column(nullable = false, length = smallLength)
    var name: String,

    @Column(nullable = false, length = smallLength)
    var surname: String,

    @Column(nullable = true, length = smallLength)
    var patronymic: String? = null,

    @Column(nullable = false, length = phoneLength)
    var phone: String,

    @Column(length = extraLargeLength)
    var organization: String? = null,

    @Column(length = extraLargeLength)
    var jobRole: String? = null,

    @Column(length = extraLargeLength)
    var university: String? = null,

    @Column
    var educationDegree: EducationDegree? = null,

    @Column
    var grade: Int? = null,

    @Column(length = specialtyLength)
    var specialty: String? = null,

    @Column(nullable = false)
    var student: Boolean = false,

    @Column(nullable = false)
    var admin: Boolean = false,

    @Column(nullable = false)
    var banned: Boolean = false,

    ) : AbstractCreatedAtEntity() {
    @Column(nullable = false, length = passwordHashMaxLength)
    lateinit var hash: String

    @get:Transient
    val fullName: String
        get() = listOfNotNull(surname, name, patronymic).joinToString(" ")

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    val publications: MutableList<Publication> = mutableListOf()

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    val events: MutableList<AbstractEvent> = mutableListOf()

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    val usefulLinks: MutableList<UsefulLink> = mutableListOf()

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    val courses: MutableList<Course> = mutableListOf()

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    val bestPractices: MutableList<BestPractice> = mutableListOf()

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    val internships: MutableList<Internship> = mutableListOf()

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "userId")],
        inverseJoinColumns = [JoinColumn(name = "schoolId")],
        name = "ModeratorsSchools",
    )
    var school: School? = null

    @get:Transient
    var moderator: Boolean = false
        get() = school != null

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "userId")],
        inverseJoinColumns = [JoinColumn(name = "internshipId")],
        name = "Questionnaire",
    )
    val appliedInternships: MutableList<Internship> = mutableListOf()

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "userId")],
        inverseJoinColumns = [JoinColumn(name = "eventId")],
        name = "UsersEvents",
    )
    val joinedEvents: MutableList<AbstractEvent> = mutableListOf()

    @PreRemove
    fun destroyRelationships() {
        publications.forEach { it.author = null }
        events.forEach { it.author = null }
        usefulLinks.forEach { it.author = null }
        courses.forEach { it.author = null }
        bestPractices.forEach { it.author = null }
        internships.forEach { it.author = null }
    }
}
