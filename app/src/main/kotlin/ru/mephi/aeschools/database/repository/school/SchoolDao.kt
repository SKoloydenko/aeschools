package ru.mephi.aeschools.database.repository.school

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.school.School
import java.util.*

interface SchoolDao : AbstractRepository<School>,
    PagingAndSortingRepository<School, UUID> {
    fun existsByName(name: String): Boolean
    fun existsByShortName(shortName: String): Boolean
    fun existsByNameAndIdNot(name: String, id: UUID): Boolean
    fun existsByShortNameAndIdNot(shortName: String, id: UUID): Boolean
    fun findByShortName(shortName: String): Optional<School>
    fun findByNameStartingWithAndHiddenFalse(prefix: String): List<School>
    fun findByNameStartingWith(prefix: String, pageable: Pageable): Page<School>
}
