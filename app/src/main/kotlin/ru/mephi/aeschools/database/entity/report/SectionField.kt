package ru.mephi.aeschools.database.entity.report

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table


@Entity
@Table(name = "SectionField")
class SectionField(
    @Column(name = "title", nullable = false, length = largeLength)
    var title: String,

    @Column(name = "comment", nullable = false, length = largeLength)
    var comment: String,

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, columnDefinition = "enum('FREE_FORM', 'FILE')")
    val type: SectionFieldType,

    @Column(name = "isBlocked", nullable = false)
    var isBlocked: Boolean = false

) : AbstractCreatedAtEntity() {

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sectionId", nullable = false)
    lateinit var section: Section


    fun getIsBlocked(): Boolean = isBlocked
    fun setIsBlocked(isBlocked: Boolean) { this.isBlocked = isBlocked }
}
