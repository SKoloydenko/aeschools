package ru.mephi.aeschools.database.entity.internship

import ru.mephi.aeschools.database.entity.AbstractContent
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.util.constants.largeLength
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.Table

@Entity
@Table(name = "Internship")
class Internship(
    title: String,
    content: String,
    publicationDate: LocalDateTime,

    @Column(length = largeLength)
    var company: String,

) : AbstractContent(title, content, publicationDate) {

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        joinColumns = [JoinColumn(name = "internshipId")],
        inverseJoinColumns = [JoinColumn(name = "userId")],
        name = "Questionnaire",
    )
    val applicants: MutableList<User> = mutableListOf()
}
