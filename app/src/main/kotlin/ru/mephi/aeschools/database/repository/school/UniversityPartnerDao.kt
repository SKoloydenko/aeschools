package ru.mephi.aeschools.database.repository.school

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.school.UniversityPartner
import java.util.*

@Repository
interface UniversityPartnerDao : AbstractRepository<UniversityPartner>,
    PagingAndSortingRepository<UniversityPartner, UUID> {
    fun findByNameStartingWithAndSchool(query: String, school: School, page: Pageable): Page<UniversityPartner>
    fun findBySchoolAndId(school: School, id: UUID): UniversityPartner

    fun deleteBySchoolAndId(school: School, id: UUID)
}
