package ru.mephi.aeschools.database.entity.activity


import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractEntity
import ru.mephi.aeschools.database.entity.user.User
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.MapsId
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "UserActivity")
class UserActivity(
    @MapsId
    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "userId", nullable = false)
    var user: User,

    ) : AbstractEntity() {

    @Column(name = "time", nullable = false)
    var time: LocalDateTime = LocalDateTime.now()
}
