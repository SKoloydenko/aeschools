package ru.mephi.aeschools.database.repository.user


import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.user.Fingerprint
import ru.mephi.aeschools.database.entity.user.User

interface FingerprintDao : AbstractRepository<Fingerprint> {
    fun deleteByDeviceId(deviceId: String)
    fun deleteByUser(user: User)
}
