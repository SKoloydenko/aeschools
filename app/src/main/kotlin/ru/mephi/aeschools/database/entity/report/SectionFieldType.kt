package ru.mephi.aeschools.database.entity.report

enum class SectionFieldType {
    FREE_FORM, FILE
}