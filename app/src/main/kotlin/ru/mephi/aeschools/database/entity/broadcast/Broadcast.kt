package ru.mephi.aeschools.database.entity.broadcast

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.model.enums.BroadcastType
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.publicationLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Lob
import javax.persistence.Table

@Entity
@Table(name = "Broadcast")
class Broadcast(
    @Column(nullable = false, length = largeLength)
    val title: String,

    @Lob
    @Column(nullable = false, length = publicationLength)
    val text: String,

    @Column(nullable = false)
    val type: BroadcastType,

    @Column(nullable = false)
    val ignorePreferences: Boolean,

) : AbstractCreatedAtEntity()
