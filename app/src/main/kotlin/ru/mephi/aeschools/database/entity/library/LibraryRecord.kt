package ru.mephi.aeschools.database.entity.library

import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "LibraryRecord")
class LibraryRecord(
    @Column(name = "title", nullable = false, length = smallLength)
    var title: String,

    @Column(name = "content", nullable = true, length = extraLargeLength)
    var content: String,

    @Column(name = "link", length = largeLength)
    var link: String? = null,
) : AbstractCreatedAtEntity()
