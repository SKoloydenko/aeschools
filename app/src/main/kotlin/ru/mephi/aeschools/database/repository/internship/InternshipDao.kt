package ru.mephi.aeschools.database.repository.internship

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.internship.Internship
import java.util.*

@Repository
interface InternshipDao : AbstractRepository<Internship>,
    PagingAndSortingRepository<Internship, UUID> {
    fun findByTitleStartsWith(query: String, pageable: Pageable): Page<Internship>

    fun findByTitleStartsWithAndTagsTitle(
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<Internship>

    @Query(
        """
            SELECT i FROM Internship i
            WHERE i.title LIKE :query% AND i.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndPublicationDateBeforeNow(
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<Internship>

    @Query(
        """
            SELECT i FROM Internship i
            JOIN i.tags it
            WHERE i.title LIKE :query% AND it.title = :tag
            AND i.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<Internship>

    fun findBySchoolIdAndTitleStartsWith(schoolId: UUID, query: String, pageable: Pageable): Page<Internship>

    fun findBySchoolIdAndTitleStartsWithAndTagsTitle(
        schoolId: UUID,
        query: String,
        tag: String,
        pageable: Pageable,
    ): Page<Internship>

    @Query(
        """
            SELECT i FROM Internship i
            JOIN i.school s
            WHERE s.id = :schoolId AND i.title LIKE :query%
            AND i.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        pageable: Pageable,
    ): Page<Internship>

    @Query(
        """
            SELECT i FROM Internship i
            JOIN i.tags it
            JOIN i.school s
            WHERE s.id = :schoolId AND i.title LIKE :query% AND it.title = :tag
            AND i.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("query") query: String,
        @Param("tag") tag: String,
        pageable: Pageable,
    ): Page<Internship>

    fun findByIdAndSchoolId(id: UUID, schoolId: UUID): Optional<Internship>

    @Query(
        """
            SELECT i FROM Internship i
            WHERE i.id = :internshipId AND i.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findByIdAndPublicationDateBeforeNow(
        @Param("internshipId") internshipId: UUID,
    ): Optional<Internship>

    @Query(
        """
            SELECT i FROM Internship i
            JOIN i.school s
            WHERE s.id = :schoolId AND i.id = :internshipId AND i.publicationDate < CURRENT_TIMESTAMP
        """
    )
    fun findBySchoolIdAndIdAndPublicationDateBeforeNow(
        @Param("schoolId") schoolId: UUID,
        @Param("internshipId") internshipId: UUID,
    ): Optional<Internship>

    fun findByApplicantsIdAndTitleStartsWith(userId: UUID, query: String, pageable: Pageable): Page<Internship>

    fun findByApplicantsIdAndTitleStartsWithAndTagsTitle(userId: UUID, query: String, tag: String, pageable: Pageable): Page<Internship>
}
