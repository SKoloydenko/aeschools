package ru.mephi.aeschools.database.repository.storage

import org.springframework.stereotype.Repository
import ru.mephi.aeschools.database.repository.AbstractRepository
import ru.mephi.aeschools.database.entity.storage.StorageFile

@Repository
interface StorageFileDao : AbstractRepository<StorageFile> {
    fun existsByName(name: String): Boolean
}
