package ru.mephi.aeschools.database.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.mephi.aeschools.database.AbstractCreatedAtEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "ContentPhoto", indexes = [Index(columnList = "photoPath")])
class ContentPhoto(
    @Column
    var photoPath: String,
) : AbstractCreatedAtEntity() {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "contentId", nullable = true)
    var content: AbstractContent? = null
}
