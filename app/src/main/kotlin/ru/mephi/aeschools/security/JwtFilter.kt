package ru.mephi.aeschools.security

import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import ru.mephi.aeschools.controller.routers.writeResponseError
import ru.mephi.aeschools.model.exceptions.auth.AuthTokenException
import ru.mephi.aeschools.model.exceptions.auth.Unauthorized
import ru.mephi.aeschools.util.TokenManager
import ru.mephi.aeschools.util.constants.API_VERSION_1
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtFilter(
    @Lazy private val tokenManager: TokenManager,
) : OncePerRequestFilter() {
    private val prefix = "Bearer "

    private fun headerIsValid(header: String?) =
        header?.startsWith(prefix) ?: false

    private fun createAuthToken(header: String): UsernamePasswordAuthenticationToken {
        val principal =
            tokenManager.parseTokenPrincipal(header.replace(prefix, ""))
        return UsernamePasswordAuthenticationToken(principal.userId, null, null)
    }

    override fun shouldNotFilter(request: HttpServletRequest): Boolean {
        val path = request.requestURI
        return path.startsWith("/api/actuator") || path.startsWith("/api${API_VERSION_1}/public")
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain,
    ) {
        val header = request.getHeader(HttpHeaders.AUTHORIZATION)
        try {
            if (headerIsValid(header)) SecurityContextHolder.getContext().authentication =
                createAuthToken(header)
            filterChain.doFilter(request, response)
        } catch (throwable: AuthTokenException) {
            response.writeResponseError(Unauthorized())
        }
    }
}
