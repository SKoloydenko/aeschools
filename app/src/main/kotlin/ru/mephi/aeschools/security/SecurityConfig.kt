package ru.mephi.aeschools.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.mephi.aeschools.config.properties.ActuatorProperties
import ru.mephi.aeschools.config.properties.SwaggerProperties
import ru.mephi.aeschools.util.constants.API_VERSION_1

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig(
    private val jwtFilter: JwtFilter,
    private val swaggerProperties: SwaggerProperties,
    private val actuatorProperties: ActuatorProperties,
) : WebSecurityConfigurerAdapter() {
    @Bean
    fun getEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    override fun configure(http: HttpSecurity) {
        http.csrf().disable().anonymous().and().authorizeRequests()
            .antMatchers("$API_VERSION_1/public/**").permitAll()
            .antMatchers("$API_VERSION_1/user/**").authenticated()
            .antMatchers("$API_VERSION_1/admin/**").authenticated()
            .and().addFilterBefore(
                jwtFilter,
                UsernamePasswordAuthenticationFilter::class.java
            )

        http.authorizeRequests().antMatchers("/actuator/**").hasRole("ADMIN")
            .and().httpBasic()

        http.authorizeRequests()
            .antMatchers("/swagger/**", "/swagger-ui/**", "/v3/api-docs/**")
            .hasRole("SWAGGER").and().httpBasic()

        http.sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication().withUser(actuatorProperties.username)
            .password(getEncoder().encode(actuatorProperties.password))
            .roles("ADMIN").and().withUser(swaggerProperties.swaggerLogin)
            .password(getEncoder().encode(swaggerProperties.swaggerPassword))
            .roles("SWAGGER")
    }
}
