package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class TooManyRequestsException : AppException(
    status = HttpStatus.TOO_MANY_REQUESTS,
    message = "Too many requests",
    errorDescription = ErrorDescription(
        ru = "Слишком много запросов",
        en = "Too many requests"
    )
)
