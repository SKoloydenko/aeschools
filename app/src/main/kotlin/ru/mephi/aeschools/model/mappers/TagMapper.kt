package ru.mephi.aeschools.model.mappers

import org.mapstruct.Context
import org.mapstruct.Mapper
import ru.mephi.aeschools.database.entity.AbstractContent
import ru.mephi.aeschools.database.entity.Tag

@Mapper(componentModel = "spring")
interface TagMapper {
    fun asEntity(title: String, @Context entity: AbstractContent) =
        Tag(title).also { it.entity = entity }

    fun asEntitySet(titles: Set<String>, @Context entity: AbstractContent): Set<Tag>

    fun asResponse(tag: Tag): String = tag.title

    fun asResponseSet(tags: Set<Tag>): Set<String>
}
