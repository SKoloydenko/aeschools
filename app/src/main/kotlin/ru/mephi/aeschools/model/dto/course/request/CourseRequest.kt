package ru.mephi.aeschools.model.dto.course.request

import ru.mephi.aeschools.model.dto.AbstractContentRequest
import java.time.LocalDateTime

class CourseRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    val tags: Set<String>,
    val enrollmentFinishingDate: LocalDateTime,
): AbstractContentRequest(title, content, publicationDate)
