package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

data class UserNotFoundByEmailException(
    val email: String,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "User with email [$email] not found",
    errorDescription = ErrorDescription(
        ru = "Пользователь с таким адресом электронной почты не найден",
        en = "No user found with this email address"
    )
)
