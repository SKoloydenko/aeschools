package ru.mephi.aeschools.model.dto.registration_limiter

class RegistrationLimiterSettingsRequest(
    val deltaTime: Long,
    val deltaCount: Long,
)
