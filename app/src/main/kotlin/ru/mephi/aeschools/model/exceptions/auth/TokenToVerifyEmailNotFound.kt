package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class TokenToVerifyEmailNotFound : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "Token to verify email not found.",
    errorDescription = ErrorDescription(
        ru = "Запрос для подтверждения почты не найден",
        en = "Email verification request not found"
    )
)