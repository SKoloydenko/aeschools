package ru.mephi.aeschools.model.dto.event.request

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.*
import ru.mephi.aeschools.model.dto.AbstractContentRequest
import java.time.LocalDateTime

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = EventRequest::class, name = "EVENT"),
    JsonSubTypes.Type(value = ForeignEventRequest::class, name = "FOREIGN_EVENT")
)
abstract class AbstractEventRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    val tags: Set<String>,
) : AbstractContentRequest(title, content, publicationDate)
