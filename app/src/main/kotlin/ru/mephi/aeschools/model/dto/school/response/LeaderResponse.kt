package ru.mephi.aeschools.model.dto.school.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

class LeaderResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val fullName: String,
    val jobRole: String,
    val ranks: String,
    val achievements: String,
    override var photoPath: String?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
