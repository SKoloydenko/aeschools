package ru.mephi.aeschools.model.dto.section.request

class CreateSectionRequest(
    val title: String,
    val isBlocked: Boolean,
)
