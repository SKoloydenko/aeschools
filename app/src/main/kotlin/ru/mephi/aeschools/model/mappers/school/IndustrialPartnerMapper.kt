package ru.mephi.aeschools.model.mappers.school

import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.school.IndustrialPartner
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.model.dto.school.response.IndustrialPartnerResponse

@Mapper(componentModel = "spring")
interface IndustrialPartnerMapper {
    fun asEntity(request: IndustrialPartnerRequest): IndustrialPartner

    fun asResponse(partner: IndustrialPartner): IndustrialPartnerResponse
    fun asPageResponse(partners: Page<IndustrialPartner>): PageResponse<IndustrialPartnerResponse>

    fun update(
        @MappingTarget partner: IndustrialPartner,
        request: IndustrialPartnerRequest,
    ): IndustrialPartner
}
