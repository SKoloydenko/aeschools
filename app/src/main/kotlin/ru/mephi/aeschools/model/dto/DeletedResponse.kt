package ru.mephi.aeschools.model.dto

import java.lang.reflect.Type
import java.util.*

class DeletedResponse(type: Type, val id: UUID) {
    var message: String

    init {
        message =
            "${(type as Class<*>).simpleName} with id [$id] successfully deleted"
    }
}
