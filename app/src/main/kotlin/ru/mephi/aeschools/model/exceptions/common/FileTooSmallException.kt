package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException


class FileTooSmallException : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "File too small",
    errorDescription = ErrorDescription(
        ru = "Слишком маленький файл",
        en = "Too small file"
    )
)