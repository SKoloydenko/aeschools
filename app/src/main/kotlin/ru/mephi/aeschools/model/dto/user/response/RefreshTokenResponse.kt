package ru.mephi.aeschools.model.dto.user.response

import java.time.LocalDateTime

class RefreshTokenResponse(
    val refreshToken: String,
    val expiresAt: LocalDateTime,
)
