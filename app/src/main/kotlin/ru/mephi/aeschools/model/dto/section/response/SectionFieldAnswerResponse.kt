package ru.mephi.aeschools.model.dto.section.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

open class SectionFieldAnswerResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val updatedAt: LocalDateTime,
    val answer: String? = null,
) : AbstractCreatedAtResponse(id, createdAt)
