package ru.mephi.aeschools.model.dto.section.request

import java.util.*

class SectionElementAnswerRequest(
    val id: UUID,
    answer: String,
) : FieldAnswerRequest(answer)
