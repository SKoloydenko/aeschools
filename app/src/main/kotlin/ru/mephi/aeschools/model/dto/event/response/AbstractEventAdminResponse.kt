package ru.mephi.aeschools.model.dto.event.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.UUID

abstract class AbstractEventAdminResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val content: String,
    val publicationDate: LocalDateTime,
    val tags: Set<String>,
    override var photoPath: String?,
    val author: UserShortResponse?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
