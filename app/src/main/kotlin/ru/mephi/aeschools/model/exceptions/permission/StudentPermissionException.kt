package ru.mephi.aeschools.model.exceptions.permission

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class StudentPermissionException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "User is not student",
    errorDescription = ErrorDescription(
        ru = "Вы не являетесь студентом. Измените настройки в профиле",
        en = "User is not student"
    )
)
