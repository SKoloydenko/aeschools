package ru.mephi.aeschools.model.exceptions.user

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class BannedException(
    id: UUID,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "User with id [$id] is banned",
    errorDescription = ErrorDescription(
        ru = "Пользователь заблокирован",
        en = "User is banned"
    )
)
