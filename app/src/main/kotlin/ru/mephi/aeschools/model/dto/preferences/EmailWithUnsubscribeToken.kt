package ru.mephi.aeschools.model.dto.preferences

interface EmailWithUnsubscribeToken {
    val email: String
    val token: String
}