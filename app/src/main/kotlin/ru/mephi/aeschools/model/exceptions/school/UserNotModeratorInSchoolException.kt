package ru.mephi.aeschools.model.exceptions.school

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserNotModeratorInSchoolException(
    schoolId: UUID,
    userId: UUID,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "User with id [$userId] is not moderator in school with id [$schoolId]",
    errorDescription = ErrorDescription(
        ru = "Пользователь не является модератором этой площадки",
        en = "User is not moderator of this school"
    )
)
