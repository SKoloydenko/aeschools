package ru.mephi.aeschools.model.mappers.event

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.event.Event
import ru.mephi.aeschools.database.entity.event.ForeignEvent
import ru.mephi.aeschools.database.entity.event.UserEvent
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.event.response.AbstractEventResponse
import ru.mephi.aeschools.model.dto.event.response.EventResponse
import ru.mephi.aeschools.model.dto.event.response.ForeignEventResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.model.exceptions.AppException
import ru.mephi.aeschools.model.mappers.TagMapper

@Mapper(componentModel = "spring", uses = [TagMapper::class])
interface UserEventMapper {

    fun asEntity(event: AbstractEvent, user: User): UserEvent

    @Mapping(target = ".", source = "user")
    fun asUserResponse(userEvent: UserEvent): UserShortResponse

    fun asResponse(userEvent: UserEvent): AbstractEventResponse {
        return when (userEvent.event) {
            is ForeignEvent -> asForeignEventResponse(userEvent.event as ForeignEvent)
            is Event -> asEventResponse(userEvent.event as Event)
            else -> throw AppException(
                status = HttpStatus.UNPROCESSABLE_ENTITY,
                message = "Invalid request"
            )
        }
    }

    fun asEventResponse(event: Event): EventResponse
    fun asForeignEventResponse(event: ForeignEvent): ForeignEventResponse

    fun asPageUserResponse(userEvents: Page<UserEvent>): PageResponse<UserShortResponse>
    fun asPageEventResponse(userEvents: Page<UserEvent>): PageResponse<AbstractEventResponse>
}
