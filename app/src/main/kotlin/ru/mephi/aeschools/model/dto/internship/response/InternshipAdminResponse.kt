package ru.mephi.aeschools.model.dto.internship.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.*

class InternshipAdminResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val content: String,
    val publicationDate: LocalDateTime,
    val company: String,
    override var photoPath: String?,
    val tags: Set<String>,
    val school: SchoolShortResponse?,
    val author: UserShortResponse?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
