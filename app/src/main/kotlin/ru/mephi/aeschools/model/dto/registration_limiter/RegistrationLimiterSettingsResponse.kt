package ru.mephi.aeschools.model.dto.registration_limiter

class RegistrationLimiterSettingsResponse(
    val deltaTime: Long,
    val deltaCount: Long,
)
