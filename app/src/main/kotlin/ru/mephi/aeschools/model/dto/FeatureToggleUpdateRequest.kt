package ru.mephi.aeschools.model.dto

class FeatureToggleUpdateRequest(
    val featureName: String,
    val enabled: Boolean,
)
