package ru.mephi.aeschools.model.dto.event.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

abstract class AbstractEventResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val content: String,
    val tags: Set<String>,
    override var photoPath: String?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
