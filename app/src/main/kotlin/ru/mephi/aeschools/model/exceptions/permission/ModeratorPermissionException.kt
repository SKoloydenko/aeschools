package ru.mephi.aeschools.model.exceptions.permission

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.enums.permission.ModeratorPermission
import ru.mephi.aeschools.model.exceptions.AppException

class ModeratorPermissionException(
    permission: ModeratorPermission,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Moderator does not have ${permission.value} permission",
    errorDescription = ErrorDescription(
        ru = "У модератора нет разрешения",
        en = "Moderator does not have permission",
    )
)
