package ru.mephi.aeschools.model.dto.useful_link.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

class UsefulLinkResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val link: String,
    override var photoPath: String?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
