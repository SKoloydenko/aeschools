package ru.mephi.aeschools.model.dto

import java.time.LocalDateTime
import java.util.*

abstract class AbstractCreatedAtResponse(
    id: UUID,
    val createdAt: LocalDateTime,
) : AbstractResponse(id)
