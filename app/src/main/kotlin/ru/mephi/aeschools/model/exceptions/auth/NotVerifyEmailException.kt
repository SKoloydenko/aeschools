package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class NotVerifyEmailException(
    val email: String,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "User with email [$email] has not verify email.",
    errorDescription = ErrorDescription(
        ru = "Вы не подтвердили свой адрес электронной почты",
        en = "You have not verified your email address"
    )
)
