package ru.mephi.aeschools.model.exceptions.storage

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class UnsupportedFileExtension : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Unsupported file extension",
    errorDescription = ErrorDescription(
        ru = "Расширение файла не поддерживается",
        en = "Unsupported file extension"
    )
)
