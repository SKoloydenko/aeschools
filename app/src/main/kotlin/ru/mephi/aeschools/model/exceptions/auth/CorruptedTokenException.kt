package ru.mephi.aeschools.model.exceptions.auth

import ru.mephi.aeschools.model.ErrorDescription

class CorruptedTokenException : AuthTokenException(
    message = "Token is corrupted",
    errorDescription = ErrorDescription(
        ru = "Токен повреждён",
        en = "Token is corrupted"
    )
)
