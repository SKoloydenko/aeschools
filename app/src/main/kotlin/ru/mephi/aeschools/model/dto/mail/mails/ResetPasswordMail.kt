package ru.mephi.aeschools.model.dto.mail.mails

class ResetPasswordMail(
    link: String,
    receiverEmail: String,
) : AbstractSingleReceiverMail(receiverEmail, mapOf("link" to link))
