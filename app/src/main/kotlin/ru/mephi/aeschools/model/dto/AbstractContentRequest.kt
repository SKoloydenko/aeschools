package ru.mephi.aeschools.model.dto

import java.time.LocalDateTime

abstract class AbstractContentRequest(
    val title: String,
    val content: String,
    val publicationDate: LocalDateTime,
)
