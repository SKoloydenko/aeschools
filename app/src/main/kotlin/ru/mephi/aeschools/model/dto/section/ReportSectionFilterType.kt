package ru.mephi.aeschools.model.dto.section

enum class ReportSectionFilterType {
    ALL, BLOCKED, OPENED
}