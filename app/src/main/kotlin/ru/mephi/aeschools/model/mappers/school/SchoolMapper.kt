package ru.mephi.aeschools.model.mappers.school

import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.model.dto.school.response.SchoolResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse

@Mapper(componentModel = "spring")
interface SchoolMapper {
    fun asEntity(request: SchoolRequest): School

    fun asResponse(school: School): SchoolResponse
    fun asShortResponse(school: School): SchoolShortResponse
    fun asListResponse(school: List<School>): List<SchoolShortResponse>
    fun asPageResponse(school: Page<School>): PageResponse<SchoolShortResponse>
    fun update(@MappingTarget school: School, request: SchoolRequest): School
}
