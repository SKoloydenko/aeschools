package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

abstract class AuthTokenException(
    message: String,
    errorDescription: ErrorDescription,
) : AppException(HttpStatus.UNAUTHORIZED, message, errorDescription)
