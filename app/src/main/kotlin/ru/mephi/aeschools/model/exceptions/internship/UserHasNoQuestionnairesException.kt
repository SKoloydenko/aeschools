package ru.mephi.aeschools.model.exceptions.internship

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserHasNoQuestionnairesException(
    userId: UUID,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "User with id [$userId] does not have questionnaires",
    errorDescription = ErrorDescription(
        ru = "У пользователя нет анкет",
        en = "User does not have questionnaires"
    )
)
