package ru.mephi.aeschools.model.dto.section.response

import ru.mephi.aeschools.database.entity.report.SectionFieldType
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import java.time.LocalDateTime
import java.util.*

class FullSectionFieldAnswerResponse(
    id: UUID,
    createdAt: LocalDateTime,
    updatedAt: LocalDateTime,
    answer: String? = null,
    val type: SectionFieldType,
    val school: SchoolShortResponse
) : SectionFieldAnswerResponse(id, createdAt, updatedAt, answer)