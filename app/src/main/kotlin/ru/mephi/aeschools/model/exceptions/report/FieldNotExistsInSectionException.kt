package ru.mephi.aeschools.model.exceptions.report

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class FieldNotExistsInSectionException(
    sectionId: UUID,
    fieldId: UUID,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "Section with id [$sectionId] have not field with id [$fieldId]",
    errorDescription = ErrorDescription(
        ru = "Указанное поле для заполнения не найдено",
        en = "The specified field was not found"
    )
)
