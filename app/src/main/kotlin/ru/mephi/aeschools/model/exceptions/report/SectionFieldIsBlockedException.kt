package ru.mephi.aeschools.model.exceptions.report

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class SectionFieldIsBlockedException(
    fieldId: UUID,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Field with id [$fieldId] is blocked.",
    errorDescription = ErrorDescription(
        ru = "Это поле заблокировано",
        en = "This field is blocked"
    )
)
