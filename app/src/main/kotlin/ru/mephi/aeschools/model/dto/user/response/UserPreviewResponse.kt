package ru.mephi.aeschools.model.dto.user.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

open class UserPreviewResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val name: String,
    val surname: String,
    val patronymic: String?,
    val email: String,
) : AbstractCreatedAtResponse(id, createdAt)
