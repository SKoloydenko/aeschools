package ru.mephi.aeschools.model.mappers.course

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.course.Course
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.model.dto.course.response.CourseAdminResponse
import ru.mephi.aeschools.model.dto.course.response.CourseResponse
import ru.mephi.aeschools.model.mappers.TagMapper

@Mapper(componentModel = "spring", uses = [TagMapper::class])
interface CourseMapper {
    @Mapping(target = "tags", ignore = true)
    fun asEntity(request: CourseRequest): Course

    @Mapping(target = "createdAt", source = "publicationDate")
    fun asResponse(course: Course): CourseResponse
    fun asAdminResponse(course: Course): CourseAdminResponse

    fun asPageResponse(courses: Page<Course>): PageResponse<CourseResponse>
    fun asAdminPageResponse(courses: Page<Course>): PageResponse<CourseAdminResponse>

    @Mapping(target = "tags", ignore = true)
    fun update(@MappingTarget course: Course, request: CourseRequest): Course
}
