package ru.mephi.aeschools.model.dto.excel.proxy

import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.model.dto.excel.ExcelFieldGetter

fun getInternshipProxy(): List<ExcelFieldGetter<Internship>> {
    return listOf(
        ExcelFieldGetter("id") { it.id },
        ExcelFieldGetter("Название") { it.title },
        ExcelFieldGetter("Описание") { it.content },
        ExcelFieldGetter("Дата публикации") { it.publicationDate },
        ExcelFieldGetter("Компания") { it.company },
    )
}
