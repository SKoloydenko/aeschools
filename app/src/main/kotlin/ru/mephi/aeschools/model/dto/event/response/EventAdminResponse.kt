package ru.mephi.aeschools.model.dto.event.response

import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.*

open class EventAdminResponse(
    id: UUID,
    createdAt: LocalDateTime,
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    tags: Set<String>,
    photoPath: String?,
    author: UserShortResponse?,
    val location: String,
    val eventDate: LocalDateTime,
    val school: SchoolShortResponse?,
) : AbstractEventAdminResponse(
    id,
    createdAt,
    title,
    content,
    publicationDate,
    tags,
    photoPath,
    author
) {
    val type: String = "EVENT"
}
