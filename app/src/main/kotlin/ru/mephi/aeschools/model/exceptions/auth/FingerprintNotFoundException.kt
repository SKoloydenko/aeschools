package ru.mephi.aeschools.model.exceptions.auth

import ru.mephi.aeschools.model.ErrorDescription

class FingerprintNotFoundException : AuthTokenException(
    message = "Fingerprint not found",
    errorDescription = ErrorDescription(
        ru = "Отпечаток устройства не найден",
        en = "Fingerprint not found"
    )
)
