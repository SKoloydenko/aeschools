package ru.mephi.aeschools.model.dto.auth.request

class ResetPasswordRequest(
    val token: String,
    val password: String,
    val fingerPrint: String,
)
