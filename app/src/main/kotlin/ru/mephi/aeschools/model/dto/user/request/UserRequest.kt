package ru.mephi.aeschools.model.dto.user.request

import ru.mephi.aeschools.model.enums.user.EducationDegree

open class UserRequest(
    var name: String,
    var surname: String,
    var patronymic: String? = null,
    var phone: String,
    var organization: String? = null,
    var jobRole: String? = null,
    var university: String? = null,
    var educationDegree: EducationDegree? = null,
    var grade: Int? = null,
    var specialty: String? = null,
    var student: Boolean,
)
