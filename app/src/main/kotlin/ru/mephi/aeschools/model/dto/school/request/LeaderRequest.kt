package ru.mephi.aeschools.model.dto.school.request

class LeaderRequest(
    val fullName: String,
    val jobRole: String,
    val ranks: String,
    val achievements: String,
)
