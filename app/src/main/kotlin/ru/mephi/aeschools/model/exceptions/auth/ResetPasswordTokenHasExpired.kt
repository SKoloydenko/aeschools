package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class ResetPasswordTokenHasExpired(
    val user: User,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Request to change password to user with email [${user.email}] has expired.",
    errorDescription = ErrorDescription(
        ru = "Срок действия ссылки для смены пароля истёк",
        en = "Password change link has expired"
    )
)
