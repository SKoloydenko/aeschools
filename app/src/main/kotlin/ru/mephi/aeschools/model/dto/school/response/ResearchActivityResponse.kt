package ru.mephi.aeschools.model.dto.school.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

class ResearchActivityResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val description: String,
) : AbstractCreatedAtResponse(id, createdAt)
