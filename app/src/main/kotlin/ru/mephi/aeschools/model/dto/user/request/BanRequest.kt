package ru.mephi.aeschools.model.dto.user.request

class BanRequest(
    val banned: Boolean,
)
