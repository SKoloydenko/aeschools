package ru.mephi.aeschools.model.mappers.action

import org.mapstruct.Mapper
import org.mapstruct.Mappings
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.action.UserAction
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.action.UserActionResponse
import ru.mephi.aeschools.model.mappers.user.UserMapper

@Mapper(componentModel = "spring", uses = [UserMapper::class])
interface UserActionMapper {
    @Mappings
    fun asPageResponse(page: Page<UserAction>): PageResponse<UserActionResponse>

    @Mappings
    fun asResponse(action: UserAction): UserActionResponse
}
