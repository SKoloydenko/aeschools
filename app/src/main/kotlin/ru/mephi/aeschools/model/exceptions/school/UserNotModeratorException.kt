package ru.mephi.aeschools.model.exceptions.school

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserNotModeratorException(
    userId: UUID,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "User with id [$userId] is not school moderator",
    errorDescription = ErrorDescription(
        ru = "Пользователь не является модератором площадки",
        en = "User is not moderator of this school"
    )
)
