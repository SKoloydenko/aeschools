package ru.mephi.aeschools.model.enums.permission

enum class ModeratorPermission(val value: String) {
    INTERNSHIP("Internship"),
    EVENT("Event"),
    PUBLICATION("Publication"),
    BEST_PRACTICE("Best Practice"),
    COURSE("Course"),
    HIDDEN_FIELDS("Hidden fields"),
    MODERATOR("Moderator")
}
