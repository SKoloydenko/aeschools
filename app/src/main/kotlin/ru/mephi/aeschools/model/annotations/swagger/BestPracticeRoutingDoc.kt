package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.BestPracticeService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/best-practice",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "listBestPractices",
        operation = Operation(
            operationId = "GET /public/best-practice",
            description = """Returns list of all best practices with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "findBestPracticeById",
        operation = Operation(
            operationId = "GET /public/best-practice/{bestPracticeId}",
            description = """Returns best practice by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/best-practice",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "listSchoolBestPractices",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/best-practice",
            description = """Returns list of school best practices with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "findSchoolBestPracticeById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/best-practice/{bestPracticeId}",
            description = """Returns school best practice by id."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/best-practice",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "listBestPracticesForAdmin",
        operation = Operation(
            operationId = "GET /admin/best-practice",
            description = """Returns list best practices with pagination for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "findBestPracticeByIdForAdmin",
        operation = Operation(
            operationId = "GET /admin/best-practice/{bestPracticeId}",
            description = """Returns best practice by id for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/best-practice",
        method = arrayOf(RequestMethod.POST),
        beanClass = BestPracticeService::class,
        beanMethod = "createBestPractice",
        operation = Operation(
            operationId = "POST /admin/best-practice",
            description = """Create best practice."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = BestPracticeService::class,
        beanMethod = "updateBestPractice",
        operation = Operation(
            operationId = "PUT /admin/best-practice/{bestPracticeId}",
            description = """Update best practice."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/best-practice/{bestPracticeId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = BestPracticeService::class,
        beanMethod = "uploadBestPracticePhoto",
        operation = Operation(
            operationId = "POST /admin/best-practice/{bestPracticeId}/photo",
            description = """Upload best practice photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = BestPracticeService::class,
        beanMethod = "deleteBestPractice",
        operation = Operation(
            operationId = "DELETE /admin/best-practice/{bestPracticeId}",
            description = """Delete best practice."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/best-practice",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "listSchoolBestPracticesForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/best-practice",
            description = """Returns list of school best practices with pagination for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = BestPracticeService::class,
        beanMethod = "findSchoolBestPracticeByIdForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/best-practice/{bestPracticeId}",
            description = """Returns school best practice by id for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/best-practice",
        method = arrayOf(RequestMethod.POST),
        beanClass = BestPracticeService::class,
        beanMethod = "createSchoolBestPractice",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/best-practice",
            description = """Create school best practice."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = BestPracticeService::class,
        beanMethod = "updateSchoolBestPractice",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/best-practice/{bestPracticeId}",
            description = """Update school best practice."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/best-practice/{bestPracticeId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = BestPracticeService::class,
        beanMethod = "uploadSchoolBestPracticePhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/best-practice/{bestPracticeId}/photo",
            description = """Upload school best practice photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/best-practice/{bestPracticeId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = BestPracticeService::class,
        beanMethod = "deleteSchoolBestPractice",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/best-practice/{bestPracticeId}",
            description = """Delete school best practice."""
        )
    ),
)

annotation class BestPracticeRoutingDoc
