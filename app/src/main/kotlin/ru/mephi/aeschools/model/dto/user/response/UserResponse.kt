package ru.mephi.aeschools.model.dto.user.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

open class UserResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val name: String,
    val surname: String,
    val patronymic: String?,
    val fullName: String,
    val email: String,
    val phone: String?,
    val admin: Boolean,
    val student: Boolean,
    val moderator: Boolean,
    val banned: Boolean,
) : AbstractCreatedAtResponse(id, createdAt)
