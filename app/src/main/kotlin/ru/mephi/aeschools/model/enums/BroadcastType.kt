package ru.mephi.aeschools.model.enums

enum class BroadcastType { USERS, MODERATORS, ALL }
