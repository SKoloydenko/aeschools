package ru.mephi.aeschools.model.mappers.publication

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.publication.Publication
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.publication.request.PublicationRequest
import ru.mephi.aeschools.model.dto.publication.response.PublicationAdminResponse
import ru.mephi.aeschools.model.dto.publication.response.PublicationResponse
import ru.mephi.aeschools.model.mappers.TagMapper

@Mapper(componentModel = "spring", uses = [TagMapper::class])
interface PublicationMapper {
    @Mapping(target = "tags", ignore = true)
    fun asEntity(request: PublicationRequest): Publication

    @Mapping(target = "createdAt", source = "publicationDate")
    fun asResponse(publication: Publication): PublicationResponse
    fun asAdminResponse(publication: Publication): PublicationAdminResponse

    fun asPageResponse(publications: Page<Publication>): PageResponse<PublicationResponse>
    fun asAdminPageResponse(publications: Page<Publication>): PageResponse<PublicationAdminResponse>

    @Mapping(target = "tags", ignore = true)
    fun update(@MappingTarget publication: Publication, request: PublicationRequest): Publication
}
