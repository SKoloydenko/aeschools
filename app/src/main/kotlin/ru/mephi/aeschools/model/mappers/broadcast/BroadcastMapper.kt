package ru.mephi.aeschools.model.mappers.broadcast

import org.mapstruct.Mapper
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.broadcast.Broadcast
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.broadcast.request.BroadcastRequest
import ru.mephi.aeschools.model.dto.broadcast.response.BroadcastResponse

@Mapper(componentModel = "spring")
interface BroadcastMapper {

    fun asEntity(request: BroadcastRequest): Broadcast

    fun asResponse(broadcast: Broadcast): BroadcastResponse

    fun asPageResponse(broadcasts: Page<Broadcast>): PageResponse<BroadcastResponse>
}
