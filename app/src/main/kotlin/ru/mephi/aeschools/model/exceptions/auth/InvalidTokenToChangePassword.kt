package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class InvalidTokenToChangePassword(
    val token: String,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "This token [$token] isn't equals with token in db.",
    errorDescription = ErrorDescription(
        ru = "Неверный токен для смены пароля",
        en = "Invalid token to change password"
    )
)
