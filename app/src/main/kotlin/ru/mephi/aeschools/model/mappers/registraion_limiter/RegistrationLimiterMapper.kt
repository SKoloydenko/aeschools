package ru.mephi.aeschools.model.mappers.registraion_limiter

import org.mapstruct.Mapper
import ru.mephi.aeschools.database.entity.registration_limiter.RegistrationLimiterSettings
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsRequest
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsResponse

@Mapper(componentModel = "spring")
interface RegistrationLimiterMapper {
    fun asEntity(request: RegistrationLimiterSettingsRequest): RegistrationLimiterSettings
    fun asResponse(entity: RegistrationLimiterSettings): RegistrationLimiterSettingsResponse
}
