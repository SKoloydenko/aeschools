package ru.mephi.aeschools.model.mappers.school

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.school.ModeratorSchool
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse

@Mapper(componentModel = "spring")
interface ModeratorSchoolMapper {

    @Mapping(target = ".", source = "user")
    fun asResponse(moderator: ModeratorSchool): UserShortResponse
    fun asPageResponse(moderators: Page<ModeratorSchool>): PageResponse<UserShortResponse>
}
