package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/school",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "listSchools",
        operation = Operation(
            operationId = "GET /public/school",
            description = """Returns list of schools with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "findSchoolById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}",
            description = """Returns full information about school by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/leader",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "listLeaders",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/leader",
            description = """Returns list of school leaders with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/leader/{leaderId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "findLeaderById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/leader/{leaderId}",
            description = """Returns school leader by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/partner/industrial",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "listIndustrialPartners",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/partner/industrial",
            description = """Returns list of school industrial partners with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/partner/industrial/{partnerId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "findIndustrialPartnerById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/partner/industrial/{partnerId}",
            description = """Returns school industrial partner by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/partner/university",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "listUniversityPartners",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/partner/university",
            description = """Returns list of school university partners with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/partner/university/{partnerId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "findUniversityPartnerById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/partner/university/{partnerId}",
            description = """Returns school university partner by id."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/school",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "listSchools",
        operation = Operation(
            operationId = "GET /admin/school",
            description = """Returns list of schools with pagination for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "findSchoolById",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}",
            description = """Returns full information about school by id for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "createSchool",
        operation = Operation(
            operationId = "POST /admin/school",
            description = """Create school."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = SchoolService::class,
        beanMethod = "updateSchool",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}",
            description = """Update school information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "uploadSchoolPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/photo",
            description = """Upload school photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/leader/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "uploadSchoolLeaderPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/leader/photo",
            description = """Upload school leader photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = SchoolService::class,
        beanMethod = "deleteSchool",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}",
            description = """Delete school."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/leader",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "createLeader",
        operation = Operation(
            operationId = "POST admin/school/{schoolId}/leader",
            description = """Create school leader."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/leader/{leaderId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = SchoolService::class,
        beanMethod = "updateLeader",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/leader/{leaderId}",
            description = """Update school leader information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/leader/{leaderId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "uploadLeaderPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/leader/{leaderId}/photo",
            description = """Upload school leader photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/leader/{leaderId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = SchoolService::class,
        beanMethod = "deleteLeader",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/leader/{leaderId}",
            description = """Delete school leader."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/industrial",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "createIndustrialPartner",
        operation = Operation(
            operationId = "POST admin/school/{schoolId}/partner/industrial",
            description = """Create school industrial partner."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/industrial/{partnerId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = SchoolService::class,
        beanMethod = "updateIndustrialPartner",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/partner/industrial/{partnerId}",
            description = """Update school industrial partner information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/industrial/{partnerId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "uploadIndustrialPartnerPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/partner/industrial/{partnerId}/photo",
            description = """Upload school industrial partner photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/industrial/{partnerId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = SchoolService::class,
        beanMethod = "deleteIndustrialPartner",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/partner/industrial/{partnerId}",
            description = """Delete school industrial partner."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/university",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "createUniversityPartner",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/partner/university",
            description = """Create school university partner."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/university/{partnerId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = SchoolService::class,
        beanMethod = "updateUniversityPartner",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/partner/university/{partnerId}",
            description = """Update school university partner information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/university/{partnerId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "uploadUniversityPartnerPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/partners/university/{partnerId}/photo",
            description = """Upload school university partner photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/partner/university/{partnerId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = SchoolService::class,
        beanMethod = "deleteUniversityPartner",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/partner/university/{partnerId}",
            description = """Delete school university partner."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/moderators",
        method = arrayOf(RequestMethod.GET),
        beanClass = SchoolService::class,
        beanMethod = "listModerators",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/moderators",
            description = """Return list of school moderators."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/moderators/{userId}",
        method = arrayOf(RequestMethod.POST),
        beanClass = SchoolService::class,
        beanMethod = "createModerator",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/moderators/{userId}",
            description = """Create school moderator."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/moderators/{userId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = SchoolService::class,
        beanMethod = "updateModerator",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/moderators/{userId}",
            description = """Update school moderator information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/moderators/{userId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = SchoolService::class,
        beanMethod = "deleteModerator",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/moderators/{userId}",
            description = """Delete school moderator."""
        )
    ),
)
annotation class SchoolRoutingDoc
