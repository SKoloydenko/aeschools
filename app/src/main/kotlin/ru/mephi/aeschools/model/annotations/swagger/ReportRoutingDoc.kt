package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.ReportService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = ReportService::class,
        beanMethod = "answerToSection",
        operation = Operation(
            operationId = "PUT /user/report/{sectionId}",
            description = """Updates entered answers to some fields in report (section)"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}/school/{schoolId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "fieldsPageOfSchool",
        operation = Operation(
            operationId = "GET /admin/report/answers/{sectionId}/school/{schoolId}",
            description = """Returns fields and answers to its with page pagination of school"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}/field/{fieldId}/school/{schoolId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "getAnswerOfSchoolToField",
        operation = Operation(
            operationId = "/admin/report/answers/{sectionId}/field/{fieldId}/school/{schoolId}",
            description = """Returns answer to this field of entered school"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = ReportService::class,
        beanMethod = "deleteAnswer",
        operation = Operation(
            operationId = "DELETE /user/report/{sectionId}",
            description = """Removes answer to field of self school"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/school",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "fieldsPageOfSchool",
        operation = Operation(
            operationId = "GET /user/report/{sectionId}/school",
            description = """Returns fields and answers to its with page pagination of self school"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}/field/{fieldId}/all",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "getAllAnswersToField",
        operation = Operation(
            operationId = "GET /admin/report/answers/{sectionId}/field/{fieldId}/all",
            description = """Returns all answers to this field."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}/field/{fieldId}/file",
        method = arrayOf(RequestMethod.POST),
        beanClass = ReportService::class,
        beanMethod = "uploadFileToField",
        operation = Operation(
            operationId = "POST /user/report/{sectionId}/field/{fieldId}/file",
            description = """Uploads file to field as answer"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}/field/{fieldId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = ReportService::class,
        beanMethod = "answerToField",
        operation = Operation(
            operationId = "PUT /user/report/{sectionId}/field/{fieldId}",
            description = """Updates answer to this field"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/answers/{sectionId}/field/{fieldId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "getAnswerToField",
        operation = Operation(
            operationId = "GET /user/report/{sectionId}/field/{fieldId}",
            description = """Returns answer to this field"""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/report",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "listSections",
        operation = Operation(
            operationId = "GET /admin/report",
            description = """Returns list of all reports with page pagination"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report",
        method = arrayOf(RequestMethod.POST),
        beanClass = ReportService::class,
        beanMethod = "createSection",
        operation = Operation(
            operationId = "POST /admin/report",
            description = """Creates new report"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "getSection",
        operation = Operation(
            operationId = "GET /admin/report/{sectionId}",
            description = """Returns report by it id"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = ReportService::class,
        beanMethod = "updateSection",
        operation = Operation(
            operationId = "PUT /admin/report/{sectionId}",
            description = """Updates entered report"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = ReportService::class,
        beanMethod = "removeSection",
        operation = Operation(
            operationId = "DELETE /admin/report/{sectionId}",
            description = """Removes report, fields in it and all answers"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/school/{schoolId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "fieldsPageOfSchool",
        operation = Operation(
            operationId = "GET /admin/report/{sectionId}/school/{schoolId}",
            description = """Returns fields and answers to its with page pagination of school"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/field",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "listFields",
        operation = Operation(
            operationId = "GET /admin/report/{sectionId}/field",
            description = """Returns fields of report with page pagination"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/field",
        method = arrayOf(RequestMethod.POST),
        beanClass = ReportService::class,
        beanMethod = "createField",
        operation = Operation(
            operationId = "POST /admin/report/{sectionId}/field",
            description = """Create new field in report"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/field/{fieldId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = ReportService::class,
        beanMethod = "getField",
        operation = Operation(
            operationId = "GET /admin/report/{sectionId}/field/{fieldId}",
            description = """Returns field in this report"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/field/{fieldId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = ReportService::class,
        beanMethod = "updateField",
        operation = Operation(
            operationId = "PUT /admin/report/{sectionId}/field/{fieldId}",
            description = """Update field in report"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/report/{sectionId}/field/{fieldId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = ReportService::class,
        beanMethod = "removeFiled",
        operation = Operation(
            operationId = "DELETE /admin/report/{sectionId}/field/{fieldId}",
            description = """Removes field and all answers to it"""
        )
    ),
)
annotation class ReportRoutingDoc
