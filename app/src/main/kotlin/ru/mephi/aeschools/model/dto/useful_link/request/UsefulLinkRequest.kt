package ru.mephi.aeschools.model.dto.useful_link.request

class UsefulLinkRequest(
    val title: String,
    val link: String,
)
