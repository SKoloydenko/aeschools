package ru.mephi.aeschools.model.dto.broadcast.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.enums.BroadcastType
import java.time.LocalDateTime
import java.util.UUID

class BroadcastResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val text: String,
    val type: BroadcastType,
    val ignorePreferences: Boolean,
): AbstractCreatedAtResponse(id, createdAt)
