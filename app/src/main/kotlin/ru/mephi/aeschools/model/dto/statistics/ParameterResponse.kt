package ru.mephi.aeschools.model.dto.statistics

class ParameterResponse(
    val name: String, now: Long, prev: Long,
) {
    val value = now
    val delta = (now - prev) / now * 100
}
