package ru.mephi.aeschools.model.dto.school.request

class SchoolRequest(
    val university: String,
    val name: String,
    val shortName: String,
    val actionScope: String,
    val about: String,
    val educationalActivities: String,
    val researchActivities: String,
    val innovationalActivities: String,
    val contacts: ContactsRequest?,
)
