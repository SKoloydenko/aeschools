package ru.mephi.aeschools.model.exceptions.event

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserAlreadyEventMemberException(
    eventId: UUID,
    userId: UUID,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "User with id [$userId] is already member of event with id [$eventId]",
    errorDescription = ErrorDescription(
        ru = "Вы уже регистрировались на это мероприятие",
        en = "You are already event member"
    )
)
