package ru.mephi.aeschools.model.exceptions

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
@ControllerAdvice
class AppExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(Throwable::class)
    fun exception(error: Throwable): ServerResponse {
        if (error is AppException) return error.asResponse()
        return InternalServerError(error).asResponse()
    }
}