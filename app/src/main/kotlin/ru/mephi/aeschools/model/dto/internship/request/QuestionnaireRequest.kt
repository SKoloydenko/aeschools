package ru.mephi.aeschools.model.dto.internship.request

class QuestionnaireRequest(
    val motivationalLetter: String,
    val achievements: String,
)
