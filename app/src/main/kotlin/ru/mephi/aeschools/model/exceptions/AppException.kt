package ru.mephi.aeschools.model.exceptions

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.routers.LOGGER
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.dto.ValidationError
import java.time.LocalDateTime


@JsonIgnoreProperties(
    "localizedMessage",
    "cause",
    "stackTrace",
    "ourStackTrace",
    "suppressed"
)
open class AppException(
    var status: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR,
    override val message: String = status.name,
    val errorDescription: ErrorDescription = ErrorDescription("", ""),
    errors: List<FieldError> = emptyList(),
    cause: Throwable? = null,
) : Exception(cause) {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    val errors =
        errors.map { ValidationError(it.field, it.rejectedValue, it.code) }

    @JsonFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss[.SSS]")
    val timestamp = LocalDateTime.now()

    fun asResponse(): ServerResponse {
        LOGGER.error("Error with Status Code \"${status.value()}\":\n ${stackTraceToString()}")
        return ServerResponse.status(status).body(this)
    }
}
