package ru.mephi.aeschools.model.dto.library

enum class LibraryFileType {
    INTERNAL, EXTERNAL
}