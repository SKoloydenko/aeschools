package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.QuestionnaireService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/user/me/internship/questionnaire/last",
        method = arrayOf(RequestMethod.GET),
        beanClass = QuestionnaireService::class,
        beanMethod = "findLastQuestionnaire",
        operation = Operation(
            operationId = "GET /user/me/internship/questionnaire/last",
            description = """Returns last questionnaire for internship for student."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me/internship/{internshipId}/questionnaire",
        method = arrayOf(RequestMethod.GET),
        beanClass = QuestionnaireService::class,
        beanMethod = "findLastQuestionnaire",
        operation = Operation(
            operationId = "GET /user/me/internship/{internshipId}/questionnaire",
            description = """Returns questionnaire for internship for student."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/user/internship/{internshipId}/questionnaire",
        method = arrayOf(RequestMethod.POST),
        beanClass = QuestionnaireService::class,
        beanMethod = "createQuestionnaire",
        operation = Operation(
            operationId = "POST /user/internship/{internshipId}/questionnaire",
            description = """Create questionnaire for internship for student."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/internship/{internshipId}/questionnaire",
        method = arrayOf(RequestMethod.PUT),
        beanClass = QuestionnaireService::class,
        beanMethod = "updateQuestionnaire",
        operation = Operation(
            operationId = "PUT /user/internship/{internshipId}/questionnaire",
            description = """Update questionnaire for internship for student."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/internship/{internshipId}/questionnaire/file",
        method = arrayOf(RequestMethod.POST),
        beanClass = QuestionnaireService::class,
        beanMethod = "uploadQuestionnaireFile",
        operation = Operation(
            operationId = "POST /user/internship/{internshipId}/questionnaire/file",
            description = """Upload file for questionnaire for internship for student."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/internship/{internshipId}/questionnaire",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = QuestionnaireService::class,
        beanMethod = "deleteQuestionnaire",
        operation = Operation(
            operationId = "DELETE /user/internship/{internshipId}/questionnaire",
            description = """Delete questionnaire for internship for student."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/internship/{internshipId}/applicant/{userId}/questionnaire",
        method = arrayOf(RequestMethod.GET),
        beanClass = QuestionnaireService::class,
        beanMethod = "findQuestionnaireById",
        operation = Operation(
            operationId = "GET /admin/internship/{internshipId}/applicant/{userId}/questionnaire",
            description = """Returns user questionnaire for internship for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship/{internshipId}/applicant/{userId}/questionnaire",
        method = arrayOf(RequestMethod.GET),
        beanClass = QuestionnaireService::class,
        beanMethod = "findSchoolQuestionnaireById",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/internship/{internshipId}/applicant/{userId}/questionnaire",
            description = """Returns user questionnaire for internship from school for admin."""
        )
    ),
)

annotation class QuestionnaireRoutingDoc
