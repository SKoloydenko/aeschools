package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.UserActionService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/actions",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserActionService::class,
        beanMethod = "listByUser",
        operation = Operation(
            operationId = "GET /actions",
            description = """Returns self actions with page pagination"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/actions/user/{userId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserActionService::class,
        beanMethod = "listByUser",
        operation = Operation(
            operationId = "GET /admin/actions/user/{userId}",
            description = """Returns actions of target user with page pagination"""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/actions/target/{targetId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserActionService::class,
        beanMethod = "listByTargetId",
        operation = Operation(
            operationId = "GET /admin/actions/target/{targetId}",
            description = """Returns actions under target object with page pagination"""
        )
    ),
)
annotation class UserActionDoc
