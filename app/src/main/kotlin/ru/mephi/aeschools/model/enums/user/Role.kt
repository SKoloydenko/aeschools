package ru.mephi.aeschools.model.enums.user

enum class Role(val community: String) {
    STUDENT("Студент"),
    MODERATOR("Модератор"),
    ADMIN("Администратор")
}
