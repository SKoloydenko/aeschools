package ru.mephi.aeschools.model.enums.storage

enum class ImageStoreDir(val path: String) {
    USER("/users"), SCHOOL("/schools"), LEADER("/leaders"),
    EVENT("/events"), PUBLICATION("/publications"), USEFUL_LINK("/links"),
    PARTNER("/partners"), INTERNSHIP("/internships"), QUESTIONNAIRE("/questionnaires"),
    BEST_PRACTICE("/best_practices"), COURSE("/courses"), CONTENT_PHOTO("/content_photos")
}
