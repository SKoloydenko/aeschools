package ru.mephi.aeschools.model.dto.best_practice.request

import ru.mephi.aeschools.model.dto.AbstractContentRequest
import java.time.LocalDateTime

class BestPracticeRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    val tags: Set<String>,
) : AbstractContentRequest(title, content, publicationDate)
