package ru.mephi.aeschools.model.dto

class ThumbnailRequest(
    val path: String
)