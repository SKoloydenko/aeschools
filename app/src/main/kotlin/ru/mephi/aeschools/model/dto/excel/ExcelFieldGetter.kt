package ru.mephi.aeschools.model.dto.excel

class ExcelFieldGetter<T>(
    val name: String,
    val getValue: (T) -> Any?,
)
