package ru.mephi.aeschools.model.dto.internship.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

class QuestionnaireResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val motivationalLetter: String,
    val achievements: String,
    val filePath: String?,
) : AbstractCreatedAtResponse(id, createdAt)
