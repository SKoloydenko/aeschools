package ru.mephi.aeschools.model.dto.mail.mails

import ru.mephi.aeschools.model.dto.mail.MailReceiver

abstract class AbstractSingleReceiverMail(
    receiverEmail: String,
    data: Map<String, String>,
) : AbstractMail(listOf(MailReceiver(receiverEmail, data)))
