package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException


class FeatureNotFoundException(
    name: String,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "Feature with name [$name] doesn't exist",
    errorDescription = ErrorDescription(
        ru = "Такой тоггл фичи не существует",
        en = "This toggle feature doesn't exist"
    )
)
