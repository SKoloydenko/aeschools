package ru.mephi.aeschools.model.dto.statistics

class StatisticsResponse(
    val registrationsTotal: ParameterResponse,
    val registrationsLastWeek: ParameterResponse,
    val activeUsersLastWeek: ParameterResponse,
    val activeUsersLastDay: ParameterResponse,
    val formsLastWeek: ParameterResponse,
)
