package ru.mephi.aeschools.model.dto.user.response

class TokenResponse(
    val accessToken: String,
)
