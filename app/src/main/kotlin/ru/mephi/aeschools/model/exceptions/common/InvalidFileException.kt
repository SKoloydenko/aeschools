package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException


class InvalidFileException : AppException(
    status = HttpStatus.UNSUPPORTED_MEDIA_TYPE,
    message = "Unsupported media type",
    errorDescription = ErrorDescription(
        ru = "Неподдерживаемый формат файла",
        en = "Unsupported file format"
    )
)
