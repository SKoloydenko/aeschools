package ru.mephi.aeschools.model.dto.user.response

import ru.mephi.aeschools.model.dto.AbstractResponse
import java.util.*

class UserShortResponse(
    id: UUID,
    val fullName: String,
) : AbstractResponse(id)
