package ru.mephi.aeschools.model.dto.excel.proxy

import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.event.Event
import ru.mephi.aeschools.model.dto.excel.ExcelFieldGetter

fun getEventProxy(): List<ExcelFieldGetter<AbstractEvent>> {
    return listOf(
        ExcelFieldGetter("id") { it.id },
        ExcelFieldGetter("Название") { it.title },
        ExcelFieldGetter("Описание") { it.content },
        ExcelFieldGetter("Дата публикации") { it.publicationDate },
        ExcelFieldGetter("Место проведения") { if (it is Event) it.location else "" },
        ExcelFieldGetter("Дата проведения") { if (it is Event) it.eventDate else "" }
    )
}
