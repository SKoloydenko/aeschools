package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.lang.reflect.Type

class ResourceAlreadyExistsException(
    type: Type? = null,
    stringTypeName: String = "",
    value: String,
) : AppException(
    status = HttpStatus.CONFLICT,
    message = "${
        type?.typeName?.split(".")?.last() ?: stringTypeName
    } [$value] already exists",
    errorDescription = ErrorDescription(
        ru = "Ресурс с такими данными уже существует",
        en = "Resource with such data already exists"
    )
)
