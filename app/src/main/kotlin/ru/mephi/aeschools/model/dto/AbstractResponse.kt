package ru.mephi.aeschools.model.dto

import java.util.*

abstract class AbstractResponse(var id: UUID)
