package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import org.springframework.validation.FieldError
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class ValidationException(
    var validationErrors: List<FieldError>,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Validation exception",
    errorDescription = ErrorDescription(
        ru = "Некорректные данные",
        en = "Invalid input data"
    ),
    validationErrors
)
