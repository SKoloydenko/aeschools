package ru.mephi.aeschools.model.mappers.user

import org.mapstruct.IterableMapping
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.mapstruct.Named
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.model.dto.user.response.UserPreviewResponse
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.model.dto.user.response.UserStudentResponse
import ru.mephi.aeschools.model.dto.user.response.UserWorkerResponse

@Mapper(componentModel = "spring")
interface UserMapper {
    fun asEntity(request: RegistrationRequest): User

    @Named("asShortResponse")
    fun asShortResponse(user: User): UserShortResponse

    @Named("asUserResponse")
    fun asResponse(user: User): UserResponse {
        return if (user.student) asStudentResponse(user) else asWorkerResponse(user)
    }

    fun asStudentResponse(user: User): UserStudentResponse

    fun asWorkerResponse(user: User): UserWorkerResponse

    fun asPreviewResponse(user: User): UserPreviewResponse

    fun asPageResponse(users: Page<User>): PageResponse<UserResponse>
    fun update(@MappingTarget user: User, request: UserRequest): User

    @IterableMapping(qualifiedByName = ["asShortResponse"])
    fun asListShortResponses(list: List<User>): List<UserShortResponse>

    @IterableMapping(qualifiedByName = ["asUserResponse"])
    fun asListResponse(list: List<User>): List<UserResponse>
}
