package ru.mephi.aeschools.model.dto.school.request

class ContactsRequest(
    val website: String?,
    val email: String?,
    val phone: String?,
)
