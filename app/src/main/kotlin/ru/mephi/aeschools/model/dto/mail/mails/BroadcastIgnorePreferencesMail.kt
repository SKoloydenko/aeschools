package ru.mephi.aeschools.model.dto.mail.mails

import ru.mephi.aeschools.model.dto.mail.MailReceiver

class BroadcastIgnorePreferencesMail(
    val title: String,
    val text: String,
    receiversEmails: List<MailReceiver>,
) : AbstractMail(receiversEmails)
