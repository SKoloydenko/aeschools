package ru.mephi.aeschools.model.dto.mail

class MailReceiver(
    val email: String,
    val personalData: Map<String, String> = mutableMapOf(),
) {
    companion object {
        fun List<String>.toMailReceiversList(): List<MailReceiver> {
            return this.map { MailReceiver(it) }
        }
    }
}
