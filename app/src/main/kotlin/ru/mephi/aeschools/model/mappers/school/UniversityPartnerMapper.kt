package ru.mephi.aeschools.model.mappers.school

import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.school.UniversityPartner
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.model.dto.school.response.UniversityPartnerResponse

@Mapper(componentModel = "spring")
interface UniversityPartnerMapper {
    fun asEntity(request: UniversityPartnerRequest): UniversityPartner

    fun asResponse(partner: UniversityPartner): UniversityPartnerResponse
    fun asPageResponse(partners: Page<UniversityPartner>): PageResponse<UniversityPartnerResponse>

    fun update(
        @MappingTarget partner: UniversityPartner,
        request: UniversityPartnerRequest,
    ): UniversityPartner
}
