package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.EventService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/event",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listEvents",
        operation = Operation(
            operationId = "GET /public/event",
            description = """Returns list of all events with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/event",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listSchoolEvents",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/event",
            description = """Returns list of school events with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/event/{eventId}",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "findEventById",
        operation = Operation(
            operationId = "GET public/event/{eventId}",
            description = """Returns event by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/event/{eventId}",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "findSchoolEventById",
        operation = Operation(
            operationId = "GET public/school/{schoolId}/event/{eventId}",
            description = """Returns school event by id."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/user/event/{eventId}",
        method = [RequestMethod.POST],
        beanClass = EventService::class,
        beanMethod = "joinEvent",
        operation = Operation(
            operationId = "POST user/event/{eventId}",
            description = """Join event."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/event/{eventId}",
        method = [RequestMethod.DELETE],
        beanClass = EventService::class,
        beanMethod = "leaveEvent",
        operation = Operation(
            operationId = "DELETE user/event/{eventId}",
            description = """Leave event."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me/event",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listEventsForUser",
        operation = Operation(
            operationId = "GET user/me/event",
            description = """Returns list of events for user with pagination."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/event",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listEventsForAdmin",
        operation = Operation(
            operationId = "GET /admin/event",
            description = "Returns list of all events for admin with pagination (includes future events)."
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/user/{userId}/event",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listEventsForUser",
        operation = Operation(
            operationId = "GET /admin/user/{userId}/event",
            description = "Returns list of user's events for admin with pagination."
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/event",
        method = [RequestMethod.POST],
        beanClass = EventService::class,
        beanMethod = "createEvent",
        operation = Operation(
            operationId = "POST /admin/event",
            description = """Create event."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/event/{eventId}",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "findEventByIdForAdmin",
        operation = Operation(
            operationId = "GET admin/event/{eventId}",
            description = """Returns event by id for admin (includes future events)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/event/{eventId}",
        method = [RequestMethod.PUT],
        beanClass = EventService::class,
        beanMethod = "updateEvent",
        operation = Operation(
            operationId = "PUT /admin/event/{eventId}",
            description = """Update event information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/event/{eventId}/members",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listEventMembers",
        operation = Operation(
            operationId = "GET /admin/event/{eventId}/members",
            description = """Returns list of event members."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/event/{eventId}/photo",
        method = [RequestMethod.POST],
        beanClass = EventService::class,
        beanMethod = "uploadEventPhoto",
        operation = Operation(
            operationId = "POST /admin/event/{eventId}/photo",
            description = """Upload event photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/event/{eventId}",
        method = [RequestMethod.DELETE],
        beanClass = EventService::class,
        beanMethod = "deleteEvent",
        operation = Operation(
            operationId = "DELETE /admin/event/{eventId}",
            description = """Delete event."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listSchoolEventsForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/event",
            description = "Returns list of school events for admin with pagination (includes future events)."
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event",
        method = [RequestMethod.POST],
        beanClass = EventService::class,
        beanMethod = "createSchoolEvent",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/event",
            description = """Create school event."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event/{eventId}",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "findSchoolEventByIdForAdmin",
        operation = Operation(
            operationId = "GET admin/event/school/{schoolId}/{eventId}",
            description = """Returns school event by id for admin (includes future events)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event/{eventId}",
        method = [RequestMethod.PUT],
        beanClass = EventService::class,
        beanMethod = "updateSchoolEvent",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/event/{eventId}",
            description = """Update school event information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event/{eventId}/members",
        method = [RequestMethod.GET],
        beanClass = EventService::class,
        beanMethod = "listSchoolEventMembers",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/event/{eventId}/members",
            description = """Returns list of school event members."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event/{eventId}/photo",
        method = [RequestMethod.POST],
        beanClass = EventService::class,
        beanMethod = "uploadSchoolEventPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/event/{eventId}/photo",
            description = """Upload school event photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/event/{eventId}",
        method = [RequestMethod.DELETE],
        beanClass = EventService::class,
        beanMethod = "deleteSchoolEvent",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/event/{eventId}",
            description = """Delete school event."""
        )
    )
)

annotation class EventRoutingDoc
