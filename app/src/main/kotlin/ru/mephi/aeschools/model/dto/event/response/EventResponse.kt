package ru.mephi.aeschools.model.dto.event.response

import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import java.time.LocalDateTime
import java.util.*

open class EventResponse(
    id: UUID,
    createdAt: LocalDateTime,
    title: String,
    content: String,
    tags: Set<String>,
    photoPath: String?,
    val location: String,
    val eventDate: LocalDateTime,
    val school: SchoolShortResponse?,
) : AbstractEventResponse(id, createdAt, title, content, tags, photoPath) {
    val type: String = "EVENT"
}
