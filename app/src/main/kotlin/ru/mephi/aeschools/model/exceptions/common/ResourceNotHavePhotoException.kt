package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.lang.reflect.Type

class ResourceNotHavePhotoException(
    type: Type,
    id: Any,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "Resource with type [${type.typeName}] and id [$id] haven't photo",
    errorDescription = ErrorDescription(
        ru = "У этого ресурса нет фотографии",
        en = "This resource haven't photo"
    )
)
