package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.LibraryService
import ru.mephi.aeschools.util.constants.API_VERSION_1


@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/library/{id}",
        method = arrayOf(RequestMethod.GET),
        beanClass = LibraryService::class,
        beanMethod = "getById",
        operation = Operation(
            operationId = " GET public/library/{id}",
            description = "Get full library record by id"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/library",
        method = arrayOf(RequestMethod.GET),
        beanClass = LibraryService::class,
        beanMethod = "fullList",
        operation = Operation(
            operationId = " GET public/library/list",
            description = "Get list (page pagination) of library records"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/library",
        method = arrayOf(RequestMethod.POST),
        beanClass = LibraryService::class,
        beanMethod = "create",
        operation = Operation(
            operationId = " POST admin/library",
            description = "Create new library record"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/library",
        method = arrayOf(RequestMethod.GET),
        beanClass = LibraryService::class,
        beanMethod = "fullList",
        operation = Operation(
            operationId = " GET admin/library/list",
            description = "Get list (page pagination) of library records"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/library/{id}",
        method = arrayOf(RequestMethod.GET),
        beanClass = LibraryService::class,
        beanMethod = "getById",
        operation = Operation(
            operationId = " GET admin/library/{id}",
            description = "Get library record by id"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/library/{id}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = LibraryService::class,
        beanMethod = "delete",
        operation = Operation(
            operationId = " DELETE admin/library/{id}",
            description = "Delete library record by id"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/library/{id}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = LibraryService::class,
        beanMethod = "update",
        operation = Operation(
            operationId = " PUT admin/library/{id}",
            description = "Update library record by id"
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/library/{id}/file",
        method = arrayOf(RequestMethod.POST),
        beanClass = LibraryService::class,
        beanMethod = "uploadFile",
        operation = Operation(
            operationId = " POST admin/library/{id}/file",
            description = "Upload file to library record"
        )
    )
)
annotation class LibraryRoutingDoc
