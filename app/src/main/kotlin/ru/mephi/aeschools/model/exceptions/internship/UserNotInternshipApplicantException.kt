package ru.mephi.aeschools.model.exceptions.internship

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserNotInternshipApplicantException(
    internshipId: UUID,
    userId: UUID,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "User with id [$userId] is not applicant of internship with id [$internshipId]",
    errorDescription = ErrorDescription(
        ru = "Вы не подавали заявку на стажировку",
        en = "You are not internship applicant"
    )
)
