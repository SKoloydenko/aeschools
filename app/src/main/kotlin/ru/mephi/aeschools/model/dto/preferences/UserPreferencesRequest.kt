package ru.mephi.aeschools.model.dto.preferences

class UserPreferencesRequest(
    val enableEmail: Boolean,
)
