package ru.mephi.aeschools.model.dto.excel.proxy

import ru.mephi.aeschools.database.entity.internship.Questionnaire
import ru.mephi.aeschools.model.dto.excel.ExcelFieldGetter

fun getQuestionnaireProxy(): List<ExcelFieldGetter<Questionnaire>> {
    return listOf(
        ExcelFieldGetter("id") { it.id },
        ExcelFieldGetter("Мотивационное письмо") { it.motivationalLetter },
        ExcelFieldGetter("Достижения") { it.achievements },
    )
}
