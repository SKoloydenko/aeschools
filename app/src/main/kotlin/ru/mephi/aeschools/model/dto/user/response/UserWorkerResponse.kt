package ru.mephi.aeschools.model.dto.user.response

import java.time.LocalDateTime
import java.util.*

class UserWorkerResponse(
    id: UUID,
    createdAt: LocalDateTime,
    name: String,
    surname: String,
    patronymic: String?,
    fullName: String,
    email: String,
    phone: String?,
    admin: Boolean,
    student: Boolean,
    moderator: Boolean,
    banned: Boolean,
    val organization: String?,
    val jobRole: String?,
) : UserResponse(
    id,
    createdAt,
    name,
    surname,
    patronymic,
    fullName,
    email,
    phone,
    admin,
    student,
    moderator,
    banned
)
