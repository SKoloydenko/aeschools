package ru.mephi.aeschools.model.mappers.report

import org.mapstruct.AfterMapping
import org.mapstruct.Context
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.mapstruct.Named
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.report.Section
import ru.mephi.aeschools.database.entity.report.SectionField
import ru.mephi.aeschools.database.entity.report.SectionFieldAnswer
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.section.request.CreateFieldRequest
import ru.mephi.aeschools.model.dto.section.request.CreateSectionRequest
import ru.mephi.aeschools.model.dto.section.request.UpdateFieldRequest
import ru.mephi.aeschools.model.dto.section.response.*
import ru.mephi.aeschools.model.mappers.school.SchoolMapper
import ru.mephi.aeschools.service.ReportService
import java.util.*

@Mapper(componentModel = "spring", uses = [SchoolMapper::class])
interface ReportMapper {

    fun asEntity(request: CreateSectionRequest): Section

    fun asEntity(request: CreateFieldRequest): SectionField

    fun asResponse(
        entity: Section,
        @Context service: ReportService,
        @Context schoolId: UUID? = null,
    ): SectionResponse

    fun asListResponses(
        entities: List<Section>,
        @Context service: ReportService,
        @Context schoolId: UUID? = null,
    ): List<SectionResponse>

    @Mappings(
        Mapping(
            target = "content",
            expression = "java(asListResponses(page.getContent(), service, schoolId))"
        )
    )
    fun asPageResponse(
        page: Page<Section>,
        @Context service: ReportService,
        @Context schoolId: UUID?,
    ): PageResponse<SectionResponse>


    @Named("asResponse 1")
    @Mapping(target = "isBlocked", source = "blocked")
    fun asResponse(
        entity: SectionField,
        @Context admin: Boolean? = null,
        @Context service: ReportService? = null,
    ): SectionFieldResponse

    fun asResponses(
        entity: Page<SectionField>,
        @Context admin: Boolean? = null,
        @Context service: ReportService? = null,
    ): List<SectionFieldResponse> {
        return entity.content.map { asResponse(it, admin, service) }
    }

    @AfterMapping
    fun asResponse(
        @MappingTarget target: SectionFieldResponse,
        entity: SectionField,
        @Context admin: Boolean? = null,
        @Context service: ReportService? = null,
    ) {
        if (admin == null || service == null || !admin) return
        target.totalSchool = service.getAllSchoolCount()
        target.gotAnswers = service.getAnswersCountToField(entity)
    }


    @Mapping(target = "isBlocked", source = "blocked")
    fun asResponseWithAnswer(entity: SectionField): SectionFieldWithAnswerResponse
    fun asResponse(entity: SectionFieldAnswer): SectionFieldAnswerResponse

    @Mappings(
        Mapping(target = "content", expression = "java(asResponses(page, admin, service))")
    )
    fun asPageFieldsResponse(
        page: Page<SectionField>,
        @Context admin: Boolean,
        @Context service: ReportService,
    ): PageResponse<SectionFieldResponse>


    @Mappings(
        Mapping(target = "content", expression = "java(asResponses(page, answers))")
    )
    fun asPageResponse(
        page: Page<SectionField>,
        answers: List<SectionFieldAnswer>,
        @Context service: ReportService,
        @Context schoolId: UUID,
    ): PageResponse<SectionFieldWithAnswerResponse>


    fun asResponses(
        page: Page<SectionField>,
        answers: List<SectionFieldAnswer>,
    ): List<SectionFieldWithAnswerResponse> {
        return page.content.map { target ->
            val response = asResponseWithAnswer(target)
            response.answer =
                answers.find { it.sectionField.id == target.id }?.let { asResponse(it) }
            response
        }
    }


    fun asResponseWithAnswer(answer: SectionFieldAnswer): SectionFieldWithAnswerResponse {
        val response = asResponseWithAnswer(answer.sectionField)
        response.answer = asResponse(answer)
        return response
    }

    fun asResponseWithAnswer(field: SectionField, answer: SectionFieldAnswer?): SectionFieldWithAnswerResponse {
        val response = asResponseWithAnswer(field)
        response.answer = if (answer == null) null else asResponse(answer)
        return response
    }

    fun update(@MappingTarget target: Section, request: CreateSectionRequest)
    fun update(@MappingTarget target: SectionField, request: UpdateFieldRequest)

    @AfterMapping
    fun asResponse(
        @MappingTarget target: SectionResponse,
        entity: Section,
        @Context service: ReportService,
        @Context schoolId: UUID? = null,
    ) {
        target.totalFields = service.getTotalFieldsCount(entity.id!!).toInt()
        if (schoolId != null) {     // moderator
            target.gotAnswers = service.getAnswersCountToSectionBySchoolId(entity.id!!, schoolId)
        } else {    //admin
            target.gotAnswers = service.getSchoolCountOfFullFillSection(entity)
            target.totalSchools = service.getAllSchoolCount()
        }
    }

    fun asPageResponse(answers: Page<SectionFieldAnswer>): PageResponse<FullSectionFieldAnswerResponse>
}
