package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class VerifyTokenExpiredException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Token to verify email was expire.",
    errorDescription = ErrorDescription(
        ru = "Срок подтверждения почты истёк",
        en = "Email verification link has expired"
    )
)