package ru.mephi.aeschools.model.mappers.preferences

import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import ru.mephi.aeschools.database.entity.preferences.UserPreferences
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesRequest
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesResponse

@Mapper(componentModel = "spring")
interface PreferencesMapper {
    fun asResponse(entity: UserPreferences): UserPreferencesResponse
    fun asEntity(request: UserPreferencesRequest, user: User): UserPreferences

    fun update(
        @MappingTarget preferences: UserPreferences,
        request: UserPreferencesRequest,
    ): UserPreferences
}
