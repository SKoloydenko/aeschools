package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException


class FileTooLargeException(
    maxSize: Long,
    currentSize: Long,
) : AppException(
    status = HttpStatus.PAYLOAD_TOO_LARGE,
    message = "File too large. Max size is $maxSize bytes but was got $currentSize bytes.",
    errorDescription = ErrorDescription(
        ru = "Слишком большой файл",
        en = "Too large file"
    )
)
