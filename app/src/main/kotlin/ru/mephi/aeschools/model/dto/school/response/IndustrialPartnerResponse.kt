package ru.mephi.aeschools.model.dto.school.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

class IndustrialPartnerResponse(
    id: UUID,
    createdAt: LocalDateTime,
    var name: String,
    var partnershipFormat: String,
    var schoolRole: String,
    var partnerRole: String,
    var cooperationDirection: String,
    override var photoPath: String?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
