package ru.mephi.aeschools.model.exceptions.report

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class FieldNotSupportsFreeFormAnswer(
    fieldId: UUID,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Field with id [$fieldId] is not supports free form support.",
    errorDescription = ErrorDescription(
        ru = "Это поле не поддерживает такой вид ответа.",
        en = "This field does not support this kind of response."
    )
)
