package ru.mephi.aeschools.model.dto.publication.request

import ru.mephi.aeschools.model.dto.AbstractContentRequest
import java.time.LocalDateTime

class PublicationRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    val tags: Set<String>,
) : AbstractContentRequest(title, content, publicationDate)
