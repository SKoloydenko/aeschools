package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.lang.reflect.Type

class ResourceNotFoundByNameException(
    type: Type,
    name: String,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "${(type as Class<*>).simpleName} with name [$name] not found",
    errorDescription = ErrorDescription(
        ru = "Ресурса с таким именем не существует",
        en = "There is no resource with this name"
    )
)
