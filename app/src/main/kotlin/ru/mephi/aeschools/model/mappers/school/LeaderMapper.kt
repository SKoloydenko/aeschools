package ru.mephi.aeschools.model.mappers.school

import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.school.Leader
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.model.dto.school.response.LeaderResponse

@Mapper(componentModel = "spring")
interface LeaderMapper {
    fun asEntity(request: LeaderRequest): Leader

    fun asResponse(leader: Leader): LeaderResponse
    fun asPageResponse(leaders: Page<Leader>): PageResponse<LeaderResponse>

    fun update(
        @MappingTarget leader: Leader,
        request: LeaderRequest,
    ): Leader
}
