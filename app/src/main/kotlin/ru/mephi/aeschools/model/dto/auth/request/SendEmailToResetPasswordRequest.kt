package ru.mephi.aeschools.model.dto.auth.request

class SendEmailToResetPasswordRequest(
    val email: String,
)
