package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class InvalidPathVariable(name: String) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Invalid URL parameter [$name]",
    errorDescription = ErrorDescription(
        ru = "Некорректный параметр URL",
        en = "Invalid URL parameter"
    )
)
