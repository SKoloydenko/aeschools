package ru.mephi.aeschools.model.exceptions.event

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserNotEventMemberException(
    eventId: UUID,
    userId: UUID,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "User with id [$userId] is not member of event with id [$eventId]",
    errorDescription = ErrorDescription(
        ru = "Вы не являетесь участником мероприятия",
        en = "You are not event member"
    )
)
