package ru.mephi.aeschools.model

import com.fasterxml.jackson.annotation.JsonIgnore

class ErrorDescription(
    val ru: String? = null,
    val en: String? = null,
) {
    companion object {
        @JsonIgnore
        val BAD_REQUEST = ErrorDescription("Bad request", "Bad request")
    }
}
