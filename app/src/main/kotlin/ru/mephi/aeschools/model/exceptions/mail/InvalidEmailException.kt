package ru.mephi.aeschools.model.exceptions.mail

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class InvalidEmailException(
    val email: String,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "This email [$email] is invalid.",
    errorDescription = ErrorDescription(
        ru = "Некорректная почта",
        en = "Incorrect email"
    )
)
