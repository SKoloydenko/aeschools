package ru.mephi.aeschools.model.dto.excel.proxy

import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.excel.ExcelFieldGetter

fun getUserProxy(): List<ExcelFieldGetter<User>> {
    return listOf(
        ExcelFieldGetter("id") { it.id },
        ExcelFieldGetter("ФИО") { it.fullName },
        ExcelFieldGetter("Почта") { it.email },
        ExcelFieldGetter("Телефон") { it.phone },
        ExcelFieldGetter("Дата регистрации") { it.createdAt },
    )
}
