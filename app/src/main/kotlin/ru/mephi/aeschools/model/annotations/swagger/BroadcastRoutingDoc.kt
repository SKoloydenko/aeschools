package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.BroadcastService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/admin/broadcast",
        method = arrayOf(RequestMethod.GET),
        beanClass = BroadcastService::class,
        beanMethod = "listBroadcasts",
        operation = Operation(
            operationId = "GET /admin/broadcast",
            description = """Returns list of broadcasts for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/broadcast/{broadcastId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = BroadcastService::class,
        beanMethod = "findBroadcastById",
        operation = Operation(
            operationId = "GET /admin/broadcast/{broadcastId}",
            description = """Returns broadcast by id for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/broadcast",
        method = arrayOf(RequestMethod.POST),
        beanClass = BroadcastService::class,
        beanMethod = "sendBroadcastMail",
        operation = Operation(
            operationId = "POST /admin/broadcast",
            description = """Creates broadcast for users, moderators or all."""
        )
    ),
)

annotation class BroadcastRoutingDoc
