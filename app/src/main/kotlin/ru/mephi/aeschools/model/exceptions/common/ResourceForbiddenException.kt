package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class ResourceForbiddenException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Resource is forbidden",
    errorDescription = ErrorDescription(
        ru = "Доступ к ресурсу запрещён",
        en = "Resource access denied"
    )
)
