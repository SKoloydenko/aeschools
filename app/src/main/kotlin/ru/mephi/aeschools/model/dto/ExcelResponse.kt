package ru.mephi.aeschools.model.dto

import org.springframework.core.io.ByteArrayResource

class ExcelResponse(
    val file: ByteArrayResource,
    val name: String
)