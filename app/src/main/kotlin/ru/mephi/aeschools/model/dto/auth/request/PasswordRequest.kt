package ru.mephi.aeschools.model.dto.auth.request

class PasswordRequest(
    val password: String,
)
