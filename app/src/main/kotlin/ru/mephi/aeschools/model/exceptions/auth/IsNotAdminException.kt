package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class IsNotAdminException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "User does not have admin role",
    errorDescription = ErrorDescription(
        ru = "Это действие может выполнить только администратор",
        en = "This action can only be guaranteed by the administrator"
    )
)
