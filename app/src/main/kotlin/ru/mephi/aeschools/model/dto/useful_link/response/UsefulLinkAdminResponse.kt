package ru.mephi.aeschools.model.dto.useful_link.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.*

class UsefulLinkAdminResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val link: String,
    override var photoPath: String?,
    val author: UserShortResponse?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
