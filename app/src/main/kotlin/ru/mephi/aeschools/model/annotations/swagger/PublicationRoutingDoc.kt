package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.PublicationService
import ru.mephi.aeschools.util.constants.API_VERSION_1


@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/publication",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "listPublications",
        operation = Operation(
            operationId = "GET /public/publication",
            description = """Returns list of all publications with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/publication",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "listSchoolPublications",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/publication",
            description = """Returns list of school publications with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/publication/{publicationId}",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "findPublicationById",
        operation = Operation(
            operationId = "GET public/publication/{publicationId}",
            description = """Returns publication by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/publication/{publicationId}",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "findSchoolPublicationById",
        operation = Operation(
            operationId = "GET public/school/{schoolId}/publication/{publicationId}",
            description = """Returns school publication by id."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/publication",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "listPublicationsForAdmin",
        operation = Operation(
            operationId = "GET /admin/publication",
            description = "Returns list of all publications for admin with pagination (includes future publications)."
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/publication",
        method = [RequestMethod.POST],
        beanClass = PublicationService::class,
        beanMethod = "createPublication",
        operation = Operation(
            operationId = "POST /admin/publication",
            description = """Create publication."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/publication/{publicationId}",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "findPublicationByIdForAdmin",
        operation = Operation(
            operationId = "GET admin/publication/{publicationId}",
            description = """Returns publication by id for admin (includes future publications)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/publication/{publicationId}",
        method = [RequestMethod.PUT],
        beanClass = PublicationService::class,
        beanMethod = "updatePublication",
        operation = Operation(
            operationId = "PUT /admin/publication/{publicationId}",
            description = """Update publication information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/publication/{publicationId}/photo",
        method = [RequestMethod.POST],
        beanClass = PublicationService::class,
        beanMethod = "uploadPublicationPhoto",
        operation = Operation(
            operationId = "POST /admin/publication/{publicationId}/photo",
            description = """Upload publication photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/publication/{publicationId}",
        method = [RequestMethod.DELETE],
        beanClass = PublicationService::class,
        beanMethod = "deletePublication",
        operation = Operation(
            operationId = "DELETE /admin/publication/{publicationId}",
            description = """Delete publication."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/publication",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "listSchoolPublicationsForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/publication",
            description = "Returns list of school publications for admin with pagination (includes future publications)."
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/publication",
        method = [RequestMethod.POST],
        beanClass = PublicationService::class,
        beanMethod = "createSchoolPublication",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/publication",
            description = """Create school publication."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/publication/{publicationId}",
        method = [RequestMethod.GET],
        beanClass = PublicationService::class,
        beanMethod = "findSchoolPublicationByIdForAdmin",
        operation = Operation(
            operationId = "GET admin/school/{schoolId}/publication/{publicationId}",
            description = """Returns publication by id for admin (includes future publications)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/publication/{publicationId}",
        method = [RequestMethod.PUT],
        beanClass = PublicationService::class,
        beanMethod = "updateSchoolPublication",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/publication/{publicationId}",
            description = """Update publication information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/publication/{publicationId}/photo",
        method = [RequestMethod.POST],
        beanClass = PublicationService::class,
        beanMethod = "uploadSchoolPublicationPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/publication/{publicationId}/photo",
            description = """Upload school publication photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/publication/{publicationId}",
        method = [RequestMethod.DELETE],
        beanClass = PublicationService::class,
        beanMethod = "deleteSchoolPublication",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/publication/{publicationId}",
            description = """Delete school publication."""
        )
    )
)
annotation class PublicationRoutingDoc
