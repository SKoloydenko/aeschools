package ru.mephi.aeschools.model.mappers.event

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.event.Event
import ru.mephi.aeschools.database.entity.event.ForeignEvent
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.event.request.AbstractEventRequest
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.model.dto.event.request.ForeignEventRequest
import ru.mephi.aeschools.model.dto.event.response.AbstractEventAdminResponse
import ru.mephi.aeschools.model.dto.event.response.AbstractEventResponse
import ru.mephi.aeschools.model.dto.event.response.EventAdminResponse
import ru.mephi.aeschools.model.dto.event.response.EventResponse
import ru.mephi.aeschools.model.dto.event.response.ForeignEventAdminResponse
import ru.mephi.aeschools.model.dto.event.response.ForeignEventResponse
import ru.mephi.aeschools.model.exceptions.AppException
import ru.mephi.aeschools.model.mappers.TagMapper

@Mapper(componentModel = "spring", uses = [TagMapper::class])
interface EventMapper {
    fun asEntity(request: AbstractEventRequest): AbstractEvent {
        return when (request) {
            is ForeignEventRequest -> asForeignEventEntity(request)
            is EventRequest -> asEventEntity(request)
            else -> throw AppException(
                status = HttpStatus.UNPROCESSABLE_ENTITY,
                message = "Invalid request"
            )
        }
    }

    @Mapping(target = "tags", ignore = true)
    fun asEventEntity(request: EventRequest): Event

    @Mapping(target = "tags", ignore = true)
    fun asForeignEventEntity(request: ForeignEventRequest): ForeignEvent

    fun asResponse(event: AbstractEvent): AbstractEventResponse {
        return when (event) {
            is ForeignEvent -> asForeignEventResponse(event)
            is Event -> asEventResponse(event)
            else -> throw AppException(
                status = HttpStatus.UNPROCESSABLE_ENTITY,
                message = "Invalid request"
            )
        }
    }

    @Mapping(target = "createdAt", source = "publicationDate")
    fun asEventResponse(event: Event): EventResponse
    @Mapping(target = "createdAt", source = "publicationDate")
    fun asForeignEventResponse(event: ForeignEvent): ForeignEventResponse

    fun asAdminResponse(event: AbstractEvent): AbstractEventAdminResponse {
        return when (event) {
            is ForeignEvent -> asForeignEventAdminResponse(event)
            is Event -> asEventAdminResponse(event)
            else -> throw AppException(
                status = HttpStatus.UNPROCESSABLE_ENTITY,
                message = "Invalid request"
            )
        }
    }

    fun asEventAdminResponse(event: Event): EventAdminResponse
    fun asForeignEventAdminResponse(event: ForeignEvent): ForeignEventAdminResponse

    fun asPageResponse(events: Page<AbstractEvent>): PageResponse<AbstractEventResponse>
    fun asAdminPageResponse(events: Page<AbstractEvent>): PageResponse<AbstractEventAdminResponse>

    fun update(@MappingTarget event: AbstractEvent, request: AbstractEventRequest): AbstractEvent {
        return if (event is ForeignEvent && request is ForeignEventRequest) updateForeignEvent(
            event,
            request
        )
        else if (event is Event && request is EventRequest) updateEvent(event, request)
        else throw AppException(
            status = HttpStatus.UNPROCESSABLE_ENTITY,
            message = "Invalid request"
        )
    }

    @Mapping(target = "tags", ignore = true)
    fun updateEvent(@MappingTarget event: Event, request: EventRequest): Event

    @Mapping(target = "tags", ignore = true)
    fun updateForeignEvent(
        @MappingTarget event: ForeignEvent,
        request: ForeignEventRequest,
    ): ForeignEvent
}
