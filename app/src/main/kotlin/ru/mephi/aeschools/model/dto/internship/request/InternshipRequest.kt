package ru.mephi.aeschools.model.dto.internship.request

import ru.mephi.aeschools.model.dto.AbstractContentRequest
import java.time.LocalDateTime

class InternshipRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    val tags: Set<String>,
    val company: String,
) : AbstractContentRequest(title, content, publicationDate)
