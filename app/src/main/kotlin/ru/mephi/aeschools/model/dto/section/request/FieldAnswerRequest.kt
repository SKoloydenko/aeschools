package ru.mephi.aeschools.model.dto.section.request

open class FieldAnswerRequest(
    val answer: String,
)
