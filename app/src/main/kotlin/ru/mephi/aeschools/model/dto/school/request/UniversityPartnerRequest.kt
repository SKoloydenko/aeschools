package ru.mephi.aeschools.model.dto.school.request

class UniversityPartnerRequest(
    val name: String,
    val description: String,
    val schoolRole: String,
    val universityRole: String,
    val cooperationDirection: String,
)
