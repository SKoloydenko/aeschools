package ru.mephi.aeschools.model.dto.user.request

import ru.mephi.aeschools.model.enums.user.EducationDegree

class RegistrationRequest(
    val email: String,
    val password: String,
    val enableEmail: Boolean,
    name: String,
    surname: String,
    patronymic: String? = null,
    phone: String,
    organization: String? = null,
    jobRole: String? = null,
    university: String? = null,
    educationDegree: EducationDegree? = null,
    grade: Int? = null,
    specialty: String? = null,
    student: Boolean,
) : UserRequest(
    name,
    surname,
    patronymic,
    phone,
    organization,
    jobRole,
    university,
    educationDegree,
    grade,
    specialty,
    student,
)
