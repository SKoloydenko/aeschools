package ru.mephi.aeschools.model.exceptions.library

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class FilePermissionDenied(
    path: String,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "File in path [$path] is not for public.",
    errorDescription = ErrorDescription(
        ru = "Доступ запрещён",
        en = "Permission denied"
    )
)
