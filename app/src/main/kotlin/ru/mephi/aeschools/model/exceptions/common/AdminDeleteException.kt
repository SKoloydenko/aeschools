package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class AdminDeleteException : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Can't delete admin",
    errorDescription = ErrorDescription(
        ru = "Нельзя удалить администратора",
        en = "Can't delete admin"
    )
)
