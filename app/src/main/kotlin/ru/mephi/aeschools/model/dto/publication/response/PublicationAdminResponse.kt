package ru.mephi.aeschools.model.dto.publication.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.*

class PublicationAdminResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val content: String,
    override var photoPath: String? = null,
    val publicationDate: LocalDateTime,
    val tags: Set<String>,
    val author: UserShortResponse?,
    val school: SchoolShortResponse?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
