package ru.mephi.aeschools.model.dto.section.request

import ru.mephi.aeschools.database.entity.report.SectionFieldType

class CreateFieldRequest(
    title: String,
    comment: String,
    isBlocked: Boolean,
    val type: SectionFieldType,
) : UpdateFieldRequest(title, comment, isBlocked)
