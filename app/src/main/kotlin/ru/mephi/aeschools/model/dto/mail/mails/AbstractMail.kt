package ru.mephi.aeschools.model.dto.mail.mails

import ru.mephi.aeschools.model.dto.mail.MailReceiver

abstract class AbstractMail(
    val receiversEmails: List<MailReceiver> = mutableListOf(),
)