package ru.mephi.aeschools.model.dto.user.response

import ru.mephi.aeschools.model.enums.user.EducationDegree
import java.time.LocalDateTime
import java.util.*

class UserStudentResponse(
    id: UUID,
    createdAt: LocalDateTime,
    name: String,
    surname: String,
    patronymic: String?,
    fullName: String,
    email: String,
    phone: String?,
    admin: Boolean,
    student: Boolean,
    moderator: Boolean,
    banned: Boolean,
    val university: String?,
    val educationDegree: EducationDegree?,
    val grade: Int?,
    val specialty: String?,
) : UserResponse(
    id,
    createdAt,
    name,
    surname,
    patronymic,
    fullName,
    email,
    phone,
    admin,
    student,
    moderator,
    banned
)
