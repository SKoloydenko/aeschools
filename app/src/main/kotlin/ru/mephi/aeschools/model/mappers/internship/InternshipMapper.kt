package ru.mephi.aeschools.model.mappers.internship

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.model.dto.internship.response.InternshipAdminResponse
import ru.mephi.aeschools.model.dto.internship.response.InternshipResponse
import ru.mephi.aeschools.model.mappers.TagMapper

@Mapper(componentModel = "spring", uses = [TagMapper::class])
interface InternshipMapper {
    @Mapping(target = "tags", ignore = true)
    fun asEntity(request: InternshipRequest): Internship

    @Mapping(target = "createdAt", source = "publicationDate")
    fun asResponse(internship: Internship): InternshipResponse
    fun asAdminResponse(internship: Internship): InternshipAdminResponse

    fun asPageResponse(internships: Page<Internship>): PageResponse<InternshipResponse>
    fun asPageAdminResponse(internships: Page<Internship>): PageResponse<InternshipAdminResponse>

    @Mapping(target = "tags", ignore = true)
    fun update(
        @MappingTarget internship: Internship,
        request: InternshipRequest,
    ): Internship
}
