package ru.mephi.aeschools.model.dto.school.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

class SchoolResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val university: String,
    val name: String,
    val shortName: String,
    val actionScope: String,
    val about: String,
    val educationalActivities: String,
    val researchActivities: String,
    val innovationalActivities: String,
    override var photoPath: String? = null,
    val contacts: ContactsResponse?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
