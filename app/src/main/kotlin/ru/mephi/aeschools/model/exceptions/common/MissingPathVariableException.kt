package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class MissingPathVariableException(
    variable: String,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Path variable [$variable] is not provided",
    errorDescription = ErrorDescription.BAD_REQUEST
)
