package ru.mephi.aeschools.model.dto.event.response

import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import java.time.LocalDateTime
import java.util.*

class ForeignEventResponse(
    id: UUID,
    createdAt: LocalDateTime,
    title: String,
    content: String,
    photoPath: String?,
    tags: Set<String>,
    val school: SchoolShortResponse?,
) : AbstractEventResponse(
    id, createdAt, title, content, tags, photoPath
) {
    val type: String = "FOREIGN_EVENT"
}
