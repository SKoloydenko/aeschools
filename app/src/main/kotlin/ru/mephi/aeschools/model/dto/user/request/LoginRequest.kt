package ru.mephi.aeschools.model.dto.user.request

class LoginRequest(
    val email: String,
    val password: String,
    val fingerprint: String,
)
