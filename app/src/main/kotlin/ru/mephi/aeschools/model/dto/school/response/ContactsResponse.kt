package ru.mephi.aeschools.model.dto.school.response

class ContactsResponse(
    val website: String?,
    val email: String?,
    val phone: String?,
)
