package ru.mephi.aeschools.model.dto.preferences

class UserPreferencesResponse(
    val enableEmail: Boolean,
)
