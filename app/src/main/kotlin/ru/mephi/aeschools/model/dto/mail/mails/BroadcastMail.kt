package ru.mephi.aeschools.model.dto.mail.mails

import ru.mephi.aeschools.model.dto.mail.MailReceiver

class BroadcastMail(
    val title: String,
    val text: String,
    val unsubscribeLink: String,
    receiversEmails: List<MailReceiver>,
) : AbstractMail(receiversEmails)
