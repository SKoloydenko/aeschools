package ru.mephi.aeschools.model.dto.school.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

class UniversityPartnerResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val name: String,
    val description: String,
    val schoolRole: String,
    val universityRole: String,
    val cooperationDirection: String,
    override var photoPath: String?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
