package ru.mephi.aeschools.model.dto.user

import java.util.*

class UserPrincipal(val userId: UUID)
