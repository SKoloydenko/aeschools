package ru.mephi.aeschools.model.mappers.internship

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.internship.Questionnaire
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.model.dto.internship.response.InternshipResponse
import ru.mephi.aeschools.model.dto.internship.response.QuestionnaireResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.model.mappers.TagMapper
import ru.mephi.aeschools.model.mappers.user.UserMapper

@Mapper(componentModel = "spring", uses = [UserMapper::class, TagMapper::class])
interface QuestionnaireMapper {
    fun asEntity(request: QuestionnaireRequest): Questionnaire

    fun asResponse(questionnaire: Questionnaire): QuestionnaireResponse
    @Mapping(target = ".", source = "internship")
    fun asInternshipResponse(questionnaire: Questionnaire): InternshipResponse
    @Mapping(target = ".", source = "user")
    fun asUserResponse(questionnaire: Questionnaire): UserShortResponse

    fun asPageResponse(questionnaires: Page<Questionnaire>): PageResponse<QuestionnaireResponse>
    fun asPageInternshipResponse(questionnaires: Page<Questionnaire>): PageResponse<InternshipResponse>
    fun asPageUserResponse(questionnaires: Page<Questionnaire>): PageResponse<UserShortResponse>

    fun update(
        @MappingTarget questionnaire: Questionnaire,
        request: QuestionnaireRequest,
    ): Questionnaire
}
