package ru.mephi.aeschools.model.dto.section.response

import ru.mephi.aeschools.database.entity.report.SectionFieldType
import java.time.LocalDateTime
import java.util.*


class SectionFieldWithAnswerResponse(
    id: UUID,
    createdAt: LocalDateTime,
    title: String,
    comment: String,
    isBlocked: Boolean,
    type: SectionFieldType,
    var answer: SectionFieldAnswerResponse? = null,
) : SectionFieldResponse(id, createdAt, title, comment, isBlocked, type)
