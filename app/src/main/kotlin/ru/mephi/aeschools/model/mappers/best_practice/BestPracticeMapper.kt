package ru.mephi.aeschools.model.mappers.best_practice

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.best_practice.BestPractice
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.best_practice.request.BestPracticeRequest
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeAdminResponse
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeResponse
import ru.mephi.aeschools.model.mappers.TagMapper

@Mapper(componentModel = "spring", uses = [TagMapper::class])
interface BestPracticeMapper {
    @Mapping(target = "tags", ignore = true)
    fun asEntity(request: BestPracticeRequest): BestPractice

    @Mapping(target = "createdAt", source = "publicationDate")
    fun asResponse(bestPractice: BestPractice): BestPracticeResponse
    fun asAdminResponse(bestPractice: BestPractice): BestPracticeAdminResponse

    fun asPageResponse(bestPractices: Page<BestPractice>): PageResponse<BestPracticeResponse>
    fun asAdminPageResponse(bestPractices: Page<BestPractice>): PageResponse<BestPracticeAdminResponse>

    @Mapping(target = "tags", ignore = true)
    fun update(
        @MappingTarget bestPractice: BestPractice,
        request: BestPracticeRequest,
    ): BestPractice
}
