package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.lang.reflect.Type
import java.util.*

class ResourceNotFoundException() : AppException(
    status = HttpStatus.NOT_FOUND,
    errorDescription = ErrorDescription(
        ru = "Ресурс не найден",
        en = "Resource not found"
    )
) {

    override lateinit var message: String

    constructor(type: Type, id: UUID) : this() {
        message = "${(type as Class<*>).simpleName} with id [$id] not found"
    }

    constructor(type: Type, name: String) : this() {
        message = "${(type as Class<*>).simpleName} with name [$name] not found"
    }
}
