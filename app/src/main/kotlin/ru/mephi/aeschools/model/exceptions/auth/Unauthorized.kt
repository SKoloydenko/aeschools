package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class Unauthorized : AppException(
    status = HttpStatus.UNAUTHORIZED,
    message = "User is unauthorized",
    errorDescription = ErrorDescription(
        ru = "Пользователь не авторизован",
        en = "User is unauthorized"
    )
)

