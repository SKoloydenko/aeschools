package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.UserPreferencesService
import ru.mephi.aeschools.util.constants.API_VERSION_1


@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/user/me/preferences",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserPreferencesService::class,
        beanMethod = "getPreferencesById",
        operation = Operation(
            operationId = "GET /user/me/preferences",
            description = """Returns self user preferences."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me/preferences",
        method = arrayOf(RequestMethod.PUT),
        beanClass = UserPreferencesService::class,
        beanMethod = "updatePreferences",
        operation = Operation(
            operationId = "PUT /user/me/preferences",
            description = """Updates self user preferences."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/preferences/{id}",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserPreferencesService::class,
        beanMethod = "getPreferencesById",
        operation = Operation(
            operationId = "admin/preferences/{id}",
            description = """Returns preferences of entered user."""
        )
    ),
)
annotation class PreferencesRoutingDoc
