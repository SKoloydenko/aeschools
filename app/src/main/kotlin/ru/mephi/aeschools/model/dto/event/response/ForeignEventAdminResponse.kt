package ru.mephi.aeschools.model.dto.event.response

import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.*

class ForeignEventAdminResponse(
    id: UUID,
    createdAt: LocalDateTime,
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    tags: Set<String>,
    photoPath: String?,
    author: UserShortResponse?,
    val school: SchoolShortResponse?,
) : AbstractEventAdminResponse(
    id,
    createdAt,
    title,
    content,
    publicationDate,
    tags,
    photoPath,
    author
) {
    val type: String = "FOREIGN_EVENT"
}
