package ru.mephi.aeschools.model.exceptions.internship

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserAlreadyInternshipApplicantException(
    internshipId: UUID,
    userId: UUID,
) : AppException(
    status = HttpStatus.CONFLICT,
    message = "User with id [$userId] is already applicant of internship with id [$internshipId]",
    errorDescription = ErrorDescription(
        ru = "Вы уже подавали заявку на стажировку",
        en = "You are already applicant of internship"
    )
)
