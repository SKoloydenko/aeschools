package ru.mephi.aeschools.model.dto.best_practice.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import java.time.LocalDateTime
import java.util.*

class BestPracticeResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val content: String,
    override var photoPath: String?,
    val tags: Set<String>,
    val school: SchoolShortResponse?,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
