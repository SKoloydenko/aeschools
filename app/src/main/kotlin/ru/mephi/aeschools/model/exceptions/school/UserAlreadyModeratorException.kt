package ru.mephi.aeschools.model.exceptions.school

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class UserAlreadyModeratorException(
    userId: UUID,
) : AppException(
    status = HttpStatus.CONFLICT,
    message = "User with id [$userId] is already a moderator",
    errorDescription = ErrorDescription(
        ru = "Пользователь уже является модератором площадки",
        en = "User is already a moderator of a school"
    )
)
