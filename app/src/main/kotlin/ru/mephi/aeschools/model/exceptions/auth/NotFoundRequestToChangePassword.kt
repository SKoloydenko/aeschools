package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.dto.auth.request.ResetPasswordRequest
import ru.mephi.aeschools.model.exceptions.AppException

class NotFoundRequestToChangePassword(
    val request: ResetPasswordRequest,
) : AppException(
    status = HttpStatus.NOT_FOUND,
    message = "Haven't request to reset password with token [${request.token}].",
    errorDescription = ErrorDescription(
        ru = "Запрос на смену пароля не найден",
        en = "Request to change password not found"
    )
)
