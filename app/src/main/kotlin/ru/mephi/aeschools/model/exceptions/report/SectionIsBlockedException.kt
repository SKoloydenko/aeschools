package ru.mephi.aeschools.model.exceptions.report

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class SectionIsBlockedException(
    sectionId: UUID,
) : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Section with id [$sectionId] is blocked.",
    errorDescription = ErrorDescription(
        ru = "Эта секция заблокирована",
        en = "This section is blocked"
    )
)
