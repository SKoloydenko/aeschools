package ru.mephi.aeschools.model.dto.event.request

import java.time.LocalDateTime

open class EventRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    tags: Set<String>,
    val location: String,
    val eventDate: LocalDateTime,
) : AbstractEventRequest(title, content, publicationDate, tags)
