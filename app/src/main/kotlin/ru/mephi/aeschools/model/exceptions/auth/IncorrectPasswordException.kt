package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class IncorrectPasswordException : AppException(
    status = HttpStatus.UNAUTHORIZED,
    message = "Password is incorrect",
    errorDescription = ErrorDescription(
        ru = "Неверный пароль",
        en = "Password is incorrect"
    )
)
