package ru.mephi.aeschools.model.dto.excel.proxy

import ru.mephi.aeschools.database.entity.report.SectionFieldAnswer
import ru.mephi.aeschools.database.entity.school.School

class ReportExcelProxyHelper(
    val school: School,
    val answers: List<SectionFieldAnswer>,
)
