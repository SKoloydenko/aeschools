package ru.mephi.aeschools.model.dto

class ValidationError(
    val field: String,
    val rejectedValue: Any?,
    val code: String?,
)
