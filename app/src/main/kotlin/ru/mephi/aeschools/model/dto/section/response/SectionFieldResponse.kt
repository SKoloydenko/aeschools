package ru.mephi.aeschools.model.dto.section.response

import ru.mephi.aeschools.database.entity.report.SectionFieldType
import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

open class SectionFieldResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val comment: String,
    val isBlocked: Boolean,
    val type: SectionFieldType,

    var totalSchool: Int = -1,
    var gotAnswers: Int = -1
) : AbstractCreatedAtResponse(id, createdAt)
