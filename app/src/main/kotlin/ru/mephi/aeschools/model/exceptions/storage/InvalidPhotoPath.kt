package ru.mephi.aeschools.model.exceptions.storage

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class InvalidPhotoPath(fullPhotoPath: String) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Invalid photo path [$fullPhotoPath]",
    errorDescription = ErrorDescription(
        ru = "Некорректный путь к фотографии",
        en = "Invalid photo path"
    )
)
