package ru.mephi.aeschools.model.dto.library.response

import ru.mephi.aeschools.model.dto.library.LibraryFileType
import java.time.LocalDateTime
import java.util.*

class LibraryRecordResponse(
    id: UUID,
    createdAt: LocalDateTime,
    title: String,
    val content: String,
    link: String? = null,
) : LibraryRecordShortResponse(id, createdAt, title, link)
