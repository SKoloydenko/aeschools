package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class PrincipalNotFoundException : AppException(
    status = HttpStatus.UNAUTHORIZED,
    message = "Principal not found",
    errorDescription = ErrorDescription(
        ru = "Не авторизован",
        en = "Not authorized"
    )
)
