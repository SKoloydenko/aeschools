package ru.mephi.aeschools.model.dto.section.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import java.time.LocalDateTime
import java.util.*

class SectionResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    val isBlocked: Boolean,

    var totalFields: Int = -1,
    var gotAnswers: Int = -1,
    var totalSchools: Int = -1,
) : AbstractCreatedAtResponse(id, createdAt)
