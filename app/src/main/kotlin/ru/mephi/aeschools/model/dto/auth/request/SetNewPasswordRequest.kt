package ru.mephi.aeschools.model.dto.auth.request

class SetNewPasswordRequest(
    val oldPassword: String,
    val newPassword: String,
)
