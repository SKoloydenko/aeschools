package ru.mephi.aeschools.model.dto.event.request

import java.time.LocalDateTime

class ForeignEventRequest(
    title: String,
    content: String,
    publicationDate: LocalDateTime,
    tags: Set<String>,
) : AbstractEventRequest(title, content, publicationDate, tags)
