package ru.mephi.aeschools.model.mappers.library

import org.mapstruct.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.library.LibraryRecord
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.library.LibraryFileType
import ru.mephi.aeschools.model.dto.library.request.LibraryRecordRequest
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordResponse
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordShortResponse
import ru.mephi.aeschools.service.impl.storage.StorageFileManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper


@Mapper(componentModel = "spring")
abstract class LibraryMapper {
    @Autowired
    protected lateinit var fileManager: StorageManagerHelper

    abstract fun asEntity(request: LibraryRecordRequest): LibraryRecord

    @Named("full")
    @Mapping(target = "type", expression = "java(fileManager.isSelfFile(entity.getLink()) " +
            "? ru.mephi.aeschools.model.dto.library.LibraryFileType.INTERNAL " +
            ": ru.mephi.aeschools.model.dto.library.LibraryFileType.EXTERNAL)")
    abstract fun asResponse(entity: LibraryRecord): LibraryRecordResponse

    @Named("short")
    @Mapping(target = "type", expression = "java(fileManager.isSelfFile(entity.getLink()) " +
            "? ru.mephi.aeschools.model.dto.library.LibraryFileType.INTERNAL " +
            ": ru.mephi.aeschools.model.dto.library.LibraryFileType.EXTERNAL)")
    abstract fun asShortResponse(entity: LibraryRecord): LibraryRecordShortResponse

    @IterableMapping(qualifiedByName = ["full"])
    abstract fun asPageResponse(page: Page<LibraryRecord>): PageResponse<LibraryRecordResponse>

    @IterableMapping(qualifiedByName = ["short"])
    abstract fun asShortPageResponse(page: Page<LibraryRecord>): PageResponse<LibraryRecordShortResponse>

    abstract fun update(@MappingTarget target: LibraryRecord, request: LibraryRecordRequest)

    @AfterMapping
    fun asPageResponse(@MappingTarget res: PageResponse<LibraryRecordResponse>, page: Page<LibraryRecord>) {
        res.content.forEach {
            it.type= if (fileManager.isSelfFile(it.link)) LibraryFileType.INTERNAL
                else LibraryFileType.EXTERNAL }
    }

    @AfterMapping
    fun asShortPageResponse(@MappingTarget res: PageResponse<LibraryRecordShortResponse>, page: Page<LibraryRecord>) {
        res.content.forEach {
            it.type = if (fileManager.isSelfFile(it.link)) LibraryFileType.INTERNAL
            else LibraryFileType.EXTERNAL }
    }
}
