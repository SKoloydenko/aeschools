package ru.mephi.aeschools.model.exceptions

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription

class InternalServerError(
    error: Throwable,
) : AppException(
    status = HttpStatus.INTERNAL_SERVER_ERROR,
    message = error.message ?: HttpStatus.INTERNAL_SERVER_ERROR.toString(),
    errorDescription = ErrorDescription(
        ru = "Неизвестная ошибка",
        en = "Internal error"
    ),
    cause = error
)
