package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class MissingParameterException(
    parameter: String,
    override val message: String = "Missing parameter [$parameter]",
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Missing parameter [$parameter]",
    errorDescription = ErrorDescription.BAD_REQUEST
)
