package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/uploads/{dir}/{fileName}",
        method = arrayOf(RequestMethod.GET),
        beanClass = StorageImageManager::class,
        beanMethod = "retrieve",
        operation = Operation(
            operationId = "/uploads/{dir}/{fileName}",
            description = """Returns file from server storage."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/uploads/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = ContentPhotoService::class,
        beanMethod = "createContentPhoto",
        operation = Operation(
            operationId = "/admin/uploads/photo",
            description = """Upload content photo."""
        )
    ),
)
annotation class StaticRoutingDoc
