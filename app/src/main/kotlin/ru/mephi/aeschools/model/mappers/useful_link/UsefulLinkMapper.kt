package ru.mephi.aeschools.model.mappers.useful_link

import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.Mappings
import org.springframework.data.domain.Page
import ru.mephi.aeschools.database.entity.useful_link.UsefulLink
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkAdminResponse
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkResponse

@Mapper(componentModel = "spring")
interface UsefulLinkMapper {
    @Mappings(
        Mapping(target = "author", ignore = true),
    )
    fun asEntity(request: UsefulLinkRequest): UsefulLink

    fun asResponse(usefulLink: UsefulLink): UsefulLinkResponse
    fun asAdminResponse(usefulLink: UsefulLink): UsefulLinkAdminResponse

    fun asPageResponse(usefulLinks: Page<UsefulLink>): PageResponse<UsefulLinkResponse>
    fun asAdminPageResponse(usefulLinks: Page<UsefulLink>): PageResponse<UsefulLinkAdminResponse>

    fun update(
        @MappingTarget usefulLink: UsefulLink,
        request: UsefulLinkRequest,
    ): UsefulLink
}
