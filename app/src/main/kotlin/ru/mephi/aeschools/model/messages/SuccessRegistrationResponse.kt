package ru.mephi.aeschools.model.messages

import ru.mephi.aeschools.model.ErrorDescription
import java.util.*


class SuccessRegistrationResponse(
    val userId: UUID,
    isVerifyEmailEnable: Boolean,
    verifyEmailTokenTtl: Long,
) {
    val description: ErrorDescription = ErrorDescription(
        ru = "Регистрация успешна!" + (if (isVerifyEmailEnable) " Чтобы авторизоваться, подтвердите email по ссылке на почте в течение $verifyEmailTokenTtl минут." else ""),
        en = "Registration successful!" + (if (isVerifyEmailEnable) " To log in, confirm your email using the link in the mail within $verifyEmailTokenTtl minutes." else "")
    )
}



