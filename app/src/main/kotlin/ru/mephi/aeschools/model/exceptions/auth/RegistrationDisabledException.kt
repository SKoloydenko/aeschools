package ru.mephi.aeschools.model.exceptions.auth

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class RegistrationDisabledException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "Registration disabled. Try again later",
    errorDescription = ErrorDescription(
        ru = "Регистрация временно отключена. Попробуйте позже",
        en = "Registration is temporarily disabled. Try again later"
    )
)
