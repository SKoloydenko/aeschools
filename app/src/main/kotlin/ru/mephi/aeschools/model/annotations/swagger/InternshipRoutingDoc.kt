package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.InternshipService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listInternships",
        operation = Operation(
            operationId = "GET /public/internship",
            description = """Returns list of internships with pagination for all users."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/internship/{internshipId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "findInternshipById",
        operation = Operation(
            operationId = "GET /public/internship/{internshipId}",
            description = """Returns internship by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listSchoolInternships",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/internship",
            description = """Returns list of school internships."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/internship/{internshipId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "findSchoolInternshipById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/internship/{internshipId}",
            description = """Returns school internship by id."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/user/me/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listInternshipsForUser",
        operation = Operation(
            operationId = "GET /user/me/internship",
            description = """Returns user internships."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listInternshipsForAdmin",
        operation = Operation(
            operationId = "GET /admin/internship",
            description = """Returns list of internships with pagination for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/user/{userId}/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listInternshipsForUser",
        operation = Operation(
            operationId = "GET /admin/user/{userId}/internship",
            description = """Returns list user's internships with pagination for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/internship",
        method = arrayOf(RequestMethod.POST),
        beanClass = InternshipService::class,
        beanMethod = "createInternship",
        operation = Operation(
            operationId = "POST /admin/internship",
            description = """Create internship."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/internship/{internshipId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "findInternshipById",
        operation = Operation(
            operationId = "GET /admin/internship/{internshipId}",
            description = """Returns internship by id for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/internship/{internshipId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = InternshipService::class,
        beanMethod = "updateInternship",
        operation = Operation(
            operationId = "PUT /admin/internship/{internshipId}",
            description = """Update internship information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/internship/{internshipId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = InternshipService::class,
        beanMethod = "uploadInternshipPhoto",
        operation = Operation(
            operationId = "POST /admin/internship/{internshipId}/photo",
            description = """Upload internship photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/internship/{internshipId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = InternshipService::class,
        beanMethod = "deleteInternship",
        operation = Operation(
            operationId = "DELETE /admin/internship/{internshipId}",
            description = """Delete internship."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listSchoolInternshipsForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/internship",
            description = """Returns list of school internships for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship",
        method = arrayOf(RequestMethod.POST),
        beanClass = InternshipService::class,
        beanMethod = "createSchoolInternship",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/internship",
            description = """Create internship for school."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship/{internshipId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "findSchoolInternshipById",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/internship/{internshipId}",
            description = """Returns school internship by id for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship/{internshipId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = InternshipService::class,
        beanMethod = "updateSchoolInternship",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/internship/{internshipId}",
            description = """Update school internship."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship/{internshipId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = InternshipService::class,
        beanMethod = "uploadSchoolInternshipPhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/internship/{internshipId}/photo",
            description = """Upload school internship photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship/{internshipId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = InternshipService::class,
        beanMethod = "deleteSchoolInternship",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/internship/{internshipId}",
            description = """Delete school internship."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/internship/{internshipId}/applicant",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listInternshipApplicants",
        operation = Operation(
            operationId = "GET /admin/internship/{internshipId}/applicant",
            description = """Returns internship applicants."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/internship/{internshipId}/applicant",
        method = arrayOf(RequestMethod.GET),
        beanClass = InternshipService::class,
        beanMethod = "listSchoolInternshipApplications",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/internship/{internshipId}/applicant",
            description = """Returns list of school internship applicants."""
        )
    ),
)

annotation class InternshipRoutingDoc
