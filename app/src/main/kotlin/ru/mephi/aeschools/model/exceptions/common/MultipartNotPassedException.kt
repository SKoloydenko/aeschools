package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class MultipartNotPassedException : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Invalid multipart request",
    errorDescription = ErrorDescription.BAD_REQUEST
)
