package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.CourseService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/course",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "listCourses",
        operation = Operation(
            operationId = "GET /public/course",
            description = """Returns list of all courses with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/course/{courseId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "findCourseById",
        operation = Operation(
            operationId = "GET /public/course/{courseId}",
            description = """Returns course by id."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/course",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "listSchoolCourses",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/course",
            description = """Returns list of school courses with pagination."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/public/school/{schoolId}/course/{courseId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "findSchoolCourseById",
        operation = Operation(
            operationId = "GET /public/school/{schoolId}/course/{courseId}",
            description = """Returns school course by id."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/course",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "listCoursesForAdmin",
        operation = Operation(
            operationId = "GET /admin/course",
            description = """Returns list courses with pagination for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/course/{courseId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "findCourseByIdForAdmin",
        operation = Operation(
            operationId = "GET /admin/course/{courseId}",
            description = """Returns course by id for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/course",
        method = arrayOf(RequestMethod.POST),
        beanClass = CourseService::class,
        beanMethod = "createCourse",
        operation = Operation(
            operationId = "POST /admin/course",
            description = """Create course."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/course/{courseId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = CourseService::class,
        beanMethod = "updateCourse",
        operation = Operation(
            operationId = "PUT /admin/course/{courseId}",
            description = """Update course."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/course/{courseId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = CourseService::class,
        beanMethod = "uploadCoursePhoto",
        operation = Operation(
            operationId = "POST /admin/course/{courseId}/photo",
            description = """Upload course photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/course/{courseId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = CourseService::class,
        beanMethod = "deleteCourse",
        operation = Operation(
            operationId = "DELETE /admin/course/{courseId}",
            description = """Delete course."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/course",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "listSchoolCoursesForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/course",
            description = """Returns list of school courses with pagination for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/course/{courseId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = CourseService::class,
        beanMethod = "findSchoolCourseByIdForAdmin",
        operation = Operation(
            operationId = "GET /admin/school/{schoolId}/course/{courseId}",
            description = """Returns school course by id for admin (includes author)."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/course",
        method = arrayOf(RequestMethod.POST),
        beanClass = CourseService::class,
        beanMethod = "createSchoolCourse",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/course",
            description = """Create school course."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/course/{courseId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = CourseService::class,
        beanMethod = "updateSchoolCourse",
        operation = Operation(
            operationId = "PUT /admin/school/{schoolId}/course/{courseId}",
            description = """Update school course."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/course/{courseId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = CourseService::class,
        beanMethod = "uploadSchoolCoursePhoto",
        operation = Operation(
            operationId = "POST /admin/school/{schoolId}/course/{courseId}/photo",
            description = """Upload school course photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/school/{schoolId}/course/{courseId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = CourseService::class,
        beanMethod = "deleteSchoolCourse",
        operation = Operation(
            operationId = "DELETE /admin/school/{schoolId}/course/{courseId}",
            description = """Delete school course."""
        )
    ),
)

annotation class CourseRoutingDoc
