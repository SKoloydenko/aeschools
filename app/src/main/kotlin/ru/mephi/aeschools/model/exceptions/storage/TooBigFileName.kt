package ru.mephi.aeschools.model.exceptions.storage

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class TooBigFileName : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Too big file name",
    errorDescription = ErrorDescription(
        ru = "Слишком длинное имя файла",
        en = "Too big file name"
    )
)
