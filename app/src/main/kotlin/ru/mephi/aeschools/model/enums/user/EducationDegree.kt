package ru.mephi.aeschools.model.enums.user

enum class EducationDegree(val degree: String) {
    SPECIALTY("Специалитет"),
    BACHELOR("Бакалавриат"),
    MAGISTRACY("Магистратура"),
}
