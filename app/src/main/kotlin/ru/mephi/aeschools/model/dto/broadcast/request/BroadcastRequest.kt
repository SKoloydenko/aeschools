package ru.mephi.aeschools.model.dto.broadcast.request

import ru.mephi.aeschools.model.enums.BroadcastType

class BroadcastRequest(
    val title: String,
    val text: String,
    val type: BroadcastType,
    val ignorePreferences: Boolean,
)
