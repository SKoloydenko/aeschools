package ru.mephi.aeschools.model.dto.mail.mails

import ru.mephi.aeschools.model.dto.mail.MailReceiver


class VerifyEmailMail(
    link: String,
    receiverEmail: String,
    val tokenTtl: Long,
) : AbstractMail(listOf(MailReceiver(receiverEmail, mapOf("link" to link))))
