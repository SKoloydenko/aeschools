package ru.mephi.aeschools.model.dto.user.request

class RefreshRequest(
    val fingerprint: String,
) {
    lateinit var refreshToken: String
}
