package ru.mephi.aeschools.model.dto.library.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.library.LibraryFileType
import java.time.LocalDateTime
import java.util.*

open class LibraryRecordShortResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val title: String,
    var link: String? = null,
) : AbstractCreatedAtResponse(id, createdAt) {
    var type: LibraryFileType = LibraryFileType.EXTERNAL
}
