package ru.mephi.aeschools.model.dto

import java.util.UUID

class ContentPhotoRequest(
    val id: UUID,
    val content: String,
)
