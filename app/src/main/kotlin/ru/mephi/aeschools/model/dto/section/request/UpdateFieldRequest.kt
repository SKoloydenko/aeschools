package ru.mephi.aeschools.model.dto.section.request

open class UpdateFieldRequest(
    val title: String,
    val comment: String,
    val isBlocked: Boolean,
)
