package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.util.constants.API_VERSION_1


@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/user/me",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserService::class,
        beanMethod = "findById",
        operation = Operation(
            operationId = "GET /user/me",
            description = """Get self user information without masking."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me/preview",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserService::class,
        beanMethod = "previewById",
        operation = Operation(
            operationId = "GET /user/me/preview",
            description = """Get user preview: full name and email."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me/school",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserService::class,
        beanMethod = "findSchool",
        operation = Operation(
            operationId = "GET /user/me/school",
            description = """Get user's school."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me",
        method = arrayOf(RequestMethod.PUT),
        beanClass = UserService::class,
        beanMethod = "update",
        operation = Operation(
            operationId = "PUT /user/me",
            description = """Update self user information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/user/me",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = UserService::class,
        beanMethod = "delete",
        operation = Operation(
            operationId = "DELETE /user/me",
            description = """Delete self account."""
        )
    ),


    RouterOperation(
        path = "$API_VERSION_1/admin/user",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserService::class,
        beanMethod = "list",
        operation = Operation(
            operationId = "GET /admin/user",
            description = """List users for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/user/{userId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = UserService::class,
        beanMethod = "findById",
        operation = Operation(
            operationId = "GET /admin/user/{userId}",
            description = """Get user information by id for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/user/{userId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = UserService::class,
        beanMethod = "delete",
        operation = Operation(
            operationId = "DELETE /admin/user/{userId}",
            description = """Delete user, admin can't delete self with this request."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/user/{userId}/ban",
        method = arrayOf(RequestMethod.PUT),
        beanClass = UserService::class,
        beanMethod = "setBanStatus",
        operation = Operation(
            operationId = "PUT /admin/user/{userId}/ban",
            description = """Sets the ban status for the user."""
        )
    ),
)
annotation class UserRoutingDoc
