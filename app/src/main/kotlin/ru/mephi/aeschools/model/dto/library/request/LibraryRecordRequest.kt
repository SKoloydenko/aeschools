package ru.mephi.aeschools.model.dto.library.request

class LibraryRecordRequest(
    val title: String,
    val content: String,
    val link: String? = null,
)
