package ru.mephi.aeschools.model.dto.school.request

class PermissionsRequest(
    val moderatorPermission: Boolean,
    val internshipPermission: Boolean,
    val eventPermission: Boolean,
    val publicationPermission: Boolean,
    val bestPracticePermission: Boolean,
    val coursePermission: Boolean,
    val hiddenFieldsPermission: Boolean,
)
