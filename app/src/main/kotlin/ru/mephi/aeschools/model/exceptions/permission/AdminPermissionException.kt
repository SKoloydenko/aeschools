package ru.mephi.aeschools.model.exceptions.permission

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class AdminPermissionException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "User is not admin",
    errorDescription = ErrorDescription(
        ru = "Пользователь не является администратором",
        en = "User is not admin"
    )
)
