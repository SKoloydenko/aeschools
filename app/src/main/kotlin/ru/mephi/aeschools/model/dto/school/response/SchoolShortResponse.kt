package ru.mephi.aeschools.model.dto.school.response

import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.PhotoPathed
import java.time.LocalDateTime
import java.util.*

class SchoolShortResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val university: String,
    val name: String,
    val shortName: String,
    override var photoPath: String? = null,
) : AbstractCreatedAtResponse(id, createdAt), PhotoPathed
