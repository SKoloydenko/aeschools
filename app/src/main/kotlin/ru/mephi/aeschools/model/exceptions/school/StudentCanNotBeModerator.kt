package ru.mephi.aeschools.model.exceptions.school

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class StudentCanNotBeModerator : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Student can not be a moderator",
    errorDescription = ErrorDescription(
        ru = "Студент не может быть модератором площадки",
        en = "Student can not be a moderator"
    )
)
