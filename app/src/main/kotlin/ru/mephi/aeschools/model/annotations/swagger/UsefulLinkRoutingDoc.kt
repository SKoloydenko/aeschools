package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.UsefulLinkService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/public/link",
        method = arrayOf(RequestMethod.GET),
        beanClass = UsefulLinkService::class,
        beanMethod = "listUsefulLinks",
        operation = Operation(
            operationId = "GET /public/link",
            description = """Returns list of useful links with pagination for all users."""
        )
    ),

    RouterOperation(
        path = "$API_VERSION_1/admin/link",
        method = arrayOf(RequestMethod.GET),
        beanClass = UsefulLinkService::class,
        beanMethod = "listUsefulLinksForAdmin",
        operation = Operation(
            operationId = "GET /admin/link",
            description = """Returns list of useful links with pagination for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/link",
        method = arrayOf(RequestMethod.POST),
        beanClass = UsefulLinkService::class,
        beanMethod = "createUsefulLink",
        operation = Operation(
            operationId = "POST /admin/link",
            description = """Create useful link."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/link/{linkId}",
        method = arrayOf(RequestMethod.GET),
        beanClass = UsefulLinkService::class,
        beanMethod = "findUsefulLinkByIdForAdmin",
        operation = Operation(
            operationId = "GET /admin/link/{linkId}",
            description = """Returns useful link for admin."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/link/{linkId}",
        method = arrayOf(RequestMethod.PUT),
        beanClass = UsefulLinkService::class,
        beanMethod = "updateUsefulLink",
        operation = Operation(
            operationId = "PUT /admin/link/{linkId}",
            description = """Update useful link information."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/link/{linkId}/photo",
        method = arrayOf(RequestMethod.POST),
        beanClass = UsefulLinkService::class,
        beanMethod = "uploadUsefulLinkPhoto",
        operation = Operation(
            operationId = "POST /admin/link/{linkId}/photo",
            description = """Upload useful link photo."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/link/{linkId}",
        method = arrayOf(RequestMethod.DELETE),
        beanClass = UsefulLinkService::class,
        beanMethod = "deleteUsefulLink",
        operation = Operation(
            operationId = "DELETE /admin/link/{linkId}",
            description = """Delete useful link."""
        )
    ),
)

annotation class UsefulLinkRoutingDoc
