package ru.mephi.aeschools.model.dto.excel.proxy

import ru.mephi.aeschools.database.entity.report.Section
import ru.mephi.aeschools.database.entity.report.SectionFieldType
import ru.mephi.aeschools.model.dto.excel.ExcelFieldGetter
import ru.mephi.aeschools.service.ReportService
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper

fun getReportProxy(
    section: Section,
    reportService: ReportService
): List<ExcelFieldGetter<ReportExcelProxyHelper>> {
    val list = mutableListOf<ExcelFieldGetter<ReportExcelProxyHelper>>()
    list.add(ExcelFieldGetter("Площадка") { it.school.name })
    section.fields.forEach { field ->
        list.add(ExcelFieldGetter(field.title) {
            it.answers.find { it.sectionField.id == field.id }?.answer?.let {
                return@let if (field.type == SectionFieldType.FILE) reportService.getLinkToReportFile(it)
                else it
            } ?: ""
        })
    }
    return list
}
