package ru.mephi.aeschools.model.dto.school.request

class IndustrialPartnerRequest(
    var name: String,
    var partnershipFormat: String,
    var schoolRole: String,
    var partnerRole: String,
    var cooperationDirection: String,
)
