package ru.mephi.aeschools.model.exceptions.common

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class AdminBanException : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Can't ban admin",
    errorDescription = ErrorDescription(
        ru = "Нельзя заблокировать администратора",
        en = "Can't ban admin"
    )
)
