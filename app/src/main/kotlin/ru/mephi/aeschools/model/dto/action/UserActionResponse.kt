package ru.mephi.aeschools.model.dto.action

import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.AbstractCreatedAtResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.time.LocalDateTime
import java.util.*

class UserActionResponse(
    id: UUID,
    createdAt: LocalDateTime,
    val actionTarget: ActionTarget,
    val actionVerb: ActionVerb? = null,
    val targetId: UUID? = null,
    val user: UserShortResponse,
) : AbstractCreatedAtResponse(id, createdAt)
