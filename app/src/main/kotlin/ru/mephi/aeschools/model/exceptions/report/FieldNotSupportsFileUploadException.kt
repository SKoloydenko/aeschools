package ru.mephi.aeschools.model.exceptions.report

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException
import java.util.*

class FieldNotSupportsFileUploadException(
    fieldId: UUID,
) : AppException(
    status = HttpStatus.BAD_REQUEST,
    message = "Field with id [$fieldId] is not supports file uploads.",
    errorDescription = ErrorDescription(
        ru = "Невозможно загрузить файл",
        en = "Unable to upload file"
    )
)
