package ru.mephi.aeschools.model.dto


class PageResponse<T : AbstractResponse>(
    content: List<T>?,
    number: Long,
    numberOfElements: Long,
    val totalPages: Long,
) {
    val content = content ?: emptyList()
    val page = number + 1
    val size = numberOfElements

    init {
        changePhoto()
    }

    private fun changePhoto() {
        if (content.firstOrNull() == null) return
        if (content.first() !is PhotoPathed) return
        content as List<out PhotoPathed>
        for (i in content.indices) {
            content[i].photoPath = getMediumPhoto(content[i].photoPath)
        }
    }

    private fun getMediumPhoto(photoPath: String?): String? {
        photoPath ?: return null
        val paths = photoPath.split("/").toMutableList()
        val names = paths.last().split(".")
        paths[paths.size - 1] = names[0] + ".medium." + names[1]
        return paths.joinToString("/")
    }
}
