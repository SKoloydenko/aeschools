package ru.mephi.aeschools.model.exceptions.permission

import org.springframework.http.HttpStatus
import ru.mephi.aeschools.model.ErrorDescription
import ru.mephi.aeschools.model.exceptions.AppException

class AdminOrModeratorPermissionException : AppException(
    status = HttpStatus.FORBIDDEN,
    message = "User is not admin or moderator",
    errorDescription = ErrorDescription(
        ru = "Пользователь не является администратором или модератором",
        en = "User is not admin or moderator"
    )
)
