package ru.mephi.aeschools.model.enums.storage

enum class FileStoreDir(val path: String, val isPublic: Boolean) {
    REPORTS("/reports", false), LIBRARY("/library", true),
    QUESTIONNAIRE("/questionnaire", false)
}
