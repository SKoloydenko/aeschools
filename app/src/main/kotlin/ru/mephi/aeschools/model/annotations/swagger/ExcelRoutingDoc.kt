package ru.mephi.aeschools.model.annotations.swagger

import io.swagger.v3.oas.annotations.Operation
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.web.bind.annotation.RequestMethod
import ru.mephi.aeschools.service.ExcelService
import ru.mephi.aeschools.util.constants.API_VERSION_1

@SwaggerDocumentation
@RouterOperations(
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/user",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportUsers",
        operation = Operation(
            operationId = "GET /admin/util/export/user",
            description = """Returns excel report about users with their questionnaires."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/user/{userId}/event",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportUserEvents",
        operation = Operation(
            operationId = "GET /admin/util/export/user/{userId}/event",
            description = """Returns excel report of user's events."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/user/{userId}/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportUserInternships",
        operation = Operation(
            operationId = "GET /admin/util/export/user/{userId}/internship",
            description = """Returns excel report of user's internships."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/event",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportEvents",
        operation = Operation(
            operationId = "GET /admin/util/export/event",
            description = """Returns excel report of events with their members."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/school/{schoolId}/event",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportSchoolEvents",
        operation = Operation(
            operationId = "GET /admin/util/export/school/{schoolId}/event",
            description = """Returns excel report of school events with their members."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/event/{eventId}/user",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportEventUsers",
        operation = Operation(
            operationId = "GET /admin/util/export/event/{eventId}/user",
            description = """Returns excel report of event's members."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportInternships",
        operation = Operation(
            operationId = "GET /admin/util/export/internship",
            description = """Returns excel report of internships with their applicants."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/school/{schoolId}/internship",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportSchoolInternships",
        operation = Operation(
            operationId = "GET /admin/util/export/school/{schoolId}/internship",
            description = """Returns excel report of school internships with their applicants."""
        )
    ),
    RouterOperation(
        path = "$API_VERSION_1/admin/util/export/internship/{internshipId}/user",
        method = arrayOf(RequestMethod.GET),
        beanClass = ExcelService::class,
        beanMethod = "exportInternshipApplicants",
        operation = Operation(
            operationId = "GET /admin/util/export/internship/{internshipId}/user",
            description = """Returns excel report of internship's applicants."""
        )
    ),
)
annotation class ExcelRoutingDoc
