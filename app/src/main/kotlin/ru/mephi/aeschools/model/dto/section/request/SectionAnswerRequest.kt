package ru.mephi.aeschools.model.dto.section.request

class SectionAnswerRequest(
    val answers: List<SectionElementAnswerRequest>,
)
