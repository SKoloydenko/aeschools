package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import ru.mephi.aeschools.database.entity.FeatureToggle
import ru.mephi.aeschools.model.dto.FeatureToggleUpdateRequest

@Tag(name = "Feature Toggle API")
interface FeatureToggleService {
    fun isFeatureEnabled(featureName: String): Boolean
    fun changeFeatureState(@RequestBody(required = true) feature: FeatureToggleUpdateRequest): FeatureToggle
    fun listFeatures(): List<FeatureToggle>
    fun findByFeatureName(@PathVariable("featureName") featureName: String): FeatureToggle
}
