package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.course.Course
import ru.mephi.aeschools.database.repository.course.CourseDao
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.model.dto.course.response.CourseAdminResponse
import ru.mephi.aeschools.model.dto.course.response.CourseResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.TagMapper
import ru.mephi.aeschools.model.mappers.course.CourseMapper
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.CourseService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.CourseValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class CourseServiceImpl(
    private val storageManager: StorageImageManager,

    private val courseValidator: CourseValidator,
    private val courseDao: CourseDao,
    private val courseMapper: CourseMapper,

    private val tagMapper: TagMapper,

    private val userService: UserService,
    private val schoolService: SchoolService,
    private val contentPhotoService: ContentPhotoService,
) : CourseService {

    private fun findEntityById(id: UUID) = courseDao.findById(id).orElseThrow {
        ResourceNotFoundException(Course::class.java, id)
    }

    private fun findSchoolEntityById(schoolId: UUID, id: UUID) =
        courseDao.findBySchoolIdAndId(schoolId, id).orElseThrow {
            ResourceNotFoundException(Course::class.java, id)
        }

    override fun listCourses(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseResponse> {
        val courses = with(courseDao) {
            if (tag.isEmpty()) findByTitleStartsWithAndPublicationDateBeforeNow(
                query,
                page.pageable
            )
            else findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                query,
                tag,
                page.pageable
            )
        }
        return courseMapper.asPageResponse(courses)
    }

    override fun listSchoolCourses(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseResponse> {
        schoolService.findSchoolById(schoolId)
        val courses = with(courseDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
                schoolId,
                query,
                page.pageable
            )
            else findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                schoolId,
                query,
                tag,
                page.pageable
            )
        }
        return courseMapper.asPageResponse(courses)
    }

    override fun listCoursesForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseAdminResponse> {
        val courses = with(courseDao) {
            if (tag.isEmpty()) findByTitleStartsWith(query, page.pageable)
            else findByTitleStartsWithAndTagsTitle(query, tag, page.pageable)
        }
        return courseMapper.asAdminPageResponse(courses)
    }

    override fun listSchoolCoursesForAdmin(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseAdminResponse> {
        schoolService.findSchoolById(schoolId)
        val courses = with(courseDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWith(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagsTitle(schoolId, query, tag, page.pageable)
        }
        return courseMapper.asAdminPageResponse(courses)
    }

    override fun findCourseById(courseId: UUID): CourseResponse {
        val course = courseDao.findByIdAndPublicationDateBeforeNow(courseId).orElseThrow {
            ResourceNotFoundException(Course::class.java, courseId)
        }
        return courseMapper.asResponse(course)
    }

    override fun findSchoolCourseById(schoolId: UUID, courseId: UUID): CourseResponse {
        val course = courseDao.findBySchoolIdAndIdAndPublicationDateBeforeNow(schoolId, courseId)
            .orElseThrow {
                ResourceNotFoundException(Course::class.java, courseId)
            }
        return courseMapper.asResponse(course)
    }

    override fun findCourseByIdForAdmin(courseId: UUID): CourseAdminResponse {
        val course = findEntityById(courseId)
        return courseMapper.asAdminResponse(course)
    }

    override fun findSchoolCourseByIdForAdmin(schoolId: UUID, courseId: UUID): CourseAdminResponse {
        schoolService.findSchoolById(schoolId)
        val course = findSchoolEntityById(schoolId, courseId)
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun createCourse(
        userId: UUID,
        request: CourseRequest,
        file: Part?,
    ): CourseAdminResponse {
        courseValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val course = courseMapper.asEntity(request).also {
            it.author = user
            it.photoPath = file?.let {
                storageManager.storeImage(file, ImageStoreDir.COURSE, null, true)
            }
            courseDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun updateCourse(
        userId: UUID,
        courseId: UUID,
        request: CourseRequest,
    ): CourseAdminResponse {
        courseValidator.validate(request)
        val course = findEntityById(courseId)
        courseMapper.update(course, request)
        course.setTags(tagMapper.asEntitySet(request.tags, course))
        contentPhotoService.queueContent(ContentPhotoRequest(courseId, course.content))
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun uploadCoursePhoto(userId: UUID, courseId: UUID, file: Part): CourseAdminResponse {
        val course = findEntityById(courseId)
        course.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.COURSE,
            course.photoPath,
            true
        )
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun deleteCourse(userId: UUID, courseId: UUID): DeletedResponse {
        val course = findEntityById(courseId)
        courseDao.delete(course)
        storageManager.remove(course.photoPath)
        return DeletedResponse(Course::class.java, courseId)
    }

    @Transactional
    override fun createSchoolCourse(
        userId: UUID,
        schoolId: UUID,
        request: CourseRequest,
        file: Part?,
    ): CourseAdminResponse {
        courseValidator.validate(request)
        val school = schoolService.findSchoolEntityById(schoolId)
        val user = userService.findUserEntityById(userId)
        val course = courseMapper.asEntity(request).also {
            it.author = user
            it.school = school
            it.photoPath = file?.let {
                storageManager.storeImage(file, ImageStoreDir.COURSE, null, true)
            }
            courseDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun updateSchoolCourse(
        userId: UUID,
        schoolId: UUID,
        courseId: UUID,
        request: CourseRequest,
    ): CourseAdminResponse {
        courseValidator.validate(request)
        schoolService.findSchoolById(schoolId)
        val course = findSchoolEntityById(schoolId, courseId)
        courseMapper.update(course, request)
        course.setTags(tagMapper.asEntitySet(request.tags, course))
        contentPhotoService.queueContent(ContentPhotoRequest(courseId, course.content))
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun uploadSchoolCoursePhoto(
        userId: UUID,
        schoolId: UUID,
        courseId: UUID,
        file: Part,
    ): CourseAdminResponse {
        schoolService.findSchoolById(schoolId)
        val course = findSchoolEntityById(schoolId, courseId)
        course.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.COURSE,
            course.photoPath,
            true
        )
        return courseMapper.asAdminResponse(course)
    }

    @Transactional
    override fun deleteSchoolCourse(userId: UUID, schoolId: UUID, courseId: UUID): DeletedResponse {
        schoolService.findSchoolById(schoolId)
        val course = findSchoolEntityById(schoolId, courseId)
        courseDao.delete(course)
        storageManager.remove(course.photoPath)
        return DeletedResponse(Course::class.java, courseId)
    }
}
