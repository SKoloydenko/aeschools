package ru.mephi.aeschools.service.impl.storage

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.storage.StorageFile
import ru.mephi.aeschools.database.repository.storage.StorageFileDao
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundByNameException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.exceptions.storage.TooBigFileName
import ru.mephi.aeschools.service.StorageService
import ru.mephi.aeschools.util.constants.mediumLength
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class StorageServiceImpl(
    val dao: StorageFileDao,
) : StorageService {

    private fun findById(id: UUID): StorageFile {
        return dao.findById(id)
            .orElseThrow { ResourceNotFoundException(StorageFile::class.java, id) }
    }

    override fun getNewFile(name: String): StorageFile {
        if (name.length > mediumLength) throw TooBigFileName()
        return dao.save(StorageFile(name))
    }

    override fun getFileName(id: UUID): String {
        return findById(id).name
    }

    override fun getFileNameByIdWithExt(name: String): String {
        return findById(UUID.fromString(name.split(".")[0])).name
    }

    override fun removeFileByIdWithExtIfExist(name: String) {
        dao.findById(UUID.fromString(name.split(".")[0])).ifPresent { dao.delete(it) }
    }

    override fun findFileByIdWithExt(name: String): StorageFile {
        return dao.findById(UUID.fromString(name.split(".")[0]))
            .orElseThrow { ResourceNotFoundByNameException(StorageFile::class.java, name) }
    }

    override fun existsByName(name: String): Boolean {
        return dao.existsByName(name)
    }

}
