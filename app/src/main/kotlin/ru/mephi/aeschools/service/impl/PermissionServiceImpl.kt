package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.enums.permission.ModeratorPermission
import ru.mephi.aeschools.model.exceptions.permission.AdminOrModeratorPermissionException
import ru.mephi.aeschools.model.exceptions.permission.AdminPermissionException
import ru.mephi.aeschools.model.exceptions.permission.ModeratorPermissionException
import ru.mephi.aeschools.model.exceptions.permission.StudentPermissionException
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class PermissionServiceImpl(
    private val schoolService: SchoolService,
    private val userService: UserService,
) : PermissionService {

    override fun isAdmin(userId: UUID): Boolean {
        val user = userService.findUserEntityById(userId)
        return user.admin
    }

    override fun isModerator(userId: UUID): Boolean {
        if (isAdmin(userId)) return true
        val user = userService.findUserEntityById(userId)
        return user.moderator
    }

    override fun internshipPermission(schoolId: UUID, userId: UUID) {
        if (isAdmin(userId)) return
        val moderatorSchool = schoolService.findModeratorSchoolEntityByIds(schoolId, userId)
        if (!moderatorSchool.permissions.internshipPermission) {
            throw ModeratorPermissionException(ModeratorPermission.INTERNSHIP)
        }
    }

    override fun eventPermission(schoolId: UUID, userId: UUID) {
        if (isAdmin(userId)) return
        val moderatorSchool = schoolService.findModeratorSchoolEntityByIds(schoolId, userId)
        if (!moderatorSchool.permissions.eventPermission) {
            throw ModeratorPermissionException(ModeratorPermission.EVENT)
        }
    }

    override fun publicationPermission(schoolId: UUID, userId: UUID) {
        if (isAdmin(userId)) return
        val moderatorSchool = schoolService.findModeratorSchoolEntityByIds(schoolId, userId)
        if (!moderatorSchool.permissions.publicationPermission) {
            throw ModeratorPermissionException(ModeratorPermission.PUBLICATION)
        }
    }

    override fun bestPracticePermission(schoolId: UUID, userId: UUID) {
        if (isAdmin(userId)) return
        val moderatorSchool = schoolService.findModeratorSchoolEntityByIds(schoolId, userId)
        if (!moderatorSchool.permissions.bestPracticePermission) {
            throw ModeratorPermissionException(ModeratorPermission.BEST_PRACTICE)
        }
    }

    override fun coursePermission(schoolId: UUID, userId: UUID) {
        if (isAdmin(userId)) return
        val moderatorSchool = schoolService.findModeratorSchoolEntityByIds(schoolId, userId)
        if (!moderatorSchool.permissions.coursePermission) {
            throw ModeratorPermissionException(ModeratorPermission.COURSE)
        }
    }

    override fun hiddenFieldsPermission(schoolId: UUID, userId: UUID) {
        if (isAdmin(userId)) return
        val moderatorSchool = schoolService.findModeratorSchoolEntityByIds(schoolId, userId)
        if (!moderatorSchool.permissions.hiddenFieldsPermission) {
            throw ModeratorPermissionException(ModeratorPermission.HIDDEN_FIELDS)
        }
    }

    override fun moderatorPermission(userId: UUID) {
        if (!isModerator(userId)) {
            throw ModeratorPermissionException(ModeratorPermission.MODERATOR)
        }
    }

    override fun studentPermission(userId: UUID) {
        val user = userService.findUserEntityById(userId)
        if (!user.student) {
            throw StudentPermissionException()
        }
    }

    override fun adminPermission(userId: UUID) {
        if (!isAdmin(userId)) throw AdminPermissionException()
    }

    override fun adminOrModeratorPermission(userId: UUID) {
        if (!isAdmin(userId) && !isModerator(userId)) throw AdminOrModeratorPermissionException()
    }
}
