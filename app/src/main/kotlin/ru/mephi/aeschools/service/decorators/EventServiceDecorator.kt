package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.event.request.AbstractEventRequest
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.model.dto.event.response.AbstractEventAdminResponse
import ru.mephi.aeschools.model.dto.event.response.EventAdminResponse
import ru.mephi.aeschools.service.EventService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class EventServiceDecorator(
    @Qualifier("eventServiceImpl")
    private val eventService: EventService,
    private val userActionService: UserActionService,
) : EventService by eventService {
    override fun createEvent(
        userId: UUID,
        request: AbstractEventRequest,
        file: Part?,
    ): AbstractEventAdminResponse {
        val response = eventService.createEvent(userId, request, file)
        userActionService.save(userId, ActionTarget.EVENT, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateEvent(
        userId: UUID,
        eventId: UUID,
        request: AbstractEventRequest,
    ): AbstractEventAdminResponse {
        val response = eventService.updateEvent(userId, eventId, request)
        userActionService.save(userId, ActionTarget.EVENT, ActionVerb.UPDATE, response.id)
        return response
    }

    override fun uploadEventPhoto(
        userId: UUID,
        eventId: UUID,
        file: Part,
    ): AbstractEventAdminResponse {
        val response = eventService.uploadEventPhoto(userId, eventId, file)
        userActionService.save(
            userId,
            ActionTarget.EVENT,
            ActionVerb.UPLOAD_PHOTO,
            eventId
        )
        return response
    }

    override fun deleteEvent(
        userId: UUID,
        eventId: UUID,
    ): DeletedResponse {
        val response = eventService.deleteEvent(userId, eventId)
        userActionService.save(userId, ActionTarget.EVENT, ActionVerb.DELETE, eventId)
        return response
    }

    override fun createSchoolEvent(
        userId: UUID,
        schoolId: UUID,
        request: EventRequest,
        file: Part?,
    ): EventAdminResponse {
        val response = eventService.createSchoolEvent(userId, schoolId, request, file)
        userActionService.save(userId, ActionTarget.EVENT, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateSchoolEvent(
        userId: UUID,
        schoolId: UUID,
        eventId: UUID,
        request: EventRequest,
    ): EventAdminResponse {
        val response =
            eventService.updateSchoolEvent(userId, schoolId, eventId, request)
        userActionService.save(userId, ActionTarget.EVENT, ActionVerb.UPDATE, eventId)
        return response
    }

    override fun uploadSchoolEventPhoto(
        userId: UUID,
        schoolId: UUID,
        eventId: UUID,
        file: Part,
    ): EventAdminResponse {
        val response = eventService.uploadSchoolEventPhoto(userId, schoolId, eventId, file)
        userActionService.save(
            userId,
            ActionTarget.EVENT,
            ActionVerb.UPLOAD_PHOTO,
            eventId
        )
        return response
    }

    override fun deleteSchoolEvent(
        userId: UUID,
        schoolId: UUID,
        eventId: UUID,
    ): DeletedResponse {
        val response = eventService.deleteSchoolEvent(userId, schoolId, eventId)
        userActionService.save(userId, ActionTarget.EVENT, ActionVerb.DELETE, eventId)
        return response
    }
}
