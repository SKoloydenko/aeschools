package ru.mephi.aeschools.service.validators.internship

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.service.validators.AbstractContentValidator
import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.tagsSize

@Component
class InternshipValidator : AbstractContentValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = InternshipRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        super.validate(target, errors)
        this.target = target as InternshipRequest
        errors += target.company.validate("company") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
        errors += target.tags.validate("tags") {
            it.isNotNull()
            it.maxSize(tagsSize)
            it.forEach { tag -> tag.maxLength(extraSmallLength) }
        }
    }
}
