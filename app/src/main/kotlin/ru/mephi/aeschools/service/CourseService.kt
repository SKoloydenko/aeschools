package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.model.dto.course.response.CourseAdminResponse
import ru.mephi.aeschools.model.dto.course.response.CourseResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Course API")
interface CourseService {

    fun listCourses(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseResponse>

    fun listSchoolCourses(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseResponse>

    fun listCoursesForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseAdminResponse>

    fun listSchoolCoursesForAdmin(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<CourseAdminResponse>

    fun findCourseById(@PathVariable courseId: UUID): CourseResponse

    fun findSchoolCourseById(
        @PathVariable schoolId: UUID,
        @PathVariable courseId: UUID,
    ): CourseResponse

    fun findCourseByIdForAdmin(@PathVariable courseId: UUID): CourseAdminResponse

    fun findSchoolCourseByIdForAdmin(
        @PathVariable schoolId: UUID,
        @PathVariable courseId: UUID,
    ): CourseAdminResponse

    fun createCourse(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: CourseRequest,
        @RequestPart file: Part?,
    ): CourseAdminResponse

    fun updateCourse(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable courseId: UUID,
        @RequestBody request: CourseRequest,
    ): CourseAdminResponse

    fun uploadCoursePhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable courseId: UUID,
        @RequestPart file: Part,
    ): CourseAdminResponse

    fun deleteCourse(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable courseId: UUID,
    ): DeletedResponse

    fun createSchoolCourse(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: CourseRequest,
        @RequestPart file: Part?,
    ): CourseAdminResponse

    fun updateSchoolCourse(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable courseId: UUID,
        @RequestBody request: CourseRequest,
    ): CourseAdminResponse

    fun uploadSchoolCoursePhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable courseId: UUID,
        @RequestPart file: Part,
    ): CourseAdminResponse

    fun deleteSchoolCourse(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable courseId: UUID,
    ): DeletedResponse
}
