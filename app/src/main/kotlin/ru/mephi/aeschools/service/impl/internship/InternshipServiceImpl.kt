package ru.mephi.aeschools.service.impl.internship

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.database.repository.internship.InternshipDao
import ru.mephi.aeschools.database.repository.internship.QuestionnaireDao
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.model.dto.internship.response.InternshipAdminResponse
import ru.mephi.aeschools.model.dto.internship.response.InternshipResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.TagMapper
import ru.mephi.aeschools.model.mappers.internship.InternshipMapper
import ru.mephi.aeschools.model.mappers.internship.QuestionnaireMapper
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.InternshipService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.internship.InternshipValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class InternshipServiceImpl(
    private val imageManager: StorageImageManager,

    private val internshipValidator: InternshipValidator,
    private val internshipDao: InternshipDao,
    private val internshipMapper: InternshipMapper,

    private val questionnaireDao: QuestionnaireDao,
    private val questionnaireMapper: QuestionnaireMapper,

    private val tagMapper: TagMapper,

    private val schoolService: SchoolService,
    private val userService: UserService,
    private val contentPhotoService: ContentPhotoService,
) : InternshipService {

    private fun findSchoolInternshipEntityById(id: UUID, schoolId: UUID): Internship {
        return internshipDao.findByIdAndSchoolId(id, schoolId)
            .orElseThrow { ResourceNotFoundException(Internship::class.java, id) }
    }

    override fun findInternshipEntityById(internshipId: UUID): Internship {
        return internshipDao.findById(internshipId)
            .orElseThrow { ResourceNotFoundException(Internship::class.java, internshipId) }
    }

    override fun isApplicant(internshipId: UUID, userId: UUID): Boolean {
        val internship = findInternshipEntityById(internshipId)
        userService.findUserEntityById(userId)
        val user = userService.findUserEntityById(userId)
        return questionnaireDao.findByInternshipAndUser(internship, user).isPresent
    }

    override fun listInternships(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipResponse> {
        val internships = with(internshipDao) {
            if (tag.isEmpty()) findByTitleStartsWithAndPublicationDateBeforeNow(query, page.pageable)
            else findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(query, tag, page.pageable)
        }
        return internshipMapper.asPageResponse(internships)
    }

    override fun listSchoolInternships(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipResponse> {
        schoolService.findSchoolById(schoolId)
        val internships = with(internshipDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(schoolId, query, tag, page.pageable)
        }
        return internshipMapper.asPageResponse(internships)
    }

    override fun listInternshipsForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipAdminResponse> {
        val internships = with(internshipDao) {
            if (tag.isEmpty()) findByTitleStartsWith(query, page.pageable)
            else findByTitleStartsWithAndTagsTitle(query, tag, page.pageable)
        }
        return internshipMapper.asPageAdminResponse(internships)
    }

    override fun listSchoolInternshipsForAdmin(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipAdminResponse> {
        schoolService.findSchoolById(schoolId)
        val internships = with(internshipDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWith(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagsTitle(schoolId, query, tag, page.pageable)
        }
        return internshipMapper.asPageAdminResponse(internships)
    }

    override fun listInternshipsForUser(
        userId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipResponse> {
        userService.findUserEntityById(userId)
        val internships = with(internshipDao) {
            if (tag.isEmpty()) findByApplicantsIdAndTitleStartsWith(userId, query, page.pageable)
            else findByApplicantsIdAndTitleStartsWithAndTagsTitle(userId, query, tag, page.pageable)
        }
        return internshipMapper.asPageResponse(internships)
    }

    override fun findInternshipById(internshipId: UUID): InternshipResponse {
        val internship = internshipDao.findByIdAndPublicationDateBeforeNow(internshipId).orElseThrow {
            throw ResourceNotFoundException(
                Internship::class.java, internshipId)
        }
        return internshipMapper.asResponse(internship)
    }

    override fun findSchoolInternshipById(schoolId: UUID, internshipId: UUID): InternshipResponse {
        schoolService.findSchoolById(schoolId)
        val internship = internshipDao.findBySchoolIdAndIdAndPublicationDateBeforeNow(schoolId, internshipId)
            .orElseThrow {
                throw ResourceNotFoundException(Internship::class.java, internshipId)
            }
        return internshipMapper.asResponse(internship)
    }

    override fun findInternshipByIdForAdmin(internshipId: UUID): InternshipAdminResponse {
        val internship = findInternshipEntityById(internshipId)
        return internshipMapper.asAdminResponse(internship)
    }

    override fun findSchoolInternshipByIdForAdmin(
        schoolId: UUID,
        internshipId: UUID,
    ): InternshipAdminResponse {
        schoolService.findSchoolById(schoolId)
        val internship = findSchoolInternshipEntityById(internshipId, schoolId)
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun createInternship(
        userId: UUID,
        request: InternshipRequest,
        file: Part?,
    ): InternshipAdminResponse {
        internshipValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val internship = internshipMapper.asEntity(request).also {
            it.author = user
            it.photoPath = file?.let {
                imageManager.storeImage(
                    file, ImageStoreDir.INTERNSHIP, null, true
                )
            }
            internshipDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun updateInternship(
        userId: UUID,
        internshipId: UUID,
        request: InternshipRequest,
    ): InternshipAdminResponse {
        internshipValidator.validate(request)
        val internship = findInternshipEntityById(internshipId)
        internshipMapper.update(internship, request)
        internship.setTags(tagMapper.asEntitySet(request.tags, internship))
        contentPhotoService.queueContent(ContentPhotoRequest(internshipId, internship.content))
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun uploadInternshipPhoto(
        userId: UUID,
        internshipId: UUID,
        file: Part,
    ): InternshipAdminResponse {
        val internship = findInternshipEntityById(internshipId)
        internship.photoPath =
            imageManager.storeImage(file, ImageStoreDir.INTERNSHIP, internship.photoPath, true)
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun deleteInternship(userId: UUID, internshipId: UUID): DeletedResponse {
        val internship = findInternshipEntityById(internshipId)
        internshipDao.delete(internship)
        imageManager.remove(internship.photoPath)
        return DeletedResponse(Internship::class.java, internship.id as UUID)
    }

    @Transactional
    override fun createSchoolInternship(
        userId: UUID,
        schoolId: UUID,
        request: InternshipRequest,
        file: Part?,
    ): InternshipAdminResponse {
        internshipValidator.validate(request)
        val school = schoolService.findSchoolEntityById(schoolId)
        val user = userService.findUserEntityById(userId)
        val internship = internshipMapper.asEntity(request).also {
            it.author = user
            it.school = school
            it.photoPath = file?.let {
                imageManager.storeImage(
                    file, ImageStoreDir.INTERNSHIP, null, true
                )
            }
            it.school = school
            internshipDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun updateSchoolInternship(
        userId: UUID,
        schoolId: UUID,
        internshipId: UUID,
        request: InternshipRequest,
    ): InternshipAdminResponse {
        internshipValidator.validate(request)
        val school = schoolService.findSchoolById(schoolId)
        val internship = findSchoolInternshipEntityById(internshipId, school.id)
        internshipMapper.update(internship, request)
        internship.setTags(tagMapper.asEntitySet(request.tags, internship))
        contentPhotoService.queueContent(ContentPhotoRequest(internshipId, internship.content))
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun uploadSchoolInternshipPhoto(
        userId: UUID,
        schoolId: UUID,
        internshipId: UUID,
        file: Part,
    ): InternshipAdminResponse {
        val school = schoolService.findSchoolById(schoolId)
        val internship = findSchoolInternshipEntityById(internshipId, school.id)
        internship.photoPath =
            imageManager.storeImage(file, ImageStoreDir.INTERNSHIP, internship.photoPath, true)
        return internshipMapper.asAdminResponse(internship)
    }

    @Transactional
    override fun deleteSchoolInternship(
        userId: UUID,
        schoolId: UUID,
        internshipId: UUID,
    ): DeletedResponse {
        val school = schoolService.findSchoolById(schoolId)
        val internship = findSchoolInternshipEntityById(internshipId, school.id)
        internshipDao.delete(internship)
        imageManager.remove(internship.photoPath)
        return DeletedResponse(Internship::class.java, internship.id as UUID)
    }

    override fun listInternshipApplicants(
        internshipId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse> {
        val internship = findInternshipEntityById(internshipId)
        val applicants = questionnaireDao.findByInternship(internship, page.pageable)
        return questionnaireMapper.asPageUserResponse(applicants)
    }

    override fun listSchoolInternshipApplicants(
        schoolId: UUID,
        internshipId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse> {
        val school = schoolService.findSchoolById(schoolId)
        val internship = findSchoolInternshipEntityById(internshipId, school.id)
        val applicants = questionnaireDao.findByInternship(internship, page.pageable)
        return questionnaireMapper.asPageUserResponse(applicants)
    }

    override fun findAllInternships(): Iterable<Internship> = internshipDao.findAll()
}
