package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import ru.mephi.aeschools.database.entity.preferences.UserPreferences
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesRequest
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesResponse
import java.util.*

@Tag(name = "User Preferences API")
interface UserPreferencesService {
    fun findEntityByUserId(userId: UUID): UserPreferences
    fun createPreferences(request: UserPreferencesRequest, user: User): UserPreferences
    fun updatePreferences(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: UserPreferencesRequest,
    ): UserPreferencesResponse

    fun turnOffEmailPreference(token: String): UserPreferencesResponse
    fun findAllUsersIdsWithEnableEmail(ignorePreferences: Boolean): List<UUID>
    fun findUsersIdsWithEnableEmail(ignorePreferences: Boolean): List<UUID>
    fun findModeratorsIdsWithEnableEmail(ignorePreferences: Boolean): List<UUID>
    fun getPreferencesById(@PathVariable("id") userId: UUID): UserPreferencesResponse

    fun getUnsubscribeFromMailLink(): String
}
