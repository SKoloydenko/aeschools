package ru.mephi.aeschools.service.impl.auth

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.VerifyEmailService
import java.util.*

@Service
class EmailVerificationScheduler(
    private val verifyEmailService: VerifyEmailService,
    private val userService: UserService,
) {

    @Scheduled(fixedDelay = 15 * 60 * 1000L)
    fun handle() {
        println("Start email verification scheduler")
        verifyEmailService.findAll().forEach {
            if (verifyEmailService.tokenIsExpired(it)) {
                userService.delete(it.id as UUID)
            }
        }
        println("End email verification scheduler")
    }
}
