package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.publication.Publication
import ru.mephi.aeschools.database.repository.publication.PublicationDao
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.publication.request.PublicationRequest
import ru.mephi.aeschools.model.dto.publication.response.PublicationAdminResponse
import ru.mephi.aeschools.model.dto.publication.response.PublicationResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.TagMapper
import ru.mephi.aeschools.model.mappers.publication.PublicationMapper
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.PublicationService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.PublicationValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class PublicationServiceImpl(
    private val imageManager: StorageImageManager,

    private val publicationValidator: PublicationValidator,
    private val publicationDao: PublicationDao,
    private val publicationMapper: PublicationMapper,

    private val tagMapper: TagMapper,

    private val userService: UserService,
    private val schoolService: SchoolService,
    private val contentPhotoService: ContentPhotoService
) : PublicationService {

    private fun findSchoolPublicationEntityById(schoolId: UUID, id: UUID) =
        publicationDao.findBySchoolIdAndId(schoolId, id)
            .orElseThrow { ResourceNotFoundException(Publication::class.java, id) }

    override fun findPublicationEntityById(id: UUID): Publication =
        publicationDao.findById(id)
            .orElseThrow { ResourceNotFoundException(Publication::class.java, id) }

    override fun listPublications(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationResponse> {
        val publications = with(publicationDao) {
            if (tag.isEmpty()) findByTitleStartsWithAndPublicationDateBeforeNow(
                query,
                page.pageable
            )
            else findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                query,
                tag,
                page.pageable
            )
        }
        return publicationMapper.asPageResponse(publications)
    }

    override fun listSchoolPublications(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationResponse> {
        schoolService.findSchoolById(schoolId)
        val publications = with(publicationDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
                schoolId,
                query,
                page.pageable
            )
            else findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                schoolId,
                query,
                tag,
                page.pageable
            )
        }
        return publicationMapper.asPageResponse(publications)
    }

    override fun listPublicationsForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationAdminResponse> {
        val publications = with(publicationDao) {
            if (tag.isEmpty()) findByTitleStartsWith(query, page.pageable)
            else findByTitleStartsWithAndTagsTitle(query, tag, page.pageable)
        }
        return publicationMapper.asAdminPageResponse(publications)
    }

    override fun listSchoolPublicationsForAdmin(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationAdminResponse> {
        schoolService.findSchoolById(schoolId)
        val publications = with(publicationDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWith(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagsTitle(schoolId, query, tag, page.pageable)
        }
        return publicationMapper.asAdminPageResponse(publications)
    }

    override fun findPublicationById(publicationId: UUID): PublicationResponse {
        val publication =
            publicationDao.findByIdAndPublicationDateBeforeNow(publicationId).orElseThrow {
                throw ResourceNotFoundException(Publication::class.java, publicationId)
            }
        return publicationMapper.asResponse(publication)
    }

    override fun findSchoolPublicationById(
        schoolId: UUID,
        publicationId: UUID,
    ): PublicationResponse {
        schoolService.findSchoolById(schoolId)
        val publication =
            publicationDao.findBySchoolIdAndIdAndPublicationDateBeforeNow(schoolId, publicationId)
                .orElseThrow {
                    throw ResourceNotFoundException(Publication::class.java, publicationId)
                }
        return publicationMapper.asResponse(publication)
    }

    override fun findPublicationByIdForAdmin(publicationId: UUID): PublicationAdminResponse {
        val publication = findPublicationEntityById(publicationId)
        return publicationMapper.asAdminResponse(publication)
    }

    override fun findSchoolPublicationByIdForAdmin(
        schoolId: UUID,
        publicationId: UUID,
    ): PublicationAdminResponse {
        schoolService.findSchoolById(schoolId)
        val publication = findSchoolPublicationEntityById(schoolId, publicationId)
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun createPublication(
        userId: UUID,
        request: PublicationRequest,
        file: Part?,
    ): PublicationAdminResponse {
        publicationValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val publication = publicationMapper.asEntity(request).also {
            it.author = user
            it.photoPath = file?.let {
                imageManager.storeImage(file, ImageStoreDir.PUBLICATION, null, true)
            }
            publicationDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun updatePublication(
        userId: UUID,
        publicationId: UUID,
        request: PublicationRequest,
    ): PublicationAdminResponse {
        publicationValidator.validate(request)
        val publication = findPublicationEntityById(publicationId)
        publicationMapper.update(publication, request)
        publication.setTags(tagMapper.asEntitySet(request.tags, publication))
        contentPhotoService.queueContent(ContentPhotoRequest(publicationId, publication.content))
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun uploadPublicationPhoto(
        userId: UUID,
        publicationId: UUID,
        file: Part,
    ): PublicationAdminResponse {
        val publication = findPublicationEntityById(publicationId)
        publication.photoPath =
            imageManager.storeImage(file, ImageStoreDir.PUBLICATION, publication.photoPath, true)
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun deletePublication(
        userId: UUID,
        publicationId: UUID,
    ): DeletedResponse {
        val publication = findPublicationEntityById(publicationId)
        publicationDao.deleteById(publication.id as UUID)
        imageManager.remove(publication.photoPath)
        return DeletedResponse(Publication::class.java, publicationId)
    }

    @Transactional
    override fun createSchoolPublication(
        userId: UUID,
        schoolId: UUID,
        request: PublicationRequest,
        file: Part?,
    ): PublicationAdminResponse {
        publicationValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val school = schoolService.findSchoolEntityById(schoolId)
        val publication = publicationMapper.asEntity(request).also {
            it.author = user
            it.school = school
            it.photoPath = file?.let {
                imageManager.storeImage(file, ImageStoreDir.PUBLICATION, null, true)
            }
            publicationDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun updateSchoolPublication(
        userId: UUID,
        schoolId: UUID,
        publicationId: UUID,
        request: PublicationRequest,
    ): PublicationAdminResponse {
        publicationValidator.validate(request)
        schoolService.findSchoolById(schoolId)
        val publication = findSchoolPublicationEntityById(schoolId, publicationId)
        publicationMapper.update(publication, request)
        publication.setTags(tagMapper.asEntitySet(request.tags, publication))
        contentPhotoService.queueContent(ContentPhotoRequest(publicationId, publication.content))
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun uploadSchoolPublicationPhoto(
        userId: UUID,
        schoolId: UUID,
        publicationId: UUID,
        file: Part,
    ): PublicationAdminResponse {
        schoolService.findSchoolById(schoolId)
        val publication = findSchoolPublicationEntityById(schoolId, publicationId)
        publication.photoPath =
            imageManager.storeImage(
                file,
                ImageStoreDir.PUBLICATION,
                publication.photoPath,
                true
            )
        return publicationMapper.asAdminResponse(publication)
    }

    @Transactional
    override fun deleteSchoolPublication(
        userId: UUID,
        schoolId: UUID,
        publicationId: UUID,
    ): DeletedResponse {
        schoolService.findSchoolById(schoolId)
        val publication = findSchoolPublicationEntityById(schoolId, publicationId)
        publicationDao.deleteById(publication.id as UUID)
        imageManager.remove(publication.photoPath)
        return DeletedResponse(Publication::class.java, publicationId)
    }
}
