package ru.mephi.aeschools.service.impl

import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.email.ResetPasswordTokenDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.dto.auth.request.PasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.ResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SendEmailToResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SetNewPasswordRequest
import ru.mephi.aeschools.model.dto.mail.mails.ResetPasswordMail
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesRequest
import ru.mephi.aeschools.model.dto.user.request.LoginRequest
import ru.mephi.aeschools.model.dto.user.request.RefreshRequest
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.model.dto.user.response.TokenResponse
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import ru.mephi.aeschools.model.exceptions.auth.IncorrectPasswordException
import ru.mephi.aeschools.model.exceptions.auth.InvalidTokenToChangePassword
import ru.mephi.aeschools.model.exceptions.auth.NotFoundRequestToChangePassword
import ru.mephi.aeschools.model.exceptions.auth.NotVerifyEmailException
import ru.mephi.aeschools.model.exceptions.auth.ResetPasswordTokenHasExpired
import ru.mephi.aeschools.model.exceptions.common.ResourceAlreadyExistsException
import ru.mephi.aeschools.model.mappers.user.UserMapper
import ru.mephi.aeschools.service.AuthService
import ru.mephi.aeschools.service.EmailSender
import ru.mephi.aeschools.service.FeatureToggleService
import ru.mephi.aeschools.service.ResetPasswordService
import ru.mephi.aeschools.service.UserPreferencesService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.VerifyEmailService
import ru.mephi.aeschools.service.validators.AuthValidator
import ru.mephi.aeschools.service.validators.PasswordValidator
import ru.mephi.aeschools.util.TokenManager
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class AuthServiceImpl(
    private val passwordEncoder: PasswordEncoder,
    private val tokenManager: TokenManager,

    private val authValidator: AuthValidator,
    private val passwordValidator: PasswordValidator,

    private val userDao: UserDao,
    private val resetPasswordTokenDao: ResetPasswordTokenDao,

    private val userMapper: UserMapper,

    private val userService: UserService,
    private val emailSender: EmailSender,
    private val verifyEmailService: VerifyEmailService,
    private val resetPasswordService: ResetPasswordService,
    private val featureToggleService: FeatureToggleService,
    private val userPreferencesService: UserPreferencesService,
) : AuthService {
    override fun register(user: User, password: String): User {
        if (userDao.existsByEmail(user.email)) return userService.findUserEntityByEmail(
            user.email
        )
        user.hash = passwordEncoder.encode(password)
        userDao.save(user)
        userPreferencesService.createPreferences(UserPreferencesRequest(enableEmail = true), user)
        return user
    }

    override fun register(request: RegistrationRequest): UserResponse {
        authValidator.validate(request)
        userDao.findByEmail(request.email).ifPresent {
            if (verifyEmailService.userHaveExpiredEmailVerification(it.id as UUID)) userService.delete(
                it
            )
            else throw ResourceAlreadyExistsException(
                stringTypeName = "Email", value = request.email
            )
        }
        val user = userMapper.asEntity(request).apply user@{
            hash = passwordEncoder.encode(request.password)
            userDao.save(this)
        }
        userPreferencesService.createPreferences(UserPreferencesRequest(request.enableEmail), user)
        if (featureToggleService.isFeatureEnabled("verifyEmail")) {
            verifyEmailService.sendEmailToVerifyEmail(user)
        }
        return userMapper.asResponse(user)
    }

    override fun login(request: LoginRequest): Pair<TokenResponse, String> {
        val user = userService.findUserEntityByEmail(request.email)
        if (!passwordEncoder.matches(
                request.password, user.hash
            )
        ) throw IncorrectPasswordException()
        if (!verifyEmailService.emailIsVerified(user)) throw NotVerifyEmailException(
            user.email
        )
        return tokenManager.generateTokenPair(user, request.fingerprint)
    }

    override fun refresh(request: RefreshRequest): Pair<TokenResponse, String> {
        return tokenManager.refreshTokenPair(request)
    }

    override fun logout(refreshToken: String) {
        tokenManager.deleteRefreshToken(refreshToken)
    }

    override fun sendEmailToResetPassword(request: SendEmailToResetPasswordRequest) {
        val user = userService.findUserEntityByEmail(request.email)
        if (!verifyEmailService.emailIsVerified(user)) throw NotVerifyEmailException(
            user.email
        )
        if (resetPasswordTokenDao.existsById(user.id as UUID)) resetPasswordTokenDao.deleteById(user.id)
        val token = resetPasswordService.createNewToken(user)
        val link = resetPasswordService.getLinkToResetPassword(token)
        emailSender.sendResetPasswordMail(ResetPasswordMail(link, user.email))
    }

    override fun resetPassword(request: ResetPasswordRequest): Pair<TokenResponse, String> {
        val token = resetPasswordTokenDao.findByToken(request.token)
            .orElseThrow { NotFoundRequestToChangePassword(request) }
        val user = token.user!!
        if (request.token != token.token) throw InvalidTokenToChangePassword(request.token)
        if (token.createdAt.isBefore(
                LocalDateTime.now().minusHours(1)
            )
        ) throw ResetPasswordTokenHasExpired(user)
        passwordValidator.validate(PasswordRequest(request.password))
        user.hash = passwordEncoder.encode(request.password)
        userDao.save(user)
        resetPasswordTokenDao.delete(token)
        tokenManager.clearUserData(user)
        return login(LoginRequest(user.email, request.password, request.fingerPrint))
    }

    override fun setNewPassword(request: SetNewPasswordRequest, userId: UUID) {
        val user = userService.findUserEntityById(userId)
        passwordValidator.validate(PasswordRequest(request.newPassword))
        if (!passwordEncoder.matches(
                request.oldPassword,
                user.hash
            )
        ) throw IncorrectPasswordException()
        user.hash = passwordEncoder.encode(request.newPassword)
        userDao.save(user)
    }

    override fun verifyEmail(token: String) {
        verifyEmailService.verifyEmail(token)
    }
}
