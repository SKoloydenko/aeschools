package ru.mephi.aeschools.service.impl.storage

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.util.createIfNotExist
import kotlin.io.path.Path

@Component
class ContentPhotoScheduler(
    private val contentPhotoService: ContentPhotoService
) {

    @Scheduled(cron = "0 0 0 * * SUN") // every Sunday
    fun handle() {
        try {
            val path = Path("uploads" + ImageStoreDir.CONTENT_PHOTO.path).createIfNotExist()
            val folder = path.toFile()
            folder.listFiles()?.forEach {
                try {
                    val photoPath = Path(ImageStoreDir.CONTENT_PHOTO.path + it.name).toString().drop(1)
                    if (!contentPhotoService.contentExists(photoPath))  {
                        contentPhotoService.deleteContentPhoto(photoPath)
                    }
                } catch (_: Exception) {
                }
            }
        } catch (_: Exception) {
        }
    }
}
