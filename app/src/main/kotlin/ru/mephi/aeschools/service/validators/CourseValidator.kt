package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.tagsSize

@Component
class CourseValidator : AbstractContentValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = CourseRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        super.validate(target, errors)
        this.target = target as CourseRequest
        errors += target.enrollmentFinishingDate.validate("enrollmentFinishingDate") {
            it.isNotNull()
            it.after(target.publicationDate)
        }
        errors += target.tags.validate("tags") {
            it.isNotNull()
            it.maxSize(tagsSize)
            it.forEach { tag -> tag.maxLength(extraSmallLength) }
        }
    }
}
