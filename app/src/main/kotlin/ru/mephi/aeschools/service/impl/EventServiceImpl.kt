package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.database.entity.event.Event
import ru.mephi.aeschools.database.entity.publication.Publication
import ru.mephi.aeschools.database.repository.event.EventDao
import ru.mephi.aeschools.database.repository.event.UserEventDao
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.event.request.AbstractEventRequest
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.model.dto.event.response.AbstractEventAdminResponse
import ru.mephi.aeschools.model.dto.event.response.AbstractEventResponse
import ru.mephi.aeschools.model.dto.event.response.EventAdminResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.exceptions.event.UserAlreadyEventMemberException
import ru.mephi.aeschools.model.exceptions.event.UserNotEventMemberException
import ru.mephi.aeschools.model.mappers.TagMapper
import ru.mephi.aeschools.model.mappers.event.EventMapper
import ru.mephi.aeschools.model.mappers.event.UserEventMapper
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.EventService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.EventValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class EventServiceImpl(
    private val imageManager: StorageImageManager,

    private val eventValidator: EventValidator,
    private val eventMapper: EventMapper,
    private val eventDao: EventDao,

    private val tagMapper: TagMapper,

    private val userEventMapper: UserEventMapper,
    private val userEventDao: UserEventDao,

    private val userService: UserService,
    private val schoolService: SchoolService,
    private val contentPhotoService: ContentPhotoService,
) : EventService {

    private fun findSchoolEntityById(schoolId: UUID, id: UUID) =
        eventDao.findBySchoolIdAndId(schoolId, id)
            .orElseThrow { ResourceNotFoundException(Publication::class.java, id) }

    override fun findEventEntityById(id: UUID): AbstractEvent = eventDao.findById(id).orElseThrow {
        ResourceNotFoundException(Event::class.java, id)
    }

    override fun listEvents(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventResponse> {
        val events = with(eventDao) {
            if (tag.isEmpty()) findByTitleStartsWithAndPublicationDateBeforeNow(
                query,
                page.pageable
            )
            else findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                query,
                tag,
                page.pageable
            )
        }
        return eventMapper.asPageResponse(events)
    }

    override fun listSchoolEvents(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventResponse> {
        schoolService.findSchoolById(schoolId)
        val events = with(eventDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(
                schoolId,
                query,
                page.pageable
            )
            else findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                schoolId,
                query,
                tag,
                page.pageable
            )
        }
        return eventMapper.asPageResponse(events)
    }

    override fun listEventsForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventAdminResponse> {
        val events = with(eventDao) {
            if (tag.isEmpty()) findByTitleStartsWith(query, page.pageable)
            else findByTitleStartsWithAndTagsTitle(query, tag, page.pageable)
        }
        return eventMapper.asAdminPageResponse(events)
    }

    override fun listSchoolEventsForAdmin(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventAdminResponse> {
        schoolService.findSchoolById(schoolId)
        val events = with(eventDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWith(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagsTitle(schoolId, query, tag, page.pageable)
        }
        return eventMapper.asAdminPageResponse(events)
    }

    @Transactional
    override fun listEventsForUser(
        userId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventResponse> {
        val user = userService.findUserEntityById(userId)
        val userEvents = with(userEventDao) {
            if (tag.isEmpty()) findByUserAndEventTitleStartsWithOrderByEventPublicationDateDesc(
                user,
                query,
                page.pageable
            )
            else findByUserAndEventTitleStartsWithAndEventTagsTitleOrderByEventPublicationDateDesc(
                user,
                query,
                tag,
                page.pageable
            )
        }
        return userEventMapper.asPageEventResponse(userEvents)
    }

    override fun findEventById(eventId: UUID): AbstractEventResponse {
        val event = eventDao.findByIdAndPublicationDateBeforeNow(eventId).orElseThrow {
            ResourceNotFoundException(Event::class.java, eventId)
        }
        return eventMapper.asResponse(event)
    }

    override fun findSchoolEventById(schoolId: UUID, eventId: UUID): AbstractEventResponse {
        schoolService.findSchoolById(schoolId)
        val event =
            eventDao.findBySchoolIdAndIdAndPublicationDateBeforeNow(schoolId, eventId).orElseThrow {
                ResourceNotFoundException(Event::class.java, eventId)
            }
        return eventMapper.asResponse(event)
    }

    override fun findEventByIdForAdmin(eventId: UUID): AbstractEventAdminResponse {
        val event = findEventEntityById(eventId)
        return eventMapper.asAdminResponse(event)
    }

    override fun findSchoolEventByIdForAdmin(
        schoolId: UUID,
        eventId: UUID,
    ): AbstractEventAdminResponse {
        schoolService.findSchoolById(schoolId)
        val event = findSchoolEntityById(schoolId, eventId)
        return eventMapper.asAdminResponse(event)
    }

    @Transactional
    override fun createEvent(
        userId: UUID,
        request: AbstractEventRequest,
        file: Part?,
    ): AbstractEventAdminResponse {
        eventValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val event = eventMapper.asEntity(request).also {
            it.author = user
            it.photoPath = file?.let {
                imageManager.storeImage(file, ImageStoreDir.EVENT, null, true)
            }
            eventDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return eventMapper.asAdminResponse(event)
    }

    @Transactional
    override fun updateEvent(
        userId: UUID,
        eventId: UUID,
        request: AbstractEventRequest,
    ): AbstractEventAdminResponse {
        eventValidator.validate(request)
        val event = findEventEntityById(eventId)
        eventMapper.update(event, request)
        event.setTags(tagMapper.asEntitySet(request.tags, event))
        contentPhotoService.queueContent(ContentPhotoRequest(eventId, event.content))
        return eventMapper.asAdminResponse(event)
    }

    @Transactional
    override fun uploadEventPhoto(
        userId: UUID,
        eventId: UUID,
        file: Part,
    ): AbstractEventAdminResponse {
        val event = findEventEntityById(eventId)
        event.photoPath = imageManager.storeImage(file, ImageStoreDir.EVENT, event.photoPath, true)
        return eventMapper.asAdminResponse(event)
    }

    @Transactional
    override fun deleteEvent(userId: UUID, eventId: UUID): DeletedResponse {
        val publication = findEventEntityById(eventId)
        eventDao.deleteById(publication.id as UUID)
        imageManager.remove(publication.photoPath)
        return DeletedResponse(Publication::class.java, eventId)
    }

    @Transactional
    override fun createSchoolEvent(
        userId: UUID,
        schoolId: UUID,
        request: EventRequest,
        file: Part?,
    ): EventAdminResponse {
        eventValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val school = schoolService.findSchoolEntityById(schoolId)
        val event = eventMapper.asEventEntity(request).also {
            it.author = user
            it.school = school
            it.photoPath = file?.let {
                imageManager.storeImage(file, ImageStoreDir.EVENT, null, true)
            }
            eventDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return eventMapper.asEventAdminResponse(event)
    }

    @Transactional
    override fun updateSchoolEvent(
        userId: UUID,
        schoolId: UUID,
        eventId: UUID,
        request: EventRequest,
    ): EventAdminResponse {
        eventValidator.validate(request)
        schoolService.findSchoolById(schoolId)
        val event = findSchoolEntityById(schoolId, eventId)
        if (event !is Event) throw ResourceNotFoundException(Event::class.java, eventId)
        eventMapper.update(event, request)
        event.setTags(tagMapper.asEntitySet(request.tags, event))
        contentPhotoService.queueContent(ContentPhotoRequest(eventId, event.content))
        return eventMapper.asEventAdminResponse(event)
    }

    @Transactional
    override fun uploadSchoolEventPhoto(
        userId: UUID,
        schoolId: UUID,
        eventId: UUID,
        file: Part,
    ): EventAdminResponse {
        schoolService.findSchoolById(schoolId)
        val event = findSchoolEntityById(schoolId, eventId)
        if (event !is Event) throw ResourceNotFoundException(Event::class.java, eventId)
        event.photoPath =
            imageManager.storeImage(file, ImageStoreDir.EVENT, event.photoPath, true)
        return eventMapper.asEventAdminResponse(event)
    }

    @Transactional
    override fun deleteSchoolEvent(userId: UUID, schoolId: UUID, eventId: UUID): DeletedResponse {
        schoolService.findSchoolById(schoolId)
        val event = findSchoolEntityById(schoolId, eventId)
        if (event !is Event) throw ResourceNotFoundException(Event::class.java, eventId)
        eventDao.deleteById(event.id as UUID)
        imageManager.remove(event.photoPath)
        return DeletedResponse(Event::class.java, eventId)
    }

    override fun listEventMembers(
        eventId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse> {
        val event = findEventEntityById(eventId)
        val userEvents = userEventDao.findByEvent(event, page.pageable)
        return userEventMapper.asPageUserResponse(userEvents)
    }

    override fun listSchoolEventMembers(
        schoolId: UUID,
        eventId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse> {
        val event = findSchoolEntityById(schoolId, eventId)
        val userEvents = userEventDao.findByEvent(event, page.pageable)
        return userEventMapper.asPageUserResponse(userEvents)
    }

    override fun joinEvent(userId: UUID, eventId: UUID): AbstractEventResponse {
        val event = findEventEntityById(eventId)
        val user = userService.findUserEntityById(userId)
        if (userEventDao.existsByEventAndUser(event, user)) {
            throw UserAlreadyEventMemberException(eventId, userId)
        }
        val userEvent = userEventMapper.asEntity(event, user)
        userEventDao.save(userEvent)
        return eventMapper.asResponse(event)
    }

    override fun leaveEvent(userId: UUID, eventId: UUID): AbstractEventResponse {
        val event = findEventEntityById(eventId)
        val user = userService.findUserEntityById(userId)
        val userEvent = userEventDao.findByEventAndUser(event, user)
            .orElseThrow { UserNotEventMemberException(eventId, userId) }
        userEventDao.delete(userEvent)
        return eventMapper.asResponse(event)
    }

    override fun findAllEvents(): Iterable<AbstractEvent> = eventDao.findAll()
}
