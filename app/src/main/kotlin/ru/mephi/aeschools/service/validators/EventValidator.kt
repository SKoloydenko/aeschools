package ru.mephi.aeschools.service.validators

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.model.dto.event.request.ForeignEventRequest
import ru.mephi.aeschools.model.exceptions.AppException
import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.tagsSize

@Component
class EventValidator : AbstractContentValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = AbstractValidator::class == clazz

    fun validateEventRequest(target: EventRequest, errors: Errors) {
        errors += target.location.validate("location") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
        errors += target.eventDate.validate("eventDate") {
            it.isNotNull()
            it.after(target.publicationDate)
        }
        errors += target.tags.validate("tags") {
            it.isNotNull()
            it.maxSize(tagsSize)
            it.forEach { tag -> tag.maxLength(extraSmallLength) }
        }
    }

    fun validateForeignEventRequest(target: ForeignEventRequest, errors: Errors) {
        errors += target.tags.validate("tags") {
            it.isNotNull()
            it.maxSize(tagsSize)
            it.forEach { tag -> tag.maxLength(extraSmallLength) }
        }
    }

    override fun validate(target: Any, errors: Errors) {
        super.validate(target, errors)
        when(target) {
            is EventRequest -> validateEventRequest(target, errors)
            is ForeignEventRequest -> validateForeignEventRequest(target, errors)
            else -> throw AppException(
                status = HttpStatus.UNPROCESSABLE_ENTITY,
                message = "Invalid request"
            )
        }
    }
}
