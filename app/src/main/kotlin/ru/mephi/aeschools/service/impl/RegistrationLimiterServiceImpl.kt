package ru.mephi.aeschools.service.impl

import com.hazelcast.core.HazelcastInstance
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.registration_limiter.RegistrationLimiterSettings
import ru.mephi.aeschools.database.repository.RegistrationLimiterDao
import ru.mephi.aeschools.model.dto.FeatureToggleUpdateRequest
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsRequest
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsResponse
import ru.mephi.aeschools.model.mappers.registraion_limiter.RegistrationLimiterMapper
import ru.mephi.aeschools.service.FeatureToggleService
import ru.mephi.aeschools.service.RegistrationLimiterService
import ru.mephi.aeschools.service.validators.RegistrationLimiterValidator
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong


@Service
class RegistrationLimiterServiceImpl(
    private val toggleFeatureToggleService: FeatureToggleService,
    private val dao: RegistrationLimiterDao,
    private val mapper: RegistrationLimiterMapper,
    private val validator: RegistrationLimiterValidator,
    @Qualifier("HzInstance")
    hazelcastInstance: HazelcastInstance,
    @Value("\${registration-limiter.deltaTime}")
    defaultDeltaTime: Long,
    @Value("\${registration-limiter.deltaCount}")
    defaultDeltaCount: Long,

    ) : RegistrationLimiterService {
    val id = AtomicLong()
    val map = hazelcastInstance.getMap<Long, String>(baseKey)
    private final var actualSettings =
        RegistrationLimiterSettings(defaultDeltaTime, defaultDeltaCount)
    private final var lastSaveFail = false

    init {
        try {
            actualSettings = dao.findByKey(settingsKey).get()
        } catch (e: Exception) {
            lastSaveFail = true
        }
    }

    override fun onRegistration() {
        if (!toggleFeatureToggleService.isFeatureEnabled("registrationLimiter")) return
        val settings = getSettings()
        map.put(id.getAndIncrement(), "", settings.deltaTime, TimeUnit.SECONDS)
        if (map.size > settings.deltaCount) {
            toggleFeatureToggleService.changeFeatureState(
                FeatureToggleUpdateRequest("registration", false)
            )
        }
    }

    override fun findSettings(): RegistrationLimiterSettings {
        return try {
            val optional = dao.findByKey(settingsKey)
            optional.orElseGet { save(actualSettings) }
        } catch (e: Exception) {
            e.printStackTrace()
            actualSettings
        }
    }

    override fun getSettings(): RegistrationLimiterSettingsResponse {
        return if (lastSaveFail) {
            return try {
                val settings = save(actualSettings)
                lastSaveFail = false
                mapper.asResponse(settings)
            } catch (e: Exception) {
                mapper.asResponse(actualSettings)
            }
        } else {
            actualSettings = findSettings()
            mapper.asResponse(actualSettings)
        }

    }

    override fun save(settings: RegistrationLimiterSettings): RegistrationLimiterSettings {
        return dao.save(settingsKey, settings)
    }

    override fun setSettings(settings: RegistrationLimiterSettingsRequest): RegistrationLimiterSettingsResponse {
        val entity = try {
            validator.validate(settings)
            map.clear()
            actualSettings = mapper.asEntity(settings)
            save(actualSettings)
        } catch (e: Exception) {
            lastSaveFail = true
            actualSettings
        }
        return mapper.asResponse(entity)
    }

    companion object {
        private const val baseKey = "registrationLimiter"
        private const val settingsKey = "$baseKey:settings"
    }
}
