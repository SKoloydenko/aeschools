package ru.mephi.aeschools.service.impl.excel

import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.xssf.usermodel.XSSFFont
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.core.io.ByteArrayResource
import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.dto.ExcelResponse
import ru.mephi.aeschools.model.dto.excel.ExcelFieldGetter
import ru.mephi.aeschools.model.dto.excel.proxy.getEventProxy
import ru.mephi.aeschools.model.dto.excel.proxy.getInternshipProxy
import ru.mephi.aeschools.model.dto.excel.proxy.getQuestionnaireProxy
import ru.mephi.aeschools.model.dto.excel.proxy.getReportProxy
import ru.mephi.aeschools.model.dto.excel.proxy.getUserProxy
import ru.mephi.aeschools.service.EventService
import ru.mephi.aeschools.service.ExcelService
import ru.mephi.aeschools.service.InternshipService
import ru.mephi.aeschools.service.QuestionnaireService
import ru.mephi.aeschools.service.ReportService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import ru.mephi.aeschools.util.writeToCell
import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


@Service
class ExcelServiceImpl(
    val userService: UserService,
    val reportService: ReportService,
    val storageHelper: StorageManagerHelper,
    val eventService: EventService,
    val schoolService: SchoolService,
    val questionnaireService: QuestionnaireService,
    val internshipService: InternshipService,
) : ExcelService {

    private fun getExcelFilename(name: String): String =
        name.replace(" ", "_") + "_" + DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .format(LocalDateTime.now())

    override fun exportUsers(): ExcelResponse {
        val users = userService.findAll()
        val data = users.map { Pair(it, questionnaireService.findAllUserQuestionnaires(it)) }
        return ExcelResponse(
            exportNested(
                "Пользователи с анкетами",
                getUserProxy(),
                getQuestionnaireProxy(),
                data
            ), getExcelFilename("users")
        )
    }

    override fun exportReport(sectionId: UUID): ExcelResponse {
        val section = reportService.findSectionById(sectionId)
        val data = reportService.getAllAnswersToSection(section)
        return ExcelResponse(
            export(
                section.title,
                getReportProxy(section, reportService),
                data
            ), getExcelFilename(section.title)
        )
    }

    override fun exportEvents(): ExcelResponse {
        val events = eventService.findAllEvents()
        val data = events.map { Pair(it, it.applicants) }
        return ExcelResponse(
            exportNested("Мероприятия", getEventProxy(), getUserProxy(), data),
            getExcelFilename("events")
        )
    }

    override fun exportSchoolEvents(schoolId: UUID): ExcelResponse {
        val school = schoolService.findSchoolEntityById(schoolId)
        return ExcelResponse(
            export(
                "Мероприятия площадки ${school.name}",
                getEventProxy(),
                school.events
            ), getExcelFilename("${school.name}_events")
        )
    }

    override fun exportEventUsers(eventId: UUID): ExcelResponse {
        val event = eventService.findEventEntityById(eventId)
        return ExcelResponse(
            export(
                "Участники мероприятия ${event.title}",
                getUserProxy(),
                event.applicants
            ), getExcelFilename("${event.title}_members")
        )
    }

    override fun exportUserEvents(userId: UUID): ExcelResponse {
        val user = userService.findUserEntityById(userId)
        return ExcelResponse(
            export("Мероприятия пользователя ${user.fullName}", getEventProxy(), user.joinedEvents),
            getExcelFilename("${user.fullName}_events")
        )
    }

    override fun exportInternships(): ExcelResponse {
        val internships = internshipService.findAllInternships()
        val data = internships.map { Pair(it, it.applicants) }
        return ExcelResponse(
            exportNested("Стажировки", getInternshipProxy(), getUserProxy(), data),
            getExcelFilename("internships")
        )
    }

    override fun exportSchoolInternships(schoolId: UUID): ExcelResponse {
        val school = schoolService.findSchoolEntityById(schoolId)
        return ExcelResponse(
            export(
                "Стажировки площадки ${school.name}",
                getInternshipProxy(),
                school.internships,
            ), getExcelFilename("${school.name}_internships")
        )
    }

    override fun exportInternshipApplicants(internshipId: UUID): ExcelResponse {
        val internship = internshipService.findInternshipEntityById(internshipId)
        return ExcelResponse(
            export(
                "Пользователи, подавшие заявку на стажировку ${internship.title}",
                getUserProxy(),
                internship.applicants
            ), getExcelFilename("${internship.title}_applicants")
        )
    }

    override fun exportUserInternships(userId: UUID): ExcelResponse {
        val user = userService.findUserEntityById(userId)
        return ExcelResponse(
            export(
                "Стажировки пользователя ${user.fullName}",
                getInternshipProxy(),
                user.appliedInternships
            ),
            getExcelFilename("${user.fullName}_internships")
        )
    }

    private fun <T> export(
        name: String,
        proxy: List<ExcelFieldGetter<T>>,
        data: Iterable<T>,
    ): ByteArrayResource {
        val workbook = XSSFWorkbook()
        val sheet = workbook.createSheet(name)
        export(name, sheet, workbook, proxy, data)

        val outputStream = ByteArrayOutputStream()
        workbook.write(outputStream)
        workbook.close()
        outputStream.close()
        return ByteArrayResource(outputStream.toByteArray())
    }

    private fun getHeaderStyle(workbook: XSSFWorkbook): CellStyle {
        val style = workbook.createCellStyle()
        val font = workbook.createFont()
        font.bold = true
        font.setFontHeight(16.0)
        style.setFont(font)
        return style
    }

    private fun getSimpleStyle(workbook: XSSFWorkbook): CellStyle {
        val style: CellStyle = workbook.createCellStyle()
        val font: XSSFFont = workbook.createFont()
        font.setFontHeight(14.0)
        style.setFont(font)
        return style
    }

    private fun <T> export(
        name: String,
        sheet: XSSFSheet,
        workbook: XSSFWorkbook,
        proxy: List<ExcelFieldGetter<T>>,
        list: Iterable<T>,
    ) {
        val headerStyle = getHeaderStyle(workbook)
        val simpleStyle = getSimpleStyle(workbook)

        var currentRow = 0
        val nameRow = sheet.createRow(currentRow++)
        nameRow.writeToCell(0, name, headerStyle)

        writeProxy(sheet, headerStyle, simpleStyle, proxy, list, currentRow)
    }

    private fun <T, V> exportNested(
        name: String,
        proxy: List<ExcelFieldGetter<T>>,
        nestedProxy: List<ExcelFieldGetter<V>>,
        data: Iterable<Pair<T, List<V>>>,
    ): ByteArrayResource {
        val workbook = XSSFWorkbook()
        val sheet = workbook.createSheet(name)
        exportNested(name, sheet, workbook, proxy, nestedProxy, data)

        val outputStream = ByteArrayOutputStream()
        workbook.write(outputStream)
        workbook.close()
        outputStream.close()
        return ByteArrayResource(outputStream.toByteArray())
    }

    private fun <T, V> exportNested(
        name: String,
        sheet: XSSFSheet,
        workbook: XSSFWorkbook,
        proxy: List<ExcelFieldGetter<T>>,
        nestedProxy: List<ExcelFieldGetter<V>>,
        list: Iterable<Pair<T, List<V>>>,
    ) {
        val headerStyle = getHeaderStyle(workbook)
        val simpleStyle = getSimpleStyle(workbook)

        var currentRowNum = 0
        val nameRow = sheet.createRow(currentRowNum++)
        nameRow.writeToCell(0, name, headerStyle)

        val headerRow = sheet.createRow(currentRowNum++)
        proxy.forEachIndexed { columnNum, getter ->
            headerRow.writeToCell(columnNum, getter.name, headerStyle)
        }
        list.forEach {
            val row = sheet.createRow(currentRowNum++)
            proxy.forEachIndexed { columnNum, getter ->
                row.writeToCell(columnNum, getter.getValue(it.first), simpleStyle)
            }
            if (it.second.isNotEmpty()) {
                currentRowNum = writeProxy(
                    sheet,
                    headerStyle,
                    simpleStyle,
                    nestedProxy,
                    it.second,
                    currentRowNum
                )
            }
            sheet.createRow(currentRowNum++)
        }
    }

    private fun <T> writeProxy(
        sheet: XSSFSheet,
        headerStyle: CellStyle,
        simpleStyle: CellStyle,
        proxy: List<ExcelFieldGetter<T>>,
        list: Iterable<T>,
        rowNum: Int,
    ): Int {
        var currentRowNum = rowNum

        val headerRow = sheet.createRow(currentRowNum++)
        proxy.forEachIndexed { columnNum, getter ->
            headerRow.writeToCell(columnNum, getter.name, headerStyle)
        }

        list.forEach {
            val row = sheet.createRow(currentRowNum++)
            proxy.forEachIndexed { columnNum, getter ->
                row.writeToCell(columnNum, getter.getValue(it), simpleStyle)
            }
        }
        return currentRowNum
    }
}
