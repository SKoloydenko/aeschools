package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Hidden
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolResponse
import ru.mephi.aeschools.model.dto.user.request.BanRequest
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.model.dto.user.response.UserPreviewResponse
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import ru.mephi.aeschools.service.impl.mail.UserIdUserEmailPair
import java.util.*

@Tag(name = "User API")
interface UserService {

    fun isBanned(userId: UUID): Boolean

    fun list(
        query: String,
        moderator: Boolean?,
        orderStudents: Boolean,
        page: ExtendedPageRequest,
    ): PageResponse<UserResponse>

    fun findUserEntityById(id: UUID): User
    fun findUserEntityByEmail(email: String): User
    fun findById(@PathVariable userId: UUID): UserResponse

    fun previewById(@PathVariable schoolId: UUID): UserPreviewResponse
    fun findSchool(@PathVariable schoolId: UUID): SchoolResponse
    fun update(
        @Parameter(hidden = true) id: UUID,
        @RequestBody request: UserRequest,
    ): UserResponse

    fun delete(
        @PathVariable userId: UUID,
        @Parameter(hidden = true) principal: UUID,
    ): DeletedResponse

    fun delete(@Parameter(hidden = true) id: UUID): DeletedResponse

    @Hidden
    fun delete(entity: User)
    fun setBanStatus(
        @PathVariable userId: UUID,
        @RequestBody request: BanRequest,
    ): UserResponse

    fun findAll(): Iterable<User>
    fun findUserIdUserEmailPairs(usersIds: List<UUID>): List<UserIdUserEmailPair>
}
