package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.model.dto.internship.response.InternshipAdminResponse
import ru.mephi.aeschools.model.dto.internship.response.InternshipResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Internship API")
interface InternshipService {
    fun findInternshipEntityById(internshipId: UUID): Internship
    fun isApplicant(internshipId: UUID, userId: UUID): Boolean
    fun listInternships(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipResponse>

    fun listSchoolInternships(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipResponse>

    fun listInternshipsForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipAdminResponse>

    fun listSchoolInternshipsForAdmin(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipAdminResponse>

    fun listInternshipsForUser(
        userId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<InternshipResponse>

    fun findInternshipById(@PathVariable internshipId: UUID): InternshipResponse

    fun findSchoolInternshipById(
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
    ): InternshipResponse

    fun findInternshipByIdForAdmin(@PathVariable internshipId: UUID): InternshipAdminResponse

    fun findSchoolInternshipByIdForAdmin(
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
    ): InternshipAdminResponse

    fun createInternship(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: InternshipRequest,
        @RequestPart file: Part?,
    ): InternshipAdminResponse

    fun updateInternship(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable internshipId: UUID,
        @RequestBody request: InternshipRequest,
    ): InternshipAdminResponse

    fun uploadInternshipPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable internshipId: UUID,
        @RequestPart file: Part,
    ): InternshipAdminResponse

    fun deleteInternship(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable internshipId: UUID,
    ): DeletedResponse

    fun createSchoolInternship(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: InternshipRequest,
        @RequestPart file: Part?,
    ): InternshipAdminResponse

    fun updateSchoolInternship(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
        @RequestBody request: InternshipRequest,
    ): InternshipAdminResponse

    fun uploadSchoolInternshipPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
        @RequestPart file: Part,
    ): InternshipAdminResponse

    fun deleteSchoolInternship(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
    ): DeletedResponse

    fun listInternshipApplicants(
        @PathVariable internshipId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse>

    fun listSchoolInternshipApplicants(
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse>

    fun findAllInternships(): Iterable<Internship>
}
