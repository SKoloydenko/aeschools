package ru.mephi.aeschools.service

import ru.mephi.aeschools.model.dto.mail.mails.BroadcastIgnorePreferencesMail
import ru.mephi.aeschools.model.dto.mail.mails.BroadcastMail
import ru.mephi.aeschools.model.dto.mail.mails.ResetPasswordMail
import ru.mephi.aeschools.model.dto.mail.mails.VerifyEmailMail

interface EmailSender {
    fun sendBroadcastMail(mail: BroadcastMail)
    fun sendBroadcastMailIgnorePreferences(mail: BroadcastIgnorePreferencesMail)
    fun sendVerifyEmailMail(mail: VerifyEmailMail)
    fun sendResetPasswordMail(mail: ResetPasswordMail)
}
