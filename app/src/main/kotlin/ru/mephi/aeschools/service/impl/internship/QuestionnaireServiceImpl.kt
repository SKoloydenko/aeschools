package ru.mephi.aeschools.service.impl.internship

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.internship.Internship
import ru.mephi.aeschools.database.entity.internship.Questionnaire
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.internship.QuestionnaireDao
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.model.dto.internship.response.QuestionnaireResponse
import ru.mephi.aeschools.model.enums.storage.FileStoreDir
import ru.mephi.aeschools.model.exceptions.internship.UserAlreadyInternshipApplicantException
import ru.mephi.aeschools.model.exceptions.internship.UserHasNoQuestionnairesException
import ru.mephi.aeschools.model.exceptions.internship.UserNotInternshipApplicantException
import ru.mephi.aeschools.model.mappers.internship.QuestionnaireMapper
import ru.mephi.aeschools.service.InternshipService
import ru.mephi.aeschools.service.QuestionnaireService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageFileManager
import ru.mephi.aeschools.service.validators.internship.QuestionnaireValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class QuestionnaireServiceImpl(
    private val fileManager: StorageFileManager,

    private val questionnaireValidator: QuestionnaireValidator,
    private val questionnaireDao: QuestionnaireDao,
    private val questionnaireMapper: QuestionnaireMapper,

    private val schoolService: SchoolService,
    private val userService: UserService,
    private val internshipService: InternshipService,
) : QuestionnaireService {

    private fun findQuestionnaire(internship: Internship, user: User) =
        questionnaireDao.findByInternshipAndUser(internship, user)
            .orElseThrow {
                UserNotInternshipApplicantException(internship.id as UUID, user.id as UUID)
            }

    private fun findSchoolQuestionnaire(school: School, internship: Internship, user: User) =
        questionnaireDao.findByInternshipSchoolAndInternshipAndUser(school, internship, user)
            .orElseThrow {
                UserNotInternshipApplicantException(internship.id as UUID, user.id as UUID)
            }

    override fun findQuestionnaireById(internshipId: UUID, userId: UUID): QuestionnaireResponse {
        val internship = internshipService.findInternshipEntityById(internshipId)
        val user = userService.findUserEntityById(userId)
        val questionnaire = findQuestionnaire(internship, user)
        return questionnaireMapper.asResponse(questionnaire)
    }

    override fun findSchoolQuestionnaireById(
        schoolId: UUID,
        internshipId: UUID,
        userId: UUID,
    ): QuestionnaireResponse {
        val school = schoolService.findSchoolEntityById(schoolId)
        val internship = internshipService.findInternshipEntityById(internshipId)
        val user = userService.findUserEntityById(userId)
        val questionnaire = findSchoolQuestionnaire(school, internship, user)
        return questionnaireMapper.asResponse(questionnaire)
    }

    override fun findLastQuestionnaire(userId: UUID): QuestionnaireResponse {
        userService.findUserEntityById(userId)
        val questionnaire = questionnaireDao.findLastQuestionnaire(userId).orElseThrow {
            UserHasNoQuestionnairesException(userId)
        }
        return questionnaireMapper.asResponse(questionnaire)
    }

    @Transactional
    override fun createQuestionnaire(
        internshipId: UUID,
        userId: UUID,
        request: QuestionnaireRequest,
        file: Part?,
    ): QuestionnaireResponse {
        questionnaireValidator.validate(request)
        val internship = internshipService.findInternshipEntityById(internshipId)
        val user = userService.findUserEntityById(userId)
        if (questionnaireDao.existsByInternshipAndUser(internship, user)) {
            throw UserAlreadyInternshipApplicantException(internshipId, userId)
        }
        val questionnaire = questionnaireMapper.asEntity(request).also {
            it.internship = internship
            it.user = user
            it.filePath = file?.let {
                fileManager.storeFile(file, FileStoreDir.QUESTIONNAIRE, null)
            }
            questionnaireDao.save(it)
        }
        return questionnaireMapper.asResponse(questionnaire)
    }

    @Transactional
    override fun updateQuestionnaire(
        internshipId: UUID,
        userId: UUID,
        request: QuestionnaireRequest,
    ): QuestionnaireResponse {
        questionnaireValidator.validate(request)
        val internship = internshipService.findInternshipEntityById(internshipId)
        val user = userService.findUserEntityById(userId)
        val questionnaire = findQuestionnaire(internship, user)
        questionnaireMapper.update(questionnaire, request)
        return questionnaireMapper.asResponse(questionnaire)
    }

    @Transactional
    override fun uploadQuestionnaireFile(
        internshipId: UUID,
        userId: UUID,
        file: Part,
    ): QuestionnaireResponse {
        val internship = internshipService.findInternshipEntityById(internshipId)
        val user = userService.findUserEntityById(userId)
        val questionnaire = findQuestionnaire(internship, user)
        questionnaire.filePath =
            fileManager.storeFile(file, FileStoreDir.QUESTIONNAIRE, questionnaire.filePath)
        return questionnaireMapper.asResponse(questionnaire)
    }

    @Transactional
    override fun deleteQuestionnaire(
        internshipId: UUID,
        userId: UUID,
    ): DeletedResponse {
        val internship = internshipService.findInternshipEntityById(internshipId)
        val user = userService.findUserEntityById(userId)
        val questionnaire = findQuestionnaire(internship, user)
        questionnaireDao.delete(questionnaire)
        fileManager.remove(questionnaire.filePath)
        return DeletedResponse(Questionnaire::class.java, questionnaire.id as UUID)
    }

    override fun findAllUserQuestionnaires(user: User): List<Questionnaire> =
        questionnaireDao.findByUser(user)
}
