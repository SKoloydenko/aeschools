package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.section.request.CreateSectionRequest
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class SectionValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = CreateSectionRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as CreateSectionRequest
        errors += target.title.validate("title") { it.maxLength(largeLength) }
    }
}
