package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.model.dto.school.response.IndustrialPartnerResponse
import ru.mephi.aeschools.model.dto.school.response.LeaderResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolResponse
import ru.mephi.aeschools.model.dto.school.response.UniversityPartnerResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class SchoolServiceDecorator(
    @Qualifier("schoolServiceImpl")
    private val schoolService: SchoolService,
    private val userActionService: UserActionService,
) : SchoolService by schoolService {

    override fun createSchool(userId: UUID, request: SchoolRequest, file: Part?): SchoolResponse {
        val response = schoolService.createSchool(userId, request, file)
        userActionService.save(userId, ActionTarget.SCHOOL, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateSchool(userId: UUID, id: UUID, request: SchoolRequest): SchoolResponse {
        val response = schoolService.updateSchool(userId, id, request)
        userActionService.save(userId, ActionTarget.SCHOOL, ActionVerb.UPDATE, id)
        return response
    }

    override fun uploadSchoolPhoto(userId: UUID, id: UUID, file: Part): SchoolResponse {
        val response = schoolService.uploadSchoolPhoto(userId, id, file)
        userActionService.save(userId, ActionTarget.SCHOOL, ActionVerb.UPLOAD_PHOTO, id)
        return response
    }

    override fun deleteSchool(userId: UUID, id: UUID): DeletedResponse {
        val response = schoolService.deleteSchool(userId, id)
        userActionService.save(userId, ActionTarget.SCHOOL, ActionVerb.DELETE, id)
        return response
    }

    override fun createLeader(
        userId: UUID,
        schoolId: UUID,
        request: LeaderRequest,
        file: Part?,
    ): LeaderResponse {
        val response = schoolService.createLeader(userId, schoolId, request, file)
        userActionService.save(userId, ActionTarget.LEADER, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateLeader(
        userId: UUID,
        schoolId: UUID,
        leaderId: UUID,
        request: LeaderRequest,
    ): LeaderResponse {
        val response = schoolService.updateLeader(userId, schoolId, leaderId, request)
        userActionService.save(userId, ActionTarget.LEADER, ActionVerb.UPDATE, leaderId)
        return response
    }

    override fun uploadLeaderPhoto(
        userId: UUID,
        schoolId: UUID,
        leaderId: UUID,
        file: Part,
    ): LeaderResponse {
        val response = schoolService.uploadLeaderPhoto(userId, schoolId, leaderId, file)
        userActionService.save(userId, ActionTarget.LEADER, ActionVerb.UPLOAD_PHOTO, leaderId)
        return response
    }

    override fun deleteLeader(userId: UUID, schoolId: UUID, leaderId: UUID): DeletedResponse {
        val response = schoolService.deleteLeader(userId, schoolId, leaderId)
        userActionService.save(userId, ActionTarget.LEADER, ActionVerb.DELETE, leaderId)
        return response
    }

    override fun createIndustrialPartner(
        userId: UUID,
        schoolId: UUID,
        request: IndustrialPartnerRequest,
        file: Part?,
    ): IndustrialPartnerResponse {
        val response = schoolService.createIndustrialPartner(userId, schoolId, request, file)
        userActionService.save(
            userId,
            ActionTarget.INDUSTRIAL_PARTNER,
            ActionVerb.CREATE,
            response.id
        )
        return response
    }

    override fun updateIndustrialPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        request: IndustrialPartnerRequest,
    ): IndustrialPartnerResponse {
        val response = schoolService.updateIndustrialPartner(userId, schoolId, partnerId, request)
        userActionService.save(
            userId,
            ActionTarget.INDUSTRIAL_PARTNER,
            ActionVerb.UPDATE,
            partnerId
        )
        return response
    }

    override fun uploadIndustrialPartnerPhoto(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        file: Part,
    ): IndustrialPartnerResponse {
        val response = schoolService.uploadIndustrialPartnerPhoto(userId, schoolId, partnerId, file)
        userActionService.save(
            userId,
            ActionTarget.INDUSTRIAL_PARTNER,
            ActionVerb.UPLOAD_PHOTO,
            partnerId
        )
        return response
    }

    override fun deleteIndustrialPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
    ): DeletedResponse {
        val response = schoolService.deleteIndustrialPartner(userId, schoolId, partnerId)
        userActionService.save(
            userId,
            ActionTarget.INDUSTRIAL_PARTNER,
            ActionVerb.DELETE,
            partnerId
        )
        return response
    }

    override fun createUniversityPartner(
        userId: UUID,
        schoolId: UUID,
        request: UniversityPartnerRequest,
        file: Part?,
    ): UniversityPartnerResponse {
        val response = schoolService.createUniversityPartner(userId, schoolId, request, file)
        userActionService.save(
            userId,
            ActionTarget.UNIVERSITY_PARTNER,
            ActionVerb.CREATE,
            response.id
        )
        return response
    }

    override fun updateUniversityPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        request: UniversityPartnerRequest,
    ): UniversityPartnerResponse {
        val response = schoolService.updateUniversityPartner(userId, schoolId, partnerId, request)
        userActionService.save(
            userId,
            ActionTarget.UNIVERSITY_PARTNER,
            ActionVerb.UPDATE,
            partnerId
        )
        return response
    }

    override fun uploadUniversityPartnerPhoto(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        file: Part,
    ): UniversityPartnerResponse {
        val response = schoolService.uploadUniversityPartnerPhoto(userId, schoolId, partnerId, file)
        userActionService.save(
            userId,
            ActionTarget.UNIVERSITY_PARTNER,
            ActionVerb.UPLOAD_PHOTO,
            partnerId
        )
        return response
    }

    override fun deleteUniversityPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
    ): DeletedResponse {
        val response = schoolService.deleteUniversityPartner(userId, schoolId, partnerId)
        userActionService.save(
            userId,
            ActionTarget.UNIVERSITY_PARTNER,
            ActionVerb.DELETE,
            partnerId
        )
        return response
    }

    override fun createModerator(
        principal: UUID,
        schoolId: UUID,
        userId: UUID,
    ): UserShortResponse {
        val response = schoolService.createModerator(principal, schoolId, userId)
        userActionService.save(principal, ActionTarget.MODERATOR, ActionVerb.CREATE, userId)
        return response
    }

    override fun deleteModerator(principal: UUID, schoolId: UUID, userId: UUID): DeletedResponse {
        val response = schoolService.deleteModerator(principal, schoolId, userId)
        userActionService.save(principal, ActionTarget.MODERATOR, ActionVerb.DELETE, userId)
        return response
    }
}
