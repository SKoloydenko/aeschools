package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.best_practice.request.BestPracticeRequest
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeAdminResponse
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Best Practice API")
interface BestPracticeService {

    fun listBestPractices(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeResponse>

    fun listSchoolBestPractices(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeResponse>

    fun listBestPracticesForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeAdminResponse>

    fun listSchoolBestPracticesForAdmin(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeAdminResponse>

    fun findBestPracticeById(@PathVariable bestPracticeId: UUID): BestPracticeResponse

    fun findSchoolBestPracticeById(
        @PathVariable schoolId: UUID,
        @PathVariable bestPracticeId: UUID,
    ): BestPracticeResponse

    fun findBestPracticeByIdForAdmin(@PathVariable bestPracticeId: UUID): BestPracticeAdminResponse

    fun findSchoolBestPracticeByIdForAdmin(
        @PathVariable schoolId: UUID,
        @PathVariable bestPracticeId: UUID,
    ): BestPracticeAdminResponse

    fun createBestPractice(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: BestPracticeRequest,
        @RequestPart file: Part?,
    ): BestPracticeAdminResponse

    fun updateBestPractice(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable bestPracticeId: UUID,
        @RequestBody request: BestPracticeRequest,
    ): BestPracticeAdminResponse

    fun uploadBestPracticePhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable bestPracticeId: UUID,
        @RequestPart file: Part,
    ): BestPracticeAdminResponse

    fun deleteBestPractice(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable bestPracticeId: UUID,
    ): DeletedResponse

    fun createSchoolBestPractice(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: BestPracticeRequest,
        @RequestPart file: Part?,
    ): BestPracticeAdminResponse

    fun updateSchoolBestPractice(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable bestPracticeId: UUID,
        @RequestBody request: BestPracticeRequest,
    ): BestPracticeAdminResponse

    fun uploadSchoolBestPracticePhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable bestPracticeId: UUID,
        @RequestPart file: Part,
    ): BestPracticeAdminResponse

    fun deleteSchoolBestPractice(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable bestPracticeId: UUID,
    ): DeletedResponse
}
