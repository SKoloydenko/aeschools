package ru.mephi.aeschools.service

import ru.mephi.aeschools.model.dto.mail.MailReceiver
import ru.mephi.aeschools.model.enums.BroadcastType
import java.util.*

interface DataSenderHelper {
    fun getReceiversIds(broadcastType: BroadcastType, ignorePreferences: Boolean): List<UUID>
    fun getMailReceivers(
        broadcastType: BroadcastType,
        ignorePreferences: Boolean,
    ): List<MailReceiver>
}
