package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsRequest

@Component
class RegistrationLimiterValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) =
        clazz == RegistrationLimiterSettingsRequest::class.java

    override fun validate(target: Any, errors: Errors) {
        this.target = target as RegistrationLimiterSettingsRequest

        if (target.deltaCount <= 0)
            errors.reject("deltaCount", "Must be positive number")
        if (target.deltaTime < 0)
            errors.reject("deltaTime", "Must be zero or positive number")
    }
}
