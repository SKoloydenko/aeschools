package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.library.request.LibraryRecordRequest
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class LibraryValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = clazz == LibraryRecordRequest::class.java

    override fun validate(target: Any, errors: Errors) {
        this.target = target as LibraryRecordRequest
        errors += target.title.validate("title") { it.maxLength(smallLength) }
        errors += target.content.validate("content") { it.maxLength(extraLargeLength) }
        if (target.link != null) errors += target.link.validate("content") {
            it.maxLength(
                largeLength
            )
        }
    }
}
