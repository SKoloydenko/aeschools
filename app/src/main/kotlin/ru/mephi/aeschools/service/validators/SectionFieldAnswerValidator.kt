package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.section.request.FieldAnswerRequest
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.publicationLength

@Component
class SectionFieldAnswerValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = FieldAnswerRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as FieldAnswerRequest
        errors += target.answer.validate("answer") { it.maxLength(publicationLength) }

    }
}
