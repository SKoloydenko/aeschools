package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.report.Section
import ru.mephi.aeschools.database.entity.report.SectionField
import ru.mephi.aeschools.database.entity.report.SectionFieldAnswer
import ru.mephi.aeschools.database.entity.report.SectionFieldType
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.repository.report.SectionDao
import ru.mephi.aeschools.database.repository.report.SectionFieldAnswerDao
import ru.mephi.aeschools.database.repository.report.SectionFieldDao
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.excel.proxy.ReportExcelProxyHelper
import ru.mephi.aeschools.model.dto.section.ReportSectionFilterType
import ru.mephi.aeschools.model.dto.section.request.CreateFieldRequest
import ru.mephi.aeschools.model.dto.section.request.CreateSectionRequest
import ru.mephi.aeschools.model.dto.section.request.FieldAnswerRequest
import ru.mephi.aeschools.model.dto.section.request.SectionAnswerRequest
import ru.mephi.aeschools.model.dto.section.request.UpdateFieldRequest
import ru.mephi.aeschools.model.dto.section.response.*
import ru.mephi.aeschools.model.enums.storage.FileStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.exceptions.report.FieldNotExistsInSectionException
import ru.mephi.aeschools.model.exceptions.report.FieldNotSupportsFileUploadException
import ru.mephi.aeschools.model.exceptions.report.FieldNotSupportsFreeFormAnswer
import ru.mephi.aeschools.model.exceptions.report.SectionFieldIsBlockedException
import ru.mephi.aeschools.model.exceptions.report.SectionIsBlockedException
import ru.mephi.aeschools.model.mappers.report.ReportMapper
import ru.mephi.aeschools.service.ReportService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageFileManager
import ru.mephi.aeschools.service.validators.SectionFieldAnswerValidator
import ru.mephi.aeschools.service.validators.SectionFieldValidator
import ru.mephi.aeschools.service.validators.SectionValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
@Transactional
class ReportServiceImpl(
    private val sectionDao: SectionDao,
    private val fieldDao: SectionFieldDao,
    private val answerDao: SectionFieldAnswerDao,

    private val mapper: ReportMapper,

    private val sectionValidator: SectionValidator,
    private val fieldValidator: SectionFieldValidator,
    private val answerValidator: SectionFieldAnswerValidator,

    private val schoolService: SchoolService,
    private val storageService: StorageFileManager,
    private val userService: UserService,
) : ReportService {

    override fun findSectionById(sectionId: UUID): Section {
        return sectionDao.findById(sectionId)
            .orElseThrow { ResourceNotFoundException(Section::class.java, sectionId) }
    }

    private fun findFieldById(fieldId: UUID): SectionField {
        return fieldDao.findById(fieldId)
            .orElseThrow { ResourceNotFoundException(SectionField::class.java, fieldId) }
    }

    private fun findFieldInSection(sectionId: UUID, fieldId: UUID): SectionField {
        if (!sectionDao.existsById(sectionId))
            throw ResourceNotFoundException(Section::class.java, sectionId)
        return fieldDao.findByIdAndSectionId(fieldId, sectionId)
            .orElseThrow { FieldNotExistsInSectionException(sectionId, fieldId) }
    }

    override fun createSection(userId: UUID, request: CreateSectionRequest): SectionResponse {
        sectionValidator.validate(request)
        val section = mapper.asEntity(request)
        val entity = sectionDao.save(section)
        return mapper.asResponse(entity, this)
    }

    override fun getSection(sectionId: UUID): SectionResponse {
        val entity = findSectionById(sectionId)
        return mapper.asResponse(entity, this)
    }

    override fun listSections(
        request: ExtendedPageRequest,
        userId: UUID,
        query: String,
        filter: ReportSectionFilterType,
    ): PageResponse<SectionResponse> {
        val user = userService.findUserEntityById(userId)
        val schoolId = if (!user.admin) schoolService.findSchoolByModeratorId(userId).id else null
        val page = if (filter == ReportSectionFilterType.ALL)
            sectionDao.findAllByTitleStartsWith(query, request.pageable)
        else sectionDao.findAllByIsBlockedAndTitleStartingWith(
                filter == ReportSectionFilterType.BLOCKED,
                query,
                request.pageable)
        return mapper.asPageResponse(page, this, schoolId)
    }

    override fun updateSection(
        userId: UUID,
        sectionId: UUID,
        request: CreateSectionRequest,
    ): SectionResponse {
        sectionValidator.validate(request)
        val entity = findSectionById(sectionId)
        mapper.update(entity, request)
        return mapper.asResponse(entity, this)
    }

    override fun removeSection(userId: UUID, sectionId: UUID): DeletedResponse {
        val entity = findSectionById(sectionId)
        sectionDao.delete(entity)
        return DeletedResponse(Section::class.java, sectionId)
    }

    override fun listFields(
        sectionId: UUID,
        request: ExtendedPageRequest,
        userId: UUID
    ): PageResponse<SectionFieldResponse> {
        val user = userService.findUserEntityById(userId)
        val section = findSectionById(sectionId)
        val page = fieldDao.findAllBySection(section, request.pageable)
        return mapper.asPageFieldsResponse(page, user.admin, this)
    }

    override fun createField(
        userId: UUID,
        sectionId: UUID,
        request: CreateFieldRequest,
    ): SectionFieldResponse {
        fieldValidator.validate(request)
        var entity = mapper.asEntity(request).also {
            it.section = findSectionById(sectionId)
        }
        entity = fieldDao.save(entity)
        return mapper.asResponse(entity, true, this)
    }

    override fun getField(sectionId: UUID, fieldId: UUID, userId: UUID): SectionFieldResponse {
        val user = userService.findUserEntityById(userId)
        val entity = findFieldInSection(sectionId, fieldId)
        return mapper.asResponse(entity, user.admin, this)
    }

    override fun getTotalFieldsCount(id: UUID): Long {
        return fieldDao.countBySectionId(id)
    }

    override fun getAnswersCountToSectionBySchoolId(id: UUID, schoolId: UUID): Int {
        return answerDao.countBySchoolIdAndSectionFieldSectionId(schoolId, id)
    }

    override fun fieldsPageByUser(
        userId: UUID,
        sectionId: UUID,
        request: ExtendedPageRequest,
    ): PageResponse<SectionFieldWithAnswerResponse> {
        val school = schoolService.findSchoolByModeratorId(userId)
        return fieldsPage(school, sectionId, request)
    }

    override fun fieldsPageOfSchool(
        schoolId: UUID,
        sectionId: UUID,
        request: ExtendedPageRequest,
    ): PageResponse<SectionFieldWithAnswerResponse> {
        val school = schoolService.findSchoolEntityById(schoolId)
        return fieldsPage(school, sectionId, request)
    }

    private fun fieldsPage(
        school: School,
        sectionId: UUID,
        request: ExtendedPageRequest,
    ): PageResponse<SectionFieldWithAnswerResponse> {
        val section = findSectionById(sectionId)
        val fields = fieldDao.findAllBySection(section, request.pageable)
        val answers =
            answerDao.findAllBySectionFieldIdInAndSchool(fields.content.map { it.id!! }
                .toMutableList(), school)
        return mapper.asPageResponse(fields, answers, this, school.id!!)
    }

    override fun updateField(
        userId: UUID,
        sectionId: UUID,
        fieldId: UUID,
        request: UpdateFieldRequest,
    ): SectionFieldResponse {
        fieldValidator.validate(request)
        val entity = findFieldInSection(sectionId, fieldId)
        mapper.update(entity, request)
        return mapper.asResponse(entity, true, this)
    }

    override fun removeFiled(userId: UUID, sectionId: UUID, fieldId: UUID): DeletedResponse {
        val entity = findFieldInSection(sectionId, fieldId)
        fieldDao.delete(entity)
        return DeletedResponse(SectionField::class.java, fieldId)
    }

    override fun answerToSection(
        userId: UUID,
        sectionId: UUID,
        request: SectionAnswerRequest,
    ): List<SectionFieldWithAnswerResponse> {
        val school = schoolService.findSchoolByModeratorId(userId)
        val section = findSectionById(sectionId)
        if (section.isBlocked) throw SectionIsBlockedException(sectionId)
        val answers = mutableListOf<SectionFieldAnswer>()
        request.answers.forEach { element ->
            answerValidator.validate(request)
            val field = section.fields.find { it.id == element.id }
                ?: throw FieldNotExistsInSectionException(sectionId, element.id)
            if (field.isBlocked) throw SectionFieldIsBlockedException(element.id)
            if (field.type != SectionFieldType.FREE_FORM) throw FieldNotSupportsFreeFormAnswer(field.id!!)
            answerDao.deleteBySectionFieldAndSchool(field, school)
            answers.add(
                answerDao.save(
                    SectionFieldAnswer(
                        school,
                        field,
                        element.answer,
                        field.type
                    )
                )
            )
        }
        return answers.map { mapper.asResponseWithAnswer(it) }
    }

    override fun answerToField(
        userId: UUID,
        sectionId: UUID,
        fieldId: UUID,
        request: FieldAnswerRequest,
    ): SectionFieldWithAnswerResponse {
        answerValidator.validate(request)
        val school = schoolService.findSchoolByModeratorId(userId)
        val field = findFieldInSection(sectionId, fieldId)
        if (field.section.isBlocked) throw SectionIsBlockedException(sectionId)
        if (field.isBlocked) throw SectionFieldIsBlockedException(fieldId)
        if (field.type != SectionFieldType.FREE_FORM) throw FieldNotSupportsFreeFormAnswer(fieldId)
        answerDao.deleteBySectionFieldAndSchool(field, school)
        val answer = answerDao.save(SectionFieldAnswer(school, field, request.answer, field.type))
        return mapper.asResponseWithAnswer(answer)
    }

    override fun deleteAnswer(userId: UUID, sectionId: UUID, fieldId: UUID): DeletedResponse {
        val user = schoolService.findSchoolByModeratorId(userId)
        val school = schoolService.findSchoolByModeratorId(userId)
        val field = findFieldInSection(sectionId, fieldId)
        if (field.section.isBlocked) throw SectionIsBlockedException(sectionId)
        if (field.isBlocked) throw SectionFieldIsBlockedException(fieldId)
        val answer = findAnswerToField(field, school)
        answerDao.delete(answer)
        return DeletedResponse(SectionFieldAnswer::class.java, answer.id!!)
    }

    override fun uploadFileToField(
        userId: UUID,
        sectionId: UUID,
        fieldId: UUID,
        file: Part,
    ): SectionFieldWithAnswerResponse {
        val school = schoolService.findSchoolByModeratorId(userId)
        val field = findFieldInSection(sectionId, fieldId)
        if (field.section.isBlocked) throw SectionIsBlockedException(sectionId)
        if (field.isBlocked) throw SectionFieldIsBlockedException(fieldId)
        if (field.type != SectionFieldType.FILE) throw FieldNotSupportsFileUploadException(fieldId)
        val answer = answerDao.findBySectionFieldAndSchool(field, school)
        val path = storageService.storeFile(file, FileStoreDir.REPORTS, answer?.answer)
        if (answer != null) answerDao.delete(answer)
        val entity = answerDao.save(SectionFieldAnswer(school, field, path, field.type))
        return mapper.asResponseWithAnswer(entity)
    }

    override fun getAnswerToField(userId: UUID, sectionId: UUID, fieldId: UUID): SectionFieldWithAnswerResponse {
        val school = schoolService.findSchoolByModeratorId(userId)
        val field = findFieldInSection(sectionId, fieldId)
        val answer = answerDao.findBySectionFieldAndSchool(field, school)
        return mapper.asResponseWithAnswer(field, answer)
    }

    override fun getAllAnswersToSection(section: Section): List<ReportExcelProxyHelper> {
        val answers = answerDao.findAllBySectionField_Section(section)
        val map = HashMap<UUID, MutableList<SectionFieldAnswer>>()
        answers.forEach {
            val key = it.school.id!!
            map.putIfAbsent(key, mutableListOf())
            map[key]!!.add(it)
        }
        return map.keys.map {
            ReportExcelProxyHelper(map[it]!!.first().school, map[it]!!)
        }
    }

    override fun existFileAnswerWithModerator(filePath: String, userId: UUID): Boolean {
        val school = schoolService.findSchoolByModeratorId(userId)
        return answerDao.existsByAnswerAndSchoolAndType(filePath, school, SectionFieldType.FILE)
    }

    override fun getAllAnswersToField(
        sectionId: UUID,
        fieldId: UUID,
        request: ExtendedPageRequest,
    ): PageResponse<FullSectionFieldAnswerResponse> {
        val field = findFieldInSection(sectionId, fieldId)
        val answers = answerDao.findAllBySectionField(field, request.pageable)
        return mapper.asPageResponse(answers)
    }

    override fun getAnswerOfSchoolToField(
        sectionId: UUID,
        fieldId: UUID,
        schoolId: UUID,
    ): SectionFieldWithAnswerResponse {
        val field = findFieldInSection(sectionId, fieldId)
        val school = schoolService.findSchoolEntityById(schoolId)
        val answer = answerDao.findBySectionFieldAndSchool(field, school)
        return mapper.asResponseWithAnswer(field, answer)
    }

    override fun getSchoolCountOfFullFillSection(section: Section): Int {
        val totalFields = fieldDao.countBySectionId(section.id!!)
        if (totalFields == 0L) return 0
        return answerDao.countBySchoolWithFullFillSection(section.id!!, totalFields).toInt()
    }

    override fun getAllSchoolCount(): Int {
        return schoolService.allSchoolsCount()
    }

    override fun getAnswersCountToField(field: SectionField): Int {
        return answerDao.countBySectionField(field).toInt()
    }

    override fun getLinkToReportFile(path: String): String {
        return "https://aeschools.mephi.ru/downloads/admin?path=/file/$path"
    }

    private fun findAnswerToField(field: SectionField, school: School): SectionFieldAnswer {
        return answerDao.findBySectionFieldAndSchool(field, school)
            ?: throw ResourceNotFoundException(SectionFieldAnswer::class.java,
                "field id [$field] and schoolId [${school.id}]")
    }
}
