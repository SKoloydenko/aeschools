package ru.mephi.aeschools.service.impl.mail

import org.springframework.stereotype.Service
import ru.mephi.aeschools.config.properties.ServerUrls
import ru.mephi.aeschools.database.entity.email.ResetPasswordToken
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.email.ResetPasswordTokenDao
import ru.mephi.aeschools.service.ResetPasswordService
import ru.mephi.aeschools.util.StringGenerator
import java.time.LocalDateTime
import java.util.*

@Service
class ResetPasswordServiceImpl(
    val tokenDao: ResetPasswordTokenDao,
    val generator: StringGenerator,
    val resetPasswordTokenDao: ResetPasswordTokenDao,
    val serverUrls: ServerUrls,
) : ResetPasswordService {

    override fun createNewToken(user: User): ResetPasswordToken {
        var haveUnitSalt = false
        var string = ""
        while (!haveUnitSalt) {
            string = generator.getRandomString(20)
            haveUnitSalt = true
            tokenDao.findByToken(string).ifPresent { haveUnitSalt = false }
        }
        val token = ResetPasswordToken(user, string)
        return tokenDao.save(token)
    }

    override fun getLinkToResetPassword(token: ResetPasswordToken): String {
        return "https://${serverUrls.baseUrl}/auth/password-reset/${token.token}"
    }

    override fun findByUserId(userId: UUID): Optional<ResetPasswordToken> {
        return tokenDao.findByUserId(userId)
    }

    override fun findByToken(token: String): Optional<ResetPasswordToken> {
        return tokenDao.findByToken(token)
    }

    override fun tokenIsExist(token: String): Boolean {
        val optional = resetPasswordTokenDao.findByToken(token)
        if (!optional.isPresent) return false
        return !optional.get().createdAt.isBefore(
            LocalDateTime.now().minusMinutes(15)
        )
    }
}
