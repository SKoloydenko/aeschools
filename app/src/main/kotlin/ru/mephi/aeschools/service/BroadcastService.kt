package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.broadcast.request.BroadcastRequest
import ru.mephi.aeschools.model.dto.broadcast.response.BroadcastResponse
import java.util.*

@Tag(name = "Broadcast API")
interface BroadcastService {

    fun listBroadcasts(page: ExtendedPageRequest): PageResponse<BroadcastResponse>

    fun findBroadcastById(@PathVariable broadcastId: UUID): BroadcastResponse

    fun sendBroadcastMail(
        @Parameter(hidden = true) userId: UUID,
        request: BroadcastRequest,
    ): BroadcastResponse
}
