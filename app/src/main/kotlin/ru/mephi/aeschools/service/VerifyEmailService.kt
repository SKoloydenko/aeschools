package ru.mephi.aeschools.service

import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.entity.user.VerifyEmailToken
import java.util.*

interface VerifyEmailService {
    fun sendEmailToVerifyEmail(user: User)
    fun verifyEmail(token: String)
    fun emailIsVerified(user: User): Boolean
    fun findVerifyTokenById(id: UUID): VerifyEmailToken
    fun findVerifyTokenByToken(token: String): VerifyEmailToken
    fun existsById(id: UUID): Boolean
    fun userHaveExpiredEmailVerification(id: UUID): Boolean
    fun findAll(): Iterable<VerifyEmailToken>
    fun tokenIsExpired(token: VerifyEmailToken): Boolean
    fun deleteById(id: UUID)
}
