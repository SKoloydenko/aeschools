package ru.mephi.aeschools.service.decorators

import io.swagger.v3.oas.annotations.Parameter
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.library.request.LibraryRecordRequest
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordResponse
import ru.mephi.aeschools.service.LibraryService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Primary
@Service
class LibraryServiceDecorator(
    @Qualifier("libraryServiceImpl")
    private val service: LibraryService,
    private val actionService: UserActionService,
) : LibraryService by service {

    override fun create(
        userId: UUID,
        request: LibraryRecordRequest,
        file: Part?,
    ): LibraryRecordResponse {
        val response = service.create(userId, request)
        actionService.save(userId, ActionTarget.LIBRARY, ActionVerb.CREATE, response.id)
        return response
    }

    override fun update(
        userId: UUID,
        recordId: UUID,
        request: LibraryRecordRequest,
        file: Part?,
    ): LibraryRecordResponse {
        val response = service.update(userId, recordId, request)
        actionService.save(userId, ActionTarget.LIBRARY, ActionVerb.UPDATE, response.id)
        return response
    }

    override fun delete(userId: UUID, recordId: UUID): DeletedResponse {
        val response = service.delete(userId, recordId)
        actionService.save(userId, ActionTarget.LIBRARY, ActionVerb.DELETE, response.id)
        return response
    }

    override fun uploadFile(userId: UUID, recordId: UUID, part: Part): LibraryRecordResponse {
        val response = service.uploadFile(userId, recordId, part)
        actionService.save(userId, ActionTarget.LIBRARY, ActionVerb.UPLOAD_FILE, response.id)
        return response
    }
}
