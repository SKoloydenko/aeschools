package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.school.IndustrialPartner
import ru.mephi.aeschools.database.entity.school.Leader
import ru.mephi.aeschools.database.entity.school.ModeratorSchool
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.database.entity.school.UniversityPartner
import ru.mephi.aeschools.database.repository.school.IndustrialPartnerDao
import ru.mephi.aeschools.database.repository.school.LeaderDao
import ru.mephi.aeschools.database.repository.school.ModeratorSchoolDao
import ru.mephi.aeschools.database.repository.school.SchoolDao
import ru.mephi.aeschools.database.repository.school.UniversityPartnerDao
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.model.dto.school.response.IndustrialPartnerResponse
import ru.mephi.aeschools.model.dto.school.response.LeaderResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import ru.mephi.aeschools.model.dto.school.response.UniversityPartnerResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceAlreadyExistsException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.exceptions.school.StudentCanNotBeModerator
import ru.mephi.aeschools.model.exceptions.school.UserAlreadyModeratorException
import ru.mephi.aeschools.model.exceptions.school.UserNotModeratorException
import ru.mephi.aeschools.model.exceptions.school.UserNotModeratorInSchoolException
import ru.mephi.aeschools.model.exceptions.user.BannedException
import ru.mephi.aeschools.model.mappers.school.IndustrialPartnerMapper
import ru.mephi.aeschools.model.mappers.school.LeaderMapper
import ru.mephi.aeschools.model.mappers.school.ModeratorSchoolMapper
import ru.mephi.aeschools.model.mappers.school.SchoolMapper
import ru.mephi.aeschools.model.mappers.school.UniversityPartnerMapper
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.school.IndustrialPartnerValidator
import ru.mephi.aeschools.service.validators.school.LeaderValidator
import ru.mephi.aeschools.service.validators.school.SchoolValidator
import ru.mephi.aeschools.service.validators.school.UniversityPartnerValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class SchoolServiceImpl(
    private var storageManager: StorageImageManager,

    private val schoolValidator: SchoolValidator,
    private val leaderValidator: LeaderValidator,
    private val industrialPartnerValidator: IndustrialPartnerValidator,
    private val universityPartnerValidator: UniversityPartnerValidator,

    private val schoolDao: SchoolDao,
    private val leaderDao: LeaderDao,
    private val moderatorSchoolDao: ModeratorSchoolDao,
    private val industrialPartnerDao: IndustrialPartnerDao,
    private val universityPartnerDao: UniversityPartnerDao,

    private val schoolMapper: SchoolMapper,
    private val leaderMapper: LeaderMapper,
    private val moderatorSchoolMapper: ModeratorSchoolMapper,
    private val industrialPartnerMapper: IndustrialPartnerMapper,
    private val universityPartnerMapper: UniversityPartnerMapper,

    private val userService: UserService,

    ) : SchoolService {

    override fun findSchoolEntityById(id: UUID): School {
        return schoolDao.findById(id)
            .orElseThrow { ResourceNotFoundException(School::class.java, id) }
    }

    override fun findSchoolEntityByShortName(shortName: String): School {
        return schoolDao.findByShortName(shortName)
            .orElseThrow { ResourceNotFoundException(School::class.java, shortName) }
    }

    override fun findModeratorSchoolEntityByIds(
        schoolId: UUID,
        userId: UUID,
    ): ModeratorSchool {
        val school = findSchoolEntityById(schoolId)
        val user = userService.findUserEntityById(userId)
        return moderatorSchoolDao.findBySchoolAndUser(school, user)
            .orElseThrow { UserNotModeratorInSchoolException(schoolId, userId) }
    }

    override fun existById(id: UUID): Boolean {
        return schoolDao.existsById(id)
    }

    override fun listSchools(
        query: String,
    ): List<SchoolShortResponse> {
        val schools = schoolDao.findByNameStartingWithAndHiddenFalse(query)
        return schoolMapper.asListResponse(schools)
    }

    override fun listSchoolsForAdmin(
        query: String,
        page: ExtendedPageRequest,
    ): PageResponse<SchoolShortResponse> {
        val schools = schoolDao.findByNameStartingWith(query, page.pageable)
        return schoolMapper.asPageResponse(schools)
    }

    override fun findSchoolById(id: UUID): SchoolResponse {
        val school = findSchoolEntityById(id)
        return schoolMapper.asResponse(school)
    }

    override fun findSchoolByShortName(shortName: String): SchoolResponse {
        val school = findSchoolEntityByShortName(shortName)
        return schoolMapper.asResponse(school)
    }

    override fun createSchool(userId: UUID, request: SchoolRequest, file: Part?): SchoolResponse {
        schoolValidator.validate(request)
        if (schoolDao.existsByName(request.name)) throw ResourceAlreadyExistsException(
            stringTypeName = "School name",
            value = request.name
        )
        if (schoolDao.existsByShortName(request.shortName)) throw ResourceAlreadyExistsException(
            stringTypeName = "School short name",
            value = request.shortName
        )
        val school = schoolMapper.asEntity(request).also {
            it.photoPath = file?.let {
                storageManager.storeImage(file, ImageStoreDir.SCHOOL, null, true)
            }
            schoolDao.save(it)
        }
        return schoolMapper.asResponse(school)
    }

    @Transactional
    override fun updateSchool(userId: UUID, id: UUID, request: SchoolRequest): SchoolResponse {
        schoolValidator.validate(request)
        if (schoolDao.existsByNameAndIdNot(request.name, id)
        ) throw ResourceAlreadyExistsException(
            stringTypeName = "School name",
            value = request.name
        )
        if (schoolDao.existsByShortNameAndIdNot(
                request.shortName,
                id
            )
        ) throw ResourceAlreadyExistsException(
            stringTypeName = "School short name",
            value = request.shortName
        )
        val school = findSchoolEntityById(id)
        schoolMapper.update(school, request)
        return schoolMapper.asResponse(school)
    }

    @Transactional
    override fun uploadSchoolPhoto(userId: UUID, id: UUID, file: Part): SchoolResponse {
        val school = findSchoolEntityById(id)
        school.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.SCHOOL,
            school.photoPath,
            true
        )
        return schoolMapper.asResponse(school)
    }

    override fun deleteSchool(userId: UUID, id: UUID): DeletedResponse {
        val school = findSchoolEntityById(id)
        schoolDao.delete(school)
        storageManager.remove(school.photoPath)
        return DeletedResponse(School::class.java, school.id as UUID)
    }

    override fun listLeaders(
        schoolId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<LeaderResponse> {
        val school = findSchoolEntityById(schoolId)
        val leaders = leaderDao.findBySchool(school, page.pageable)
        return leaderMapper.asPageResponse(leaders)
    }

    override fun findLeaderById(schoolId: UUID, leaderId: UUID): LeaderResponse {
        val school = findSchoolEntityById(schoolId)
        val leader = leaderDao.findBySchoolAndId(school, leaderId)
        return leaderMapper.asResponse(leader)
    }

    @Transactional
    override fun createLeader(
        userId: UUID,
        schoolId: UUID,
        request: LeaderRequest,
        file: Part?,
    ): LeaderResponse {
        leaderValidator.validate(request)
        val school = findSchoolEntityById(schoolId)
        val leader = leaderMapper.asEntity(request).also {
            it.school = school
            leaderDao.save(it)
        }
        file?.let {
            leader.photoPath = storageManager.storeImage(
                file,
                ImageStoreDir.LEADER,
                null,
                true
            )
        }
        return leaderMapper.asResponse(leader)
    }

    @Transactional
    override fun updateLeader(
        userId: UUID,
        schoolId: UUID,
        leaderId: UUID,
        request: LeaderRequest,
    ): LeaderResponse {
        leaderValidator.validate(request)
        val school = findSchoolEntityById(schoolId)
        val leader = leaderDao.findBySchoolAndId(school, leaderId)
        leaderMapper.update(leader, request)
        return leaderMapper.asResponse(leader)
    }

    @Transactional
    override fun uploadLeaderPhoto(
        userId: UUID,
        schoolId: UUID,
        leaderId: UUID,
        file: Part,
    ): LeaderResponse {
        val school = findSchoolEntityById(schoolId)
        val leader = leaderDao.findBySchoolAndId(school, leaderId)
        leader.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.LEADER,
            leader.photoPath,
            true
        )
        return leaderMapper.asResponse(leader)
    }

    @Transactional
    override fun deleteLeader(userId: UUID, schoolId: UUID, leaderId: UUID): DeletedResponse {
        val school = findSchoolEntityById(schoolId)
        leaderDao.deleteBySchoolAndId(school, leaderId)
        return DeletedResponse(Leader::class.java, leaderId)
    }

    override fun listIndustrialPartners(
        query: String,
        schoolId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<IndustrialPartnerResponse> {
        val school = findSchoolEntityById(schoolId)
        val partners = industrialPartnerDao.findByNameStartingWithAndSchool(query, school, page.pageable)
        return industrialPartnerMapper.asPageResponse(partners)
    }

    override fun findIndustrialPartnerById(
        schoolId: UUID,
        partnerId: UUID,
    ): IndustrialPartnerResponse {
        val school = findSchoolEntityById(schoolId)
        val partner = industrialPartnerDao.findBySchoolAndId(school, partnerId)
        return industrialPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun createIndustrialPartner(
        userId: UUID,
        schoolId: UUID,
        request: IndustrialPartnerRequest,
        file: Part?,
    ): IndustrialPartnerResponse {
        industrialPartnerValidator.validate(request)
        val school = findSchoolEntityById(schoolId)
        val partner = industrialPartnerMapper.asEntity(request).also {
            it.school = school
            industrialPartnerDao.save(it)
        }
        file?.let {
            partner.photoPath = storageManager.storeImage(
                file,
                ImageStoreDir.PARTNER,
                null,
                true
            )
        }
        return industrialPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun updateIndustrialPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        request: IndustrialPartnerRequest,
    ): IndustrialPartnerResponse {
        industrialPartnerValidator.validate(request)
        val school = findSchoolEntityById(schoolId)
        val partner = industrialPartnerDao.findBySchoolAndId(school, partnerId)
        industrialPartnerMapper.update(partner, request)
        return industrialPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun uploadIndustrialPartnerPhoto(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        file: Part,
    ): IndustrialPartnerResponse {
        val school = findSchoolEntityById(schoolId)
        val partner = industrialPartnerDao.findBySchoolAndId(school, partnerId)
        partner.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.PARTNER,
            partner.photoPath,
            true
        )
        return industrialPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun deleteIndustrialPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
    ): DeletedResponse {
        val school = findSchoolEntityById(schoolId)
        industrialPartnerDao.deleteBySchoolAndId(school, partnerId)
        return DeletedResponse(IndustrialPartner::class.java, partnerId)
    }

    override fun listUniversityPartners(
        query: String,
        schoolId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UniversityPartnerResponse> {
        val school = findSchoolEntityById(schoolId)
        val partners = universityPartnerDao.findByNameStartingWithAndSchool(query, school, page.pageable)
        return universityPartnerMapper.asPageResponse(partners)
    }

    override fun findUniversityPartnerById(
        schoolId: UUID,
        partnerId: UUID
    ): UniversityPartnerResponse {
        val school = findSchoolEntityById(schoolId)
        val partner = universityPartnerDao.findBySchoolAndId(school, partnerId)
        return universityPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun createUniversityPartner(
        userId: UUID,
        schoolId: UUID,
        request: UniversityPartnerRequest,
        file: Part?,
    ): UniversityPartnerResponse {
        universityPartnerValidator.validate(request)
        val school = findSchoolEntityById(schoolId)
        val partner = universityPartnerMapper.asEntity(request).also {
            it.school = school
            universityPartnerDao.save(it)
        }
        file?.let {
            partner.photoPath = storageManager.storeImage(
                file,
                ImageStoreDir.PARTNER,
                null,
                true
            )
        }
        return universityPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun updateUniversityPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        request: UniversityPartnerRequest,
    ): UniversityPartnerResponse {
        universityPartnerValidator.validate(request)
        val school = findSchoolEntityById(schoolId)
        val partner = universityPartnerDao.findBySchoolAndId(school, partnerId)
        universityPartnerMapper.update(partner, request)
        return universityPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun uploadUniversityPartnerPhoto(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
        file: Part,
    ): UniversityPartnerResponse {
        val school = findSchoolEntityById(schoolId)
        val partner = universityPartnerDao.findBySchoolAndId(school, partnerId)
        partner.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.PARTNER,
            partner.photoPath,
            true
        )
        return universityPartnerMapper.asResponse(partner)
    }

    @Transactional
    override fun deleteUniversityPartner(
        userId: UUID,
        schoolId: UUID,
        partnerId: UUID,
    ): DeletedResponse {
        val school = findSchoolEntityById(schoolId)
        universityPartnerDao.deleteBySchoolAndId(school, partnerId)
        return DeletedResponse(UniversityPartner::class.java, partnerId)
    }

    override fun listModerators(
        id: UUID, query: String, page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse> {
        val school = findSchoolEntityById(id)
        val moderators =
            moderatorSchoolDao.findBySchoolAndUserSurnameStartsWithOrderByUserNameAsc(school, query, page.pageable)
        return moderatorSchoolMapper.asPageResponse(moderators)
    }

    @Transactional
    override fun createModerator(
        principal: UUID, schoolId: UUID, userId: UUID,
    ): UserShortResponse {
        val school = findSchoolEntityById(schoolId)
        val user = userService.findUserEntityById(userId)
        if (user.banned) {
            throw BannedException(user.id as UUID)
        }
        if (user.student) {
            throw StudentCanNotBeModerator()
        }
        if (moderatorSchoolDao.existsByUserId(userId)) {
            throw UserAlreadyModeratorException(userId)
        }
        val moderatorSchool = ModeratorSchool().also {
            it.user = user
            it.school = school
            moderatorSchoolDao.save(it)
        }
        return moderatorSchoolMapper.asResponse(moderatorSchool)
    }

    override fun deleteModerator(
        principal: UUID,
        schoolId: UUID,
        userId: UUID,
    ): DeletedResponse {
        val moderatorSchool = findModeratorSchoolEntityByIds(schoolId, userId)
        moderatorSchoolDao.delete(moderatorSchool)
        return DeletedResponse(ModeratorSchool::class.java, moderatorSchool.id as UUID)
    }

    override fun findSchoolByModeratorId(userId: UUID): School {
        return moderatorSchoolDao.findByUserId(userId)
            .orElseThrow { UserNotModeratorException(userId) }.school
    }

    override fun allSchoolsCount(): Int {
        return schoolDao.count().toInt()
    }
}
