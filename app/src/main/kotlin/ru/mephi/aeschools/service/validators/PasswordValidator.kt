package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.auth.request.PasswordRequest
import ru.mephi.aeschools.util.constants.passwordMinLength

@Component
class PasswordValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = clazz == PasswordRequest::class.java

    override fun validate(target: Any, errors: Errors) {
        this.target = target as PasswordRequest
        errors += target.password.validate("password") { it.minLength(passwordMinLength) }
    }
}
