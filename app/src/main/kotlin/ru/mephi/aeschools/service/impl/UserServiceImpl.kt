package ru.mephi.aeschools.service.impl

import org.springframework.data.domain.Sort
import org.springframework.data.domain.Sort.Order
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolResponse
import ru.mephi.aeschools.model.dto.user.request.BanRequest
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.model.dto.user.response.UserPreviewResponse
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import ru.mephi.aeschools.model.exceptions.auth.UserNotFoundByEmailException
import ru.mephi.aeschools.model.exceptions.common.AdminBanException
import ru.mephi.aeschools.model.exceptions.common.AdminDeleteException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.exceptions.school.StudentCanNotBeModerator
import ru.mephi.aeschools.model.exceptions.school.UserNotModeratorException
import ru.mephi.aeschools.model.mappers.school.SchoolMapper
import ru.mephi.aeschools.model.mappers.user.UserMapper
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.mail.UserIdUserEmailPair
import ru.mephi.aeschools.service.validators.UserValidator
import java.util.*
import javax.transaction.Transactional

@Service
class UserServiceImpl(
    private val userValidator: UserValidator,
    private val userDao: UserDao,
    private val userMapper: UserMapper,

    private val schoolMapper: SchoolMapper,
) : UserService {

    private fun orderStudents(page: ExtendedPageRequest): ExtendedPageRequest {
        val orders = listOf(Order(Sort.Direction.ASC, "student"), Order(page.order, page.field))
        page.updateRequest(orders)
        return page
    }

    override fun isBanned(userId: UUID): Boolean = findUserEntityById(userId).banned

    override fun list(
        query: String,
        moderator: Boolean?,
        orderStudents: Boolean,
        page: ExtendedPageRequest,
    ): PageResponse<UserResponse> {
        val newPage = orderStudents(page)
        val users = userDao.findBySurnameStartsWithAndModeratorAndOrderStudent(
            query,
            moderator,
            newPage.pageable
        )
        return userMapper.asPageResponse(users)
    }

    override fun findUserEntityById(id: UUID): User {
        return userDao.findById(id)
            .orElseThrow { ResourceNotFoundException(User::class.java, id) }
    }

    override fun findUserEntityByEmail(email: String): User {
        return userDao.findByEmail(email)
            .orElseThrow { UserNotFoundByEmailException(email) }
    }

    @Transactional
    override fun findById(userId: UUID): UserResponse {
        val user = findUserEntityById(userId)
        return userMapper.asResponse(user)
    }

    override fun previewById(schoolId: UUID): UserPreviewResponse {
        val user = findUserEntityById(schoolId)
        return userMapper.asPreviewResponse(user)
    }

    override fun findSchool(schoolId: UUID): SchoolResponse {
        val user = findUserEntityById(schoolId)
        val school = user.school ?: throw UserNotModeratorException(user.id as UUID)
        return schoolMapper.asResponse(school)
    }

    @Transactional
    override fun update(id: UUID, request: UserRequest): UserResponse {
        userValidator.validate(request)
        val user = findUserEntityById(id)
        if (user.moderator && request.student) {
            throw StudentCanNotBeModerator()
        }
        userMapper.update(user, request)
        return userMapper.asResponse(user)
    }

    override fun delete(userId: UUID, principal: UUID): DeletedResponse {
        val user = findUserEntityById(userId)
        if (user.id == principal) throw AdminDeleteException()
        userDao.delete(user)
        return DeletedResponse(User::class.java, user.id as UUID)
    }

    override fun delete(id: UUID): DeletedResponse {
        val user = findUserEntityById(id)
        userDao.delete(user)
        return DeletedResponse(User::class.java, user.id as UUID)
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    override fun delete(entity: User) {
        userDao.delete(entity)
    }

    @Transactional
    override fun setBanStatus(userId: UUID, request: BanRequest): UserResponse {
        val user = findUserEntityById(userId)
        if (user.admin) {
            throw AdminBanException()
        }
        user.banned = request.banned
        return userMapper.asResponse(user)
    }

    override fun findAll(): Iterable<User> {
        return userDao.findAll()
    }

    override fun findUserIdUserEmailPairs(usersIds: List<UUID>): List<UserIdUserEmailPair> {
        return userDao.findUserIdUserEmailPairs(usersIds)
    }
}
