package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class UsefulLinkValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = UsefulLinkRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as UsefulLinkRequest
        errors += target.title.validate("title") {
            it.isNotNull()
            it.maxLength(smallLength)
        }
        errors += target.link.validate("link") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
    }
}
