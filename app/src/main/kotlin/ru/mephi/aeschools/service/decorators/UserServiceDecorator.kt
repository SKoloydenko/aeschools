package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import ru.mephi.aeschools.service.UserActionService
import ru.mephi.aeschools.service.UserService
import java.util.*

@Service
@Primary
class UserServiceDecorator(
    @Qualifier("userServiceImpl")
    private val userService: UserService,
    private val userActionService: UserActionService,
) : UserService by userService {
    override fun update(id: UUID, request: UserRequest): UserResponse {
        val response = userService.update(id, request)
        userActionService.save(id, ActionTarget.PROFILE, ActionVerb.UPDATE, response.id)
        return response
    }
}
