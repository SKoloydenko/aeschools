package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.RequestBody
import ru.mephi.aeschools.database.entity.registration_limiter.RegistrationLimiterSettings
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsRequest
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsResponse

@Tag(name = "Registration Limiter API")
interface RegistrationLimiterService {
    fun onRegistration()
    fun findSettings(): RegistrationLimiterSettings
    fun save(settings: RegistrationLimiterSettings): RegistrationLimiterSettings
    fun getSettings(): RegistrationLimiterSettingsResponse
    fun setSettings(@RequestBody settings: RegistrationLimiterSettingsRequest): RegistrationLimiterSettingsResponse
}
