package ru.mephi.aeschools.service.validators.school

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.service.validators.AbstractValidator
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class IndustrialPartnerValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = IndustrialPartnerRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as IndustrialPartnerRequest
        errors += target.name.validate("name") {
            it.isNotNull()
            it.maxLength(smallLength)
        }
        errors += target.partnershipFormat.validate("partnershipFormat") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.schoolRole.validate("schoolRole") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.partnerRole.validate("partnerRole") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.cooperationDirection.validate("cooperationDirection") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
    }
}
