package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.library.LibraryRecord
import ru.mephi.aeschools.database.repository.library.LibraryDao
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.library.request.LibraryRecordRequest
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordResponse
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordShortResponse
import ru.mephi.aeschools.model.enums.storage.FileStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.library.LibraryMapper
import ru.mephi.aeschools.service.LibraryService
import ru.mephi.aeschools.service.impl.storage.StorageFileManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import ru.mephi.aeschools.service.validators.LibraryValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
@Transactional
class LibraryServiceImpl(
    private val validator: LibraryValidator,
    private val mapper: LibraryMapper,
    private val fileStorage: StorageFileManager,
    private val dao: LibraryDao,
    private val storageHelper: StorageManagerHelper
) : LibraryService {

    private fun findEntityById(id: UUID): LibraryRecord {
        return dao.findById(id)
            .orElseThrow { ResourceNotFoundException(LibraryRecord::class.java, id) }
    }

    override fun create(userId: UUID, request: LibraryRecordRequest, file: Part?): LibraryRecordResponse {
        validator.validate(request)
        val entity = dao.save(mapper.asEntity(request))
        file?.let {
            val path = fileStorage.storeFile(file, FileStoreDir.LIBRARY, entity.link)
            entity.link = storageHelper.getFilePath(path, FileStoreDir.LIBRARY.isPublic)
        }
        return mapper.asResponse(entity)
    }

    override fun update(
        userId: UUID,
        recordId: UUID,
        request: LibraryRecordRequest,
        file: Part?
    ): LibraryRecordResponse {
        validator.validate(request)
        val entity = findEntityById(recordId)
        if (entity.link != request.link)
            fileStorage.remove(entity.link)
        mapper.update(entity, request)
        file?.let {
            val path = fileStorage.storeFile(file, FileStoreDir.LIBRARY, entity.link)
            entity.link = storageHelper.getFilePath(path, FileStoreDir.LIBRARY.isPublic)
        }
        return mapper.asResponse(entity)
    }

    override fun delete(userId: UUID, recordId: UUID): DeletedResponse {
        val entity = findEntityById(recordId)
        dao.delete(entity)
        fileStorage.remove(entity.link)
        return DeletedResponse(LibraryRecord::class.java, recordId)
    }

    override fun uploadFile(userId: UUID, recordId: UUID, part: Part): LibraryRecordResponse {
        val entity = findEntityById(recordId)
        val path = fileStorage.storeFile(part, FileStoreDir.LIBRARY, entity.link)
        entity.link = storageHelper.getFilePath(path, FileStoreDir.LIBRARY.isPublic)
        return mapper.asResponse(entity)
    }

    override fun fullList(request: ExtendedPageRequest, query: String): PageResponse<LibraryRecordResponse> {
        val page = dao.findAllByTitleStartsWith(query, request.pageable)
        return mapper.asPageResponse(page)
    }

    override fun shortList(request: ExtendedPageRequest, query: String): PageResponse<LibraryRecordShortResponse> {
        val page = dao.findAllByTitleStartsWith(query, request.pageable)
        return mapper.asShortPageResponse(page)
    }

    override fun getById(id: UUID): LibraryRecordResponse {
        val entity = findEntityById(id)
        return mapper.asResponse(entity)
    }
}
