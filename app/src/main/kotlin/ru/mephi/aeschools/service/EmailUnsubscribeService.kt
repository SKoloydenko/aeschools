package ru.mephi.aeschools.service

import java.util.*

interface EmailUnsubscribeService {
    fun getToken(userId: UUID): String
    fun parseUserId(token: String): UUID
}
