package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.database.entity.event.AbstractEvent
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.event.request.AbstractEventRequest
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.model.dto.event.response.AbstractEventAdminResponse
import ru.mephi.aeschools.model.dto.event.response.AbstractEventResponse
import ru.mephi.aeschools.model.dto.event.response.EventAdminResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Event API")
interface EventService {
    fun findEventEntityById(id: UUID): AbstractEvent

    fun listEvents(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventResponse>

    fun listSchoolEvents(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventResponse>

    fun listEventsForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventAdminResponse>

    fun listSchoolEventsForAdmin(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventAdminResponse>

    fun listEventsForUser(
        userId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<AbstractEventResponse>

    fun findEventById(@PathVariable eventId: UUID): AbstractEventResponse

    fun findSchoolEventById(
        @PathVariable schoolId: UUID,
        @PathVariable eventId: UUID,
    ): AbstractEventResponse

    fun findEventByIdForAdmin(@PathVariable eventId: UUID): AbstractEventAdminResponse

    fun findSchoolEventByIdForAdmin(
        @PathVariable schoolId: UUID,
        @PathVariable eventId: UUID,
    ): AbstractEventAdminResponse

    fun createEvent(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: AbstractEventRequest,
        @RequestPart file: Part?,
    ): AbstractEventAdminResponse

    fun updateEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable eventId: UUID,
        @RequestBody request: AbstractEventRequest,
    ): AbstractEventAdminResponse

    fun uploadEventPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable eventId: UUID,
        @RequestPart file: Part,
    ): AbstractEventAdminResponse

    fun deleteEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable eventId: UUID,
    ): DeletedResponse

    fun createSchoolEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: EventRequest,
        @RequestPart file: Part?,
    ): EventAdminResponse

    fun updateSchoolEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable eventId: UUID,
        @RequestBody request: EventRequest,
    ): EventAdminResponse

    fun uploadSchoolEventPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable eventId: UUID,
        @RequestPart file: Part,
    ): EventAdminResponse

    fun deleteSchoolEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable eventId: UUID,
    ): DeletedResponse

    fun listEventMembers(
        @PathVariable eventId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse>

    fun listSchoolEventMembers(
        @PathVariable schoolId: UUID,
        @PathVariable eventId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse>

    fun joinEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable eventId: UUID,
    ): AbstractEventResponse

    fun leaveEvent(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable eventId: UUID,
    ): AbstractEventResponse

    fun findAllEvents(): Iterable<AbstractEvent>
}
