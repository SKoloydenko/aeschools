package ru.mephi.aeschools.service.impl.preferences

import org.springframework.stereotype.Service
import ru.mephi.aeschools.config.properties.ServerUrls
import ru.mephi.aeschools.database.entity.preferences.PreferencesTokens
import ru.mephi.aeschools.database.entity.preferences.UserPreferences
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.repository.preferences.PreferencesTokensDao
import ru.mephi.aeschools.database.repository.preferences.UserPreferencesDao
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesRequest
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesResponse
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundByNameException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.preferences.PreferencesMapper
import ru.mephi.aeschools.service.UserPreferencesService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.util.StringGenerator
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.constants.extraSmallLength
import java.util.*
import javax.transaction.Transactional

@Service
class UserPreferencesServiceImpl(
    private val dao: UserPreferencesDao,
    private val preferencesTokenDao: PreferencesTokensDao,
    private val mapper: PreferencesMapper,
    private val userService: UserService,
    private val serverUrls: ServerUrls,
    private val stringGenerator: StringGenerator,
) : UserPreferencesService {
    override fun findEntityByUserId(userId: UUID): UserPreferences {
        return dao.findById(userId).orElseThrow {
            ResourceNotFoundException(UserPreferences::class.java, userId)
        }
    }

    override fun createPreferences(
        request: UserPreferencesRequest,
        user: User,
    ): UserPreferences {
        preferencesTokenDao.save(PreferencesTokens(user, generateUniqueEmailToken()))
        val preferences = mapper.asEntity(request, user)
        return dao.save(preferences)
    }

    @Transactional
    override fun turnOffEmailPreference(token: String): UserPreferencesResponse {
        val preferences =
            preferencesTokenDao.findUserPreferencesByEmailToken(token)
                .orElseThrow {
                    ResourceNotFoundByNameException(PreferencesTokens::class.java, token)
                }
        preferences.enableEmail = false
        return mapper.asResponse(preferences)
    }

    @Transactional
    override fun updatePreferences(
        userId: UUID,
        request: UserPreferencesRequest,
    ): UserPreferencesResponse {
        userService.findUserEntityById(userId)
        val preferences = findEntityByUserId(userId)
        mapper.update(preferences, request)
        return mapper.asResponse(preferences)
    }

    override fun findAllUsersIdsWithEnableEmail(ignorePreferences: Boolean): List<UUID> {
        return dao.findAllUsersIdsWithEnableEmail(ignorePreferences)
    }

    override fun findUsersIdsWithEnableEmail(ignorePreferences: Boolean): List<UUID> {
        return dao.findUsersIdsWithEnableEmail(ignorePreferences)
    }

    override fun findModeratorsIdsWithEnableEmail(ignorePreferences: Boolean): List<UUID> {
        return dao.findModeratorsIdsWithEnableEmail(ignorePreferences)
    }

    override fun getPreferencesById(userId: UUID): UserPreferencesResponse {
        val entity = findEntityByUserId(userId)
        return mapper.asResponse(entity)
    }

    override fun getUnsubscribeFromMailLink(): String {
        return "https://${serverUrls.baseUrl}/api$API_VERSION_1/user/me/preferences/turnOff"
    }

    private fun generateUniqueEmailToken(): String {
        var token = ""
        do {
            token = stringGenerator.getRandomString(extraSmallLength)
        } while (preferencesTokenDao.existsByEmailToken(token))
        return token
    }
}
