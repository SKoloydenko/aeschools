package ru.mephi.aeschools.service.validators

import org.springframework.validation.BeanPropertyBindingResult
import java.time.LocalDate
import java.time.LocalDateTime

class ValidatorBuilder<T>(target: Any, val it: T) {
    private val errors =
        BeanPropertyBindingResult(target, target::class.java.simpleName)

    fun Any?.isNotNull() {
        if (this == null) errors.reject("Must not be null")
    }

    fun Any?.isNull() {
        if (this != null) errors.reject("Must be null")
    }

    fun <T> Collection<T>.maxSize(maxSize: Int) {
        if (size > maxSize) errors.reject("Must be at most [$maxSize] elements long")
    }

    fun String?.isNotNullNorEmpty() {
        if (this.isNullOrEmpty()) errors.reject("Must not be null nor empty")
    }

    fun String.minLength(minLength: Int) {
        if (length < minLength) errors.reject("Must be at least [$minLength] characters long")
    }

    fun String.maxLength(maxLength: Int) {
        if (length > maxLength) errors.reject("Must be at most [$maxLength] characters long")
    }

    fun LocalDateTime.after(after: LocalDateTime) {
        if (after >= this) errors.reject("Must be after [$after]")
    }

    fun LocalDate.between(min: LocalDate, max: LocalDate) {
        if (this !in min..max) errors.reject("Must be from [$min] to [$max]")
    }

    fun Int.between(min: Int, max: Int) {
        if (this !in min..max) errors.reject("Must be from [$min] to [$max]")
    }

    fun String.matches(pattern: Regex) {
        if (!pattern.matches(this)) errors.reject("Must match [$pattern] pattern")
    }

    fun <E> List<E>.blacklist(vararg blacklist: E) {
        forEach { if (it in blacklist) errors.reject("[$it] element in black list") }
    }

    fun errorCodes() = errors.allErrors.mapNotNull { it.code }
}
