package ru.mephi.aeschools.service.validators

import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.Errors
import org.springframework.validation.Validator
import ru.mephi.aeschools.model.exceptions.common.ValidationException

abstract class AbstractValidator : Validator {

    abstract var target: Any

    protected inline fun <reified T> T.validate(
        name: String,
        init: ValidatorBuilder<T>.() -> Unit,
    ): Errors {
        val validator = ValidatorBuilder(target, this)
        val valueErrors = targetErrors(target)
        validator.init()
        validator.errorCodes()
            .forEach { code -> valueErrors.rejectValue(name, code) }
        return valueErrors
    }

    protected fun targetErrors(target: Any) =
        BeanPropertyBindingResult(target, target::class.java.simpleName)

    protected operator fun Errors.plusAssign(validate: Errors?) {
        validate ?: return
        addAllErrors(validate)
    }

    open fun <T : Any> validate(target: T) {
        this.target = target
        val errors = targetErrors(target)
        validate(target, errors)
        if (errors.hasErrors()) throw ValidationException(validationErrors = errors.fieldErrors)
    }
}
