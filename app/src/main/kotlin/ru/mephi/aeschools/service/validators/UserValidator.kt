package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.phoneLength

@Component
class UserValidator : AbstractValidator() {

    override lateinit var target: Any

    private val specialtyPattern = Regex("\\d{2}\\.\\d{2}\\.\\d{2}")

    private fun validatePersonals(request: UserRequest, errors: Errors) {
        errors += request.name.validate("name") {
            it.isNotNull()
            it.maxLength(extraSmallLength)
        }
        errors += request.surname.validate("surname") {
            it.isNotNull()
            it.maxLength(extraSmallLength)
        }
        errors += request.patronymic?.validate("patronymic") {
            it.maxLength(extraSmallLength)
        }
        errors += request.phone.validate("phone") {
            it.isNotNull()
            it.maxLength(phoneLength)
        }
    }

    private fun validateStudent(request: UserRequest, errors: Errors) {
        errors += request.university.validate("university") {
            it.isNotNull()
            it?.maxLength(extraLargeLength)
        }
        errors += request.educationDegree.validate("educationDegree") {
            it.isNotNull()
        }
        errors += request.grade.validate("grade") {
            it.isNotNull()
            it?.between(1, 6)
        }
        errors += request.specialty.validate("specialty") {
            it.isNotNull()
            it?.matches(specialtyPattern)
        }
        errors += request.organization.validate("organization") {
            it.isNull()
        }
        errors += request.jobRole.validate("jobRole") {
            it.isNull()
        }
    }

    private fun validateUser(request: UserRequest, errors: Errors) {
        errors += request.organization.validate("organization") {
            it?.maxLength(extraLargeLength)
        }
        errors += request.jobRole.validate("jobRole") {
            it?.maxLength(extraLargeLength)
        }
        errors += request.university.validate("university") {
            it.isNull()
        }
        errors += request.educationDegree.validate("educationDegree") {
            it.isNull()
        }
        errors += request.grade.validate("grade") {
            it.isNull()
        }
        errors += request.specialty.validate("specialty") {
            it.isNull()
        }
    }

    private fun validateCommon(request: UserRequest, errors: Errors) {
        validatePersonals(request, errors)
    }

    override fun supports(clazz: Class<*>) = clazz == UserRequest::class.java

    override fun validate(target: Any, errors: Errors) {
        this.target = target as UserRequest
        validateCommon(target, errors)
        if (target.student) validateStudent(target, errors)
        else validateUser(target, errors)
    }
}
