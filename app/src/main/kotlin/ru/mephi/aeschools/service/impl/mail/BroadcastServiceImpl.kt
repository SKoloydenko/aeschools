package ru.mephi.aeschools.service.impl.mail

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.broadcast.Broadcast
import ru.mephi.aeschools.database.repository.broadcast.BroadcastDao
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.broadcast.request.BroadcastRequest
import ru.mephi.aeschools.model.dto.broadcast.response.BroadcastResponse
import ru.mephi.aeschools.model.dto.mail.mails.BroadcastIgnorePreferencesMail
import ru.mephi.aeschools.model.dto.mail.mails.BroadcastMail
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.broadcast.BroadcastMapper
import ru.mephi.aeschools.service.BroadcastService
import ru.mephi.aeschools.service.DataSenderHelper
import ru.mephi.aeschools.service.EmailSender
import ru.mephi.aeschools.service.UserPreferencesService
import ru.mephi.aeschools.service.validators.BroadcastValidator
import java.util.*

@Service
class BroadcastServiceImpl(
    private val broadcastValidator: BroadcastValidator,
    private val broadcastDao: BroadcastDao,
    private val broadcastMapper: BroadcastMapper,

    private val sender: EmailSender,
    private val dataSenderHelper: DataSenderHelper,
    private val preferencesService: UserPreferencesService,
) : BroadcastService {

    private fun findBroadcastEntityById(broadcastId: UUID) =
        broadcastDao.findById(broadcastId).orElseThrow {
            ResourceNotFoundException(Broadcast::class.java, broadcastId)
        }

    override fun listBroadcasts(page: ExtendedPageRequest): PageResponse<BroadcastResponse> {
        val broadcasts = broadcastDao.findAll(page.pageable)
        return broadcastMapper.asPageResponse(broadcasts)
    }

    override fun findBroadcastById(broadcastId: UUID): BroadcastResponse {
        val broadcast = findBroadcastEntityById(broadcastId)
        return broadcastMapper.asResponse(broadcast)
    }

    override fun sendBroadcastMail(userId: UUID, request: BroadcastRequest): BroadcastResponse {
        broadcastValidator.validate(request)
        val mailReceivers =
            dataSenderHelper.getMailReceivers(request.type, request.ignorePreferences)
        if (request.ignorePreferences) {
            val mail = BroadcastIgnorePreferencesMail(request.title, request.text, mailReceivers)
            sender.sendBroadcastMailIgnorePreferences(mail)
        } else {
            val mail = BroadcastMail(
                request.title,
                request.text,
                preferencesService.getUnsubscribeFromMailLink(),
                mailReceivers
            )
            sender.sendBroadcastMail(mail)
        }
        val broadcast = broadcastMapper.asEntity(request).also {
            broadcastDao.save(it)
        }
        return broadcastMapper.asResponse(broadcast)
    }
}
