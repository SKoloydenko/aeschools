package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.tags.Tag
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.auth.request.ResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SendEmailToResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SetNewPasswordRequest
import ru.mephi.aeschools.model.dto.user.request.LoginRequest
import ru.mephi.aeschools.model.dto.user.request.RefreshRequest
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.model.dto.user.response.TokenResponse
import ru.mephi.aeschools.model.dto.user.response.UserResponse
import java.util.*

@Tag(name = "Auth API")
interface AuthService {
    fun register(@RequestBody request: RegistrationRequest): UserResponse

    fun register(user: User, password: String): User
    fun login(@RequestBody request: LoginRequest): Pair<TokenResponse, String>
    fun refresh(@RequestBody request: RefreshRequest): Pair<TokenResponse, String>
    fun logout(@Parameter(hidden = true) refreshToken: String)
    fun sendEmailToResetPassword(@RequestBody request: SendEmailToResetPasswordRequest)
    fun resetPassword(@RequestBody request: ResetPasswordRequest): Pair<TokenResponse, String>
    fun setNewPassword(
        @RequestBody request: SetNewPasswordRequest,
        @Parameter(hidden = true) userId: UUID,
    )

    fun verifyEmail(@RequestBody token: String)
}
