package ru.mephi.aeschools.service.validators

import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.AbstractContentRequest
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.publicationLength

abstract class AbstractContentValidator : AbstractValidator() {

    override fun supports(clazz: Class<*>) = AbstractContentRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as AbstractContentRequest
        errors += target.title.validate("title") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
        errors += target.content.validate("content") {
            it.isNotNull()
            it.maxLength(publicationLength)
        }
        errors += target.publicationDate.validate("publicationDate") {
            it.isNotNull()
        }
    }
}

