package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.publication.request.PublicationRequest
import ru.mephi.aeschools.model.dto.publication.response.PublicationAdminResponse
import ru.mephi.aeschools.service.PublicationService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class PublicationServiceDecorator(
    @Qualifier("publicationServiceImpl")
    private val publicationService: PublicationService,
    private val userActionService: UserActionService,
) : PublicationService by publicationService {

    override fun createPublication(
        userId: UUID,
        request: PublicationRequest,
        file: Part?,
    ): PublicationAdminResponse {
        val response = publicationService.createPublication(userId, request, file)
        userActionService.save(userId, ActionTarget.PUBLICATION, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updatePublication(
        userId: UUID,
        publicationId: UUID,
        request: PublicationRequest,
    ): PublicationAdminResponse {
        val response = publicationService.updatePublication(userId, publicationId, request)
        userActionService.save(userId, ActionTarget.PUBLICATION, ActionVerb.UPDATE, response.id)
        return response
    }

    override fun uploadPublicationPhoto(
        userId: UUID,
        publicationId: UUID,
        file: Part,
    ): PublicationAdminResponse {
        val response = publicationService.uploadPublicationPhoto(userId, publicationId, file)
        userActionService.save(
            userId,
            ActionTarget.PUBLICATION,
            ActionVerb.UPLOAD_PHOTO,
            publicationId
        )
        return response
    }

    override fun deletePublication(
        userId: UUID,
        publicationId: UUID,
    ): DeletedResponse {
        val response = publicationService.deletePublication(userId, publicationId)
        userActionService.save(userId, ActionTarget.PUBLICATION, ActionVerb.DELETE, publicationId)
        return response
    }

    override fun createSchoolPublication(
        userId: UUID,
        schoolId: UUID,
        request: PublicationRequest,
        file: Part?,
    ): PublicationAdminResponse {
        val response = publicationService.createSchoolPublication(userId, schoolId, request, file)
        userActionService.save(userId, ActionTarget.PUBLICATION, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateSchoolPublication(
        userId: UUID,
        schoolId: UUID,
        publicationId: UUID,
        request: PublicationRequest,
    ): PublicationAdminResponse {
        val response =
            publicationService.updateSchoolPublication(userId, schoolId, publicationId, request)
        userActionService.save(userId, ActionTarget.PUBLICATION, ActionVerb.UPDATE, publicationId)
        return response
    }

    override fun uploadSchoolPublicationPhoto(
        userId: UUID,
        schoolId: UUID,
        publicationId: UUID,
        file: Part,
    ): PublicationAdminResponse {
        val response = publicationService.uploadPublicationPhoto(userId, publicationId, file)
        userActionService.save(
            userId,
            ActionTarget.PUBLICATION,
            ActionVerb.UPLOAD_PHOTO,
            publicationId
        )
        return response
    }

    override fun deleteSchoolPublication(
        userId: UUID,
        schoolId: UUID,
        publicationId: UUID,
    ): DeletedResponse {
        val response = publicationService.deleteSchoolPublication(userId, schoolId, publicationId)
        userActionService.save(userId, ActionTarget.PUBLICATION, ActionVerb.DELETE, publicationId)
        return response
    }
}
