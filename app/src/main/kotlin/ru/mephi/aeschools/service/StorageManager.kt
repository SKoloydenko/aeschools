package ru.mephi.aeschools.service

import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path

abstract class StorageManager {
    val root: Path = Path("uploads")
    abstract fun remove(path: String?): Boolean
    abstract fun retrieve(path: String): File
}
