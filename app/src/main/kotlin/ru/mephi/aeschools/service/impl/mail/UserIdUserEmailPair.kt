package ru.mephi.aeschools.service.impl.mail

import java.util.*

interface UserIdUserEmailPair {
    val userId: UUID
    val email: String
}
