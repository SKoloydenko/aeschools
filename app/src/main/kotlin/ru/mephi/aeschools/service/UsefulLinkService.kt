package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkAdminResponse
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Useful Link API")
interface UsefulLinkService {
    fun listUsefulLinks(page: ExtendedPageRequest): PageResponse<UsefulLinkResponse>

    fun listUsefulLinksForAdmin(page: ExtendedPageRequest): PageResponse<UsefulLinkAdminResponse>

    fun findUsefulLinkByIdForAdmin(@PathVariable linkId: UUID): UsefulLinkAdminResponse
    fun createUsefulLink(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: UsefulLinkRequest,
        @RequestPart file: Part?,
    ): UsefulLinkAdminResponse

    fun updateUsefulLink(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable linkId: UUID,
        @RequestBody request: UsefulLinkRequest,
    ): UsefulLinkAdminResponse

    fun uploadUsefulLinkPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable linkId: UUID,
        @RequestPart file: Part,
    ): UsefulLinkAdminResponse

    fun deleteUsefulLink(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable linkId: UUID,
    ): DeletedResponse
}
