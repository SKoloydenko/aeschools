package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.best_practice.BestPractice
import ru.mephi.aeschools.database.repository.best_practice.BestPracticeDao
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.best_practice.request.BestPracticeRequest
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeAdminResponse
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.TagMapper
import ru.mephi.aeschools.model.mappers.best_practice.BestPracticeMapper
import ru.mephi.aeschools.service.BestPracticeService
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.BestPracticeValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class BestPracticeServiceImpl(
    private var storageManager: StorageImageManager,

    private val bestPracticeValidator: BestPracticeValidator,
    private val bestPracticeDao: BestPracticeDao,
    private val bestPracticeMapper: BestPracticeMapper,

    private val tagMapper: TagMapper,

    private val userService: UserService,
    private val schoolService: SchoolService,
    private val contentPhotoService: ContentPhotoService
) : BestPracticeService {

    private fun findEntityById(id: UUID) = bestPracticeDao.findById(id).orElseThrow {
        ResourceNotFoundException(BestPractice::class.java, id)
    }

    private fun findSchoolEntityById(schoolId: UUID, id: UUID) =
        bestPracticeDao.findBySchoolIdAndId(schoolId, id).orElseThrow {
            ResourceNotFoundException(BestPractice::class.java, id)
        }

    override fun listBestPractices(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeResponse> {
        val bestPractices = with(bestPracticeDao) {
            if (tag.isEmpty()) findByTitleStartsWithAndPublicationDateBeforeNow(query, page.pageable)
            else findByTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(query, tag, page.pageable)
        }
        return bestPracticeMapper.asPageResponse(bestPractices)
    }

    override fun listSchoolBestPractices(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeResponse> {
        schoolService.findSchoolById(schoolId)
        val bestPractices = with(bestPracticeDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWithAndPublicationDateBeforeNow(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagTitleAndPublicationDateBeforeNow(
                schoolId,
                query,
                tag,
                page.pageable
            )
        }
        return bestPracticeMapper.asPageResponse(bestPractices)
    }

    override fun listBestPracticesForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeAdminResponse> {
        val bestPractices = with(bestPracticeDao) {
            if (tag.isEmpty()) findByTitleStartsWith(query, page.pageable)
            else findByTitleStartsWithAndTagsTitle(
                query,
                tag,
                page.pageable
            )
        }
        return bestPracticeMapper.asAdminPageResponse(bestPractices)
    }

    override fun listSchoolBestPracticesForAdmin(
        schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<BestPracticeAdminResponse> {
        schoolService.findSchoolById(schoolId)
        val bestPractices = with(bestPracticeDao) {
            if (tag.isEmpty()) findBySchoolIdAndTitleStartsWith(schoolId, query, page.pageable)
            else findBySchoolIdAndTitleStartsWithAndTagsTitle(
                schoolId,
                query,
                tag,
                page.pageable
            )
        }
        return bestPracticeMapper.asAdminPageResponse(bestPractices)
    }

    override fun findBestPracticeById(bestPracticeId: UUID): BestPracticeResponse {
        val bestPractice = bestPracticeDao.findByIdAndPublicationDateBeforeNow(bestPracticeId).orElseThrow {
            throw ResourceNotFoundException(BestPractice::class.java, bestPracticeId)
        }
        return bestPracticeMapper.asResponse(bestPractice)
    }

    override fun findSchoolBestPracticeById(
        schoolId: UUID,
        bestPracticeId: UUID,
    ): BestPracticeResponse {
        schoolService.findSchoolById(schoolId)
        val bestPractice = bestPracticeDao.findBySchoolIdAndIdAndPublicationDateBeforeNow(schoolId, bestPracticeId)
            .orElseThrow {
                throw ResourceNotFoundException(BestPractice::class.java, bestPracticeId)
            }
        return bestPracticeMapper.asResponse(bestPractice)
    }

    override fun findBestPracticeByIdForAdmin(bestPracticeId: UUID): BestPracticeAdminResponse {
        val bestPractice = findEntityById(bestPracticeId)
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    override fun findSchoolBestPracticeByIdForAdmin(
        schoolId: UUID,
        bestPracticeId: UUID,
    ): BestPracticeAdminResponse {
        schoolService.findSchoolById(schoolId)
        val bestPractice = findSchoolEntityById(schoolId, bestPracticeId)
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun createBestPractice(
        userId: UUID,
        request: BestPracticeRequest,
        file: Part?,
    ): BestPracticeAdminResponse {
        bestPracticeValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val bestPractice = bestPracticeMapper.asEntity(request).also {
            it.author = user
            it.photoPath = file?.let {
                storageManager.storeImage(file, ImageStoreDir.BEST_PRACTICE, null, true)
            }
            bestPracticeDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun updateBestPractice(
        userId: UUID,
        bestPracticeId: UUID,
        request: BestPracticeRequest,
    ): BestPracticeAdminResponse {
        bestPracticeValidator.validate(request)
        val bestPractice = findEntityById(bestPracticeId)
        bestPracticeMapper.update(bestPractice, request)
        bestPractice.setTags(tagMapper.asEntitySet(request.tags, bestPractice))
        contentPhotoService.queueContent(ContentPhotoRequest(bestPracticeId, bestPractice.content))
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun uploadBestPracticePhoto(
        userId: UUID,
        bestPracticeId: UUID,
        file: Part,
    ): BestPracticeAdminResponse {
        val bestPractice = findEntityById(bestPracticeId)
        bestPractice.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.BEST_PRACTICE,
            bestPractice.photoPath,
            true
        )
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun deleteBestPractice(userId: UUID, bestPracticeId: UUID): DeletedResponse {
        val bestPractice = findEntityById(bestPracticeId)
        bestPracticeDao.delete(bestPractice)
        storageManager.remove(bestPractice.photoPath)
        return DeletedResponse(BestPractice::class.java, bestPracticeId)
    }

    @Transactional
    override fun createSchoolBestPractice(
        userId: UUID,
        schoolId: UUID,
        request: BestPracticeRequest,
        file: Part?,
    ): BestPracticeAdminResponse {
        bestPracticeValidator.validate(request)
        val school = schoolService.findSchoolEntityById(schoolId)
        val user = userService.findUserEntityById(userId)
        val bestPractice = bestPracticeMapper.asEntity(request).also {
            it.author = user
            it.school = school
            it.photoPath = file?.let {
                storageManager.storeImage(file, ImageStoreDir.BEST_PRACTICE, null, true)
            }
            bestPracticeDao.save(it)
            it.setTags(tagMapper.asEntitySet(request.tags, it))
            contentPhotoService.queueContent(ContentPhotoRequest(it.id as UUID, it.content))
        }
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun updateSchoolBestPractice(
        userId: UUID,
        schoolId: UUID,
        bestPracticeId: UUID,
        request: BestPracticeRequest,
    ): BestPracticeAdminResponse {
        bestPracticeValidator.validate(request)
        schoolService.findSchoolById(schoolId)
        val bestPractice = findSchoolEntityById(schoolId, bestPracticeId)
        bestPracticeMapper.update(bestPractice, request)
        bestPractice.setTags(tagMapper.asEntitySet(request.tags, bestPractice))
        contentPhotoService.queueContent(ContentPhotoRequest(bestPracticeId, bestPractice.content))
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun uploadSchoolBestPracticePhoto(
        userId: UUID,
        schoolId: UUID,
        bestPracticeId: UUID,
        file: Part,
    ): BestPracticeAdminResponse {
        schoolService.findSchoolById(schoolId)
        val bestPractice = findSchoolEntityById(schoolId, bestPracticeId)
        bestPractice.photoPath = storageManager.storeImage(
            file,
            ImageStoreDir.BEST_PRACTICE,
            bestPractice.photoPath,
            true
        )
        return bestPracticeMapper.asAdminResponse(bestPractice)
    }

    @Transactional
    override fun deleteSchoolBestPractice(
        userId: UUID,
        schoolId: UUID,
        bestPracticeId: UUID,
    ): DeletedResponse {
        schoolService.findSchoolById(schoolId)
        val bestPractice = findSchoolEntityById(schoolId, bestPracticeId)
        bestPracticeDao.delete(bestPractice)
        storageManager.remove(bestPractice.photoPath)
        return DeletedResponse(BestPractice::class.java, bestPracticeId)
    }
}
