package ru.mephi.aeschools.service.impl.auth

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.mephi.aeschools.config.properties.ServerUrls
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.database.entity.user.VerifyEmailToken
import ru.mephi.aeschools.database.repository.user.VerifyEmailTokenDao
import ru.mephi.aeschools.model.dto.mail.mails.VerifyEmailMail
import ru.mephi.aeschools.model.exceptions.auth.TokenToVerifyEmailNotFound
import ru.mephi.aeschools.model.exceptions.auth.VerifyTokenExpiredException
import ru.mephi.aeschools.service.EmailSender
import ru.mephi.aeschools.service.VerifyEmailService
import ru.mephi.aeschools.util.StringGenerator
import java.time.LocalDateTime
import java.util.*

@Service
class VerifyEmailServiceImpl(
    val dao: VerifyEmailTokenDao,
    val emailSender: EmailSender,
    val generator: StringGenerator,
    val serverUrls: ServerUrls,
    @Value("\${verifyEmail.tokenTtl}")
    val tokenTtl: Long,
) : VerifyEmailService {

    override fun sendEmailToVerifyEmail(user: User) {
        var str = ""
        do {
            str = generator.getRandomString(20)
        } while (dao.existsByToken(str))

        val token = VerifyEmailToken(user, str)
        dao.save(token)
        emailSender.sendVerifyEmailMail(VerifyEmailMail(buildLink(token), user.email, tokenTtl))
    }

    override fun verifyEmail(token: String) {
        val entity = findVerifyTokenByToken(token)
        if (tokenIsExpired(entity))
            throw VerifyTokenExpiredException()
        dao.delete(entity)
    }

    override fun emailIsVerified(user: User): Boolean {
        return !existsById(user.id as UUID)
    }

    override fun findVerifyTokenById(id: UUID): VerifyEmailToken {
        return dao.findById(id).orElseThrow { TokenToVerifyEmailNotFound() }
    }

    override fun findVerifyTokenByToken(token: String): VerifyEmailToken {
        return dao.findByToken(token)
            .orElseThrow { TokenToVerifyEmailNotFound() }
    }

    override fun existsById(id: UUID): Boolean {
        return dao.existsById(id)
    }

    override fun userHaveExpiredEmailVerification(id: UUID): Boolean {
        return existsById(id) && tokenIsExpired(findVerifyTokenById(id))
    }

    override fun findAll(): Iterable<VerifyEmailToken> {
        return dao.findAll()
    }

    override fun tokenIsExpired(token: VerifyEmailToken): Boolean {
        return token.createdAt.isBefore(
            LocalDateTime.now().minusMinutes(tokenTtl)
        )
    }

    override fun deleteById(id: UUID) {
        dao.deleteById(id)
    }

    private fun buildLink(token: VerifyEmailToken): String {
        return "https://${serverUrls.baseUrl}/api/v1/public/auth/verify-email?token=${token.token}"
    }
}
