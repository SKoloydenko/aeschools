package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.model.dto.internship.response.InternshipAdminResponse
import ru.mephi.aeschools.service.InternshipService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class InternshipServiceDecorator(
    @Qualifier("internshipServiceImpl")
    private val internshipService: InternshipService,
    private val userActionService: UserActionService,
) : InternshipService by internshipService {

    override fun createInternship(
        userId: UUID,
        request: InternshipRequest,
        file: Part?,
    ): InternshipAdminResponse {
        val response = internshipService.createInternship(userId, request, file)
        userActionService.save(userId, ActionTarget.INTERNSHIP, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateInternship(
        userId: UUID,
        internshipId: UUID,
        request: InternshipRequest,
    ): InternshipAdminResponse {
        val response = internshipService.updateInternship(userId, internshipId, request)
        userActionService.save(userId, ActionTarget.INTERNSHIP, ActionVerb.UPDATE, internshipId)
        return response
    }

    override fun uploadInternshipPhoto(
        userId: UUID,
        internshipId: UUID,
        file: Part,
    ): InternshipAdminResponse {
        val response = internshipService.uploadInternshipPhoto(userId, internshipId, file)
        userActionService.save(
            userId,
            ActionTarget.INTERNSHIP,
            ActionVerb.UPLOAD_PHOTO,
            internshipId
        )
        return response
    }

    override fun deleteInternship(userId: UUID, internshipId: UUID): DeletedResponse {
        val response = internshipService.deleteInternship(userId, internshipId)
        userActionService.save(userId, ActionTarget.INTERNSHIP, ActionVerb.DELETE, internshipId)
        return response
    }

    override fun createSchoolInternship(
        userId: UUID,
        schoolId: UUID,
        request: InternshipRequest,
        file: Part?,
    ): InternshipAdminResponse {
        val response = internshipService.createSchoolInternship(userId, schoolId, request, file)
        userActionService.save(userId, ActionTarget.INTERNSHIP, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateSchoolInternship(
        userId: UUID,
        schoolId: UUID,
        internshipId: UUID,
        request: InternshipRequest,
    ): InternshipAdminResponse {
        val response =
            internshipService.updateSchoolInternship(userId, schoolId, internshipId, request)
        userActionService.save(userId, ActionTarget.INTERNSHIP, ActionVerb.UPDATE, internshipId)
        return response
    }

    override fun uploadSchoolInternshipPhoto(
        userId: UUID,
        schoolId: UUID,
        internshipId: UUID,
        file: Part,
    ): InternshipAdminResponse {
        val response =
            internshipService.uploadSchoolInternshipPhoto(userId, schoolId, internshipId, file)
        userActionService.save(
            userId,
            ActionTarget.INTERNSHIP,
            ActionVerb.UPLOAD_PHOTO,
            internshipId
        )
        return response
    }

    override fun deleteSchoolInternship(
        userId: UUID,
        schoolId: UUID,
        internshipId: UUID,
    ): DeletedResponse {
        val response = internshipService.deleteSchoolInternship(userId, schoolId, internshipId)
        userActionService.save(userId, ActionTarget.INTERNSHIP, ActionVerb.DELETE, internshipId)
        return response
    }
}
