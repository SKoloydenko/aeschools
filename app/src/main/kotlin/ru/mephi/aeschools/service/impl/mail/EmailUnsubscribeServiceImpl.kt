package ru.mephi.aeschools.service.impl.mail

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.exceptions.auth.CorruptedTokenException
import ru.mephi.aeschools.service.EmailUnsubscribeService
import java.util.*

@Service
class EmailUnsubscribeServiceImpl(
    @Value("\${spring.security.email.unsubscribe-secret}") private val secret: String,
) : EmailUnsubscribeService {

    private val algorithm = SignatureAlgorithm.HS256
    private val key = Keys.hmacShaKeyFor(secret.toByteArray())

    private fun parseToken(token: String): Jws<Claims> {
        return try {
            Jwts.parserBuilder().setSigningKey(key).build()
                .parseClaimsJws(token)
        } catch (throwable: JwtException) {
            throw CorruptedTokenException()
        }
    }

    override fun getToken(userId: UUID): String {
        return Jwts.builder().claim("userId", userId).signWith(key, algorithm)
            .compact()
    }

    override fun parseUserId(token: String): UUID {
        val claims = parseToken(token)
        return try {
            UUID.fromString(claims.body["userId"].toString())
        } catch (e: Exception) {
            throw CorruptedTokenException()
        }
    }

}
