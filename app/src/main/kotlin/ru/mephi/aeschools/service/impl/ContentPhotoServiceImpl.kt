package ru.mephi.aeschools.service.impl

import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.ContentPhoto
import ru.mephi.aeschools.database.repository.AbstractContentDao
import ru.mephi.aeschools.database.repository.ContentPhotoDao
import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.ContentPhotoResponse
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.impl.storage.StorageManagerHelper
import ru.mephi.aeschools.util.constants.CONTENT_PHOTO_QUEUE
import ru.mephi.aeschools.util.parseContentPhotoPaths
import java.util.UUID
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class ContentPhotoServiceImpl(
    private val template: RabbitTemplate,
    private val imageManager: StorageImageManager,
    private val storageManagerHelper: StorageManagerHelper,
    private val contentPhotoDao: ContentPhotoDao,
    private val abstractContentDao: AbstractContentDao,
) : ContentPhotoService {

    override fun queueContent(request: ContentPhotoRequest) {
        template.convertAndSend(CONTENT_PHOTO_QUEUE, request)
    }

    private fun findByPhotoPath(photoPath: String) =
        contentPhotoDao.findByPhotoPath(photoPath).orElseThrow {
            ResourceNotFoundException(ContentPhoto::class.java, photoPath)
        }

    override fun contentExists(photoPath: String): Boolean =
        findByPhotoPath(photoPath).content != null

    override fun createContentPhoto(file: Part): ContentPhotoResponse {
        val path = imageManager.storeImage(file, ImageStoreDir.CONTENT_PHOTO, null, true)
        contentPhotoDao.save(ContentPhoto(path))
        return ContentPhotoResponse(storageManagerHelper.getPhotoPath(path))
    }

    @Transactional
    override fun updateContentPhoto(request: ContentPhotoRequest) {
        val content = abstractContentDao.findById(request.id)
        if (content.isEmpty) return
        val photoPaths = request.content.parseContentPhotoPaths()
        val shortPhotoPaths = photoPaths.map { path ->
            val photoPath = storageManagerHelper.getShortPhotoPath(path)
            val contentPhoto = contentPhotoDao.findByPhotoPath(photoPath)
            if (contentPhoto.isPresent) contentPhoto.get().content = content.get()
            photoPath
        }
        if (shortPhotoPaths.isNotEmpty()) {
            contentPhotoDao.deleteByContentIdAndPhotoPathNotIn(request.id, shortPhotoPaths)
        } else {
            contentPhotoDao.deleteByContentId(request.id)
        }
    }

    @Transactional
    override fun deleteContentPhoto(photoPath: String): DeletedResponse {
        val contentPhoto = findByPhotoPath(photoPath)
        contentPhotoDao.delete(contentPhoto)
        imageManager.remove(photoPath)
        return DeletedResponse(ContentPhoto::class.java, contentPhoto.id as UUID)
    }
}
