package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.FeatureToggle
import ru.mephi.aeschools.database.repository.FeatureToggleDao
import ru.mephi.aeschools.initializers.FeatureToggleInitializer
import ru.mephi.aeschools.model.dto.FeatureToggleUpdateRequest
import ru.mephi.aeschools.model.exceptions.common.FeatureNotFoundException
import ru.mephi.aeschools.service.FeatureToggleService

@Service
class FeatureToggleServiceImpl(
    private val dao: FeatureToggleDao,
    featureToggleInitializer: FeatureToggleInitializer,
) : FeatureToggleService {

    init {
        featureToggleInitializer.featureState.forEach {
            if (!dao.existsByFeatureName(it.key))
                dao.save(
                    FeatureToggle(
                        it.key,
                        featureToggleInitializer.featureDescription[it.key]!!,
                        it.value
                    )
                )
        }
    }

    override fun isFeatureEnabled(featureName: String): Boolean {
        val feature = dao.findByFeatureName(featureName)
            .orElseGet { throw FeatureNotFoundException(featureName) }
        return feature.enabled
    }

    override fun changeFeatureState(feature: FeatureToggleUpdateRequest): FeatureToggle {
        if (dao.existsByFeatureName(feature.featureName)) {
            dao.updateFeatureFlag(feature.featureName, feature.enabled)
            return dao.findByFeatureName(feature.featureName).get()
        } else throw FeatureNotFoundException(feature.featureName)
    }

    override fun listFeatures(): List<FeatureToggle> {
        return dao.findAll().toList()
    }

    override fun findByFeatureName(featureName: String): FeatureToggle {
        return dao.findByFeatureName(featureName).orElseGet {
            throw FeatureNotFoundException(featureName)
        }
    }
}
