package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.model.dto.course.response.CourseAdminResponse
import ru.mephi.aeschools.service.CourseService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class CourseServiceDecorator(
    @Qualifier("courseServiceImpl")
    private val courseService: CourseService,
    private val userActionService: UserActionService,
) : CourseService by courseService {

    override fun createCourse(
        userId: UUID,
        request: CourseRequest,
        file: Part?,
    ): CourseAdminResponse {
        val response = courseService.createCourse(userId, request, file)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateCourse(
        userId: UUID,
        courseId: UUID,
        request: CourseRequest,
    ): CourseAdminResponse {
        val response = courseService.updateCourse(userId, courseId, request)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.UPDATE, courseId)
        return response
    }

    override fun uploadCoursePhoto(
        userId: UUID,
        courseId: UUID,
        file: Part,
    ): CourseAdminResponse {
        val response = courseService.uploadCoursePhoto(userId, courseId, file)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.UPLOAD_PHOTO, courseId)
        return response
    }

    override fun deleteCourse(userId: UUID, courseId: UUID): DeletedResponse {
        val response = courseService.deleteCourse(userId, courseId)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.DELETE, courseId)
        return response
    }

    override fun createSchoolCourse(
        userId: UUID,
        schoolId: UUID,
        request: CourseRequest,
        file: Part?,
    ): CourseAdminResponse {
        val response = courseService.createSchoolCourse(userId, schoolId, request, file)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateSchoolCourse(
        userId: UUID,
        schoolId: UUID,
        courseId: UUID,
        request: CourseRequest,
    ): CourseAdminResponse {
        val response = courseService.updateSchoolCourse(userId, schoolId, courseId, request)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.UPDATE, courseId)
        return response
    }

    override fun uploadSchoolCoursePhoto(
        userId: UUID,
        schoolId: UUID,
        courseId: UUID,
        file: Part,
    ): CourseAdminResponse {
        val response = courseService.uploadCoursePhoto(userId, courseId, file)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.UPLOAD_PHOTO, courseId)
        return response
    }

    override fun deleteSchoolCourse(
        userId: UUID,
        schoolId: UUID,
        courseId: UUID,
    ): DeletedResponse {
        val response = courseService.deleteSchoolCourse(userId, schoolId, courseId)
        userActionService.save(userId, ActionTarget.COURSE, ActionVerb.DELETE, courseId)
        return response
    }
}
