package ru.mephi.aeschools.service.impl

import net.coobird.thumbnailator.Thumbnails
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.dto.ThumbnailRequest
import ru.mephi.aeschools.service.ThumbnailatorService
import ru.mephi.aeschools.service.impl.storage.StorageFileManager
import ru.mephi.aeschools.util.constants.IMAGE_THUMBNAIL_QUEUE
import java.io.File

@Service
class ThumbnailatorServiceImpl(
    private val rabbitTemplate: RabbitTemplate
) : ThumbnailatorService {

    override fun sendToQueue(request: ThumbnailRequest) {
        rabbitTemplate.convertAndSend(IMAGE_THUMBNAIL_QUEUE, request)
    }
}