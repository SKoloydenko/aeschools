package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.broadcast.request.BroadcastRequest
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.publicationLength

@Component
class BroadcastValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = BroadcastRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as BroadcastRequest
        errors += target.title.validate("title") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
        errors += target.text.validate("text") {
            it.isNotNull()
            it.maxLength(publicationLength)
        }
        errors += target.type.validate("type") {
            it.isNotNull()
        }
        errors += target.ignorePreferences.validate("ignorePreferences") {
            it.isNotNull()
        }
    }
}
