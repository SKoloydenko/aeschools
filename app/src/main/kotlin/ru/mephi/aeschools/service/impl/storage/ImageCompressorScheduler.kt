package ru.mephi.aeschools.service.impl.storage

import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.util.createIfNotExist
import kotlin.io.path.Path
import kotlin.io.path.pathString

@Component
class ImageCompressorScheduler(
    val storageManager: StorageImageManager,
) {

    @Scheduled(fixedDelay = 3 * 24 * 3600 * 1000L) // 3 days
    fun compression() {
        println("Image storage scheduler was STARTed.")
        ImageStoreDir.values().forEach { dir ->
            try {
                val path =
                    Path(storageManager.root.pathString + dir.path).createIfNotExist()
                val folder = path.toFile()
                if (!folder.isDirectory) return@forEach
                folder.listFiles()?.forEach { file ->
                    try {
                        storageManager.compressionIfNeed(file)
                    } catch (_: Exception) {

                    }
                }
            } catch (_: Exception) {

            }
        }
        println("Image storage scheduler was STOPed.")
    }
}
