package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesRequest
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesResponse
import ru.mephi.aeschools.service.UserActionService
import ru.mephi.aeschools.service.UserPreferencesService
import java.util.*

@Service
@Primary
class UserPreferencesServiceDecorator(
    @Qualifier("userPreferencesServiceImpl")
    private val preferencesService: UserPreferencesService,
    private val userActionService: UserActionService,
) : UserPreferencesService by preferencesService {
    override fun updatePreferences(
        userId: UUID,
        request: UserPreferencesRequest,
    ): UserPreferencesResponse {
        val response = preferencesService.updatePreferences(userId, request)
        userActionService.save(userId, ActionTarget.PREFERENCES, ActionVerb.UPDATE, userId)
        return response
    }
}
