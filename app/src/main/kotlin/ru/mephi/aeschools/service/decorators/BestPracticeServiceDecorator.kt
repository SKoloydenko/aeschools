package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.best_practice.request.BestPracticeRequest
import ru.mephi.aeschools.model.dto.best_practice.response.BestPracticeAdminResponse
import ru.mephi.aeschools.service.BestPracticeService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class BestPracticeServiceDecorator(
    @Qualifier("bestPracticeServiceImpl")
    private val bestPracticeService: BestPracticeService,
    private val userActionService: UserActionService,
) : BestPracticeService by bestPracticeService {

    override fun createBestPractice(
        userId: UUID,
        request: BestPracticeRequest,
        file: Part?,
    ): BestPracticeAdminResponse {
        val response = bestPracticeService.createBestPractice(userId, request, file)
        userActionService.save(userId, ActionTarget.BEST_PRACTICE, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateBestPractice(
        userId: UUID,
        bestPracticeId: UUID,
        request: BestPracticeRequest,
    ): BestPracticeAdminResponse {
        val response = bestPracticeService.updateBestPractice(userId, bestPracticeId, request)
        userActionService.save(
            userId,
            ActionTarget.BEST_PRACTICE,
            ActionVerb.UPDATE,
            bestPracticeId
        )
        return response
    }

    override fun uploadBestPracticePhoto(
        userId: UUID,
        bestPracticeId: UUID,
        file: Part,
    ): BestPracticeAdminResponse {
        val response = bestPracticeService.uploadBestPracticePhoto(userId, bestPracticeId, file)
        userActionService.save(
            userId,
            ActionTarget.BEST_PRACTICE,
            ActionVerb.UPLOAD_PHOTO,
            bestPracticeId
        )
        return response
    }

    override fun deleteBestPractice(userId: UUID, bestPracticeId: UUID): DeletedResponse {
        val response = bestPracticeService.deleteBestPractice(userId, bestPracticeId)
        userActionService.save(
            userId,
            ActionTarget.BEST_PRACTICE,
            ActionVerb.DELETE,
            bestPracticeId
        )
        return response
    }

    override fun createSchoolBestPractice(
        userId: UUID,
        schoolId: UUID,
        request: BestPracticeRequest,
        file: Part?,
    ): BestPracticeAdminResponse {
        val response = bestPracticeService.createSchoolBestPractice(userId, schoolId, request, file)
        userActionService.save(userId, ActionTarget.BEST_PRACTICE, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateSchoolBestPractice(
        userId: UUID,
        schoolId: UUID,
        bestPracticeId: UUID,
        request: BestPracticeRequest,
    ): BestPracticeAdminResponse {
        val response =
            bestPracticeService.updateSchoolBestPractice(userId, schoolId, bestPracticeId, request)
        userActionService.save(
            userId,
            ActionTarget.BEST_PRACTICE,
            ActionVerb.UPDATE,
            bestPracticeId
        )
        return response
    }

    override fun uploadSchoolBestPracticePhoto(
        userId: UUID,
        schoolId: UUID,
        bestPracticeId: UUID,
        file: Part,
    ): BestPracticeAdminResponse {
        val response = bestPracticeService.uploadBestPracticePhoto(userId, bestPracticeId, file)
        userActionService.save(
            userId,
            ActionTarget.BEST_PRACTICE,
            ActionVerb.UPLOAD_PHOTO,
            bestPracticeId
        )
        return response
    }

    override fun deleteSchoolBestPractice(
        userId: UUID,
        schoolId: UUID,
        bestPracticeId: UUID,
    ): DeletedResponse {
        val response =
            bestPracticeService.deleteSchoolBestPractice(userId, schoolId, bestPracticeId)
        userActionService.save(
            userId,
            ActionTarget.BEST_PRACTICE,
            ActionVerb.DELETE,
            bestPracticeId
        )
        return response
    }
}
