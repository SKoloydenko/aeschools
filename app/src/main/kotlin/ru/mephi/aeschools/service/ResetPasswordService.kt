package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.tags.Tag
import ru.mephi.aeschools.database.entity.email.ResetPasswordToken
import ru.mephi.aeschools.database.entity.user.User
import java.util.*

@Tag(name = "Auth API")
interface ResetPasswordService {
    fun createNewToken(user: User): ResetPasswordToken
    fun getLinkToResetPassword(token: ResetPasswordToken): String
    fun findByUserId(userId: UUID): Optional<ResetPasswordToken>
    fun findByToken(token: String): Optional<ResetPasswordToken>
    fun tokenIsExist(token: String): Boolean
}
