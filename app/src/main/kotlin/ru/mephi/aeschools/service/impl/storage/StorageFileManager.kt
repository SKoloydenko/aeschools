package ru.mephi.aeschools.service.impl.storage

import org.apache.commons.io.FilenameUtils
import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.enums.storage.FileStoreDir
import ru.mephi.aeschools.model.exceptions.storage.UnsupportedFileExtension
import ru.mephi.aeschools.service.StorageManager
import ru.mephi.aeschools.service.StorageService
import ru.mephi.aeschools.util.createIfNotExist
import java.io.File
import java.nio.file.Files
import javax.servlet.http.Part
import javax.transaction.Transactional
import kotlin.io.path.Path
import kotlin.io.path.deleteIfExists
import kotlin.io.path.name
import kotlin.io.path.pathString

@Service
class StorageFileManager(val service: StorageService, val storageHelper: StorageManagerHelper) : StorageManager() {

    val whitelistedExtensions = arrayOf("doc", "docx", "pptx", "xsl", "pdf", "csv", "txt", "rtf")

    private fun checkExtension(filename: String): String {
        val extension = FilenameUtils.getExtension(filename)
        if (!whitelistedExtensions.contains(extension)) {
            throw UnsupportedFileExtension()
        }
        return extension
    }

    @Transactional
    override fun remove(path: String?): Boolean {
        path ?: return false
        if (!storageHelper.isSelfFile(path)) return true
        try {
            val list = path.split("/")
            val file = File("${root.name}/${list[list.size - 2]}/${list[list.size - 1]}")
            service.removeFileByIdWithExtIfExist(list[list.size - 1])
            return root.resolve(file.absolutePath).deleteIfExists()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    override fun retrieve(path: String): File {
        return File("${root.name}/$path")
    }

    @Transactional
    fun storeFile(part: Part, dir: FileStoreDir, oldPath: String?): String {
        val extension = checkExtension(part.submittedFileName ?: "")
        remove(oldPath)
        val file = service.getNewFile(part.submittedFileName ?: "")
        val filename = "${file.id}.$extension"
        val path = Path(root.pathString + dir.path).createIfNotExist()
        val strPath: String
        path.resolve(filename).also {
            Files.copy(part.inputStream, it)
            strPath = "${it.parent.name}/${it.name}"
        }
        return strPath
    }
}



