package ru.mephi.aeschools.service.impl.activity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.activity.UserActivity
import ru.mephi.aeschools.database.repository.activity.UserActivityDao
import ru.mephi.aeschools.service.UserActivityService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.util.getPrincipal
import java.time.LocalDateTime
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.transaction.Transactional

@Service
class UserActivityServiceImpl : UserActivityService {
    @Autowired
    private lateinit var userActivityDao: UserActivityDao

    @Autowired
    private lateinit var userService: UserService

    @Transactional
    override fun handle(request: HttpServletRequest) {
        try {
            val userId = request.getPrincipal()
            val activity = findEntityById(userId)
            activity.time = LocalDateTime.now()
        } catch (_: Exception) {
        }
    }

    @Transactional
    override fun findEntityById(userId: UUID): UserActivity {
        return userActivityDao.findById(userId).orElseGet {
            val user = userService.findUserEntityById(userId)
            userActivityDao.save(UserActivity(user))
        }
    }

    override fun findAll(): List<UserActivity> {
        return userActivityDao.findAll().toList()
    }

    override fun deleteById(userId: UUID) {
        userActivityDao.deleteById(userId)
    }
}
