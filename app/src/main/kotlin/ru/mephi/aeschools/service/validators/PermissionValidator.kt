package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.school.request.PermissionsRequest

@Component
class PermissionValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = PermissionsRequest::class == clazz
    override fun validate(target: Any, errors: Errors) {
        this.target = target as PermissionsRequest
        errors += target.moderatorPermission.validate("moderatorPermission") {
            it.isNotNull()
        }
        errors += target.internshipPermission.validate("internshipPermission") {
            it.isNotNull()
        }
        errors += target.eventPermission.validate("eventPermission") {
            it.isNotNull()
        }
        errors += target.publicationPermission.validate("publicationPermission") {
            it.isNotNull()
        }
        errors += target.hiddenFieldsPermission.validate("hiddenFieldsPermission") {
            it.isNotNull()
        }
    }
}
