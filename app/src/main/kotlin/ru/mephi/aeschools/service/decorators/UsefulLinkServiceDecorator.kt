package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkAdminResponse
import ru.mephi.aeschools.service.UsefulLinkService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class UsefulLinkServiceDecorator(
    @Qualifier("usefulLinkServiceImpl")
    private val usefulLinkService: UsefulLinkService,
    private val userActionService: UserActionService,
) : UsefulLinkService by usefulLinkService {

    override fun createUsefulLink(
        userId: UUID,
        request: UsefulLinkRequest,
        file: Part?,
    ): UsefulLinkAdminResponse {
        val response = usefulLinkService.createUsefulLink(userId, request, file)
        userActionService.save(userId, ActionTarget.USEFUL_LINK, ActionVerb.CREATE, response.id)
        return response
    }

    override fun updateUsefulLink(
        userId: UUID,
        linkId: UUID,
        request: UsefulLinkRequest,
    ): UsefulLinkAdminResponse {
        val response = usefulLinkService.updateUsefulLink(userId, linkId, request)
        userActionService.save(userId, ActionTarget.USEFUL_LINK, ActionVerb.UPDATE, linkId)
        return response
    }

    override fun uploadUsefulLinkPhoto(userId: UUID, linkId: UUID, file: Part): UsefulLinkAdminResponse {
        val response = usefulLinkService.uploadUsefulLinkPhoto(userId, linkId, file)
        userActionService.save(userId, ActionTarget.USEFUL_LINK, ActionVerb.UPDATE, linkId)
        return response
    }

    override fun deleteUsefulLink(userId: UUID, linkId: UUID): DeletedResponse {
        val response = usefulLinkService.deleteUsefulLink(userId, linkId)
        userActionService.save(userId, ActionTarget.USEFUL_LINK, ActionVerb.UPDATE, linkId)
        return response
    }
}
