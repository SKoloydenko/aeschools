package ru.mephi.aeschools.service.validators.school

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.school.request.ContactsRequest
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.service.validators.AbstractValidator
import ru.mephi.aeschools.util.constants.extraSmallLength
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.publicationLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class SchoolValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = SchoolRequest::class == clazz

    private fun validateSchool(request: SchoolRequest, errors: Errors) {
        errors += request.university.validate("university") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
        errors += request.name.validate("name") {
            it.isNotNull()
            it.maxLength(largeLength)
        }

        errors += request.shortName.validate("shortName") {
            it.isNotNull()
            it.maxLength(extraSmallLength)
        }
        errors += request.actionScope.validate("actionScope") {
            it.isNotNull()
            it.maxLength(largeLength)
        }
        errors += request.about.validate("about") {
            it.isNotNull()
            it.maxLength(publicationLength)
        }
    }

    private fun validateSchoolActivities(request: SchoolRequest, errors: Errors) {
        errors += request.educationalActivities.validate("educationalActivities") {
            it.isNotNull()
            it.maxLength(publicationLength)
        }
        errors += request.researchActivities.validate("researchActivities") {
            it.isNotNull()
            it.maxLength(publicationLength)
        }
        errors += request.innovationalActivities.validate("innovationalActivities") {
            it.isNotNull()
            it.maxLength(publicationLength)
        }
    }

    private fun validateContacts(request: ContactsRequest, errors: Errors) {
        errors += request.website.validate("website") {
            it?.maxLength(smallLength)
        }
        errors += request.email.validate("email") {
            it?.maxLength(smallLength)
        }
        errors += request.phone.validate("phone") {
            it?.maxLength(extraSmallLength)
        }
    }

    override fun validate(target: Any, errors: Errors) {
        this.target = target as SchoolRequest
        validateSchool(target, errors)
        validateSchoolActivities(target, errors)
        if (target.contacts != null) {
            validateContacts(target.contacts, errors)
        }
    }
}
