package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.database.entity.school.ModeratorSchool
import ru.mephi.aeschools.database.entity.school.School
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.model.dto.school.response.IndustrialPartnerResponse
import ru.mephi.aeschools.model.dto.school.response.LeaderResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolResponse
import ru.mephi.aeschools.model.dto.school.response.SchoolShortResponse
import ru.mephi.aeschools.model.dto.school.response.UniversityPartnerResponse
import ru.mephi.aeschools.model.dto.user.response.UserShortResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "School API")
interface SchoolService {
    fun findSchoolEntityById(id: UUID): School
    fun findSchoolEntityByShortName(shortName: String): School
    fun findModeratorSchoolEntityByIds(
        schoolId: UUID,
        userId: UUID,
    ): ModeratorSchool

    fun existById(id: UUID): Boolean

    fun listSchools(query: String): List<SchoolShortResponse>

    fun listSchoolsForAdmin(query: String, page: ExtendedPageRequest): PageResponse<SchoolShortResponse>

    fun findSchoolById(@PathVariable id: UUID): SchoolResponse
    fun findSchoolByShortName(@PathVariable shortName: String): SchoolResponse
    fun createSchool(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: SchoolRequest,
        @RequestPart file: Part?,
    ): SchoolResponse

    fun updateSchool(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable id: UUID,
        @RequestBody request: SchoolRequest,
    ): SchoolResponse

    fun uploadSchoolPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable id: UUID,
        @RequestPart file: Part,
    ): SchoolResponse

    fun deleteSchool(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable id: UUID,
    ): DeletedResponse

    fun listLeaders(
        @PathVariable schoolId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<LeaderResponse>

    fun findLeaderById(
        @PathVariable schoolId: UUID,
        @PathVariable leaderId: UUID,
    ): LeaderResponse

    fun createLeader(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: LeaderRequest,
        @RequestPart file: Part?,
    ): LeaderResponse

    fun updateLeader(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable leaderId: UUID,
        @RequestBody request: LeaderRequest,
    ): LeaderResponse

    fun uploadLeaderPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable leaderId: UUID,
        @RequestPart file: Part,
    ): LeaderResponse

    fun deleteLeader(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable leaderId: UUID,
    ): DeletedResponse

    fun listIndustrialPartners(
        query: String,
        @PathVariable schoolId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<IndustrialPartnerResponse>

    fun findIndustrialPartnerById(
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
    ): IndustrialPartnerResponse

    fun createIndustrialPartner(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: IndustrialPartnerRequest,
        @RequestPart file: Part?,
    ): IndustrialPartnerResponse

    fun updateIndustrialPartner(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
        @RequestBody request: IndustrialPartnerRequest,
    ): IndustrialPartnerResponse

    fun uploadIndustrialPartnerPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
        @RequestPart file: Part,
    ): IndustrialPartnerResponse

    fun deleteIndustrialPartner(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
    ): DeletedResponse

    fun listUniversityPartners(
        query: String,
        @PathVariable schoolId: UUID,
        page: ExtendedPageRequest,
    ): PageResponse<UniversityPartnerResponse>

    fun findUniversityPartnerById(
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
    ): UniversityPartnerResponse

    fun createUniversityPartner(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: UniversityPartnerRequest,
        @RequestPart file: Part?,
    ): UniversityPartnerResponse

    fun updateUniversityPartner(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
        @RequestBody request: UniversityPartnerRequest,
    ): UniversityPartnerResponse

    fun uploadUniversityPartnerPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
        @RequestPart file: Part,
    ): UniversityPartnerResponse

    fun deleteUniversityPartner(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable partnerId: UUID,
    ): DeletedResponse

    fun listModerators(
        @PathVariable id: UUID, query: String,
        page: ExtendedPageRequest,
    ): PageResponse<UserShortResponse>

    fun createModerator(
        @Parameter(hidden = true) principal: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable userId: UUID,
    ): UserShortResponse

    fun deleteModerator(
        @Parameter(hidden = true) principal: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable userId: UUID,
    ): DeletedResponse

    fun findSchoolByModeratorId(userId: UUID): School
    fun allSchoolsCount(): Int
}
