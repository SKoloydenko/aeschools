package ru.mephi.aeschools.service

import ru.mephi.aeschools.model.dto.ThumbnailRequest

interface ThumbnailatorService {
    fun sendToQueue(request: ThumbnailRequest)
}