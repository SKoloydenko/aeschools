package ru.mephi.aeschools.service.validators.school

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.service.validators.AbstractValidator
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class UniversityPartnerValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = UniversityPartnerRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as UniversityPartnerRequest
        errors += target.name.validate("name") {
            it.isNotNull()
            it.maxLength(smallLength)
        }
        errors += target.description.validate("description") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.schoolRole.validate("schoolRole") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.universityRole.validate("universityRole") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.cooperationDirection.validate("cooperationDirection") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
    }
}
