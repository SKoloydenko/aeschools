package ru.mephi.aeschools.service

import java.util.*

interface PermissionService {
    fun isAdmin(userId: UUID): Boolean
    fun isModerator(userId: UUID): Boolean
    fun internshipPermission(schoolId: UUID, userId: UUID)
    fun eventPermission(schoolId: UUID, userId: UUID)
    fun publicationPermission(schoolId: UUID, userId: UUID)
    fun bestPracticePermission(schoolId: UUID, userId: UUID)
    fun coursePermission(schoolId: UUID, userId: UUID)
    fun hiddenFieldsPermission(schoolId: UUID, userId: UUID)
    fun moderatorPermission(userId: UUID)
    fun studentPermission(userId: UUID)
    fun adminPermission(userId: UUID)
    fun adminOrModeratorPermission(userId: UUID)
}
