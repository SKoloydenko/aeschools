package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.repository.activity.UserActivityDao
import ru.mephi.aeschools.database.repository.user.UserDao
import ru.mephi.aeschools.model.dto.statistics.ParameterResponse
import ru.mephi.aeschools.service.StatisticsService
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Service
class StatisticsServiceImpl(
    private val userDao: UserDao,
    private val userActivityDao: UserActivityDao,
) : StatisticsService {

    private fun countUsersTotal(): ParameterResponse {
        return ParameterResponse(
            name = "Всего пользователей",
            userDao.count(),
            0
        )
    }

    private fun countUsersLastWeek(): ParameterResponse {
        val time = LocalDateTime.now()
        val now = userDao.countByCreatedAtAfterAndCreatedAtBefore(
            time.minusWeeks(1),
            time
        )
        val prev =
            userDao.countByCreatedAtAfterAndCreatedAtBefore(
                time.minusWeeks(2),
                time.minusWeeks(1)
            )
        return ParameterResponse("Новых пользователей за неделю", now, prev)
    }

    private fun countActiveUsersLastWeek(): ParameterResponse {
        val time = LocalDateTime.now()
        val now =
            userActivityDao.countDistinctUserByTimeAfterAndTimeBefore(
                time.minusWeeks(
                    1
                ), time
            )
        val prev = userActivityDao.countDistinctUserByTimeAfterAndTimeBefore(
            time.minusWeeks(2),
            time.minusWeeks(1)
        )
        return ParameterResponse("Активных пользователей за неделю", now, prev)
    }

    private fun countActiveUsersLastDay(): ParameterResponse {
        val time = LocalDateTime.now()
        val date = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT)
        val now = userActivityDao.countDistinctUserByTimeAfterAndTimeBefore(
            date,
            time
        )
        val prev =
            userActivityDao.countDistinctUserByTimeAfterAndTimeBefore(
                date.minusDays(
                    1
                ), date
            )
        return ParameterResponse("Активных пользователей за день", now, prev)
    }

    private fun countFormsLastWeek(): ParameterResponse {
        return ParameterResponse("Отправленных анкет за неделю", 1, 1)
    }

    override fun getStatistics(): List<ParameterResponse> {
        return listOf(
            countUsersTotal(),
            countUsersLastWeek(),
            countActiveUsersLastWeek(),
            countActiveUsersLastDay(),
            countFormsLastWeek(),
        )
    }
}
