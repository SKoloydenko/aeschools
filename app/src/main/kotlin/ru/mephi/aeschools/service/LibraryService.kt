package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.library.request.LibraryRecordRequest
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordResponse
import ru.mephi.aeschools.model.dto.library.response.LibraryRecordShortResponse
import java.util.*
import javax.servlet.http.Part

interface LibraryService {
    fun create(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: LibraryRecordRequest,
        @RequestPart file: Part? = null
    ): LibraryRecordResponse

    fun update(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable("id") recordId: UUID,
        @RequestBody request: LibraryRecordRequest,
        @RequestPart file: Part? = null
    ): LibraryRecordResponse

    fun delete(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable("id") recordId: UUID,
    ): DeletedResponse

    fun uploadFile(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable("id") recordId: UUID,
        @RequestBody part: Part,
    ): LibraryRecordResponse

    fun fullList(@RequestBody request: ExtendedPageRequest, query: String = ""): PageResponse<LibraryRecordResponse>
    fun shortList(@RequestBody request: ExtendedPageRequest, query: String = ""): PageResponse<LibraryRecordShortResponse>
    fun getById(@PathVariable("id") id: UUID): LibraryRecordResponse
}
