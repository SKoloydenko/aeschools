package ru.mephi.aeschools.service.validators.school

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.service.validators.AbstractValidator
import ru.mephi.aeschools.util.constants.extraLargeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class LeaderValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = LeaderRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as LeaderRequest
        errors += target.fullName.validate("fullName") {
            it.isNotNull()
            it.maxLength(smallLength)
        }
        errors += target.jobRole.validate("jobRole") {
            it.isNotNull()
            it.maxLength(smallLength)
        }
        errors += target.ranks.validate("ranks") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.achievements.validate("achievements") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
    }
}
