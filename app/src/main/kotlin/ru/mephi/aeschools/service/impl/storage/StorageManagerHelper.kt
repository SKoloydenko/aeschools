package ru.mephi.aeschools.service.impl.storage

import org.springframework.stereotype.Service
import ru.mephi.aeschools.config.properties.ServerUrls
import ru.mephi.aeschools.model.exceptions.storage.InvalidPhotoPath
import ru.mephi.aeschools.util.constants.API_VERSION_1

@Service
class StorageManagerHelper(
    private val urls: ServerUrls
) {

    fun getPhotoPath(path: String): String = "${urls.fullBaseUrl}/api$API_VERSION_1/public/uploads/photo/$path"

    fun getShortPhotoPath(fullPhotoPath: String): String {
        return try {
            fullPhotoPath.split("uploads/photo/")[1]
        } catch (e: Exception) {
            throw InvalidPhotoPath(fullPhotoPath)
        }
    }

    fun getFilePath(path: String, isPublic: Boolean = false): String {
        return if (isPublic)
            "${urls.fullBaseUrl}/api/v1/public/uploads/file/$path"
        else
            "${urls.fullBaseUrl}/api/v1/uploads/file/$path"
    }

    fun isSelfFile(path: String?): Boolean {
        path ?: return false
        return path.startsWith("${urls.fullBaseUrl}/api/v1/public/uploads/") ||
                path.startsWith("${urls.fullBaseUrl}/api/v1/uploads/file/") || path.split("/").size == 2
    }
}
