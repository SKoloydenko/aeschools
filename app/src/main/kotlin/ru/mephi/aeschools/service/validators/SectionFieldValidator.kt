package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.section.request.UpdateFieldRequest
import ru.mephi.aeschools.util.constants.largeLength
import ru.mephi.aeschools.util.constants.smallLength

@Component
class SectionFieldValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = UpdateFieldRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as UpdateFieldRequest
        errors += target.title.validate("title") { it.maxLength(largeLength) }
        errors += target.comment.validate("comment") { it.maxLength(largeLength) }
    }
}
