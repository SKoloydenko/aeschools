package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.auth.request.SetNewPasswordRequest
import ru.mephi.aeschools.service.AuthService
import ru.mephi.aeschools.service.UserActionService
import java.util.*

@Service
@Primary
class AuthServiceDecorator(
    @Qualifier("authServiceImpl")
    private val authService: AuthService,
    private val userActionService: UserActionService,
) : AuthService by authService {
    override fun setNewPassword(request: SetNewPasswordRequest, userId: UUID) {
        val response = authService.setNewPassword(request, userId)
        userActionService.save(userId, ActionTarget.PASSWORD, ActionVerb.UPDATE, userId)
        return response
    }
}
