package ru.mephi.aeschools.service

import ru.mephi.aeschools.database.entity.activity.UserActivity
import java.util.*
import javax.servlet.http.HttpServletRequest

interface UserActivityService {
    fun handle(request: HttpServletRequest)
    fun findEntityById(userId: UUID): UserActivity
    fun findAll(): List<UserActivity>
    fun deleteById(userId: UUID)
}
