package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import org.springframework.web.bind.annotation.RequestBody
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.database.entity.action.UserAction
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.action.UserActionResponse
import java.util.*

interface UserActionService {
    fun save(
        userId: UUID,
        type: ActionTarget,
        verb: ActionVerb? = null,
        targetId: UUID? = null,
    ): UserAction

    fun listByUser(
        @Parameter userId: UUID,
        @RequestBody request: ExtendedPageRequest,
    ): PageResponse<UserActionResponse>

    fun listByTargetId(
        @Parameter targetId: UUID,
        @RequestBody request: ExtendedPageRequest,
    ): PageResponse<UserActionResponse>
}
