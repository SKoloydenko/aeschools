package ru.mephi.aeschools.service.impl.mail

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.dto.mail.mails.BroadcastIgnorePreferencesMail
import ru.mephi.aeschools.model.dto.mail.mails.BroadcastMail
import ru.mephi.aeschools.model.dto.mail.mails.ResetPasswordMail
import ru.mephi.aeschools.model.dto.mail.mails.VerifyEmailMail
import ru.mephi.aeschools.service.EmailSender
import ru.mephi.aeschools.util.constants.MAIL_BROADCAST
import ru.mephi.aeschools.util.constants.MAIL_BROADCAST_IGNORE_PREFERENCES
import ru.mephi.aeschools.util.constants.MAIL_RESET_PASSWORD
import ru.mephi.aeschools.util.constants.MAIL_VERIFY_EMAIL

@Service
class EmailSenderImpl(
    val template: AmqpTemplate,
) : EmailSender {

    override fun sendBroadcastMail(mail: BroadcastMail) {
        template.convertAndSend(MAIL_BROADCAST, mail)
    }

    override fun sendBroadcastMailIgnorePreferences(mail: BroadcastIgnorePreferencesMail) {
        template.convertAndSend(MAIL_BROADCAST_IGNORE_PREFERENCES, mail)
    }

    override fun sendVerifyEmailMail(mail: VerifyEmailMail) {
        template.convertAndSend(MAIL_VERIFY_EMAIL, mail)
    }

    override fun sendResetPasswordMail(mail: ResetPasswordMail) {
        template.convertAndSend(MAIL_RESET_PASSWORD, mail)
    }
}
