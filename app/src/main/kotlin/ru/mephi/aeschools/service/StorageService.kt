package ru.mephi.aeschools.service

import ru.mephi.aeschools.database.entity.storage.StorageFile
import java.util.*

interface StorageService {
    fun getNewFile(name: String): StorageFile
    fun getFileName(id: UUID): String
    fun getFileNameByIdWithExt(name: String): String
    fun removeFileByIdWithExtIfExist(name: String)
    fun findFileByIdWithExt(name: String): StorageFile
    fun existsByName(name: String): Boolean
}
