package ru.mephi.aeschools.service.impl.mail

import org.springframework.stereotype.Service
import ru.mephi.aeschools.model.dto.mail.MailReceiver
import ru.mephi.aeschools.model.enums.BroadcastType
import ru.mephi.aeschools.service.DataSenderHelper
import ru.mephi.aeschools.service.EmailUnsubscribeService
import ru.mephi.aeschools.service.UserPreferencesService
import ru.mephi.aeschools.service.UserService
import java.util.*

@Service
class DataSenderHelperImpl(
    private val preferencesService: UserPreferencesService,
    private val userService: UserService,
    private val unsubscribeService: EmailUnsubscribeService,
) : DataSenderHelper {

    override fun getReceiversIds(
        broadcastType: BroadcastType,
        ignorePreferences: Boolean,
    ): List<UUID> {
        return when (broadcastType) {
            BroadcastType.ALL -> preferencesService.findAllUsersIdsWithEnableEmail(ignorePreferences)
            BroadcastType.MODERATORS -> preferencesService.findModeratorsIdsWithEnableEmail(
                ignorePreferences
            )

            BroadcastType.USERS -> preferencesService.findUsersIdsWithEnableEmail(ignorePreferences)
        }
    }

    override fun getMailReceivers(
        broadcastType: BroadcastType,
        ignorePreferences: Boolean,
    ): List<MailReceiver> {
        val ids = getReceiversIds(broadcastType, ignorePreferences)
        return userService.findUserIdUserEmailPairs(ids)
            .map {
                if (!ignorePreferences) {
                    val token = unsubscribeService.getToken(it.userId)
                    MailReceiver(it.email, mapOf("unsubscribeEmailToken" to token))
                } else MailReceiver(it.email)
            }
    }
}
