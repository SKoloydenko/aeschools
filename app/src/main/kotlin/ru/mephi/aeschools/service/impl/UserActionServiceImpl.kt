package ru.mephi.aeschools.service.impl

import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.database.entity.action.UserAction
import ru.mephi.aeschools.database.repository.action.UserActionDao
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.action.UserActionResponse
import ru.mephi.aeschools.model.mappers.action.UserActionMapper
import ru.mephi.aeschools.service.UserActionService
import ru.mephi.aeschools.service.UserService
import java.util.*
import javax.transaction.Transactional

@Service
class UserActionServiceImpl(
    private val dao: UserActionDao,
    @Lazy private val userService: UserService,
    private val mapper: UserActionMapper,
) : UserActionService {

    @Transactional
    override fun save(
        userId: UUID,
        type: ActionTarget,
        verb: ActionVerb?,
        targetId: UUID?,
    ): UserAction {
        val action = UserAction(type, verb, targetId)
        action.user = userService.findUserEntityById(userId)
        return dao.save(action)
    }

    @Transactional
    override fun listByUser(
        userId: UUID,
        request: ExtendedPageRequest,
    ): PageResponse<UserActionResponse> {
        val page = dao.findAllByUserId(userId, request.pageable)
        return mapper.asPageResponse(page)
    }

    override fun listByTargetId(
        targetId: UUID,
        request: ExtendedPageRequest,
    ): PageResponse<UserActionResponse> {
        val page = dao.findAllByTargetId(targetId, request.pageable)
        return mapper.asPageResponse(page)
    }
}
