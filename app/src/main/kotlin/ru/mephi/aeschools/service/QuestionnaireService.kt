package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.database.entity.internship.Questionnaire
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.model.dto.internship.response.QuestionnaireResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Questionnaire API")
interface QuestionnaireService {
    fun findQuestionnaireById(
        @PathVariable internshipId: UUID,
        @PathVariable userId: UUID,
    ): QuestionnaireResponse

    fun findSchoolQuestionnaireById(
        @PathVariable schoolId: UUID,
        @PathVariable internshipId: UUID,
        @PathVariable userId: UUID,
    ): QuestionnaireResponse

    fun findLastQuestionnaire(
        @Parameter(hidden = true) userId: UUID,
    ): QuestionnaireResponse

    fun createQuestionnaire(
        @PathVariable internshipId: UUID,
        @PathVariable userId: UUID,
        @RequestBody request: QuestionnaireRequest,
        @RequestPart file: Part?,
    ): QuestionnaireResponse

    fun updateQuestionnaire(
        @PathVariable internshipId: UUID,
        @PathVariable userId: UUID,
        @RequestBody request: QuestionnaireRequest,
    ): QuestionnaireResponse

    fun uploadQuestionnaireFile(
        @PathVariable internshipId: UUID,
        @PathVariable userId: UUID,
        @RequestPart file: Part,
    ): QuestionnaireResponse

    fun deleteQuestionnaire(
        @PathVariable internshipId: UUID,
        @PathVariable userId: UUID,
    ): DeletedResponse

    fun findAllUserQuestionnaires(user: User): List<Questionnaire>
}
