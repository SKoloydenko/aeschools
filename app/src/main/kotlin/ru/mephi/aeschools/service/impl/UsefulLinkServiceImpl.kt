package ru.mephi.aeschools.service.impl

import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.useful_link.UsefulLink
import ru.mephi.aeschools.database.repository.useful_link.UsefulLinkDao
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkAdminResponse
import ru.mephi.aeschools.model.dto.useful_link.response.UsefulLinkResponse
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.mappers.useful_link.UsefulLinkMapper
import ru.mephi.aeschools.service.UsefulLinkService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.service.validators.UsefulLinkValidator
import java.util.*
import javax.servlet.http.Part
import javax.transaction.Transactional

@Service
class UsefulLinkServiceImpl(
    private var storageManager: StorageImageManager,

    private val usefulLinkValidator: UsefulLinkValidator,
    private val usefulLinkDao: UsefulLinkDao,
    private val usefulLinkMapper: UsefulLinkMapper,

    private val userService: UserService,
) : UsefulLinkService {

    private fun findEntityById(linkId: UUID) = usefulLinkDao.findById(linkId).orElseThrow {
        ResourceNotFoundException(UsefulLink::class.java, linkId)
    }

    override fun listUsefulLinks(page: ExtendedPageRequest): PageResponse<UsefulLinkResponse> {
        val links = usefulLinkDao.findAll(page.pageable)
        return usefulLinkMapper.asPageResponse(links)
    }

    override fun listUsefulLinksForAdmin(page: ExtendedPageRequest): PageResponse<UsefulLinkAdminResponse> {
        val links = usefulLinkDao.findAll(page.pageable)
        return usefulLinkMapper.asAdminPageResponse(links)
    }

    override fun findUsefulLinkByIdForAdmin(linkId: UUID): UsefulLinkAdminResponse {
        val link = findEntityById(linkId)
        return usefulLinkMapper.asAdminResponse(link)
    }

    override fun createUsefulLink(
        userId: UUID, request: UsefulLinkRequest, file: Part?,
    ): UsefulLinkAdminResponse {
        usefulLinkValidator.validate(request)
        val user = userService.findUserEntityById(userId)
        val link = usefulLinkMapper.asEntity(request).also {
            it.author = user
            it.photoPath = file?.let {
                storageManager.storeImage(file, ImageStoreDir.USEFUL_LINK, null, true)
            }
            usefulLinkDao.save(it)
        }
        return usefulLinkMapper.asAdminResponse(link)
    }

    @Transactional
    override fun updateUsefulLink(
        userId: UUID, linkId: UUID, request: UsefulLinkRequest,
    ): UsefulLinkAdminResponse {
        usefulLinkValidator.validate(request)
        val link = findEntityById(linkId)
        usefulLinkMapper.update(link, request)
        return usefulLinkMapper.asAdminResponse(link)
    }

    @Transactional
    override fun uploadUsefulLinkPhoto(
        userId: UUID,
        linkId: UUID,
        file: Part,
    ): UsefulLinkAdminResponse {
        val link = findEntityById(linkId)
        link.photoPath = storageManager.storeImage(
            file, ImageStoreDir.USEFUL_LINK, link.photoPath, true
        )
        return usefulLinkMapper.asAdminResponse(link)
    }

    override fun deleteUsefulLink(userId: UUID, linkId: UUID): DeletedResponse {
        val link = findEntityById(linkId)
        usefulLinkDao.delete(link)
        storageManager.remove(link.photoPath)
        return DeletedResponse(UsefulLink::class.java, linkId)
    }
}
