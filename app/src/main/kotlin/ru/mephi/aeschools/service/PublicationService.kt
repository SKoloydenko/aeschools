package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestPart
import ru.mephi.aeschools.database.entity.publication.Publication
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.publication.request.PublicationRequest
import ru.mephi.aeschools.model.dto.publication.response.PublicationAdminResponse
import ru.mephi.aeschools.model.dto.publication.response.PublicationResponse
import java.util.*
import javax.servlet.http.Part

@Tag(name = "Publication API")
interface PublicationService {
    fun findPublicationEntityById(id: UUID): Publication
    fun listPublications(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationResponse>

    fun listSchoolPublications(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationResponse>

    fun listPublicationsForAdmin(
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationAdminResponse>

    fun listSchoolPublicationsForAdmin(
        @PathVariable schoolId: UUID,
        query: String,
        tag: String,
        page: ExtendedPageRequest,
    ): PageResponse<PublicationAdminResponse>

    fun findPublicationById(
        @PathVariable publicationId: UUID,
    ): PublicationResponse

    fun findSchoolPublicationById(
        @PathVariable schoolId: UUID,
        @PathVariable publicationId: UUID,
    ): PublicationResponse

    fun findPublicationByIdForAdmin(
        @PathVariable publicationId: UUID,
    ): PublicationAdminResponse

    fun findSchoolPublicationByIdForAdmin(
        @PathVariable schoolId: UUID,
        @PathVariable publicationId: UUID,
    ): PublicationAdminResponse

    fun createPublication(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: PublicationRequest,
        @RequestPart file: Part?,
    ): PublicationAdminResponse

    fun updatePublication(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable publicationId: UUID,
        @RequestBody request: PublicationRequest,
    ): PublicationAdminResponse

    fun uploadPublicationPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable publicationId: UUID,
        @RequestPart file: Part,
    ): PublicationAdminResponse

    fun deletePublication(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable publicationId: UUID,
    ): DeletedResponse

    fun createSchoolPublication(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @RequestBody request: PublicationRequest,
        @RequestPart file: Part?,
    ): PublicationAdminResponse

    fun updateSchoolPublication(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable publicationId: UUID,
        @RequestBody request: PublicationRequest,
    ): PublicationAdminResponse

    fun uploadSchoolPublicationPhoto(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable publicationId: UUID,
        @RequestPart file: Part,
    ): PublicationAdminResponse

    fun deleteSchoolPublication(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable schoolId: UUID,
        @PathVariable publicationId: UUID,
    ): DeletedResponse
}
