package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.Parameter
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import ru.mephi.aeschools.database.entity.report.Section
import ru.mephi.aeschools.database.entity.report.SectionField
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.ExtendedPageRequest
import ru.mephi.aeschools.model.dto.PageResponse
import ru.mephi.aeschools.model.dto.excel.proxy.ReportExcelProxyHelper
import ru.mephi.aeschools.model.dto.section.ReportSectionFilterType
import ru.mephi.aeschools.model.dto.section.request.CreateFieldRequest
import ru.mephi.aeschools.model.dto.section.request.CreateSectionRequest
import ru.mephi.aeschools.model.dto.section.request.FieldAnswerRequest
import ru.mephi.aeschools.model.dto.section.request.SectionAnswerRequest
import ru.mephi.aeschools.model.dto.section.request.UpdateFieldRequest
import ru.mephi.aeschools.model.dto.section.response.*
import java.util.*
import javax.servlet.http.Part

interface ReportService {
    fun createSection(
        @Parameter(hidden = true) userId: UUID,
        @RequestBody request: CreateSectionRequest,
    ): SectionResponse

    fun getSection(@PathVariable sectionId: UUID): SectionResponse
    fun listSections(
        @RequestBody request: ExtendedPageRequest,
        @Parameter(hidden = true) userId: UUID,
        @RequestParam query: String,
        @RequestParam filter: ReportSectionFilterType,
    ): PageResponse<SectionResponse>

    fun updateSection(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @RequestBody request: CreateSectionRequest,
    ): SectionResponse

    fun removeSection(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
    ): DeletedResponse

    fun listFields(
        @PathVariable sectionId: UUID,
        @RequestBody request: ExtendedPageRequest,
        @Parameter(hidden = true) userId: UUID,
    ): PageResponse<SectionFieldResponse>

    fun createField(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @RequestBody request: CreateFieldRequest,
    ): SectionFieldResponse

    fun getField(
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
        @Parameter(hidden = true) userId: UUID,
    ): SectionFieldResponse

    fun getTotalFieldsCount(id: UUID): Long
    fun getAnswersCountToSectionBySchoolId(id: UUID, schoolId: UUID): Int

    fun fieldsPageByUser(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @RequestBody request: ExtendedPageRequest,
    ): PageResponse<SectionFieldWithAnswerResponse>

    fun fieldsPageOfSchool(
        @PathVariable schoolId: UUID,
        @PathVariable sectionId: UUID,
        @RequestBody request: ExtendedPageRequest,
    ): PageResponse<SectionFieldWithAnswerResponse>

    fun updateField(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
        @RequestBody request: UpdateFieldRequest,
    ): SectionFieldResponse

    fun removeFiled(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
    ): DeletedResponse

    fun answerToSection(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @RequestBody request: SectionAnswerRequest,
    ): List<SectionFieldWithAnswerResponse>

    fun answerToField(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
        @RequestBody request: FieldAnswerRequest,
    ): SectionFieldWithAnswerResponse

    fun deleteAnswer(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
    ): DeletedResponse

    fun uploadFileToField(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
        file: Part,
    ): SectionFieldWithAnswerResponse

    fun getAnswerToField(
        @Parameter(hidden = true) userId: UUID,
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
    ): SectionFieldWithAnswerResponse

    fun getAllAnswersToSection(section: Section): List<ReportExcelProxyHelper>
    fun findSectionById(sectionId: UUID): Section

    fun existFileAnswerWithModerator(filePath: String, userId: UUID): Boolean

    fun getAllAnswersToField(
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
        @RequestBody request: ExtendedPageRequest,
    ): PageResponse<FullSectionFieldAnswerResponse>

    fun getAnswerOfSchoolToField(
        @PathVariable sectionId: UUID,
        @PathVariable fieldId: UUID,
        @PathVariable schoolId: UUID,
    ): SectionFieldWithAnswerResponse

    fun getSchoolCountOfFullFillSection(section: Section): Int
    fun getAllSchoolCount(): Int
    fun getAnswersCountToField(field: SectionField): Int

    fun getLinkToReportFile(path: String): String
}
