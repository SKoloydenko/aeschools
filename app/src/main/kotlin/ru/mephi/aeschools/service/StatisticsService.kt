package ru.mephi.aeschools.service

import ru.mephi.aeschools.model.dto.statistics.ParameterResponse

interface StatisticsService {
    fun getStatistics(): List<ParameterResponse>
}
