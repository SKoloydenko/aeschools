package ru.mephi.aeschools.service.decorators

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import ru.mephi.aeschools.database.entity.action.ActionTarget
import ru.mephi.aeschools.database.entity.action.ActionVerb
import ru.mephi.aeschools.model.dto.DeletedResponse
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.model.dto.internship.response.QuestionnaireResponse
import ru.mephi.aeschools.service.QuestionnaireService
import ru.mephi.aeschools.service.UserActionService
import java.util.*
import javax.servlet.http.Part

@Service
@Primary
class QuestionnaireServiceDecorator(
    @Qualifier("questionnaireServiceImpl")
    private val questionnaireService: QuestionnaireService,
    private val userActionService: UserActionService,
) : QuestionnaireService by questionnaireService {
    override fun createQuestionnaire(
        internshipId: UUID,
        userId: UUID,
        request: QuestionnaireRequest,
        file: Part?,
    ): QuestionnaireResponse {
        val response =
            questionnaireService.createQuestionnaire(internshipId, userId, request, file)
        userActionService.save(
            userId,
            ActionTarget.INTERNSHIP_APPLICATION,
            ActionVerb.CREATE,
            response.id
        )
        return response
    }

    override fun deleteQuestionnaire(
        internshipId: UUID,
        userId: UUID,
    ): DeletedResponse {
        val response =
            questionnaireService.deleteQuestionnaire(internshipId, userId)
        userActionService.save(
            userId,
            ActionTarget.INTERNSHIP_APPLICATION,
            ActionVerb.DELETE,
            response.id
        )
        return response
    }
}
