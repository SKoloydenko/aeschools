package ru.mephi.aeschools.service.validators

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.util.constants.mediumLength
import ru.mephi.aeschools.util.constants.passwordMaxLength
import ru.mephi.aeschools.util.constants.passwordMinLength

@Component
class AuthValidator(
    private val userValidator: UserValidator,
) : AbstractValidator() {
    override lateinit var target: Any

    private val emailPattern = Regex(
        "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                + "((([0-1]?\\d{1,2}|25[0-5]|2[0-4]\\d)\\.([0-1]?"
                + "\\d{1,2}|25[0-5]|2[0-4]\\d)\\."
                + "([0-1]?\\d{1,2}|25[0-5]|2[0-4]\\d)\\.([0-1]?"
                + "\\d{1,2}|25[0-5]|2[0-4]\\d))|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
    )

    private fun validateCredentials(request: RegistrationRequest, errors: Errors) {
        errors += request.email.validate("email") {
            it.isNotNullNorEmpty()
            it.matches(emailPattern)
            it.maxLength(mediumLength)
        }
        errors += request.password.validate("password") {
            it.isNotNullNorEmpty()
            it.minLength(passwordMinLength)
            it.maxLength(passwordMaxLength)
        }
    }

    private fun validatePreferences(request: RegistrationRequest, errors: Errors) {
        errors += request.enableEmail.validate("enableEmail") {
            it.isNotNull()
        }
    }

    override fun supports(clazz: Class<*>) = RegistrationRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as RegistrationRequest
        validateCredentials(target, errors)
        validatePreferences(target, errors)
        userValidator.validate(target, errors)
    }
}
