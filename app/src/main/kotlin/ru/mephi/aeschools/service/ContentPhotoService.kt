package ru.mephi.aeschools.service

import ru.mephi.aeschools.model.dto.ContentPhotoRequest
import ru.mephi.aeschools.model.dto.ContentPhotoResponse
import ru.mephi.aeschools.model.dto.DeletedResponse
import javax.servlet.http.Part

interface ContentPhotoService {
    fun queueContent(request: ContentPhotoRequest)
    fun contentExists(photoPath: String): Boolean
    fun createContentPhoto(file: Part): ContentPhotoResponse
    fun updateContentPhoto(request: ContentPhotoRequest)
    fun deleteContentPhoto(photoPath: String): DeletedResponse
}
