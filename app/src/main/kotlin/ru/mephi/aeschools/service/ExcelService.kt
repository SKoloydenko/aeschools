package ru.mephi.aeschools.service

import io.swagger.v3.oas.annotations.tags.Tag
import ru.mephi.aeschools.model.dto.ExcelResponse
import java.util.*

@Tag(name = "Excel API")
interface ExcelService {
    fun exportUsers(): ExcelResponse
    fun exportReport(sectionId: UUID): ExcelResponse

    fun exportEvents(): ExcelResponse
    fun exportSchoolEvents(schoolId: UUID): ExcelResponse
    fun exportEventUsers(eventId: UUID): ExcelResponse
    fun exportUserEvents(userId: UUID): ExcelResponse

    fun exportInternships(): ExcelResponse
    fun exportSchoolInternships(schoolId: UUID): ExcelResponse
    fun exportInternshipApplicants(internshipId: UUID): ExcelResponse
    fun exportUserInternships(userId: UUID): ExcelResponse
}
