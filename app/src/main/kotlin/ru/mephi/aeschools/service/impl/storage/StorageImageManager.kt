package ru.mephi.aeschools.service.impl.storage

import org.apache.tika.Tika
import org.springframework.stereotype.Service
import ru.mephi.aeschools.initializers.StorageConstants
import ru.mephi.aeschools.model.dto.ThumbnailRequest
import ru.mephi.aeschools.model.enums.storage.ImageStoreDir
import ru.mephi.aeschools.model.exceptions.common.FileTooLargeException
import ru.mephi.aeschools.model.exceptions.common.FileTooSmallException
import ru.mephi.aeschools.model.exceptions.common.InvalidFileException
import ru.mephi.aeschools.service.StorageManager
import ru.mephi.aeschools.service.StorageService
import ru.mephi.aeschools.service.ThumbnailatorService
import ru.mephi.aeschools.util.createIfNotExist
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.File
import java.io.InputStream
import java.nio.file.Files
import javax.imageio.ImageIO
import javax.servlet.http.Part
import javax.transaction.Transactional
import kotlin.io.path.Path
import kotlin.io.path.deleteIfExists
import kotlin.io.path.name
import kotlin.io.path.pathString


@Service
class StorageImageManager(
    private val constants: StorageConstants,
    private val thumbnailService: ThumbnailatorService,
    private val service: StorageService,
) : StorageManager() {

    private val allowedMimes = listOf("image")

    private fun checkImageMime(stream: InputStream): String {
        val (mime, ext) = Tika().detect(stream).split("/")
        if (mime !in allowedMimes) throw InvalidFileException()
        return ext
    }

    fun storeImage(
        part: Part,
        dir: ImageStoreDir,
        oldPath: String?,
        isAdmin: Boolean,
    ): String {
        val targetSize =
            if (isAdmin) constants.maxSizeAdmin else constants.maxSizeUser
        if (part.size > targetSize.toBytes()) throw FileTooLargeException(
            targetSize.toBytes(), part.size
        )

        val extension = checkImageMime(part.inputStream)
        val storageFile = service.getNewFile(part.submittedFileName ?: "")
        val filename = "${storageFile.id}.$extension"

        val path = Path(root.pathString + dir.path).createIfNotExist()
        val strPath: String
        path.resolve(filename).also {
            Files.copy(part.inputStream, it)
            strPath = "${it.parent.name}/${it.name}"
        }
        val file = retrieve(strPath)

        if (!isAdmin && part.size < constants.criticalSize.toBytes()) {
            val image = ImageIO.read(file)
            if (image.width < constants.minWidth || image.height < constants.minHeight) {
                file.delete()
                throw FileTooSmallException()
            }
        }
        remove(oldPath)
        thumbnailService.sendToQueue(ThumbnailRequest(strPath))
        return strPath
    }

    fun compression(
        image: BufferedImage,
        file: File,
        extension: String,
        scale: Double,
    ) {
        val width = (image.width * scale).toInt()   // 1000
        val height =
            (image.height * scale).toInt() // 1000 * image.height / image.width
        val resultingImage: Image =
            image.getScaledInstance(width, height, Image.SCALE_DEFAULT)
        val outputImage =
            BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        outputImage.graphics.drawImage(resultingImage, 0, 0, null)
        ImageIO.write(outputImage, extension, file)
    }

    fun compressionIfNeed(file: File) {
        if (Files.size(file.toPath()) <= constants.maxStoredSize.toBytes()) return
        val image = ImageIO.read(file)
        val extension = file.extension
        val scale = 0.7
        compression(image, file, extension, scale)
    }

    @Transactional
    override fun remove(path: String?): Boolean {
        path ?: return false
        val mainFile = File("${root.name}/$path")
        val list = path.split("/")
        service.removeFileByIdWithExtIfExist(list[list.size - 1])
        val mediumFile = File(root.name + "/" + path.toMediumFile())
        return root.resolve(mainFile.absolutePath).deleteIfExists()
                && root.resolve(mediumFile.absolutePath).deleteIfExists()
    }

    override fun retrieve(path: String): File {
        val paths = path.split("/").toMutableList()
        var name = paths.removeAt(paths.size - 1)
        if (name.split(".").size == 3) {
            if (name.split(".")[1] == "medium") {
                val file = File("${root.name}/$path")
                if (file.exists()) return file
                name = name.replace(".medium.", ".")
            }
        }
        paths.add(name)
        val path = paths.joinToString("/")
        return File("${root.name}/$path")
    }


    fun getFullPhotoName(name: String): String {
        return name.replace(".medium.", ".")
    }

}

private fun String.toMediumFile(): String {
    return this.split("/").let { it ->
        val it = it.toMutableList()
        val names = it.last().split(".").toMutableList()
        names.add(1, "medium")
        it[it.size - 1] = names.joinToString(".")
        it
    }.joinToString("/")
}
