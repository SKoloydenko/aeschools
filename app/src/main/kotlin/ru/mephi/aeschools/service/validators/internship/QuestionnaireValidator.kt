package ru.mephi.aeschools.service.validators.internship

import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.service.validators.AbstractValidator
import ru.mephi.aeschools.util.constants.extraLargeLength

@Component
class QuestionnaireValidator : AbstractValidator() {
    override lateinit var target: Any

    override fun supports(clazz: Class<*>) = QuestionnaireRequest::class == clazz

    override fun validate(target: Any, errors: Errors) {
        this.target = target as QuestionnaireRequest
        errors += target.motivationalLetter.validate("motivationalLetter") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
        errors += target.achievements.validate("achievements") {
            it.isNotNull()
            it.maxLength(extraLargeLength)
        }
    }
}
