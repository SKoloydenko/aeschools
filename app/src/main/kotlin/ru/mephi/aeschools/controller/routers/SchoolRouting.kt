package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.BestPracticeHandler
import ru.mephi.aeschools.controller.handlers.SchoolHandler
import ru.mephi.aeschools.model.annotations.swagger.SchoolRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class SchoolRouting(
    private val handler: SchoolHandler,
    private val bestPracticeHandler: BestPracticeHandler,
) {
    @Bean
    @SchoolRoutingDoc
    fun schoolRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public/school".nest {
                "/{schoolId}".nest {
                    "/leader".nest {
                        "/{leaderId}".nest {
                            GET(handler::findLeaderById)
                        }
                        GET(handler::listLeaders)
                    }
                    "/partner".nest {
                        "/industrial".nest {
                            GET("/{partnerId}", handler::findIndustrialPartnerById)
                            GET(handler::listIndustrialPartners)
                        }
                        "/university".nest {
                            GET("/{partnerId}", handler::findUniversityPartnerById)
                            GET(handler::listUniversityPartners)
                        }
                    }
                    "/best-practice".nest {
                        GET("/{bestPracticeId}", bestPracticeHandler::findSchoolBestPracticeById)
                        GET(bestPracticeHandler::listSchoolBestPractices)
                    }
                    GET(handler::findSchoolById)
                }
                GET(handler::listSchools)
            }

            "/admin/school".nest {
                "/{schoolId}".nest {
                    "/best-practice".nest {
                        "/{bestPracticeId}".nest {
                            POST("/photo", bestPracticeHandler::uploadSchoolBestPracticePhoto)
                            GET(bestPracticeHandler::findSchoolBestPracticeByIdForAdmin)
                            PUT(bestPracticeHandler::updateSchoolBestPractice)
                            DELETE(bestPracticeHandler::deleteSchoolBestPractice)
                        }
                        GET(bestPracticeHandler::listSchoolBestPracticesForAdmin)
                        POST(bestPracticeHandler::createSchoolBestPractice)
                    }
                    "/leader".nest {
                        "/{leaderId}".nest {
                            POST("/photo", handler::uploadLeaderPhoto)
                            PUT(handler::updateLeader)
                            DELETE(handler::deleteLeader)
                        }
                        POST(handler::createLeader)
                    }
                    "/partner".nest {
                        "/industrial".nest {
                            "/{partnerId}".nest {
                                POST("/photo", handler::uploadIndustrialPartnerPhoto)
                                PUT(handler::updateIndustrialPartner)
                                DELETE(handler::deleteIndustrialPartner)
                            }
                            POST(handler::createIndustrialPartner)
                        }
                        "/university".nest {
                            "/{partnerId}".nest {
                                POST("/photo", handler::uploadUniversityPartnerPhoto)
                                PUT(handler::updateUniversityPartner)
                                DELETE(handler::deleteUniversityPartner)
                            }
                            POST(handler::createUniversityPartner)
                        }
                    }
                    "/moderators".nest {
                        "/{userId}".nest {
                            POST(handler::createModerator)
                            DELETE(handler::deleteModerator)
                        }
                        GET(handler::listModerators)
                    }
                    POST("/photo", handler::uploadSchoolPhoto)
                    GET(handler::findSchoolByIdForAdmin)
                    PUT(handler::updateSchool)
                    DELETE(handler::deleteSchool)
                }
                GET(handler::listSchoolsForAdmin)
                POST(handler::createSchool)
            }
        }
    }
}
