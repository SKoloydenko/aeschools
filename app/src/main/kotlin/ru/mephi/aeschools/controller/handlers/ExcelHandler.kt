package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface ExcelHandler {
    fun exportUsers(request: ServerRequest): ServerResponse
    fun exportReport(request: ServerRequest): ServerResponse
    fun exportEvents(request: ServerRequest): ServerResponse
    fun exportSchoolEvents(request: ServerRequest): ServerResponse
    fun exportEventUsers(request: ServerRequest): ServerResponse
    fun exportUserEvents(request: ServerRequest): ServerResponse
    fun exportInternships(request: ServerRequest): ServerResponse
    fun exportSchoolInternships(request: ServerRequest): ServerResponse
    fun exportInternshipApplicants(request: ServerRequest): ServerResponse
    fun exportUserInternships(request: ServerRequest): ServerResponse
}
