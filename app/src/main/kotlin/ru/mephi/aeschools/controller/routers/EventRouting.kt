package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.EventHandler
import ru.mephi.aeschools.model.annotations.swagger.EventRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class EventRouting(
    private val handler: EventHandler,
) {

    @Bean
    @EventRoutingDoc
    fun eventRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public".nest {
                "/school/{schoolId}/event".nest {
                    GET("/{eventId}", handler::findSchoolEventById)
                    GET(handler::listSchoolEvents)
                }
                "/event".nest {
                    GET("/{eventId}", handler::findEventById)
                    GET(handler::listEvents)
                }
            }

            "/user".nest {
                "/event/{eventId}".nest {
                    POST(handler::joinEvent)
                    DELETE(handler::leaveEvent)
                }
                // GET("/me/event", handler::listEventsForUser)
            }

            "/admin".nest {
                "/school/{schoolId}/event".nest {
                    "/{eventId}".nest {
                        GET("/members", handler::listSchoolEventMembers)
                        POST("/photo", handler::uploadSchoolEventPhoto)
                        GET(handler::findSchoolEventByIdForAdmin)
                        PUT(handler::updateSchoolEvent)
                        DELETE(handler::deleteSchoolEvent)
                    }
                    GET(handler::listSchoolEventsForAdmin)
                    POST(handler::createSchoolEvent)
                }
                "/event".nest {
                    "/{eventId}".nest {
                        GET("/members", handler::listEventMembers)
                        POST("/photo", handler::uploadEventPhoto)
                        GET(handler::findEventByIdForAdmin)
                        PUT(handler::updateEvent)
                        DELETE(handler::deleteEvent)
                    }
                    GET(handler::listEventsForAdmin)
                    POST(handler::createEvent)
                }
            }
        }
    }
}
