package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.PreferenceHandler
import ru.mephi.aeschools.model.dto.preferences.UserPreferencesRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.UserPreferencesService
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Service
class PreferenceHandlerImpl(
    private val preferencesService: UserPreferencesService,
    private val permissionService: PermissionService,
) : PreferenceHandler {

    override fun getPreferences(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val response = preferencesService.getPreferencesById(userId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updatePreferences(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val updateRequest = request.body(UserPreferencesRequest::class.java)
        val response = preferencesService.updatePreferences(userId, updateRequest)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getPreferencesOfUser(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = preferencesService.getPreferencesById(userId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun turnOffEmailPreference(request: ServerRequest): ServerResponse {
        val token = request.pathVariableOrThrow<String>("token")
        preferencesService.turnOffEmailPreference(token)
        return "Вы успешно отписались от почтовых рассылок!".toOkBodyWithContentTypeJSON()
    }
}
