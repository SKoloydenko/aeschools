package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface FeatureToggleHandler {
    fun changeFeatureState(request: ServerRequest): ServerResponse
    fun listAllFeatures(request: ServerRequest): ServerResponse
    fun getFeatureByName(request: ServerRequest): ServerResponse
}
