package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface CourseHandler {
    fun listCourses(request: ServerRequest): ServerResponse
    fun listSchoolCourses(request: ServerRequest): ServerResponse
    fun listCoursesForAdmin(request: ServerRequest): ServerResponse
    fun listSchoolCoursesForAdmin(request: ServerRequest): ServerResponse
    fun findCourseById(request: ServerRequest): ServerResponse
    fun findSchoolCourseById(request: ServerRequest): ServerResponse
    fun findCourseByIdForAdmin(request: ServerRequest): ServerResponse
    fun findSchoolCourseByIdForAdmin(request: ServerRequest): ServerResponse
    fun createCourse(request: ServerRequest): ServerResponse
    fun updateCourse(request: ServerRequest): ServerResponse
    fun uploadCoursePhoto(request: ServerRequest): ServerResponse
    fun deleteCourse(request: ServerRequest): ServerResponse
    fun createSchoolCourse(request: ServerRequest): ServerResponse
    fun updateSchoolCourse(request: ServerRequest): ServerResponse
    fun uploadSchoolCoursePhoto(request: ServerRequest): ServerResponse
    fun deleteSchoolCourse(request: ServerRequest): ServerResponse
}
