package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.QuestionnaireHandler
import ru.mephi.aeschools.model.dto.internship.request.QuestionnaireRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.QuestionnaireService
import ru.mephi.aeschools.util.getMultiPartFile
import ru.mephi.aeschools.util.getMultiPartFileOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class QuestionnaireHandlerImpl(
    private val questionnaireService: QuestionnaireService,
    private val permissionService: PermissionService,
) : QuestionnaireHandler {

    override fun findQuestionnaireById(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = questionnaireService.findQuestionnaireById(internshipId, userId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolQuestionnaireById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = questionnaireService.findSchoolQuestionnaireById(schoolId, internshipId, userId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun findLastQuestionnaire(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val questionnaire = questionnaireService.findLastQuestionnaire(principal)
        return questionnaire.toOkBodyWithContentTypeJSON()
    }

    override fun findUserQuestionnaire(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val response = questionnaireService.findQuestionnaireById(internshipId, principal)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun createQuestionnaire(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val questionnaireRequest = request.body<QuestionnaireRequest>()
        val file = request.getMultiPartFileOrNull()
        val internshipApplication =
            questionnaireService.createQuestionnaire(
                internshipId,
                principal,
                questionnaireRequest,
                file
            )
        return internshipApplication.toCreatedBodyWithContentTypeJSON(
            request.uri(),
            internshipApplication.id
        )
    }

    override fun updateQuestionnaire(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val questionnaireRequest = request.body<QuestionnaireRequest>()
        val questionnaire =
            questionnaireService.updateQuestionnaire(internshipId, principal, questionnaireRequest)
        return questionnaire.toOkBodyWithContentTypeJSON()
    }

    override fun uploadQuestionnaireFile(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val file = request.getMultiPartFile()
        val questionnaire =
            questionnaireService.uploadQuestionnaireFile(internshipId, principal, file)
        return questionnaire.toOkBodyWithContentTypeJSON()
    }

    override fun deleteQuestionnaire(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship =
            questionnaireService.deleteQuestionnaire(internshipId, principal)
        return internship.toOkBodyWithContentTypeJSON()
    }
}
