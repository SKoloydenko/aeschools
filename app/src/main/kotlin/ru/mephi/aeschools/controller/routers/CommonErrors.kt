package ru.mephi.aeschools.controller.routers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.apache.tomcat.util.http.fileupload.impl.InvalidContentTypeException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.dao.InvalidDataAccessApiUsageException
import org.springframework.data.mapping.PropertyReferenceException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.App
import ru.mephi.aeschools.model.exceptions.AppException
import ru.mephi.aeschools.model.exceptions.InternalServerError
import java.lang.reflect.UndeclaredThrowableException
import java.nio.charset.StandardCharsets.UTF_8
import javax.servlet.http.HttpServletResponse

val LOGGER: Logger = LoggerFactory.getLogger(App::class.java)

fun handleCommonError(throwable: Throwable): ServerResponse {
    return when (throwable) {
        is AppException -> throwable
        is InvalidContentTypeException -> AppException(
            status = HttpStatus.NOT_ACCEPTABLE,
            cause = throwable
        )

        is MissingKotlinParameterException,
        is HttpMessageNotReadableException,
        is JsonProcessingException -> AppException(
            status = HttpStatus.UNPROCESSABLE_ENTITY,
            cause = throwable,
            message = throwable.localizedMessage
        )

        is InvalidDataAccessApiUsageException,
        is PropertyReferenceException -> AppException(
            status = HttpStatus.BAD_REQUEST,
            cause = throwable
        )

        is UndeclaredThrowableException -> return handleCommonError(throwable.undeclaredThrowable)
        else -> InternalServerError(throwable)

    }.asResponse()
}

fun HttpServletResponse.writeResponseError(
    exception: AppException
) {
    this.contentType = MediaType.APPLICATION_JSON_VALUE
    this.characterEncoding = UTF_8.name()
    this.status = exception.status.value()
    val body = ObjectMapper().also { it.findAndRegisterModules() }
        .writeValueAsString(exception)
    this.writer.print(body)
}
