package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.CourseHandler
import ru.mephi.aeschools.model.dto.course.request.CourseRequest
import ru.mephi.aeschools.service.CourseService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.constants.DEFAULT_PUBLICATION_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class CourseHandlerImpl(
    private val permissionService: PermissionService,
    private val courseService: CourseService,
) : CourseHandler {
    override fun listCourses(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val courses = courseService.listCourses(query, tag, page)
        return courses.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolCourses(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val courses = courseService.listSchoolCourses(schoolId, query, tag, page)
        return courses.toOkBodyWithContentTypeJSON()
    }

    override fun listCoursesForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val courses = courseService.listCoursesForAdmin(query, tag, page)
        return courses.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolCoursesForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.coursePermission(schoolId, request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val courses =
            courseService.listSchoolCoursesForAdmin(schoolId, query, tag, page)
        return courses.toOkBodyWithContentTypeJSON()
    }

    override fun findCourseById(request: ServerRequest): ServerResponse {
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val course = courseService.findCourseById(courseId)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolCourseById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val course = courseService.findSchoolCourseById(schoolId, courseId)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun findCourseByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val course = courseService.findCourseByIdForAdmin(courseId)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolCourseByIdForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val course = courseService.findSchoolCourseByIdForAdmin(schoolId, courseId)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun createCourse(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val courseRequest = request.bodyOrParam<CourseRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val course =
            courseService.createCourse(principal, courseRequest, file)
        return course.toCreatedBodyWithContentTypeJSON(request.uri(), course.id)
    }

    override fun updateCourse(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val courseRequest = request.bodyOrParam<CourseRequest>()
        val course =
            courseService.updateCourse(principal, courseId, courseRequest)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun uploadCoursePhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val file = request.getMultiPartPhoto()
        val course =
            courseService.uploadCoursePhoto(principal, courseId, file)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun deleteCourse(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val course =
            courseService.deleteCourse(principal, courseId)
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun createSchoolCourse(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.coursePermission(schoolId, principal)
        val courseRequest = request.bodyOrParam<CourseRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val course =
            courseService.createSchoolCourse(
                principal,
                schoolId,
                courseRequest,
                file
            )
        return course.toCreatedBodyWithContentTypeJSON(request.uri(), course.id)
    }

    override fun updateSchoolCourse(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.coursePermission(schoolId, principal)
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val courseRequest = request.bodyOrParam<CourseRequest>()
        val course =
            courseService.updateSchoolCourse(
                principal,
                schoolId,
                courseId,
                courseRequest
            )
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun uploadSchoolCoursePhoto(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.coursePermission(schoolId, principal)
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val file = request.getMultiPartPhoto()
        val course =
            courseService.uploadSchoolCoursePhoto(
                principal,
                schoolId,
                courseId,
                file
            )
        return course.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSchoolCourse(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.coursePermission(schoolId, principal)
        val courseId = request.pathVariableOrThrow<UUID>("courseId")
        val course =
            courseService.deleteSchoolCourse(principal, schoolId, courseId)
        return course.toOkBodyWithContentTypeJSON()
    }
}
