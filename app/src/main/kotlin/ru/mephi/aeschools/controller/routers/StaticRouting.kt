package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.StaticHandler
import ru.mephi.aeschools.model.annotations.swagger.StaticRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class StaticRouting(
    private val handler: StaticHandler,
) {
    @Bean
    @StaticRoutingDoc
    fun staticRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public/uploads".nest {
                "/photo".nest {
                    GET("/{dir}/{fileName}", handler::servePublicPhoto)
                }
                "/file".nest {
                    GET("/{dir}/{fileName}", handler::servePublicFile)
                }
            }

            "/admin/uploads".nest {
                "/photo".nest {
                    POST(handler::createContentPhoto)
                }
                "/file".nest {
                    GET("/{dir}/{fileName}", handler::servePrivateFile)
                }
            }

        }
    }
}
