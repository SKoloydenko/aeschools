package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface UsefulLinkHandler {
    fun listUsefulLinks(request: ServerRequest): ServerResponse
    fun listUsefulLinksForAdmin(request: ServerRequest): ServerResponse
    fun findUsefulLinkByIdForAdmin(request: ServerRequest): ServerResponse
    fun createUsefulLink(request: ServerRequest): ServerResponse
    fun updateUsefulLink(request: ServerRequest): ServerResponse
    fun updateUsefulLinkPhoto(request: ServerRequest): ServerResponse
    fun deleteUsefulLink(request: ServerRequest): ServerResponse
}
