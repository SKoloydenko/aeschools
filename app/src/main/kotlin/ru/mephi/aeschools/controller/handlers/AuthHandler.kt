package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface AuthHandler {
    fun register(request: ServerRequest): ServerResponse
    fun login(request: ServerRequest): ServerResponse
    fun refresh(request: ServerRequest): ServerResponse
    fun logout(request: ServerRequest): ServerResponse
    fun sendEmailToResetPassword(request: ServerRequest): ServerResponse
    fun resetPassword(request: ServerRequest): ServerResponse
    fun resetPasswordTokenIsExist(request: ServerRequest): ServerResponse
    fun setNewPassword(request: ServerRequest): ServerResponse
    fun verifyEmail(request: ServerRequest): ServerResponse
}
