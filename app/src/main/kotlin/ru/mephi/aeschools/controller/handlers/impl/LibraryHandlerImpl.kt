package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.LibraryHandler
import ru.mephi.aeschools.model.dto.library.request.LibraryRecordRequest
import ru.mephi.aeschools.service.LibraryService
import ru.mephi.aeschools.util.*
import java.util.*

@Component
class LibraryHandlerImpl(
    private val service: LibraryService,
) : LibraryHandler {

    override fun create(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val record = request.body<LibraryRecordRequest>()
        val file = request.getMultiPartFileOrNull()
        val response = service.create(userId, record, file)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun fullList(request: ServerRequest): ServerResponse {
        val page = request.getExtendedPageRequest()
        val query = request.paramOrElse("query") { "" }
        val response = service.fullList(page, query)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun shortList(request: ServerRequest): ServerResponse {
        val page = request.getExtendedPageRequest()
        val response = service.shortList(page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getById(request: ServerRequest): ServerResponse {
        val recordId = request.pathVariableOrThrow<UUID>("id")
        val response = service.getById(recordId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun update(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val recordId = request.pathVariableOrThrow<UUID>("id")
        val record = request.body<LibraryRecordRequest>()
        val response = service.update(userId, recordId, record)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun delete(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val recordId = request.pathVariableOrThrow<UUID>("id")
        val response = service.delete(userId, recordId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun uploadFile(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val recordId = request.pathVariableOrThrow<UUID>("id")
        val part = request.getMultiPartPhoto("file")
        val response = service.uploadFile(userId, recordId, part)
        return response.toOkBodyWithContentTypeJSON()
    }
}
