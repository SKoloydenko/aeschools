package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.RegistrationLimiterHandler
import ru.mephi.aeschools.model.dto.registration_limiter.RegistrationLimiterSettingsRequest
import ru.mephi.aeschools.service.RegistrationLimiterService
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON

@Service
class RegistrationLimiterHandlerImpl(
    val service: RegistrationLimiterService,
) : RegistrationLimiterHandler {

    override fun setSettings(request: ServerRequest): ServerResponse {
        val settings =
            request.body(RegistrationLimiterSettingsRequest::class.java)
        val response = service.setSettings(settings)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getSettings(request: ServerRequest): ServerResponse {
        val response = service.getSettings()
        return response.toOkBodyWithContentTypeJSON()
    }
}
