package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.StatisticsHandler
import ru.mephi.aeschools.service.StatisticsService
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON

@Service
class StatisticsHandlerImpl(
    private val service: StatisticsService,
) : StatisticsHandler {
    override fun getStatistics(request: ServerRequest): ServerResponse {
        return service.getStatistics().toOkBodyWithContentTypeJSON()
    }
}
