package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface ReportHandler {
    fun createSection(request: ServerRequest): ServerResponse
    fun getSection(request: ServerRequest): ServerResponse
    fun listSections(request: ServerRequest): ServerResponse
    fun updateSection(request: ServerRequest): ServerResponse
    fun removeSection(request: ServerRequest): ServerResponse

    fun listFields(request: ServerRequest): ServerResponse

    fun createField(request: ServerRequest): ServerResponse
    fun getField(request: ServerRequest): ServerResponse
    fun updateField(request: ServerRequest): ServerResponse
    fun removeField(request: ServerRequest): ServerResponse

    fun removeAnswer(request: ServerRequest): ServerResponse

    fun getFieldsWithAnswersOfSelfSchool(request: ServerRequest): ServerResponse
    fun getFieldsWithAnswersOfSchool(request: ServerRequest): ServerResponse
    fun getAnswerToField(request: ServerRequest): ServerResponse
    fun getAllAnswersToField(request: ServerRequest): ServerResponse

    fun updateAnswersToSection(request: ServerRequest): ServerResponse
    fun updateAnswerToField(request: ServerRequest): ServerResponse
    fun updateFileToField(request: ServerRequest): ServerResponse

    fun getAnswerOfSchoolToField(request: ServerRequest): ServerResponse
}