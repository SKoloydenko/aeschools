package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface BroadcastHandler {
    fun listBroadcasts(request: ServerRequest): ServerResponse
    fun findBroadcastById(request: ServerRequest): ServerResponse
    fun sendBroadcastEmail(request: ServerRequest): ServerResponse
}
