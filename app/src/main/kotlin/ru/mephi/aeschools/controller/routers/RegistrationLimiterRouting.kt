package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.RegistrationLimiterHandler
import ru.mephi.aeschools.model.annotations.swagger.RegistrationLimiterRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class RegistrationLimiterRouting(
    val handler: RegistrationLimiterHandler,
) {

    @Bean
    @RegistrationLimiterRoutingDoc
    fun registrationLimiterRouter() = modernRouter {
        "$API_VERSION_1/admin".nest {
            "/settings/registration-limiter".nest {
                PUT(handler::setSettings)
                GET(handler::getSettings)
            }
        }
    }
}
