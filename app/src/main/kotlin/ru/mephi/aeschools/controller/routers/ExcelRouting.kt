package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.ExcelHandler
import ru.mephi.aeschools.model.annotations.swagger.ExcelRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class ExcelRouting(
    val handler: ExcelHandler,
) {

    @Bean
    @ExcelRoutingDoc
    fun excelRouter() = modernRouter {
        API_VERSION_1.nest {
            "/admin/util/export".nest {
                "/school".nest {
                    GET("/{schoolId}/event", handler::exportSchoolEvents)
                    GET("/{schoolId}/internship", handler::exportSchoolInternships)
                }
                "/event".nest {
                    GET("/{eventId}/user", handler::exportEventUsers)
                    GET(handler::exportEvents)
                }
                "/internship".nest {
                    GET("/{internshipId}/user", handler::exportInternshipApplicants)
                    GET(handler::exportInternships)
                }
                "/user".nest {
                    GET("/{userId}/event", handler::exportUserEvents)
                    GET("/{userId}/internship", handler::exportUserInternships)
                    GET(handler::exportUsers)
                }
                GET("/report/{id}", handler::exportReport)
            }
        }
    }
}
