package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import ru.mephi.aeschools.controller.handlers.ReportHandler
import ru.mephi.aeschools.model.annotations.swagger.ReportRoutingDoc
import ru.mephi.aeschools.model.exceptions.AppException
import ru.mephi.aeschools.model.exceptions.auth.Unauthorized
import ru.mephi.aeschools.model.exceptions.school.UserNotModeratorException
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class ReportRouting(
    private val handler: ReportHandler,
) {

    @Bean
    @ReportRoutingDoc
    fun reportRouter() = modernRouter {
        API_VERSION_1.nest {
            "/admin/report".nest {
                "/answers/{sectionId}".nest {
                    "/school".nest {
                        GET("/{schoolId}", handler::getFieldsWithAnswersOfSchool)
                    }
                    "/field".nest {
                        "/{fieldId}".nest {
                            "/school".nest {
                                  GET("/{schoolId}", handler::getAnswerOfSchoolToField)
                            }
                            GET("/all", handler::getAllAnswersToField)
                            "/file".nest {
                                POST(handler::updateFileToField)
                            }
                            PUT(handler::updateAnswerToField)
                            DELETE(handler::removeAnswer)
                            GET(handler::getAnswerToField)
                        }
                    }
                    PUT(handler::updateAnswersToSection)
                }

                "/{sectionId}".nest {
                    "/field".nest {
                        "/{fieldId}".nest {
                            GET(handler::getField)
                            PUT(handler::updateField)
                            DELETE(handler::removeField)
                        }
                        POST(handler::createField)
                        GET(handler::listFields)
                    }
                    "/school".nest {
                        GET("/{schoolId}", handler::getFieldsWithAnswersOfSchool)
                        GET(handler::getFieldsWithAnswersOfSelfSchool)
                    }
                    GET(handler::getSection)
                    PUT(handler::updateSection)
                    DELETE(handler::removeSection)
                }
                GET(handler::listSections)
                POST(handler::createSection)
            }
        }
        onError<UserNotModeratorException> { e, _ ->
            e as UserNotModeratorException
            AppException(
                status = HttpStatus.FORBIDDEN,
                cause = e,
                message = e.message,
                errorDescription = e.errorDescription
            ).asResponse()
        }
    }
}
