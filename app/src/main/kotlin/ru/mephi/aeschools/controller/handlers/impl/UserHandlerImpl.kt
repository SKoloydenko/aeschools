package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.UserHandler
import ru.mephi.aeschools.model.dto.user.request.BanRequest
import ru.mephi.aeschools.model.dto.user.request.UserRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.UserService
import ru.mephi.aeschools.util.constants.DEFAULT_USER_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class UserHandlerImpl(
    private val userService: UserService,
    private val permissionService: PermissionService
) : UserHandler {
    override fun listForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val page = request.getExtendedPageRequest(field = DEFAULT_USER_PAGE_FIELD_PARAM)
        val query = request.paramOrElse("query") { "" }
        val moderator = request.paramOrElse<Boolean?>("moderator") { null }
        val orderStudents = request.paramOrElse("orderStudents") { false }
        val users = userService.list(query, moderator, orderStudents, page)
        return users.toOkBodyWithContentTypeJSON()
    }

    override fun findByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.moderatorPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val user = userService.findById(userId)
        return user.toOkBodyWithContentTypeJSON()
    }

    override fun previewSelf(request: ServerRequest): ServerResponse {
        val user = userService.previewById(request.getPrincipal())
        return user.toOkBodyWithContentTypeJSON()
    }

    override fun getSelf(request: ServerRequest): ServerResponse {
        val user = userService.findById(request.getPrincipal())
        return user.toOkBodyWithContentTypeJSON()
    }

    override fun getSchool(request: ServerRequest): ServerResponse {
        val user = userService.findSchool(request.getPrincipal())
        return user.toOkBodyWithContentTypeJSON()
    }

    override fun update(request: ServerRequest): ServerResponse {
        val updateRequest = request.body<UserRequest>()
        val user = userService.update(request.getPrincipal(), updateRequest)
        return user.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSelf(request: ServerRequest): ServerResponse {
        val response = userService.delete(request.getPrincipal())
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun setBanStatus(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val banRequest = request.body<BanRequest>()
        val response = userService.setBanStatus(userId, banRequest)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun delete(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val principal = request.getPrincipal()
        val response = userService.delete(userId, principal)
        return response.toOkBodyWithContentTypeJSON()
    }
}
