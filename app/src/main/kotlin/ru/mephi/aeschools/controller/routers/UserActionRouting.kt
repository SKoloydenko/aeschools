package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.UserActionHandler
import ru.mephi.aeschools.model.annotations.swagger.UserActionDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class UserActionRouting(
    private val handler: UserActionHandler,
) {

    @Bean
    @UserActionDoc
    fun userActionRouter() = modernRouter {
        API_VERSION_1.nest {
            "/admin/actions".nest {
                GET("/user/{userId}", handler::getListByUser)
                GET("/target/{userId}", handler::getListByTargetId)
            }
            "/actions".nest {
                GET(handler::getSelfList)
            }
        }
    }
}
