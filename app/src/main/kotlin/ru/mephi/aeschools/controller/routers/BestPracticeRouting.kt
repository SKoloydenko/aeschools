package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.BestPracticeHandler
import ru.mephi.aeschools.model.annotations.swagger.BestPracticeRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class BestPracticeRouting(
    private val handler: BestPracticeHandler,
) {

    @Bean
    @BestPracticeRoutingDoc
    fun bestPracticeRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public".nest {
                // "/school/{schoolId}/best-practice".nest {
                //     GET("/{bestPracticeId}", handler::findSchoolBestPracticeById)
                //     GET(handler::listSchoolBestPractices)
                // }
                "/best-practice".nest {
                    GET("/{bestPracticeId}", handler::findBestPracticeById)
                    GET(handler::listBestPractices)
                }
            }

            "/admin".nest {
                // "/school/{schoolId}/best-practice".nest {
                //     "/{bestPracticeId}".nest {
                //         POST("/photo", handler::uploadSchoolBestPracticePhoto)
                //         GET(handler::findSchoolBestPracticeByIdForAdmin)
                //         PUT(handler::updateSchoolBestPractice)
                //         DELETE(handler::deleteSchoolBestPractice)
                //     }
                //     GET(handler::listSchoolBestPracticesForAdmin)
                //     POST(handler::createSchoolBestPractice)
                // }
                "/best-practice".nest {
                    "/{bestPracticeId}".nest {
                        POST("/photo", handler::uploadBestPracticePhoto)
                        GET(handler::findBestPracticeByIdForAdmin)
                        PUT(handler::updateBestPractice)
                        DELETE(handler::deleteBestPractice)
                    }
                    GET(handler::listBestPracticesForAdmin)
                    POST(handler::createBestPractice)
                }
            }
        }
    }
}
