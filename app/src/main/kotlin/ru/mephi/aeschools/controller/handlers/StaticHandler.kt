package ru.mephi.aeschools.controller.handlers

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

@Tag(name = "Static files API")
interface StaticHandler {
    @Operation(description = "Serve photo")
    fun servePublicPhoto(request: ServerRequest): ServerResponse

    @Operation(description = "Serve file")
    fun servePublicFile(request: ServerRequest): ServerResponse
    fun servePrivateFile(request: ServerRequest): ServerResponse

    fun createContentPhoto(request: ServerRequest): ServerResponse
}
