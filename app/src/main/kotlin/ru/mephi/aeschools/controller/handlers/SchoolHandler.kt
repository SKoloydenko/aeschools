package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface SchoolHandler {
    fun listSchools(request: ServerRequest): ServerResponse
    fun listSchoolsForAdmin(request: ServerRequest): ServerResponse
    fun findSchoolById(request: ServerRequest): ServerResponse
    fun findSchoolByIdForAdmin(request: ServerRequest): ServerResponse
    fun createSchool(request: ServerRequest): ServerResponse
    fun updateSchool(request: ServerRequest): ServerResponse
    fun uploadSchoolPhoto(request: ServerRequest): ServerResponse
    fun deleteSchool(request: ServerRequest): ServerResponse
    fun listLeaders(request: ServerRequest): ServerResponse
    fun findLeaderById(request: ServerRequest): ServerResponse
    fun createLeader(request: ServerRequest): ServerResponse
    fun updateLeader(request: ServerRequest): ServerResponse
    fun uploadLeaderPhoto(request: ServerRequest): ServerResponse
    fun deleteLeader(request: ServerRequest): ServerResponse
    fun listIndustrialPartners(request: ServerRequest): ServerResponse
    fun findIndustrialPartnerById(request: ServerRequest): ServerResponse
    fun createIndustrialPartner(request: ServerRequest): ServerResponse
    fun updateIndustrialPartner(request: ServerRequest): ServerResponse
    fun uploadIndustrialPartnerPhoto(request: ServerRequest): ServerResponse
    fun deleteIndustrialPartner(request: ServerRequest): ServerResponse
    fun listUniversityPartners(request: ServerRequest): ServerResponse
    fun findUniversityPartnerById(request: ServerRequest): ServerResponse
    fun createUniversityPartner(request: ServerRequest): ServerResponse
    fun updateUniversityPartner(request: ServerRequest): ServerResponse
    fun uploadUniversityPartnerPhoto(request: ServerRequest): ServerResponse
    fun deleteUniversityPartner(request: ServerRequest): ServerResponse
    fun listModerators(request: ServerRequest): ServerResponse
    fun createModerator(request: ServerRequest): ServerResponse
    fun deleteModerator(request: ServerRequest): ServerResponse
}
