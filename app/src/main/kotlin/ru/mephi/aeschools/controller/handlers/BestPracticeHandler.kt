package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface BestPracticeHandler {
    fun listBestPractices(request: ServerRequest): ServerResponse
    fun listSchoolBestPractices(request: ServerRequest): ServerResponse
    fun listBestPracticesForAdmin(request: ServerRequest): ServerResponse
    fun listSchoolBestPracticesForAdmin(request: ServerRequest): ServerResponse
    fun findBestPracticeById(request: ServerRequest): ServerResponse
    fun findSchoolBestPracticeById(request: ServerRequest): ServerResponse
    fun findBestPracticeByIdForAdmin(request: ServerRequest): ServerResponse
    fun findSchoolBestPracticeByIdForAdmin(request: ServerRequest): ServerResponse
    fun createBestPractice(request: ServerRequest): ServerResponse
    fun updateBestPractice(request: ServerRequest): ServerResponse
    fun uploadBestPracticePhoto(request: ServerRequest): ServerResponse
    fun deleteBestPractice(request: ServerRequest): ServerResponse
    fun createSchoolBestPractice(request: ServerRequest): ServerResponse
    fun updateSchoolBestPractice(request: ServerRequest): ServerResponse
    fun uploadSchoolBestPracticePhoto(request: ServerRequest): ServerResponse
    fun deleteSchoolBestPractice(request: ServerRequest): ServerResponse
}
