package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.InternshipHandler
import ru.mephi.aeschools.controller.handlers.QuestionnaireHandler
import ru.mephi.aeschools.model.annotations.swagger.InternshipRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class InternshipRouting(
    private val handler: InternshipHandler,
    private val questionnaireHandler: QuestionnaireHandler,
) {

    @Bean
    @InternshipRoutingDoc
    fun internshipRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public".nest {
                "/school/{schoolId}/internship".nest {
                    GET("/internshipId", handler::findSchoolInternshipById)
                    GET(handler::listSchoolInternships)
                }
                "/internship".nest {
                    GET("/{internshipId}", handler::findInternshipById)
                    GET(handler::listInternships)
                }
            }

            "/admin".nest {
                "/school/{schoolId}/internship".nest {
                    "/{internshipId}".nest {
                        "/applicant".nest {
                            GET("/{userId}/questionnaire", questionnaireHandler::findSchoolQuestionnaireById)
                            GET(handler::listSchoolInternshipApplicants)
                        }
                        POST("/photo", handler::uploadSchoolInternshipPhoto)
                        GET(handler::findSchoolInternshipByIdForAdmin)
                        PUT(handler::updateSchoolInternship)
                        DELETE(handler::deleteSchoolInternship)
                    }
                    POST(handler::createSchoolInternship)
                    GET(handler::listSchoolInternshipsForAdmin)
                }
                "/internship".nest {
                    "/{internshipId}".nest {
                        "/applicant".nest {
                            GET("/{userId}/questionnaire", questionnaireHandler::findQuestionnaireById)
                            GET(handler::listInternshipApplicants)
                        }
                        POST("/photo", handler::uploadInternshipPhoto)
                        GET(handler::findInternshipByIdForAdmin)
                        PUT(handler::updateInternship)
                        DELETE(handler::deleteInternship)
                    }
                    POST(handler::createInternship)
                    GET(handler::listInternshipsForAdmin)
                }
            }
        }
    }
}
