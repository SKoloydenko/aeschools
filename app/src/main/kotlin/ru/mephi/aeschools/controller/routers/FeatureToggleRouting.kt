package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.FeatureToggleHandler
import ru.mephi.aeschools.model.annotations.swagger.FeatureToggleRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class FeatureToggleRouting(private val handler: FeatureToggleHandler) {

    @Bean
    @FeatureToggleRoutingDoc
    fun featureRouter() = modernRouter {
        "$API_VERSION_1/admin/feature".nest {
            GET("/{featureName}", handler::getFeatureByName)
            PUT(handler::changeFeatureState)
            GET(handler::listAllFeatures)
        }
    }
}
