package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.PreferenceHandler
import ru.mephi.aeschools.model.annotations.swagger.PreferencesRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class PreferenceRouting(
    val handler: PreferenceHandler,
) {
    @Bean
    @PreferencesRoutingDoc
    fun preferenceRouter() = modernRouter {
        API_VERSION_1.nest {
            "user/me/preferences".nest {
                GET(handler::getPreferences)
                PUT(handler::updatePreferences)
                GET("/turnOff/{token}", handler::turnOffEmailPreference)
            }
            "/admin/preferences".nest {
                GET("/{userId}", handler::getPreferencesOfUser)
            }
        }
    }
}
