package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.EventHandler
import ru.mephi.aeschools.model.dto.event.request.AbstractEventRequest
import ru.mephi.aeschools.model.dto.event.request.EventRequest
import ru.mephi.aeschools.service.EventService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.constants.DEFAULT_PUBLICATION_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class EventHandlerImpl(
    private val eventService: EventService,
    private val permissionService: PermissionService,
) : EventHandler {
    override fun listEvents(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val events = eventService.listEvents(query, tag, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolEvents(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val events = eventService.listSchoolEvents(schoolId, query, tag, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun listEventsForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val events = eventService.listEventsForAdmin(query, tag, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolEventsForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val events = eventService.listSchoolEventsForAdmin(schoolId, query, tag, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun listEventsForUser(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val events = eventService.listEventsForUser(request.getPrincipal(), query, tag, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun listUserEventsForAdmin(request: ServerRequest): ServerResponse {
        permissionService.moderatorPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val events = eventService.listEventsForUser(userId, query, tag, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun findEventById(request: ServerRequest): ServerResponse {
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val event = eventService.findEventById(eventId)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolEventById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val event = eventService.findSchoolEventById(schoolId, eventId)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun findEventByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val event = eventService.findEventByIdForAdmin(eventId)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolEventByIdForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val event = eventService.findSchoolEventByIdForAdmin(schoolId, eventId)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun createEvent(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val eventRequest = request.bodyOrParam<AbstractEventRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val event = eventService.createEvent(principal, eventRequest, file)
        return event.toCreatedBodyWithContentTypeJSON(request.uri(), event.id)
    }

    override fun updateEvent(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val eventRequest = request.body<AbstractEventRequest>()
        val event =
            eventService.updateEvent(principal, eventId, eventRequest)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun uploadEventPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val file = request.getMultiPartPhoto()
        val event = eventService.uploadEventPhoto(principal, eventId, file)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun deleteEvent(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val response = eventService.deleteEvent(principal, eventId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun createSchoolEvent(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.eventPermission(schoolId, principal)
        val eventRequest = request.body<EventRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val event = eventService.createSchoolEvent(
            principal,
            schoolId,
            eventRequest,
            file
        )
        return event.toCreatedBodyWithContentTypeJSON(request.uri(), event.id)
    }

    override fun updateSchoolEvent(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.eventPermission(schoolId, principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val eventRequest = request.body<EventRequest>()
        val event =
            eventService.updateSchoolEvent(principal, schoolId, eventId, eventRequest)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun uploadSchoolEventPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.eventPermission(schoolId, principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val file = request.getMultiPartPhoto()
        val event = eventService.uploadSchoolEventPhoto(principal, schoolId, eventId, file)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSchoolEvent(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.eventPermission(schoolId, principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val event = eventService.deleteSchoolEvent(principal, schoolId, eventId)
        return event.toOkBodyWithContentTypeJSON()
    }

    override fun listEventMembers(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val page = request.getExtendedPageRequest()
        val events = eventService.listEventMembers(eventId, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolEventMembers(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.eventPermission(schoolId, principal)
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val page = request.getExtendedPageRequest()
        val events = eventService.listSchoolEventMembers(schoolId, eventId, page)
        return events.toOkBodyWithContentTypeJSON()
    }

    override fun joinEvent(request: ServerRequest): ServerResponse {
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val response = eventService.joinEvent(request.getPrincipal(), eventId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun leaveEvent(request: ServerRequest): ServerResponse {
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val response = eventService.leaveEvent(request.getPrincipal(), eventId)
        return response.toOkBodyWithContentTypeJSON()
    }
}
