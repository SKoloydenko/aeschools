package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.notFound
import org.springframework.web.servlet.function.ServerResponse.ok
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.AuthHandler
import ru.mephi.aeschools.model.dto.auth.request.ResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SendEmailToResetPasswordRequest
import ru.mephi.aeschools.model.dto.auth.request.SetNewPasswordRequest
import ru.mephi.aeschools.model.dto.user.request.LoginRequest
import ru.mephi.aeschools.model.dto.user.request.RefreshRequest
import ru.mephi.aeschools.model.dto.user.request.RegistrationRequest
import ru.mephi.aeschools.model.exceptions.auth.CorruptedTokenException
import ru.mephi.aeschools.model.exceptions.auth.RegistrationDisabledException
import ru.mephi.aeschools.model.messages.SuccessRegistrationResponse
import ru.mephi.aeschools.service.AuthService
import ru.mephi.aeschools.service.FeatureToggleService
import ru.mephi.aeschools.service.ResetPasswordService
import ru.mephi.aeschools.util.constants.REFRESH_TOKEN_COOKIE
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import javax.servlet.http.Cookie

@Component
class AuthHandlerImpl(
    private val authService: AuthService,
    private val featureToggleService: FeatureToggleService,
    private val resetPasswordService: ResetPasswordService,

    @Value("\${spring.security.jwt.refresh.lifetime}")
    private val refreshTokenLifetime: Int,

    @Value("\${verifyEmail.tokenTtl}")
    private val verifyEmailTokenTtl: Long,
) : AuthHandler {

    private fun generateRefreshCookie(token: String = ""): Cookie {
        val cookie = Cookie(REFRESH_TOKEN_COOKIE, token)
        cookie.isHttpOnly = true
        cookie.maxAge = refreshTokenLifetime * 60 * 60
        cookie.path = "/"
        return cookie
    }

    override fun register(request: ServerRequest): ServerResponse {
        if (!featureToggleService.isFeatureEnabled("registration")) throw RegistrationDisabledException()
        val registrationRequest = request.body<RegistrationRequest>()
        val user = authService.register(registrationRequest)
        val verifyEmailEnable = featureToggleService.isFeatureEnabled("verifyEmail")
        val response = SuccessRegistrationResponse(user.id, verifyEmailEnable, verifyEmailTokenTtl)
        return if (verifyEmailEnable)
            response.toCreatedBodyWithContentTypeJSON(request.uri(), user.id)
        else
            response.toOkBodyWithContentTypeJSON()
    }

    override fun login(request: ServerRequest): ServerResponse {
        val loginRequest = request.body<LoginRequest>()
        val (response, refreshToken) = authService.login(loginRequest)
        return ok().cookie(generateRefreshCookie(refreshToken)).body(response)
    }

    override fun refresh(request: ServerRequest): ServerResponse {
        val refreshToken = request.cookies().getFirst(REFRESH_TOKEN_COOKIE)
            ?: throw CorruptedTokenException()
        val refreshRequest =
            request.body<RefreshRequest>().apply { this.refreshToken = refreshToken.value }
        val (response, newRefreshToken) = authService.refresh(refreshRequest)
        return ok().cookie(generateRefreshCookie(newRefreshToken)).body(response)
    }

    override fun logout(request: ServerRequest): ServerResponse {
        val refreshToken =
            request.cookies().getFirst(REFRESH_TOKEN_COOKIE)
                ?: throw CorruptedTokenException()
        authService.logout(refreshToken.value)
        return ok().cookie(generateRefreshCookie().apply { maxAge = 0 }).build()
    }

    override fun sendEmailToResetPassword(request: ServerRequest): ServerResponse {
        val resetRequest = request.body<SendEmailToResetPasswordRequest>()
        authService.sendEmailToResetPassword(resetRequest)
        return ok().build()
    }

    override fun resetPassword(request: ServerRequest): ServerResponse {
        val changeRequest = request.body<ResetPasswordRequest>()
        val (response, refreshToken) = authService.resetPassword(changeRequest)
        return ok().cookie(generateRefreshCookie(refreshToken)).body(response)
    }

    override fun resetPasswordTokenIsExist(request: ServerRequest): ServerResponse {
        val token = request.paramOrThrow<String>("token")
        return if (resetPasswordService.tokenIsExist(token))
            ok().build()
        else
            notFound().build()
    }

    override fun setNewPassword(request: ServerRequest): ServerResponse {
        val setPasswordRequest = request.body<SetNewPasswordRequest>()
        val userId = request.getPrincipal()
        authService.setNewPassword(setPasswordRequest, userId)
        return ok().cookie(generateRefreshCookie().apply { maxAge = 0 }).build()
    }

    override fun verifyEmail(request: ServerRequest): ServerResponse {
        val token = request.param("token").get()
        authService.verifyEmail(token)
        return ok().body("Почта успешно подтверждена.")
    }
}
