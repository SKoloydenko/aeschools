package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.FeatureToggleHandler
import ru.mephi.aeschools.model.dto.FeatureToggleUpdateRequest
import ru.mephi.aeschools.service.FeatureToggleService
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON

@Component
class FeatureToggleHandlerImpl(
    private val service: FeatureToggleService,
) : FeatureToggleHandler {

    override fun changeFeatureState(request: ServerRequest): ServerResponse {
        val feature = request.body(FeatureToggleUpdateRequest::class.java)
        return service.changeFeatureState(feature).toOkBodyWithContentTypeJSON()
    }

    override fun listAllFeatures(request: ServerRequest): ServerResponse {
        return service.listFeatures().toOkBodyWithContentTypeJSON()
    }

    override fun getFeatureByName(request: ServerRequest): ServerResponse {
        val featureName = request.pathVariable("featureName")
        return service.findByFeatureName(featureName)
            .toOkBodyWithContentTypeJSON()
    }
}
