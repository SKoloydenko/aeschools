package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface PublicationHandler {
    fun listPublications(request: ServerRequest): ServerResponse
    fun listSchoolPublications(request: ServerRequest): ServerResponse
    fun listPublicationsForAdmin(request: ServerRequest): ServerResponse
    fun listSchoolPublicationsForAdmin(request: ServerRequest): ServerResponse
    fun findPublicationById(request: ServerRequest): ServerResponse
    fun findSchoolPublicationById(request: ServerRequest): ServerResponse
    fun findPublicationByIdForAdmin(request: ServerRequest): ServerResponse
    fun findSchoolPublicationByIdForAdmin(request: ServerRequest): ServerResponse
    fun createPublication(request: ServerRequest): ServerResponse
    fun updatePublication(request: ServerRequest): ServerResponse
    fun uploadPublicationPhoto(request: ServerRequest): ServerResponse
    fun deletePublication(request: ServerRequest): ServerResponse
    fun createSchoolPublication(request: ServerRequest): ServerResponse
    fun updateSchoolPublication(request: ServerRequest): ServerResponse
    fun uploadSchoolPublicationPhoto(request: ServerRequest): ServerResponse
    fun deleteSchoolPublication(request: ServerRequest): ServerResponse
}
