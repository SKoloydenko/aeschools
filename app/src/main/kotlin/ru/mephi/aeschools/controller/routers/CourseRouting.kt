package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.CourseHandler
import ru.mephi.aeschools.model.annotations.swagger.CourseRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class CourseRouting(
    private val handler: CourseHandler,
) {

    @Bean
    @CourseRoutingDoc
    fun courseRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public".nest {
                "/school/{schoolId}/course".nest {
                    GET("/{courseId}", handler::findSchoolCourseById)
                    GET(handler::listSchoolCourses)
                }
                "/course".nest {
                    GET("/{courseId}", handler::findCourseById)
                    GET(handler::listCourses)
                }
            }

            "/admin".nest {
                "/school/{schoolId}/course".nest {
                    "/{courseId}".nest {
                        POST("/photo", handler::uploadSchoolCoursePhoto)
                        GET(handler::findSchoolCourseByIdForAdmin)
                        PUT(handler::updateSchoolCourse)
                        DELETE(handler::deleteSchoolCourse)
                    }
                    GET(handler::listSchoolCoursesForAdmin)
                    POST(handler::createSchoolCourse)
                }
                "/course".nest {
                    "/{courseId}".nest {
                        POST("/photo", handler::uploadCoursePhoto)
                        GET(handler::findCourseByIdForAdmin)
                        PUT(handler::updateCourse)
                        DELETE(handler::deleteCourse)
                    }
                    GET(handler::listCoursesForAdmin)
                    POST(handler::createCourse)
                }
            }
        }
    }
}
