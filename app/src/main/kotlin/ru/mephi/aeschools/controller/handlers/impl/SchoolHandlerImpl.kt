package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.ok
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.SchoolHandler
import ru.mephi.aeschools.model.dto.school.request.IndustrialPartnerRequest
import ru.mephi.aeschools.model.dto.school.request.LeaderRequest
import ru.mephi.aeschools.model.dto.school.request.SchoolRequest
import ru.mephi.aeschools.model.dto.school.request.UniversityPartnerRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.SchoolService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class SchoolHandlerImpl(
    private val schoolService: SchoolService,
    private val permissionService: PermissionService,
) : SchoolHandler {
    override fun listSchools(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val schools = schoolService.listSchools(query)
        return schools.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolsForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val page = request.getExtendedPageRequest()
        val schools = schoolService.listSchoolsForAdmin(query, page)
        return schools.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val school = schoolService.findSchoolById(schoolId)
        return school.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val school = schoolService.findSchoolById(schoolId)
        return school.toOkBodyWithContentTypeJSON()
    }

    override fun createSchool(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val schoolRequest = request.bodyOrParam<SchoolRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val school = schoolService.createSchool(principal, schoolRequest, file)
        return school.toCreatedBodyWithContentTypeJSON(request.uri(), school.id)
    }

    override fun updateSchool(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val schoolRequest = request.body<SchoolRequest>()
        val school = schoolService.updateSchool(principal, schoolId, schoolRequest)
        return school.toOkBodyWithContentTypeJSON()
    }

    override fun uploadSchoolPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val file = request.getMultiPartPhoto()
        val school = schoolService.uploadSchoolPhoto(principal, schoolId, file)
        return school.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSchool(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(request.getPrincipal())
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val school = schoolService.deleteSchool(principal, schoolId)
        return school.toOkBodyWithContentTypeJSON()
    }

    override fun listLeaders(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val page = request.getExtendedPageRequest()
        val leaders = schoolService.listLeaders(schoolId, page)
        return leaders.toOkBodyWithContentTypeJSON()
    }

    override fun findLeaderById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val leaderId = request.pathVariableOrThrow<UUID>("leaderId")
        val leader =
            schoolService.findLeaderById(schoolId, leaderId)
        return leader.toOkBodyWithContentTypeJSON()
    }

    override fun createLeader(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val leaderRequest = request.bodyOrParam<LeaderRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val leader =
            schoolService.createLeader(principal, schoolId, leaderRequest, file)
        return leader.toCreatedBodyWithContentTypeJSON(request.uri(), leader.id)
    }

    override fun updateLeader(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val leaderId = request.pathVariableOrThrow<UUID>("leaderId")
        val leaderRequest = request.body<LeaderRequest>()
        val leader =
            schoolService.updateLeader(principal, schoolId, leaderId, leaderRequest)
        return leader.toOkBodyWithContentTypeJSON()
    }

    override fun uploadLeaderPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val leaderId = request.pathVariableOrThrow<UUID>("leaderId")
        val file = request.getMultiPartPhoto()
        val school = schoolService.uploadLeaderPhoto(principal, schoolId, leaderId, file)
        return school.toOkBodyWithContentTypeJSON()
    }

    override fun deleteLeader(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val leaderId = request.pathVariableOrThrow<UUID>("leaderId")
        val leader = schoolService.deleteLeader(principal, schoolId, leaderId)
        return leader.toOkBodyWithContentTypeJSON()
    }

    override fun listIndustrialPartners(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val page = request.getExtendedPageRequest()
        val partners = schoolService.listIndustrialPartners(query, schoolId, page)
        return partners.toOkBodyWithContentTypeJSON()
    }

    override fun findIndustrialPartnerById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val partner =
            schoolService.findIndustrialPartnerById(schoolId, partnerId)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun createIndustrialPartner(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerRequest = request.bodyOrParam<IndustrialPartnerRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val partner =
            schoolService.createIndustrialPartner(principal, schoolId, partnerRequest, file)
        return partner.toCreatedBodyWithContentTypeJSON(request.uri(), partner.id)
    }

    override fun updateIndustrialPartner(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val partnerRequest = request.body<IndustrialPartnerRequest>()
        val partner =
            schoolService.updateIndustrialPartner(principal, schoolId, partnerId, partnerRequest)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun uploadIndustrialPartnerPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val file = request.getMultiPartPhoto()
        val partner =
            schoolService.uploadIndustrialPartnerPhoto(principal, schoolId, partnerId, file)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun deleteIndustrialPartner(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val partner = schoolService.deleteIndustrialPartner(principal, schoolId, partnerId)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun listUniversityPartners(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val page = request.getExtendedPageRequest()
        val partners = schoolService.listUniversityPartners(query, schoolId, page)
        return partners.toOkBodyWithContentTypeJSON()
    }

    override fun findUniversityPartnerById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val partner =
            schoolService.findUniversityPartnerById(schoolId, partnerId)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun createUniversityPartner(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerRequest = request.bodyOrParam<UniversityPartnerRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val partner =
            schoolService.createUniversityPartner(principal, schoolId, partnerRequest, file)
        return partner.toCreatedBodyWithContentTypeJSON(request.uri(), partner.id)
    }

    override fun updateUniversityPartner(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val partnerRequest = request.body<UniversityPartnerRequest>()
        val partner =
            schoolService.updateUniversityPartner(principal, schoolId, partnerId, partnerRequest)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun uploadUniversityPartnerPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val file = request.getMultiPartPhoto()
        val partner =
            schoolService.uploadUniversityPartnerPhoto(principal, schoolId, partnerId, file)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun deleteUniversityPartner(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(principal)
        val partnerId = request.pathVariableOrThrow<UUID>("partnerId")
        val partner = schoolService.deleteUniversityPartner(principal, schoolId, partnerId)
        return partner.toOkBodyWithContentTypeJSON()
    }

    override fun listModerators(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val page = request.getExtendedPageRequest()
        val query = request.paramOrElse("query") { "" }
        val response = schoolService.listModerators(schoolId, query, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun createModerator(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = schoolService.createModerator(principal, schoolId, userId)
        return response.toCreatedBodyWithContentTypeJSON(request.uri(), response.id)
    }



    override fun deleteModerator(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = schoolService.deleteModerator(principal, schoolId, userId)
        return ok().body(response)
    }
}
