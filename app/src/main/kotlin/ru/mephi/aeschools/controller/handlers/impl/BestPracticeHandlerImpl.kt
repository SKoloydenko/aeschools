package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.BestPracticeHandler
import ru.mephi.aeschools.model.dto.best_practice.request.BestPracticeRequest
import ru.mephi.aeschools.service.BestPracticeService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.constants.DEFAULT_PUBLICATION_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class BestPracticeHandlerImpl(
    private val permissionService: PermissionService,
    private val bestPracticeService: BestPracticeService,
) : BestPracticeHandler {

    override fun listBestPractices(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val bestPractices = bestPracticeService.listBestPractices(query, tag, page)
        return bestPractices.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolBestPractices(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val bestPractices = bestPracticeService.listSchoolBestPractices(schoolId, query, tag, page)
        return bestPractices.toOkBodyWithContentTypeJSON()
    }

    override fun listBestPracticesForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val bestPractices = bestPracticeService.listBestPracticesForAdmin(query, tag, page)
        return bestPractices.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolBestPracticesForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.bestPracticePermission(schoolId, request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val bestPractices =
            bestPracticeService.listSchoolBestPracticesForAdmin(schoolId, query, tag, page)
        return bestPractices.toOkBodyWithContentTypeJSON()
    }

    override fun findBestPracticeById(request: ServerRequest): ServerResponse {
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPractice = bestPracticeService.findBestPracticeById(bestPracticeId)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolBestPracticeById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPractice = bestPracticeService.findSchoolBestPracticeById(schoolId, bestPracticeId)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun findBestPracticeByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPractice = bestPracticeService.findBestPracticeByIdForAdmin(bestPracticeId)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolBestPracticeByIdForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPractice =
            bestPracticeService.findSchoolBestPracticeByIdForAdmin(schoolId, bestPracticeId)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun createBestPractice(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val bestPracticeRequest = request.bodyOrParam<BestPracticeRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val bestPractice =
            bestPracticeService.createBestPractice(principal, bestPracticeRequest, file)
        return bestPractice.toCreatedBodyWithContentTypeJSON(request.uri(), bestPractice.id)
    }

    override fun updateBestPractice(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPracticeRequest = request.bodyOrParam<BestPracticeRequest>()
        val bestPractice =
            bestPracticeService.updateBestPractice(principal, bestPracticeId, bestPracticeRequest)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun uploadBestPracticePhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val file = request.getMultiPartPhoto()
        val bestPractice =
            bestPracticeService.uploadBestPracticePhoto(principal, bestPracticeId, file)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun deleteBestPractice(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPractice =
            bestPracticeService.deleteBestPractice(principal, bestPracticeId)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun createSchoolBestPractice(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.bestPracticePermission(schoolId, principal)
        val bestPracticeRequest = request.bodyOrParam<BestPracticeRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val bestPractice =
            bestPracticeService.createSchoolBestPractice(
                principal,
                schoolId,
                bestPracticeRequest,
                file
            )
        return bestPractice.toCreatedBodyWithContentTypeJSON(request.uri(), bestPractice.id)
    }

    override fun updateSchoolBestPractice(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.bestPracticePermission(schoolId, principal)
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPracticeRequest = request.bodyOrParam<BestPracticeRequest>()
        val bestPractice =
            bestPracticeService.updateSchoolBestPractice(
                principal,
                schoolId,
                bestPracticeId,
                bestPracticeRequest
            )
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun uploadSchoolBestPracticePhoto(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.bestPracticePermission(schoolId, principal)
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val file = request.getMultiPartPhoto()
        val bestPractice =
            bestPracticeService.uploadSchoolBestPracticePhoto(
                principal,
                schoolId,
                bestPracticeId,
                file
            )
        return bestPractice.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSchoolBestPractice(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val principal = request.getPrincipal()
        permissionService.bestPracticePermission(schoolId, principal)
        val bestPracticeId = request.pathVariableOrThrow<UUID>("bestPracticeId")
        val bestPractice =
            bestPracticeService.deleteSchoolBestPractice(principal, schoolId, bestPracticeId)
        return bestPractice.toOkBodyWithContentTypeJSON()
    }
}
