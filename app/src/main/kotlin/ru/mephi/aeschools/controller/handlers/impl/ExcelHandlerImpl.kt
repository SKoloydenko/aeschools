package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.ok
import ru.mephi.aeschools.controller.handlers.ExcelHandler
import ru.mephi.aeschools.model.dto.ExcelResponse
import ru.mephi.aeschools.service.ExcelService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.pathVariableOrThrow
import java.nio.charset.Charset
import java.util.*


@Service
class ExcelHandlerImpl(
    private val service: ExcelService,
    private val permissionService: PermissionService
) : ExcelHandler {

    private fun export(file: ExcelResponse): ServerResponse {
        return ok()
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .contentLength(file.file.contentLength())
            .header(
                HttpHeaders.CONTENT_DISPOSITION,
                ContentDisposition.attachment()
                    .filename("${file.name}.xlsx", Charset.forName("utf-8"))
                    .build().toString()
            )
            .body(file.file)
    }

    override fun exportUsers(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val response = service.exportUsers()
        return export(response)
    }

    override fun exportReport(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val sectionId = request.pathVariableOrThrow<UUID>("id")
        val response = service.exportReport(sectionId)
        return export(response)
    }

    override fun exportEvents(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val response = service.exportEvents()
        return export(response)
    }

    override fun exportSchoolEvents(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val response = service.exportSchoolEvents(schoolId)
        return export(response)
    }

    override fun exportEventUsers(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val eventId = request.pathVariableOrThrow<UUID>("eventId")
        val response = service.exportEventUsers(eventId)
        return export(response)
    }

    override fun exportUserEvents(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = service.exportUserEvents(userId)
        return export(response)
    }

    override fun exportInternships(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val response = service.exportInternships()
        return export(response)
    }

    override fun exportSchoolInternships(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val response = service.exportSchoolInternships(schoolId)
        return export(response)
    }

    override fun exportInternshipApplicants(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val response = service.exportInternshipApplicants(internshipId)
        return export(response)
    }

    override fun exportUserInternships(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val response = service.exportUserInternships(userId)
        return export(response)
    }
}
