package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.BroadcastHandler
import ru.mephi.aeschools.model.dto.broadcast.request.BroadcastRequest
import ru.mephi.aeschools.service.BroadcastService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class BroadcastHandlerImpl(
    private val permissionService: PermissionService,
    private val broadcastService: BroadcastService,
) : BroadcastHandler {

    override fun listBroadcasts(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val page = request.getExtendedPageRequest()
        val response = broadcastService.listBroadcasts(page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun findBroadcastById(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val broadcastId = request.pathVariableOrThrow<UUID>("broadcastId")
        val response = broadcastService.findBroadcastById(broadcastId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun sendBroadcastEmail(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val mailRequest = request.body<BroadcastRequest>()
        val response = broadcastService.sendBroadcastMail(principal, mailRequest)
        return response.toCreatedBodyWithContentTypeJSON(request.uri(), response.id)
    }
}
