package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import ru.mephi.aeschools.controller.handlers.UserActionHandler
import ru.mephi.aeschools.service.UserActionService
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class UserActionHandlerImpl(
    private val service: UserActionService,
) : UserActionHandler {

    override fun getListByUser(request: ServerRequest): ServerResponse {
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val page = request.getExtendedPageRequest()
        val response = service.listByUser(userId, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getSelfList(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val page = request.getExtendedPageRequest()
        val response = service.listByUser(userId, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getListByTargetId(request: ServerRequest): ServerResponse {
        val targetId = request.pathVariableOrThrow<UUID>("targetId")
        val page = request.getExtendedPageRequest()
        val response = service.listByTargetId(targetId, page)
        return response.toOkBodyWithContentTypeJSON()
    }

}
