package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.PublicationHandler
import ru.mephi.aeschools.model.annotations.swagger.PublicationRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class PublicationRouting(
    private val handler: PublicationHandler,
) {

    @Bean
    @PublicationRoutingDoc
    fun publicationRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public".nest {
                "/school/{schoolId}/publication".nest {
                    GET("/{publicationId}", handler::findSchoolPublicationById)
                    GET(handler::listSchoolPublications)
                }
                "/publication".nest {
                    GET("/{publicationId}", handler::findPublicationById)
                    GET(handler::listPublications)
                }
            }

            "/admin".nest {
                "/school/{schoolId}/publication".nest {
                    "/{publicationId}".nest {
                        POST("/photo", handler::uploadSchoolPublicationPhoto)
                        GET(handler::findSchoolPublicationByIdForAdmin)
                        PUT(handler::updateSchoolPublication)
                        DELETE(handler::deleteSchoolPublication)
                    }
                    POST(handler::createSchoolPublication)
                    GET(handler::listSchoolPublicationsForAdmin)
                }
                "/publication".nest {
                    "/{publicationId}".nest {
                        POST("/photo", handler::uploadPublicationPhoto)
                        GET(handler::findPublicationByIdForAdmin)
                        PUT(handler::updatePublication)
                        DELETE(handler::deletePublication)
                    }
                    POST(handler::createPublication)
                    GET(handler::listPublicationsForAdmin)
                }
            }
        }
    }
}

