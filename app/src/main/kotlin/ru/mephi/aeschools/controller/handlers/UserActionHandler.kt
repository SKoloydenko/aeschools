package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface UserActionHandler {
    fun getListByUser(request: ServerRequest): ServerResponse
    fun getSelfList(request: ServerRequest): ServerResponse
    fun getListByTargetId(request: ServerRequest): ServerResponse
}