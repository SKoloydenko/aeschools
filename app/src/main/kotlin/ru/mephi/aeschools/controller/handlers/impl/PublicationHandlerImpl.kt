package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.PublicationHandler
import ru.mephi.aeschools.model.dto.publication.request.PublicationRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.PublicationService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.constants.DEFAULT_PUBLICATION_PAGE_FIELD_PARAM
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class PublicationHandlerImpl(
    private val publicationService: PublicationService,
    private val permissionService: PermissionService,
) : PublicationHandler {

    override fun listPublications(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val publications = publicationService.listPublications(query, tag, page)
        return publications.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolPublications(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val publications =
            publicationService.listSchoolPublications(schoolId, query, tag, page)
        return publications.toOkBodyWithContentTypeJSON()
    }

    override fun listPublicationsForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val publications = publicationService.listPublicationsForAdmin(query, tag, page)
        return publications.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolPublicationsForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest(field = DEFAULT_PUBLICATION_PAGE_FIELD_PARAM)
        val publications =
            publicationService.listSchoolPublicationsForAdmin(schoolId, query, tag, page)
        return publications.toOkBodyWithContentTypeJSON()
    }

    override fun findPublicationById(request: ServerRequest): ServerResponse {
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val publication = publicationService.findPublicationById(publicationId)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolPublicationById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val publication = publicationService.findSchoolPublicationById(schoolId, publicationId)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun findPublicationByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val publication = publicationService.findPublicationByIdForAdmin(publicationId)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolPublicationByIdForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.moderatorPermission(request.getPrincipal())
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val publication =
            publicationService.findSchoolPublicationByIdForAdmin(schoolId, publicationId)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun createPublication(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val publicationRequest = request.bodyOrParam<PublicationRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val publication = publicationService.createPublication(principal, publicationRequest, file)
        return publication.toCreatedBodyWithContentTypeJSON(request.uri(), publication.id)
    }

    override fun updatePublication(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val publicationRequest = request.body<PublicationRequest>()
        val publication =
            publicationService.updatePublication(principal, publicationId, publicationRequest)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun uploadPublicationPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val file = request.getMultiPartPhoto()
        val publication = publicationService.uploadPublicationPhoto(principal, publicationId, file)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun deletePublication(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val response = publicationService.deletePublication(principal, publicationId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun createSchoolPublication(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.publicationPermission(schoolId, principal)
        val publicationRequest = request.body<PublicationRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val publication = publicationService.createSchoolPublication(
            principal,
            schoolId,
            publicationRequest,
            file
        )
        return publication.toCreatedBodyWithContentTypeJSON(request.uri(), publication.id)
    }

    override fun updateSchoolPublication(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.publicationPermission(schoolId, principal)
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val publicationRequest = request.body<PublicationRequest>()
        val publication =
            publicationService.updatePublication(principal, publicationId, publicationRequest)
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun uploadSchoolPublicationPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.publicationPermission(schoolId, principal)
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val file = request.getMultiPartPhoto()
        val publication = publicationService.uploadSchoolPublicationPhoto(
            principal,
            schoolId,
            publicationId,
            file
        )
        return publication.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSchoolPublication(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.publicationPermission(schoolId, principal)
        val publicationId = request.pathVariableOrThrow<UUID>("publicationId")
        val response =
            publicationService.deleteSchoolPublication(principal, schoolId, publicationId)
        return response.toOkBodyWithContentTypeJSON()
    }
}
