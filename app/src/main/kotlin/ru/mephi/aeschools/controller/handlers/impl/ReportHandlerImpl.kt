package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import org.springframework.web.servlet.function.principalOrNull
import ru.mephi.aeschools.controller.handlers.ReportHandler
import ru.mephi.aeschools.model.dto.section.ReportSectionFilterType
import ru.mephi.aeschools.model.dto.section.request.CreateFieldRequest
import ru.mephi.aeschools.model.dto.section.request.CreateSectionRequest
import ru.mephi.aeschools.model.dto.section.request.FieldAnswerRequest
import ru.mephi.aeschools.model.dto.section.request.SectionAnswerRequest
import ru.mephi.aeschools.model.dto.section.request.UpdateFieldRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.ReportService
import ru.mephi.aeschools.util.*
import java.util.*

@Component
class ReportHandlerImpl(
    private val service: ReportService,
    private val permissionService: PermissionService,
) : ReportHandler {

    override fun createSection(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val section = request.body<CreateSectionRequest>()
        val response = service.createSection(userId, section)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getSection(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminOrModeratorPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val response = service.getSection(sectionId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun listSections(request: ServerRequest): ServerResponse {
        val page = request.getExtendedPageRequest()
        val userId = request.getPrincipal()
        permissionService.adminOrModeratorPermission(userId)
        val filter = try {
            ReportSectionFilterType.valueOf(request.param("filter").orElse("ALL").uppercase())
        } catch (e: Exception) {
            e.printStackTrace()
            ReportSectionFilterType.ALL
        }
        val query = request.paramOrElse("query") { "" }
        val response = service.listSections(page, userId, query, filter)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updateSection(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val section = request.body<CreateSectionRequest>()
        val response = service.updateSection(userId, sectionId, section)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun removeSection(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val response = service.removeSection(userId, sectionId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun listFields(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminOrModeratorPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val page = request.getExtendedPageRequest()
        val response = service.listFields(sectionId, page, userId)
        return response.toOkBodyWithContentTypeJSON()
    }


    override fun createField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val field = request.body<CreateFieldRequest>()
        val response = service.createField(userId, sectionId, field)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminOrModeratorPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val response = service.getField(sectionId, fieldId, userId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updateField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val field = request.body<UpdateFieldRequest>()
        val response = service.updateField(userId, sectionId, fieldId, field)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun removeField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val response = service.removeFiled(userId, sectionId, fieldId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun removeAnswer(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val response = service.deleteAnswer(userId, sectionId, fieldId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getFieldsWithAnswersOfSelfSchool(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val page = request.getExtendedPageRequest()
        val response = service.fieldsPageByUser(userId, sectionId, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getFieldsWithAnswersOfSchool(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val page = request.getExtendedPageRequest()
        val response = service.fieldsPageOfSchool(schoolId, sectionId, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getAnswerToField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val response = service.getAnswerToField(userId, sectionId, fieldId)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getAllAnswersToField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val page = request.getExtendedPageRequest()
        val response = service.getAllAnswersToField(sectionId, fieldId, page)
        return response.toOkBodyWithContentTypeJSON()
    }


    override fun updateAnswersToSection(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val answer = request.body<SectionAnswerRequest>()
        val response = service.answerToSection(userId, sectionId, answer)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updateAnswerToField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val answer = request.body<FieldAnswerRequest>()
        val response = service.answerToField(userId, sectionId, fieldId, answer)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updateFileToField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val file = request.getMultiPartPhoto("file")
        val response = service.uploadFileToField(userId, sectionId, fieldId, file)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun getAnswerOfSchoolToField(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminPermission(userId)
        val sectionId = request.pathVariableOrThrow<UUID>("sectionId")
        val fieldId = request.pathVariableOrThrow<UUID>("fieldId")
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val response = service.getAnswerOfSchoolToField(sectionId, fieldId, schoolId)
        return response.toOkBodyWithContentTypeJSON()
    }
}
