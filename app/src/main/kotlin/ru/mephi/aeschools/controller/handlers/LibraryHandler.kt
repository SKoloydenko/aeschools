package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface LibraryHandler {
    fun create(request: ServerRequest): ServerResponse
    fun fullList(request: ServerRequest): ServerResponse
    fun shortList(request: ServerRequest): ServerResponse
    fun getById(request: ServerRequest): ServerResponse
    fun update(request: ServerRequest): ServerResponse
    fun delete(request: ServerRequest): ServerResponse
    fun uploadFile(request: ServerRequest): ServerResponse
}