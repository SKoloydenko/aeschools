package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface EventHandler {
    fun listEvents(request: ServerRequest): ServerResponse
    fun listSchoolEvents(request: ServerRequest): ServerResponse
    fun listEventsForAdmin(request: ServerRequest): ServerResponse
    fun listSchoolEventsForAdmin(request: ServerRequest): ServerResponse
    fun listEventsForUser(request: ServerRequest): ServerResponse
    fun listUserEventsForAdmin(request: ServerRequest): ServerResponse
    fun findEventById(request: ServerRequest): ServerResponse
    fun findSchoolEventById(request: ServerRequest): ServerResponse
    fun findEventByIdForAdmin(request: ServerRequest): ServerResponse
    fun findSchoolEventByIdForAdmin(request: ServerRequest): ServerResponse
    fun createEvent(request: ServerRequest): ServerResponse
    fun updateEvent(request: ServerRequest): ServerResponse
    fun uploadEventPhoto(request: ServerRequest): ServerResponse
    fun deleteEvent(request: ServerRequest): ServerResponse
    fun createSchoolEvent(request: ServerRequest): ServerResponse
    fun updateSchoolEvent(request: ServerRequest): ServerResponse
    fun uploadSchoolEventPhoto(request: ServerRequest): ServerResponse
    fun deleteSchoolEvent(request: ServerRequest): ServerResponse
    fun listEventMembers(request: ServerRequest): ServerResponse
    fun listSchoolEventMembers(request: ServerRequest): ServerResponse
    fun joinEvent(request: ServerRequest): ServerResponse
    fun leaveEvent(request: ServerRequest): ServerResponse
}
