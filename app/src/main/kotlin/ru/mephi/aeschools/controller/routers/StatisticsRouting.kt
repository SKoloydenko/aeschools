package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.StatisticsHandler
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class StatisticsRouting(private val handler: StatisticsHandler) {
    @Bean
    fun statisticsRouter() = modernRouter {
        GET("$API_VERSION_1/admin/statistics", handler::getStatistics)
    }
}
