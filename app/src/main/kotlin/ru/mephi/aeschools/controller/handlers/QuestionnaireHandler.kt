package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface QuestionnaireHandler {
    fun findQuestionnaireById(request: ServerRequest): ServerResponse
    fun findSchoolQuestionnaireById(request: ServerRequest): ServerResponse
    fun findLastQuestionnaire(request: ServerRequest): ServerResponse
    fun findUserQuestionnaire(request: ServerRequest): ServerResponse
    fun createQuestionnaire(request: ServerRequest): ServerResponse
    fun updateQuestionnaire(request: ServerRequest): ServerResponse
    fun uploadQuestionnaireFile(request: ServerRequest): ServerResponse
    fun deleteQuestionnaire(request: ServerRequest): ServerResponse
}
