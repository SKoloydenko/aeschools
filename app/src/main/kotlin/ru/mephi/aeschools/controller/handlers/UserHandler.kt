package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface UserHandler {
    fun listForAdmin(request: ServerRequest): ServerResponse
    fun findByIdForAdmin(request: ServerRequest): ServerResponse
    fun previewSelf(request: ServerRequest): ServerResponse
    fun getSelf(request: ServerRequest): ServerResponse
    fun getSchool(request: ServerRequest): ServerResponse
    fun update(request: ServerRequest): ServerResponse
    fun deleteSelf(request: ServerRequest): ServerResponse
    fun setBanStatus(request: ServerRequest): ServerResponse
    fun delete(request: ServerRequest): ServerResponse
}
