package ru.mephi.aeschools.controller.handlers

import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse

interface InternshipHandler {
    fun listInternships(request: ServerRequest): ServerResponse
    fun listSchoolInternships(request: ServerRequest): ServerResponse
    fun listInternshipsForAdmin(request: ServerRequest): ServerResponse
    fun listSchoolInternshipsForAdmin(request: ServerRequest): ServerResponse
    fun listInternshipsForUser(request: ServerRequest): ServerResponse
    fun listUserInternshipsForAdmin(request: ServerRequest): ServerResponse
    fun findInternshipById(request: ServerRequest): ServerResponse
    fun findSchoolInternshipById(request: ServerRequest): ServerResponse
    fun findInternshipByIdForAdmin(request: ServerRequest): ServerResponse
    fun findSchoolInternshipByIdForAdmin(request: ServerRequest): ServerResponse
    fun createInternship(request: ServerRequest): ServerResponse
    fun updateInternship(request: ServerRequest): ServerResponse
    fun uploadInternshipPhoto(request: ServerRequest): ServerResponse
    fun deleteInternship(request: ServerRequest): ServerResponse
    fun createSchoolInternship(request: ServerRequest): ServerResponse
    fun updateSchoolInternship(request: ServerRequest): ServerResponse
    fun uploadSchoolInternshipPhoto(request: ServerRequest): ServerResponse
    fun deleteSchoolInternship(request: ServerRequest): ServerResponse
    fun listInternshipApplicants(request: ServerRequest): ServerResponse
    fun listSchoolInternshipApplicants(request: ServerRequest): ServerResponse
}
