package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.InternshipHandler
import ru.mephi.aeschools.model.dto.internship.request.InternshipRequest
import ru.mephi.aeschools.service.InternshipService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.paramOrElse
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toCreatedBodyWithContentTypeJSON
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class InternshipHandlerImpl(
    private val internshipService: InternshipService,
    private val permissionService: PermissionService,
) : InternshipHandler {
    override fun listInternships(request: ServerRequest): ServerResponse {
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val internships = internshipService.listInternships(query, tag, page)
        return internships.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolInternships(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val internships = internshipService.listSchoolInternships(schoolId, query, tag, page)
        return internships.toOkBodyWithContentTypeJSON()
    }

    override fun listInternshipsForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val internships = internshipService.listInternships(query, tag, page)
        return internships.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolInternshipsForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, request.getPrincipal())
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val internships = internshipService.listSchoolInternships(schoolId, query, tag, page)
        return internships.toOkBodyWithContentTypeJSON()
    }

    override fun listInternshipsForUser(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.studentPermission(principal)
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val response = internshipService.listInternshipsForUser(principal, query, tag, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun listUserInternshipsForAdmin(request: ServerRequest): ServerResponse {
        permissionService.moderatorPermission(request.getPrincipal())
        val userId = request.pathVariableOrThrow<UUID>("userId")
        val query = request.paramOrElse("query") { "" }
        val tag = request.paramOrElse("tag") { "" }
        val page = request.getExtendedPageRequest()
        val response = internshipService.listInternshipsForUser(userId, query, tag, page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun findInternshipById(request: ServerRequest): ServerResponse {
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship = internshipService.findInternshipById(internshipId)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolInternshipById(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship = internshipService.findSchoolInternshipById(schoolId, internshipId)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun findInternshipByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship = internshipService.findInternshipByIdForAdmin(internshipId)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun findSchoolInternshipByIdForAdmin(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship = internshipService.findSchoolInternshipById(schoolId, internshipId)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun createInternship(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val internshipRequest = request.bodyOrParam<InternshipRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val internship =
            internshipService.createInternship(principal, internshipRequest, file)
        return internship.toCreatedBodyWithContentTypeJSON(request.uri(), internship.id)
    }

    override fun updateInternship(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internshipRequest = request.body<InternshipRequest>()
        val internship =
            internshipService.updateInternship(principal, internshipId, internshipRequest)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun uploadInternshipPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val file = request.getMultiPartPhoto()
        val internship = internshipService.uploadInternshipPhoto(principal, internshipId, file)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun deleteInternship(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.isAdmin(request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship = internshipService.deleteInternship(principal, internshipId)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun createSchoolInternship(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, principal)
        val internshipRequest = request.bodyOrParam<InternshipRequest>()
        val file = request.getMultiPartPhotoOrNull()
        val internship =
            internshipService.createSchoolInternship(principal, schoolId, internshipRequest, file)
        return internship.toCreatedBodyWithContentTypeJSON(request.uri(), internship.id)
    }

    override fun updateSchoolInternship(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internshipRequest = request.body<InternshipRequest>()
        val internship =
            internshipService.updateInternship(principal, internshipId, internshipRequest)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun uploadSchoolInternshipPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val file = request.getMultiPartPhoto()
        val internship = internshipService.uploadInternshipPhoto(principal, internshipId, file)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun deleteSchoolInternship(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, principal)
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val internship = internshipService.deleteInternship(principal, internshipId)
        return internship.toOkBodyWithContentTypeJSON()
    }

    override fun listInternshipApplicants(request: ServerRequest): ServerResponse {
        permissionService.isAdmin(request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val page = request.getExtendedPageRequest()
        val applicants = internshipService.listInternshipApplicants(internshipId, page)
        return applicants.toOkBodyWithContentTypeJSON()
    }

    override fun listSchoolInternshipApplicants(request: ServerRequest): ServerResponse {
        val schoolId = request.pathVariableOrThrow<UUID>("schoolId")
        permissionService.internshipPermission(schoolId, request.getPrincipal())
        val internshipId = request.pathVariableOrThrow<UUID>("internshipId")
        val page = request.getExtendedPageRequest()
        val applicants = internshipService.listInternshipApplicants(internshipId, page)
        return applicants.toOkBodyWithContentTypeJSON()
    }
}
