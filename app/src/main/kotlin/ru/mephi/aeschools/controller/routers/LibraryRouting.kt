package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.LibraryHandler
import ru.mephi.aeschools.model.annotations.swagger.LibraryRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class LibraryRouting(
    private val handler: LibraryHandler,
) {

    @Bean
    @LibraryRoutingDoc
    fun libraryRouter() = modernRouter {
        API_VERSION_1.nest {
            "/admin/library".nest {
                "/{id}".nest {
                    POST("/file", handler::uploadFile)
                    GET(handler::getById)
                    PUT(handler::update)
                    DELETE(handler::delete)
                }
                GET(handler::fullList)
                POST(handler::create)
            }

            "/public/library".nest {
                GET("/{id}", handler::getById)
                GET(handler::fullList)
            }
        }
    }
}
