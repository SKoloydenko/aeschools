package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import ru.mephi.aeschools.controller.handlers.UsefulLinkHandler
import ru.mephi.aeschools.model.dto.useful_link.request.UsefulLinkRequest
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.UsefulLinkService
import ru.mephi.aeschools.util.bodyOrParam
import ru.mephi.aeschools.util.getExtendedPageRequest
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getMultiPartPhotoOrNull
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.pathVariableOrThrow
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.util.*

@Component
class UsefulLinkHandlerImpl(
    private val usefulLinkService: UsefulLinkService,
    private val permissionService: PermissionService,
) : UsefulLinkHandler {

    override fun listUsefulLinks(request: ServerRequest): ServerResponse {
        val page = request.getExtendedPageRequest()
        val response = usefulLinkService.listUsefulLinks(page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun listUsefulLinksForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val page = request.getExtendedPageRequest()
        val response = usefulLinkService.listUsefulLinksForAdmin(page)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun findUsefulLinkByIdForAdmin(request: ServerRequest): ServerResponse {
        permissionService.adminPermission(request.getPrincipal())
        val id = request.pathVariableOrThrow<UUID>("linkId")
        val response = usefulLinkService.findUsefulLinkByIdForAdmin(id)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun createUsefulLink(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val file = request.getMultiPartPhotoOrNull()
        val usefulLinkRequest = request.bodyOrParam<UsefulLinkRequest>()
        val response = usefulLinkService.createUsefulLink(
            principal, usefulLinkRequest, file
        )
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updateUsefulLink(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val id = request.pathVariableOrThrow<UUID>("linkId")
        val usefulLinkRequest = request.body<UsefulLinkRequest>()
        val response = usefulLinkService.updateUsefulLink(principal, id, usefulLinkRequest)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun updateUsefulLinkPhoto(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val id = request.pathVariableOrThrow<UUID>("linkId")
        val file = request.getMultiPartPhoto()
        val response = usefulLinkService.uploadUsefulLinkPhoto(principal, id, file)
        return response.toOkBodyWithContentTypeJSON()
    }

    override fun deleteUsefulLink(request: ServerRequest): ServerResponse {
        val principal = request.getPrincipal()
        permissionService.adminPermission(principal)
        val id = request.pathVariableOrThrow<UUID>("linkId")
        val response = usefulLinkService.deleteUsefulLink(principal, id)
        return response.toOkBodyWithContentTypeJSON()
    }
}
