package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.BroadcastHandler
import ru.mephi.aeschools.model.annotations.swagger.BroadcastRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class BroadcastRouting(
    private val handler: BroadcastHandler,
) {

    @Bean
    @BroadcastRoutingDoc
    fun broadcastRouter() = modernRouter {
        API_VERSION_1.nest {
            "/admin/broadcast".nest {
                GET("/{broadcastId}", handler::findBroadcastById)
                GET(handler::listBroadcasts)
                POST(handler::sendBroadcastEmail)
            }
        }
    }
}
