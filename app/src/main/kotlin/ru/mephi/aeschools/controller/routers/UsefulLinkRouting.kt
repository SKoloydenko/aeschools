package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.UsefulLinkHandler
import ru.mephi.aeschools.model.annotations.swagger.UsefulLinkRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class UsefulLinkRouting(
    private val handler: UsefulLinkHandler,
) {

    @Bean
    @UsefulLinkRoutingDoc
    fun usefulLinkRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public/link".nest {
                GET(handler::listUsefulLinks)
            }

            "/admin/link".nest {
                "/{linkId}".nest {
                    POST("/photo", handler::updateUsefulLinkPhoto)
                    GET(handler::findUsefulLinkByIdForAdmin)
                    PUT(handler::updateUsefulLink)
                    DELETE(handler::deleteUsefulLink)
                }
                GET(handler::listUsefulLinksForAdmin)
                POST(handler::createUsefulLink)
            }
        }
    }
}
