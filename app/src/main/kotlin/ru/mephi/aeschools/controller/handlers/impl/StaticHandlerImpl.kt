package ru.mephi.aeschools.controller.handlers.impl

import org.springframework.context.annotation.Lazy
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.ok
import ru.mephi.aeschools.controller.handlers.StaticHandler
import ru.mephi.aeschools.model.enums.storage.FileStoreDir
import ru.mephi.aeschools.model.exceptions.common.InvalidFileException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.model.exceptions.library.FilePermissionDenied
import ru.mephi.aeschools.service.ContentPhotoService
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.service.ReportService
import ru.mephi.aeschools.service.StorageService
import ru.mephi.aeschools.service.impl.storage.StorageFileManager
import ru.mephi.aeschools.service.impl.storage.StorageImageManager
import ru.mephi.aeschools.util.getMultiPartPhoto
import ru.mephi.aeschools.util.getPrincipal
import ru.mephi.aeschools.util.toOkBodyWithContentTypeJSON
import java.io.File
import java.nio.charset.Charset
import java.nio.file.Files

@Component
class StaticHandlerImpl(
    private val imageManager: StorageImageManager,
    private val fileManager: StorageFileManager,
    private val storageService: StorageService,
    private val contentPhotoService: ContentPhotoService,
    private val permissionService: PermissionService,
    @Lazy
    private val reportService: ReportService,
) : StaticHandler {

    private fun getPhotoResponse(file: File, name: String): ServerResponse {
        return if (file.exists()) ok().contentType(
            when (file.extension) {
                "png" -> MediaType.IMAGE_PNG
                "jpg", "jpeg" -> MediaType.IMAGE_JPEG
                "gif" -> MediaType.IMAGE_GIF
                else -> throw InvalidFileException()
            }
        ).header(
            HttpHeaders.CONTENT_DISPOSITION,
            ContentDisposition.attachment()
                .filename(name, Charset.forName("utf-8"))
                .build().toString()
        ).body(file.readBytes())
        else ResourceNotFoundException(File::class.java, file.name).asResponse()
    }

    private fun getFileResponse(file: File, fileName: String): ServerResponse {
        return if (file.exists())
            ok().header(
                HttpHeaders.CONTENT_DISPOSITION,
                ContentDisposition.attachment()
                    .filename(fileName, Charset.forName("utf-8"))
                    .build().toString()
            ).header(
                HttpHeaders.CONTENT_TYPE,
                Files.probeContentType(file.toPath())
            )
                .body(file.readBytes())
        else ResourceNotFoundException(File::class.java, fileName).asResponse()
    }

    override fun servePublicPhoto(request: ServerRequest): ServerResponse {
        val dir = request.pathVariable("dir")
        val fileName = request.pathVariable("fileName")
        val path = "$dir/$fileName"
        val file = imageManager.retrieve(path)
        val name = storageService.getFileNameByIdWithExt(imageManager.getFullPhotoName(fileName))
        return getPhotoResponse(file, name)
    }

    override fun servePublicFile(request: ServerRequest): ServerResponse {
        val dir = request.pathVariable("dir")
        val fileName = request.pathVariable("fileName")
        val path = "$dir/$fileName"
        val fileDir = FileStoreDir.valueOf(dir.uppercase())
        if (!fileDir.isPublic) throw FilePermissionDenied(path)
        val file = fileManager.retrieve(path)
        val name = storageService.getFileNameByIdWithExt(fileName)
        return getFileResponse(file, name)
    }

    override fun servePrivateFile(request: ServerRequest): ServerResponse {
        val userId = request.getPrincipal()
        permissionService.adminOrModeratorPermission(userId)
        val dir = request.pathVariable("dir")
        val fileName = request.pathVariable("fileName")
        val path = "$dir/$fileName"
        val fileDir = FileStoreDir.valueOf(dir.uppercase())
        if (fileDir.isPublic || permissionService.isAdmin(userId)) return getFile(path, fileName)

        if (fileDir == FileStoreDir.REPORTS) {
            if (reportService.existFileAnswerWithModerator(path, userId)) {
                return getFile(path, fileName)
            }
        }

        return ResourceNotFoundException(File::class.java, fileName).asResponse()
    }

    private fun getFile(path: String, fileName: String): ServerResponse {
        val file = fileManager.retrieve(path)
        val name = storageService.getFileNameByIdWithExt(fileName)
        return getFileResponse(file, name)
    }

    override fun createContentPhoto(request: ServerRequest): ServerResponse {
        permissionService.moderatorPermission(request.getPrincipal())
        val file = request.getMultiPartPhoto()
        val response = contentPhotoService.createContentPhoto(file)
        return response.toOkBodyWithContentTypeJSON()
    }
}
