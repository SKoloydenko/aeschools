package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.QuestionnaireHandler
import ru.mephi.aeschools.model.annotations.swagger.QuestionnaireRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class QuestionnaireRouting(
    private val handler: QuestionnaireHandler,
) {

    @Bean
    @QuestionnaireRoutingDoc
    fun questionnaireRouter() = modernRouter {
        API_VERSION_1.nest {
            "/user".nest {
                "/internship".nest {
                    "/{internshipId}/questionnaire".nest {
                        POST("/file", handler::uploadQuestionnaireFile)
                        POST(handler::createQuestionnaire)
                        PUT(handler::updateQuestionnaire)
                        DELETE(handler::deleteQuestionnaire)
                    }
                }
            }
        }
    }
}
