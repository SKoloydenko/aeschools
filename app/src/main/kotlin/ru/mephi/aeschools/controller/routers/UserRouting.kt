package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.EventHandler
import ru.mephi.aeschools.controller.handlers.InternshipHandler
import ru.mephi.aeschools.controller.handlers.QuestionnaireHandler
import ru.mephi.aeschools.controller.handlers.UserHandler
import ru.mephi.aeschools.model.annotations.swagger.UserRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class UserRouting(
    private val handler: UserHandler,
    private val internshipHandler: InternshipHandler,
    private val questionnaireHandler: QuestionnaireHandler,
    private val eventHandler: EventHandler
) {

    @Bean
    @UserRoutingDoc
    fun userRouter() = modernRouter {
        API_VERSION_1.nest {
            "/user".nest {
                "/me".nest {
                    GET("/preview", handler::previewSelf)
                    GET("/school", handler::getSchool)
                    "/internship".nest {
                        GET("/questionnaire/last", questionnaireHandler::findLastQuestionnaire)
                        GET("/{internshipId}/questionnaire", questionnaireHandler::findUserQuestionnaire)
                    }
                    GET("/internship", internshipHandler::listInternshipsForUser)
                    GET("/event", eventHandler::listEventsForUser)
                    GET(handler::getSelf)
                    PUT(handler::update)
                    DELETE(handler::deleteSelf)
                }
            }

            "/admin/user".nest {
                "/{userId}".nest {
                    PUT("/ban", handler::setBanStatus)
                    GET("/internship", internshipHandler::listUserInternshipsForAdmin)
                    GET("/event", eventHandler::listUserEventsForAdmin)
                    GET(handler::findByIdForAdmin)
                    DELETE(handler::delete)
                }
                GET(handler::listForAdmin)
            }
        }
    }
}
