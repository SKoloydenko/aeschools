package ru.mephi.aeschools.controller.routers

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.mephi.aeschools.controller.handlers.AuthHandler
import ru.mephi.aeschools.model.annotations.swagger.AuthRoutingDoc
import ru.mephi.aeschools.util.constants.API_VERSION_1
import ru.mephi.aeschools.util.modernRouter

@Configuration
class AuthRouting(
    private val handler: AuthHandler,
) {
    @Bean
    @AuthRoutingDoc
    fun authRouter() = modernRouter {
        API_VERSION_1.nest {
            "/public/auth".nest {
                POST("/register", handler::register)
                POST("/login", handler::login)
                POST("/refresh", handler::refresh)
                POST("/logout", handler::logout)
                POST("/reset-password-by-email", handler::sendEmailToResetPassword)
                POST("/accept-reset-password", handler::resetPassword)
                GET("/reset-password-token-exist", handler::resetPasswordTokenIsExist)

                GET("/verify-email", handler::verifyEmail)
            }
            POST("/user/me/password/new", handler::setNewPassword)
        }
    }
}
