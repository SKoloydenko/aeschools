package ru.mephi.aeschools.interceptor

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import ru.mephi.aeschools.controller.routers.writeResponseError
import ru.mephi.aeschools.model.exceptions.common.PrincipalNotFoundException
import ru.mephi.aeschools.model.exceptions.common.ResourceNotFoundException
import ru.mephi.aeschools.security.RateLimiter
import ru.mephi.aeschools.service.PermissionService
import ru.mephi.aeschools.util.containsAnyOf
import ru.mephi.aeschools.util.getPrincipal
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class RateLimiterInterceptor : HandlerInterceptor {
    @Autowired
    private lateinit var rateLimiter: RateLimiter

    @Autowired
    private lateinit var permissionService: PermissionService

    private fun isPublic(request: HttpServletRequest): Boolean {
        // /error to handle PrincipalNotFoundException
        return request.requestURI.containsAnyOf("/public", "/error")
    }

    private fun isSwagger(request: HttpServletRequest): Boolean {
        // /error to handle PrincipalNotFoundException
        return request.requestURI.containsAnyOf(
            "/swagger", "/swagger-ui", "/v3/api-docs"
        )
    }

    private fun isFromSwagger(request: HttpServletRequest): Boolean {
        val referer = request.getHeader("Referer")
        return referer?.contains("swagger-ui") ?: false
    }

    override fun preHandle(
        request: HttpServletRequest, response: HttpServletResponse, handler: Any,
    ): Boolean {
        if (request.method == "GET") {
            return true
        }
        val probe = try {
            if (isPublic(request)) {
                rateLimiter.tryUnAuthorizedAccess(request.remoteAddr)
            } else if (isFromSwagger(request) || isSwagger(request)) {
                rateLimiter.trySwaggerAccess()
            } else {
                val principal = request.getPrincipal()
                val isAdmin = permissionService.isAdmin(principal)
                rateLimiter.tryAuthorizedAccess(principal.toString(), isAdmin)
            }
        } catch (e: PrincipalNotFoundException) {
            response.sendError(403, e.message)
            return false
        } catch (e: ResourceNotFoundException) {
            response.writeResponseError(e)
            return false
        }
        if (probe.isConsumed) {
            response.addHeader(
                "X-Rate-Limit-Remaining", probe.remainingTokens.toString()
            )
            return true
        }
        response.status = HttpStatus.TOO_MANY_REQUESTS.value()
        response.addHeader(
            "X-Rate-Limit-Retry-After-Milliseconds",
            TimeUnit.NANOSECONDS.toMillis(probe.nanosToWaitForRefill).toString()
        )
        val minutesToTryAgain =
            TimeUnit.NANOSECONDS.toMinutes(probe.nanosToWaitForRefill)
        response.sendError(
            429,
            "Too Many Requests: Try Again in $minutesToTryAgain ${if (minutesToTryAgain > 1) "minutes" else "minute"}"
        )
        return false
    }
}
