package ru.mephi.aeschools.interceptor

import org.slf4j.LoggerFactory
import org.springframework.web.filter.AbstractRequestLoggingFilter
import ru.mephi.aeschools.util.containsAnyOf
import javax.servlet.http.HttpServletRequest

class RequestLoggingFilter : AbstractRequestLoggingFilter() {
    private val log = LoggerFactory.getLogger(this::class.java)

    override fun shouldLog(request: HttpServletRequest): Boolean {
        return true
    }

    private fun shouldSkip(request: HttpServletRequest): Boolean {
        return request.requestURI.containsAnyOf("/api/actuator", "/api/v3/api-docs/")
    }

    private fun registrationMask(message: String): String {
        return message.replace(
            "\"password\": \"[^\"]*\"".toRegex(), "\"password\": \"*****\""
        )
    }

    /**
     * Writes a log message before the request is processed.
     */
    override fun beforeRequest(request: HttpServletRequest, message: String) {
        if (shouldSkip(request)) return
        log.info(message)
    }

    /**
     * Writes a log message after the request is processed.
     */
    override fun afterRequest(request: HttpServletRequest, message: String) {
        if (shouldSkip(request)) return

        var logMessage = message

        if (request.requestURI.containsAnyOf("/auth/register", "/auth/login"))
            logMessage = registrationMask(message)

        log.info(logMessage)
    }
}