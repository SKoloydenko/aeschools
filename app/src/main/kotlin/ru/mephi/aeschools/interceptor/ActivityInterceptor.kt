package ru.mephi.aeschools.interceptor

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import ru.mephi.aeschools.service.UserActivityService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class ActivityInterceptor : HandlerInterceptor {
    @Autowired
    private lateinit var service: UserActivityService

    override fun postHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any,
        modelAndView: ModelAndView?,
    ) {
        try {
            service.handle(request)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        super.postHandle(request, response, handler, modelAndView)
    }
}
