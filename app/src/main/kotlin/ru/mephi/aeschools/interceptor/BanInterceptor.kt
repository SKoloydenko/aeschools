package ru.mephi.aeschools.interceptor

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import ru.mephi.aeschools.controller.routers.writeResponseError
import ru.mephi.aeschools.model.exceptions.AppException
import ru.mephi.aeschools.model.exceptions.auth.Unauthorized
import ru.mephi.aeschools.model.exceptions.user.BannedException
import ru.mephi.aeschools.service.UserService
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class BanInterceptor(
    private val userService: UserService,
) : HandlerInterceptor {

    override fun preHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any,
    ): Boolean {
        try {
            val principal = SecurityContextHolder.getContext().authentication.principal
                ?: throw Unauthorized()
            if (principal is UUID && userService.isBanned(principal)) {
                response.writeResponseError(BannedException(principal))
                return false
            }
            return true
        } catch (e: AppException) {
            response.writeResponseError(e)
            return false
        }
    }
}
