package ru.mephi.aeschools.initializers

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import ru.mephi.aeschools.database.entity.user.User
import ru.mephi.aeschools.service.AuthService
import javax.annotation.PostConstruct
import javax.transaction.Transactional

@Component
@Transactional
class AdminInitializer(
    @Value("\${spring.admin.email}")
    private val email: String,

    @Value("\${spring.admin.password}")
    private val password: String,

    private val authService: AuthService,
) {
    @PostConstruct
    fun init() {
        val admin = User(
            admin = true,
            email = email,
            name = "Admin",
            surname = "Admin",
            phone = "+79999999999",
            student = false,
            organization = "Organization",
            jobRole = "Job Role"
        )
        authService.register(admin, password)
    }
}
