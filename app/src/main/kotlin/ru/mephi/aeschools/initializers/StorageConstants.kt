package ru.mephi.aeschools.initializers

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.util.unit.DataSize

@Component
@ConfigurationProperties(prefix = "storage.image")
class StorageConstants {
    lateinit var criticalSize: DataSize
    lateinit var maxSizeUser: DataSize
    lateinit var maxSizeAdmin: DataSize
    lateinit var maxStoredSize: DataSize
    var minWidth: Long = 500
    var minHeight: Long = 500
}
