package ru.mephi.aeschools.initializers

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "feature-toggle")
class FeatureToggleInitializer {
    var featureState: Map<String, Boolean> = mapOf()
    var featureDescription: Map<String, String> = mapOf()
}